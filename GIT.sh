#!/bin/bash

# Define batch size
BATCH_SIZE=5

# Change directory to the specific directory
cd /home/cavisson/Controller_NV/workspace/admin/system/cavisson/ || { echo "Directory not found"; exit 1; }

# Get list of modified files
MODIFIED_FILES=$(git status --porcelain | grep "^ M" | cut -c 4-)

# Check if there are modified files
if [ -z "$MODIFIED_FILES" ]; then
    echo "No modified files found."
    exit 0
fi

# Split modified files into batches
IFS=$'\n' read -d '' -r -a FILES_ARRAY <<<"$MODIFIED_FILES"

# Loop through batches and perform add, commit, push
for ((i=0; i<${#FILES_ARRAY[@]}; i+=BATCH_SIZE)); do
    batch=("${FILES_ARRAY[@]:i:BATCH_SIZE}")
    git add "${batch[@]}"
    git commit -m "Batch commit: adding ${#batch[@]} files"
    git push
done

echo "All batches pushed successfully."

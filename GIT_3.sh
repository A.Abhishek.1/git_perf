#!/bin/bash

# Define batch size
BATCH_SIZE=5

# Get list of modified and untracked files
MODIFIED_FILES=$(git status --porcelain | grep "^ M" | cut -c 4-)
UNTRACKED_FILES=$(git status --porcelain | grep "^??" | cut -c 4-)

# Combine modified and untracked files
ALL_FILES="$MODIFIED_FILES"$'\n'"$UNTRACKED_FILES"

# Check if there are any files
if [ -z "$ALL_FILES" ]; then
    echo "No modified or untracked files found."
    exit 0
fi

# Split files into batches
IFS=$'\n' read -d '' -r -a FILES_ARRAY <<<"$ALL_FILES"

# Loop through batches and perform add, commit, push
for ((i=0; i<${#FILES_ARRAY[@]}; i+=BATCH_SIZE)); do
    batch=("${FILES_ARRAY[@]:i:BATCH_SIZE}")
    git add "${batch[@]}"
    git commit -m "Batch commit: adding ${#batch[@]} files"
    git push
done

echo "All batches pushed successfully."


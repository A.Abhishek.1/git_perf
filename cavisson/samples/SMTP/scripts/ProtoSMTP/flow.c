/*-----------------------------------------------------------------------------
    Name: flow
    Generated By: netstorm
    Date of generation: 05/11/2023 06:25:27
    Flow details:
    Build details: 4.11.0 (build# 47)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"


void flow() 
{
ns_start_transaction("SMTP_Session");
    ns_smtp_send("smtp_session",
      "SMTP_SERVER=127.0.0.1",
      "USER_ID=sender@cavisson.com",
      "PASSWORD=password@123",
      "FROM_EMAIL=sender@cavisson.com",
      "TO_EMAILS=receiver@cavisson.com",
      "CC_EMAILS=ccreceiver@cavisson.com",
      "BCC_EMAILS=bccreceiver@cavisson.com",
      "SUBJECT=SMTP test Mail",
      "BODY=Hello you are invited to the annual event",
      "HEADER=X-Priority:3",
      "MESSAGE_COUNT=1");
ns_end_transaction("SMTP_Session",NS_AUTO_STATUS);

}

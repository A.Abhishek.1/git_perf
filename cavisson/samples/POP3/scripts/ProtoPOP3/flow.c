/*-----------------------------------------------------------------------------
    Name: flow
    Generated By: netstorm
    Date of generation: 05/11/2023 06:31:24
    Flow details:
    Build details: 4.11.0 (build# 47)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"


void flow() 
{
    ns_pop_stat("stat_email",
      "POP_SERVER=127.0.0.1",
      "USER_ID=user1@cavisson.com",
      "PASSWORD=pass@123"
    );
    ns_pop_list("list_email",
      "POP_SERVER=127.0.0.1",
      "USER_ID=user1@cavisson.com",
      "PASSWORD=pass@123"
    );
    ns_pop_get("get_email",
      "POP_SERVER=127.0.0.1",
      "USER_ID=user1@cavisson.com",
      "PASSWORD=pass@123"
    );
 }

/*-----------------------------------------------------------------------------
    Name: flow
    Created By: cavisson
    Date of creation: 07/15/2024 01:30:58
    Flow details:
    Build details: 4.13.0 (build# 141)
    Modification History:
-----------------------------------------------------------------------------*/
/* Notes :
    There are few additional arguments in all click and script APIs which can be used based on the need:
    
    #NetworkIdleTimeout=<network_idle_timeout_ms>
    The network idle timeout, measured in milliseconds (ms), has a default value of 1500 ms. 
    If the onLoad event is triggered, and there is no network activity for this duration, the page will be considered as fully loaded. 
    It may be necessary to adjust this setting when the expected time gap between two calls exceeds the default value of 1500 ms.

    #PageLoadTimeout=<page_load_timeout_sec>
    This defines the maximum waiting time in seconds for a VUser (Virtual User) to wait for a page to load.
    If the page doesn't load within this duration, the page loading will be aborted, and any captured requests up to that point will be saved. 
    This feature is particularly useful when a specific page's load time exceeds the configured limit in the scenario. 
    The default value in the scenario is 60 seconds, which can be adjusted, but it will apply to all steps and pages.

    #VisualIdleTimeout=<visual_idle_timeout_ms>
    This parameter sets the visual idle timeout in milliseconds, with a default value of 1500 ms. After the network becomes idle, the VUser will wait for visual changes to stabilize.
    If there are no visual changes within this window of time after the network becomes idle, the page will be marked as visually complete. 
    This functionality proves valuable when a specific page's load time exceeds the configured limit in the scenario. 
    The default value in the scenario is 60 seconds, which can be adjusted, though it will apply to all steps and pages.

    #AuthCredential=<username>:<password>
    Provide authentication credentials in the format <username>:<password>. AuthCredential is used to supply authentication credentials for websites that require login. 
    The credentials (username and password) are passed in the first API request, either in ns_browser or ns_web_url.

    #Optional=<0 or 1>
    Optional is a flag used to prevent a specific transaction from affecting the rest of the transactions. 
    For instance, in cases involving popups, if the popup doesn't appear, the transaction could fail, disrupting the entire test. By setting Optional to 1, the test will continue even if the transaction fails.

    #heroElementID=<element id selector>
    #heroElementXPATH=<element xpath selector>	
    #heroElementXPATH1=<alternative element xpath selector>
    #heroElementCSSPATH=<csspath selector>	
    #heroElementDOMSTRING=<domstring>
    #heroElementMark=<value>
    #heroElementMeasure=<value>
    Visual progress is considered complete when the HeroElement becomes visible on the screen. Users can specify any element as the hero element using the above arguments.
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include "ns_string.h"

void flow()
{

    ns_start_transaction("index");
    ns_browser("index",
        "url=https://netcloud2.cav-test.com/UnifiedDashboard/#/loading",
        "Navigation=1",
        "browserurl=https://netcloud2.cav-test.com/",
        "action=Home",
        "title=Cavisson",
        "Snapshot=webpage_1721024973209.png");
    ns_end_transaction("index", NS_AUTO_STATUS);

    ns_page_think_time(24.648);



    ns_start_transaction("enterUsername");
    ns_edit_field("enterUsername",
        "url=https://netcloud2.cav-test.com/UnifiedDashboard/#/login",
        "Navigation=1",
        "action=change",
        attributes=["autocomplete=new-text","formcontrolname=username","placeholder=Username","type=text","class=w-100-p input-lg ui-inputtext ui-corner-all ui-state-default ui-widget ng-untouched ui-state-filled ng-dirty ng-valid"],
        "class=w-100-p input-lg ui-inputtext ui-corner-all ui-state-default ui-widget ng-untouched ui-state-filled ng-dirty ng-valid",
        "csspath=[type=\"text\"]",
        "xpath=//INPUT[@placeholder=\"Username\"]",
        "xpath1=//BODY[normalize-space(text())=\"SIGN IN let themeName = sessionStorage.getItem('theme'); if (themeName) changeTheme1(themeName, 'Light'); function changeTheme1(themeName, oldTheme) { let body = document.getElementsByTagName('body')[0]; body.classList.remove(oldTheme); //remove the class body.classList.add(themeName); }\"]/APP-ROOT[1]/APP-LOGIN[1]/DIV[1]/DIV[1]/DIV[1]/DIV[2]/FORM[1]/DIV[1]/DIV[1]/INPUT[1]",
        "xpath2=//*[@id=\"maiBody\"]/APP-ROOT[1]/APP-LOGIN[1]/DIV[1]/DIV[1]/DIV[1]/DIV[2]/FORM[1]/DIV[1]/DIV[1]/INPUT[1]",
        "xpath3=HTML/BODY[1]/APP-ROOT[1]/APP-LOGIN[1]/DIV[1]/DIV[1]/DIV[1]/DIV[2]/FORM[1]/DIV[1]/DIV[1]/INPUT[1]",
        "tagName=INPUT",
        "value=guest",
        "Snapshot=webpage_1721024997405.png");
    ns_end_transaction("enterUsername", NS_AUTO_STATUS);

    ns_page_think_time(3.381);



    ns_start_transaction("enterPassword");
    ns_edit_field("enterPassword",
        "url=https://netcloud2.cav-test.com/UnifiedDashboard/#/login",
        "Navigation=0",
        "action=change",
        attributes=["formcontrolname=password","autocomplete=new-password","placeholder=Password","type=password","class=w-100-p pl-6 pr-25 input-lg ui-inputtext ui-corner-all ui-state-default ui-widget ng-untouched ui-state-filled ng-dirty ng-valid"],
        "class=w-100-p pl-6 pr-25 input-lg ui-inputtext ui-corner-all ui-state-default ui-widget ng-untouched ui-state-filled ng-dirty ng-valid",
        "csspath=[autocomplete=\"new-password\"]",
        "xpath=//INPUT[@placeholder=\"Password\"]",
        "xpath1=//SPAN[normalize-space(text())=\"SIGN IN\"]/../../../DIV[2]/DIV[1]/DIV[1]/INPUT[1]",
        "xpath2=//*[@id=\"maiBody\"]/APP-ROOT[1]/APP-LOGIN[1]/DIV[1]/DIV[1]/DIV[1]/DIV[2]/FORM[1]/DIV[2]/DIV[1]/DIV[1]/INPUT[1]",
        "xpath3=HTML/BODY[1]/APP-ROOT[1]/APP-LOGIN[1]/DIV[1]/DIV[1]/DIV[1]/DIV[2]/FORM[1]/DIV[2]/DIV[1]/DIV[1]/INPUT[1]",
        "tagName=INPUT",
        "value=ns_decrypt(\"IUUkRDE=\")",
        "Snapshot=webpage_1721025000764.png");
    ns_end_transaction("enterPassword", NS_AUTO_STATUS);

    ns_page_think_time(0.076);



    ns_start_transaction("clickSIGNIN_2");
    ns_span("clickSIGNIN_2",
        "url=https://netcloud2.cav-test.com/UnifiedDashboard/#/login",
        "Navigation=0",
        "type=SPAN",
        "action=click",
        attributes=["class=ui-button-text ui-clickable"],
        "class=ui-button-text ui-clickable",
        "csspath=span",
        "xpath=//SPAN[normalize-space(text())=\"SIGN IN\"]",
        "xpath1=//INPUT[@placeholder=\"Password\"]/../../../../DIV[3]/BUTTON[1]/SPAN[1]",
        "xpath2=//*[@id=\"maiBody\"]/APP-ROOT[1]/APP-LOGIN[1]/DIV[1]/DIV[1]/DIV[1]/DIV[2]/FORM[1]/DIV[3]/BUTTON[1]/SPAN[1]",
        "xpath3=HTML/BODY[1]/APP-ROOT[1]/APP-LOGIN[1]/DIV[1]/DIV[1]/DIV[1]/DIV[2]/FORM[1]/DIV[3]/BUTTON[1]/SPAN[1]",
        "tagName=SPAN",
        "content=SIGN IN",
        "Snapshot=webpage_1721025000840.png");
    ns_end_transaction("clickSIGNIN_2", NS_AUTO_STATUS);

    ns_page_think_time(16.293);



    ns_start_transaction("UnifiedDashboard_2");
    ns_link("UnifiedDashboard_2",
        "url=https://netcloud2.cav-test.com/UnifiedDashboard/#/home/dashboard",
        "Navigation=1",
        "type=ELEMENT",
        "action=click",
        attributes=["class=cav4 time-filter-watch font-14"],
        "class=cav4 time-filter-watch font-14",
        "tagName=I",
        "csspath=[ptooltip=\"Time\\ Filter\"] i",
        "coordinates=[[870, 18]]",
        "xpath=//BUTTON[@title=\"Next Segment\"]/../BUTTON[4]/I[1]",
        "xpath1=//BUTTON[@title=\"Pause Auto Refresh\"]/../BUTTON[4]/I[1]",
        "xpath2=//*[@id=\"maiBody\"]/APP-ROOT[1]/APP-HOME[1]/DIV[1]/DIV[2]/APP-TIME-BAR[1]/DIV[1]/DIV[1]/UL[1]/LI[4]/BUTTON[4]/I[1]",
        "xpath3=HTML/BODY[1]/APP-ROOT[1]/APP-HOME[1]/DIV[1]/DIV[2]/APP-TIME-BAR[1]/DIV[1]/DIV[1]/UL[1]/LI[4]/BUTTON[4]/I[1]",
        "Snapshot=webpage_1721025017133.png");
    ns_end_transaction("UnifiedDashboard_2", NS_AUTO_STATUS);

    ns_page_think_time(4.271);



    ns_start_transaction("UnifiedDashboard_4");
    ns_link("UnifiedDashboard_4",
        "url=https://netcloud2.cav-test.com/UnifiedDashboard/#/home/dashboard",
        "Navigation=0",
        "type=ELEMENT",
        "action=click",
        attributes=["class=icons8 icons8-expand-arrow ph-5"],
        "class=icons8 icons8-expand-arrow ph-5",
        "tagName=I",
        "csspath=[role]:nth-child(45) div[style] > :nth-child(2) i",
        "coordinates=[[326, 130]]",
        "xpath=(//SPAN[normalize-space(text())=\"Last 4 Hours\"])[3]/../I[1]",
        "xpath1=//*[@id=\"maiBody\"]/DIV[30]/DIV[2]/DIV[1]/DIV[2]/BUTTON[1]/I[1]",
        "xpath2=HTML/BODY[1]/DIV[30]/DIV[2]/DIV[1]/DIV[2]/BUTTON[1]/I[1]",
        "Snapshot=webpage_1721025021404.png");
    ns_end_transaction("UnifiedDashboard_4", NS_AUTO_STATUS);

    ns_page_think_time(2.561);



    ns_start_transaction("clickLive_2");
    ns_link("clickLive_2",
        "url=https://netcloud2.cav-test.com/UnifiedDashboard/#/home/dashboard",
        "Navigation=0",
        "content=Live",
        "type=LINK",
        "class=ui-menuitem-link ui-corner-all ng-star-inserted",
        "csspath=p-slidemenusub[root] li:nth-of-type(1)[style] > a",
        "xpath=//DIV[normalize-space(text())=\"07/14/2024 21:30:07 (Sunday)\"]/../../../DIV[2]/P-SLIDEMENU[1]/DIV[1]/DIV[1]/DIV[1]/P-SLIDEMENUSUB[1]/UL[1]/LI[1]/A[1]",
        "xpath1=//SPAN[normalize-space(text())=\"Whole Scenario\"]/../../../LI[1]/A[1]",
        "xpath2=//*[@id=\"time-period\"]/DIV[1]/DIV[1]/DIV[1]/P-SLIDEMENUSUB[1]/UL[1]/LI[1]/A[1]",
        "xpath3=HTML/BODY[1]/DIV[30]/DIV[2]/DIV[1]/DIV[2]/P-SLIDEMENU[1]/DIV[1]/DIV[1]/DIV[1]/P-SLIDEMENUSUB[1]/UL[1]/LI[1]/A[1]",
        "action=click",
        attributes=["tabindex=0","class=ui-menuitem-link ui-corner-all ng-star-inserted"],
        "tagName=A",
        "Snapshot=webpage_1721025023964.png");
    ns_end_transaction("clickLive_2", NS_AUTO_STATUS);

    ns_page_think_time(7.845);



    ns_start_transaction("clickLast7Days_2");
    ns_link("clickLast7Days_2",
        "url=https://netcloud2.cav-test.com/UnifiedDashboard/#/home/dashboard",
        "Navigation=0",
        "content=Last 7 Days",
        "type=LINK",
        "ID=LIVE11",
        "class=ui-menuitem-link ui-corner-all ng-star-inserted",
        "csspath=#LIVE11",
        "xpath=//SPAN[normalize-space(text())=\"Last 7 Days\"]/..",
        "xpath1=//SPAN[normalize-space(text())=\"Last 30 Days\"]/../../../LI[12]/A[1]",
        "xpath2=//*[@id=\"time-period\"]/DIV[1]/DIV[1]/DIV[1]/P-SLIDEMENUSUB[1]/UL[1]/LI[1]/P-SLIDEMENUSUB[1]/UL[1]/LI[12]/A[1]",
        "xpath3=HTML/BODY[1]/DIV[30]/DIV[2]/DIV[1]/DIV[2]/P-SLIDEMENU[1]/DIV[1]/DIV[1]/DIV[1]/P-SLIDEMENUSUB[1]/UL[1]/LI[1]/P-SLIDEMENUSUB[1]/UL[1]/LI[12]/A[1]",
        "action=click",
        attributes=["id=LIVE11","class=ui-menuitem-link ui-corner-all ng-star-inserted","tabindex=0","href="],
        "tagName=A",
        "Snapshot=webpage_1721025031809.png");
    ns_end_transaction("clickLast7Days_2", NS_AUTO_STATUS);

    ns_page_think_time(2.033);



    ns_start_transaction("clickAPPLY_2");
    ns_button("clickAPPLY_2",
        "url=https://netcloud2.cav-test.com/UnifiedDashboard/#/home/dashboard",
        "Navigation=0",
        "content=APPLY",        
        "action=click",
        attributes=["label=APPLY","type=submit","pbutton=","class=w-100-p ui-button-rounded ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"],
        "class=w-100-p ui-button-rounded ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only",
        "csspath=[role]:nth-child(45) [type=\"submit\"]",
        "xpath=(//SPAN[normalize-space(text())=\"APPLY\"])[2]/..",
        "xpath1=(//SPAN[normalize-space(text())=\"Auto\"])[2]/../../../../../DIV[3]/BUTTON[1]",
        "xpath2=//*[@id=\"maiBody\"]/DIV[30]/DIV[3]/BUTTON[1]",
        "xpath3=HTML/BODY[1]/DIV[30]/DIV[3]/BUTTON[1]",
        "tagName=BUTTON",
        "Snapshot=webpage_1721025033843.png");
    ns_end_transaction("clickAPPLY_2", NS_AUTO_STATUS);

    ns_page_think_time(7.541);



    ns_start_transaction("UnifiedDashboard_6");
    ns_link("UnifiedDashboard_6",
        "url=https://netcloud2.cav-test.com/UnifiedDashboard/#/home/dashboard",
        "Navigation=0",
        "type=ELEMENT",
        "action=click",
        attributes=["class=cav4 time-filter-watch font-14"],
        "class=cav4 time-filter-watch font-14",
        "tagName=I",
        "csspath=[ptooltip=\"Time\\ Filter\"] i",
        "coordinates=[[867, 15]]",
        "xpath=//BUTTON[@title=\"Next Segment\"]/../BUTTON[4]/I[1]",
        "xpath1=//BUTTON[@title=\"Pause Auto Refresh\"]/../BUTTON[4]/I[1]",
        "xpath2=//*[@id=\"maiBody\"]/APP-ROOT[1]/APP-HOME[1]/DIV[1]/DIV[2]/APP-TIME-BAR[1]/DIV[1]/DIV[1]/UL[1]/LI[4]/BUTTON[4]/I[1]",
        "xpath3=HTML/BODY[1]/APP-ROOT[1]/APP-HOME[1]/DIV[1]/DIV[2]/APP-TIME-BAR[1]/DIV[1]/DIV[1]/UL[1]/LI[4]/BUTTON[4]/I[1]",
        "Snapshot=webpage_1721025041384.png");
    ns_end_transaction("UnifiedDashboard_6", NS_AUTO_STATUS);

    ns_page_think_time(3.549);



    ns_start_transaction("UnifiedDashboard_8");
    ns_span("UnifiedDashboard_8",
        "url=https://netcloud2.cav-test.com/UnifiedDashboard/#/home/dashboard",
        "Navigation=0",
        "type=SPAN",
        "action=click",
        attributes=["class=ui-radiobutton-icon ui-clickable"],
        "class=ui-radiobutton-icon ui-clickable",
        "csspath=[label=\"Custom\"] span",
        "xpath=//LABEL[normalize-space(text())=\"Custom\"]/../DIV[1]/DIV[@role=\"radio\"][1]/SPAN[1]",
        "xpath1=//LABEL[normalize-space(text())=\"Preset\"]/../../P-RADIOBUTTON[2]/DIV[1]/DIV[@role=\"radio\"][1]/SPAN[1]",
        "xpath2=//*[@id=\"maiBody\"]/DIV[30]/DIV[2]/DIV[1]/DIV[1]/P-RADIOBUTTON[2]/DIV[1]/DIV[2]/SPAN[1]",
        "xpath3=HTML/BODY[1]/DIV[30]/DIV[2]/DIV[1]/DIV[1]/P-RADIOBUTTON[2]/DIV[1]/DIV[2]/SPAN[1]",
        "tagName=SPAN",
        "content=",
        "Snapshot=webpage_1721025044932.png");
    ns_end_transaction("UnifiedDashboard_8", NS_AUTO_STATUS);

    ns_page_think_time(3.481);



    ns_start_transaction("clickAPPLY_4");
    ns_span("clickAPPLY_4",
        "url=https://netcloud2.cav-test.com/UnifiedDashboard/#/home/dashboard",
        "Navigation=0",
        "type=SPAN",
        "action=click",
        attributes=["class=ui-button-text ui-clickable"],
        "class=ui-button-text ui-clickable",
        "csspath=[role]:nth-child(45) [type=\"submit\"] span",
        "xpath=(//SPAN[normalize-space(text())=\"APPLY\"])[2]",
        "xpath1=//INPUT[@placeholder=\"Custom Time Name\"]/../../../../../DIV[3]/BUTTON[1]/SPAN[1]",
        "xpath2=//*[@id=\"maiBody\"]/DIV[30]/DIV[3]/BUTTON[1]/SPAN[1]",
        "xpath3=HTML/BODY[1]/DIV[30]/DIV[3]/BUTTON[1]/SPAN[1]",
        "tagName=SPAN",
        "content=APPLY",
        "Snapshot=webpage_1721025048413.png");
    ns_end_transaction("clickAPPLY_4", NS_AUTO_STATUS);

    ns_page_think_time(9.673);


}

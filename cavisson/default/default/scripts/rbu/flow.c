/*-----------------------------------------------------------------------------
    Name: flow
    Created By: Abhishek
    Date of creation: 06/13/2024 02:27:02
    Flow details:
    Build details: 4.13.0 (build# 125)
    Modification History:
-----------------------------------------------------------------------------*/
/* Notes :
    There are few additional arguments in all click and script APIs which can be used based on the need:
    
    #NetworkIdleTimeout=<network_idle_timeout_ms>
    The network idle timeout, measured in milliseconds (ms), has a default value of 1500 ms. 
    If the onLoad event is triggered, and there is no network activity for this duration, the page will be considered as fully loaded. 
    It may be necessary to adjust this setting when the expected time gap between two calls exceeds the default value of 1500 ms.

    #PageLoadTimeout=<page_load_timeout_sec>
    This defines the maximum waiting time in seconds for a VUser (Virtual User) to wait for a page to load.
    If the page doesn't load within this duration, the page loading will be aborted, and any captured requests up to that point will be saved. 
    This feature is particularly useful when a specific page's load time exceeds the configured limit in the scenario. 
    The default value in the scenario is 60 seconds, which can be adjusted, but it will apply to all steps and pages.

    #VisualIdleTimeout=<visual_idle_timeout_ms>
    This parameter sets the visual idle timeout in milliseconds, with a default value of 1500 ms. After the network becomes idle, the VUser will wait for visual changes to stabilize.
    If there are no visual changes within this window of time after the network becomes idle, the page will be marked as visually complete. 
    This functionality proves valuable when a specific page's load time exceeds the configured limit in the scenario. 
    The default value in the scenario is 60 seconds, which can be adjusted, though it will apply to all steps and pages.

    #AuthCredential=<username>:<password>
    Provide authentication credentials in the format <username>:<password>. AuthCredential is used to supply authentication credentials for websites that require login. 
    The credentials (username and password) are passed in the first API request, either in ns_browser or ns_web_url.

    #Optional=<0 or 1>
    Optional is a flag used to prevent a specific transaction from affecting the rest of the transactions. 
    For instance, in cases involving popups, if the popup doesn't appear, the transaction could fail, disrupting the entire test. By setting Optional to 1, the test will continue even if the transaction fails.

    #heroElementID=<element id selector>
    #heroElementXPATH=<element xpath selector>	
    #heroElementXPATH1=<alternative element xpath selector>
    #heroElementCSSPATH=<csspath selector>	
    #heroElementDOMSTRING=<domstring>
    #heroElementMark=<value>
    #heroElementMeasure=<value>
    Visual progress is considered complete when the HeroElement becomes visible on the screen. Users can specify any element as the hero element using the above arguments.
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include "ns_string.h"

void flow()
{

    ns_start_transaction("index_html");
    ns_browser("index_html",
        "url=http://10.10.30.73:9002/tours/index.html",
        "Navigation=1",
        "browserurl=http://10.10.30.73:9002/tours/index.html",
        "action=Home",
        "title=Mercury Tours",
        "Snapshot=webpage_1718263533256.png");
    ns_end_transaction("index_html", NS_AUTO_STATUS);

    ns_page_think_time(58.032);



    ns_start_transaction("enterUsername");
    ns_edit_field("enterUsername",
        "url=http://10.10.30.73:9002/tours/index.html",
        "Navigation=0",
        "action=change",
        attributes=["name=username","type=text"],
        "csspath=[type=\"text\"]",
        "xpath=(//TD[normalize-space(text())=\"\"])[6]/INPUT[1]",
        "xpath1=(//TR[normalize-space(text())=\"\"])[6]/TD[1]/INPUT[1]",
        "xpath2=HTML/BODY[1]/TABLE[1]/TBODY[1]/TR[2]/TD/TABLE[1]/TBODY[1]/TR[1]/TD[1]/FORM[1]/CENTER[1]/TABLE[1]/TBODY[1]/TR[2]/TD[1]/INPUT[1]",
        "tagName=INPUT",
        "value=guest",
        "Snapshot=webpage_1718263591180.png");
    ns_end_transaction("enterUsername", NS_AUTO_STATUS);

    ns_page_think_time(4.1);



    ns_start_transaction("enterPassword");
    ns_edit_field("enterPassword",
        "url=http://10.10.30.73:9002/tours/index.html",
        "Navigation=0",
        "action=change",
        attributes=["name=password","type=password"],
        "csspath=[type=\"password\"]",
        "xpath=(//TD[normalize-space(text())=\"\"])[8]/INPUT[1]",
        "xpath1=(//TR[normalize-space(text())=\"\"])[8]/TD[1]/INPUT[1]",
        "xpath2=HTML/BODY[1]/TABLE[1]/TBODY[1]/TR[2]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/FORM[1]/CENTER[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/INPUT[1]",
        "tagName=INPUT",
        "value=ns_decrypt(\"IUUkRDE=\")",
        "Snapshot=webpage_1718263595278.png");
    ns_end_transaction("enterPassword", NS_AUTO_STATUS);

    ns_page_think_time(0.009);



    ns_start_transaction("clickLogin_2");
    ns_link("clickLogin_2",
        "url=http://10.10.30.73:9002/tours/index.html",
        "Navigation=0",
        "action=click",
        attributes=["alt=Login","src=Merc10-dev/images/login.gif","name=login","height=25","type=image","width=95","value=Login","border=0"],
        //"csspath=[type=\"image\"]",
        //"xpath=//INPUT[@alt=\"Login\"]",
      //  "xpath1=(//TD[normalize-space(text())=\"\"])[9]/INPUT[1]",
        //"xpath2=HTML/BODY[1]/TABLE[1]/TBODY[1]/TR[2]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/FORM[1]/CENTER[1]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/INPUT[1]",
        "tagName=INPUT",
        "Snapshot=webpage_1718263595300.png");
    ns_end_transaction("clickLogin_2", NS_AUTO_STATUS);

    ns_page_think_time(2.58);



    ns_start_transaction("clickSearchFlightsButton_2");
    ns_link("clickSearchFlightsButton_2",
        "url=http://10.10.30.73:9002/cgi-bin/login?userSession=75893.0884568651DQADHfApHDHfcDtccpfAttcf&username=guest&password=guest&login.x=57&login.y=16&JSFormSubmit=off",
        "Navigation=1",
        "type=IMAGE_LINK",
        "action=click",
        attributes=["alt=Search Flights Button","width=95","border=0","src=/tours/Merc10-dev/images/flights.gif","height=25"],
        "csspath=[alt=\"Search\\ Flights\\ Button\"]",
        "xpath=//IMG[@alt=\"Search Flights Button\"]",
        "xpath1=(//TD[normalize-space(text())=\"\"])[5]/A[1]/IMG[1]",
        "xpath2=HTML/BODY[1]/TABLE[1]/TBODY[1]/TR[2]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/CENTER[1]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/A[1]/IMG[1]",
        "tagName=IMG",
        "Snapshot=webpage_1718263598012.png");
    ns_end_transaction("clickSearchFlightsButton_2", NS_AUTO_STATUS);

    ns_page_think_time(2.754);



    ns_start_transaction("clickFindFlights_2");
    ns_link("clickFindFlights_2",
        "url=http://10.10.30.73:9002/cgi-bin/reservation",
        "Navigation=1",
        "action=click",
        attributes=["value=Submit","name=findFlights","border=0","src=/tours/images/continue.gif","type=IMAGE"],
        "csspath=[type=\"IMAGE\"]",
        "xpath=(//TD[normalize-space(text())=\"\"])[17]/INPUT[1]",
        "xpath1=//TR[normalize-space(text())=\"Aisle Window None First Business Coach\"]/../TR[7]/TD[1]/INPUT[1]",
        "xpath2=HTML/BODY[1]/TABLE[1]/TBODY[1]/TR[2]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[7]/TD[1]/INPUT[1]",
        "tagName=INPUT",
        "Snapshot=webpage_1718263601044.png");
    ns_end_transaction("clickFindFlights_2", NS_AUTO_STATUS);

    ns_page_think_time(2.038);



    ns_start_transaction("clickOutboundFlight");
    ns_radio_group("clickOutboundFlight",
        "url=http://10.10.30.73:9002/cgi-bin/findflight?depart=Acapulco&departDate=06-14-2024&arrive=Acapulco&returnDate=06-15-2024&numPassengers=1&seatPref=None&seatType=Coach&findFlights.x=42&findFlights.y=6",
        "Navigation=1",
        "action=click",
        attributes=["type=radio","name=outboundFlight","value=button3"],
        "csspath=tr:nth-of-type(5) [type=\"radio\"]",
        "xpath=//TD[normalize-space(text())=\"Blue Sky Air 003\"]/INPUT[1]",
        "xpath1=//TD[normalize-space(text())=\"Blue Sky Air 002\"]/../../TR[5]/TD[1]/INPUT[1]",
        "xpath2=HTML/BODY[1]/TABLE[1]/TBODY[1]/TR[2]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/FORM[1]/CENTER[1]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/INPUT[1]",
        "tagName=INPUT",
        "value=button3",
        "Snapshot=webpage_1718263603282.png");
    ns_end_transaction("clickOutboundFlight", NS_AUTO_STATUS);

    ns_page_think_time(2.208);



    ns_start_transaction("clickReserveFlights_2");
    ns_link("clickReserveFlights_2",
        "url=http://10.10.30.73:7777/cgi-bin/findflight?depart=Acapulco&departDate=06-14-2024&arrive=Acapulco&returnDate=06-15-2024&numPassengers=1&seatPref=None&seatType=Coach&findFlights.x=42&findFlights.y=6",
        "Navigation=0",
        "action=click",
        attributes=["type=image","src=/tours/images/continue.gif","name=reserveFlights","border=0"],
        "csspath=[name=\"reserveFlights\"]",
        "xpath=(//TD[normalize-space(text())=\"\"])[11]/INPUT[1]",
        "xpath1=//CENTER[normalize-space(text())=\"Flight departing from Acapulco to Acapulco on 06-14-2024 FlightDeparture timeCost Blue Sky Air 000 8am $ 0 Blue Sky Air 001 1pm $ 0 Blue Sky Air 002 5pm $ 0 Blue Sky Air 003 11pm $ 0\"]/CENTER[1]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/INPUT[1]",
        "xpath2=HTML/BODY[1]/TABLE[1]/TBODY[1]/TR[2]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/FORM[1]/CENTER[1]/CENTER[1]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/INPUT[1]",
        "tagName=INPUT",
        "Snapshot=webpage_1718263605490.png");
    ns_end_transaction("clickReserveFlights_2", NS_AUTO_STATUS);

    ns_page_think_time(4.159);



    ns_start_transaction("clickBuyFlights_2");
    ns_link("clickBuyFlights_2",
        "url=http://10.10.30.73:9002/cgi-bin/findflight?hidden_outboundFlight_button0=000%7C0%7C06-14-2024&hidden_outboundFlight_button1=001%7C0%7C06-14-2024&hidden_outboundFlight_button2=002%7C0%7C06-14-2024&outboundFlight=button3&hidden_outboundFlight_button3=003%7C0%7C06-14-2024&numPassengers=1&advanceDiscount=&seatType=Coach&seatPref=None&reserveFlights.x=75&reserveFlights.y=11",
        "Navigation=1",
        "action=click",
        attributes=["border=0","name=buyFlights","src=/tours/images/purchaseflight.gif","type=image"],
        "csspath=[name=\"buyFlights\"]",
        "xpath=HTML/BODY[1]/TABLE[1]/TBODY[1]/TR[2]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/BLOCKQUOTE[1]/FORM[1]/INPUT[6]",
        "tagName=INPUT",
        "Snapshot=webpage_1718263609786.png");
    ns_end_transaction("clickBuyFlights_2", NS_AUTO_STATUS);

    ns_page_think_time(5.608);



    ns_start_transaction("clickHomeButton_2");
    ns_link("clickHomeButton_2",
        "url=http://10.10.30.73:9002/cgi-bin/findflight?firstName=Tiger&lastName=Scott&address1=4261+Stevenson+Blvd.&address2=Fremont%2C+CA+94538&pass1=Scott+Tiger&creditCard=&expDate=&oldCCOption=&numPassengers=1&seatType=Coach&seatPref=None&outboundFlight=003%7C0%7C06-14-2024&advanceDiscount=&buyFlights.x=80&buyFlights.y=13&.cgifields=saveCC",
        "Navigation=1",
        "type=IMAGE_LINK",
        "action=click",
        attributes=["height=25","width=95","border=0","alt=Home Button","src=/tours/Merc10-dev/images/home.gif"],
        "csspath=[alt=\"Home\\ Button\"]",
        "xpath=//IMG[@alt=\"Home Button\"]",
        "xpath1=(//TD[normalize-space(text())=\"\"])[6]/A[1]/IMG[1]",
        "xpath2=HTML/BODY[1]/TABLE[1]/TBODY[1]/TR[2]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/CENTER[1]/TABLE[1]/TBODY[1]/TR[2]/TD[1]/A[1]/IMG[1]",
        "tagName=IMG",
        "Snapshot=webpage_1718263615473.png");
    ns_end_transaction("clickHomeButton_2", NS_AUTO_STATUS);

    ns_page_think_time(2.904);



    ns_start_transaction("clickSignOffButton_2");
    ns_link("clickSignOffButton_2",
        "url=http://10.10.30.73:9002/cgi-bin/home",
        "Navigation=1",
        "type=IMAGE_LINK",
        "action=click",
        attributes=["src=/tours/Merc10-dev/images/signoff.gif","width=95","alt=SignOff Button","border=0","height=25"],
        "csspath=[alt=\"SignOff\\ Button\"]",
        "xpath=//IMG[@alt=\"SignOff Button\"]",
        "xpath1=(//TD[normalize-space(text())=\"\"])[7]/A[1]/IMG[1]",
        "xpath2=HTML/BODY[1]/TABLE[1]/TBODY[1]/TR[2]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/CENTER[1]/TABLE[1]/TBODY[1]/TR[3]/TD[1]/A[1]/IMG[1]",
        "tagName=IMG",
        "Snapshot=webpage_1718263618446.png");
    ns_end_transaction("clickSignOffButton_2", NS_AUTO_STATUS);

    ns_page_think_time(4.253);


}

/*-----------------------------------------------------------------------------
    Name: flow
    Created By: cavisson
    Date of creation: 05/11/2024 05:38:06
    Flow details:
    Build details: 4.13.0 (build# 111)
    Modification History:
-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include "ns_string.h"

void flow()
{
    ns_start_transaction("index_html");
    ns_web_url("index_html",
        "URL=https://10.10.30.9:8003/tours/index.html",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/banner_animated.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/sun_swede.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/login.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/images/banner_merctur.jpg", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://10.10.30.9:8003/nv/netvision/nv_bootstrap.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://10.10.30.9:8003/nv/netvision/cav_nv.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://10.10.30.9:8003/favicon.ico", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://10.10.30.9:8003/nv/netvision/config.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://10.10.30.9:8003/nv/netvision/jquery.min.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://10.10.30.9:4444/nv/netvision/%3CFEEDBACK-MODULE%3E/feedback.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavSF", END_INLINE,
            "URL=https://10.10.30.9:8003/test_rum?s=000000000000000000000&p=1&op=timing&pi=1&CavStore=-1&pid=-1&d=1|0|-1|1|387|-1|0|383|407|2|-2|162|0|0|585|503|-1|503|0|1||https%3A%2F%2F10.10.30.9%3A8003%2Ftours%2Findex.html|10.10.30.9%3A8003|CavSF%3DcavnvComplete%2C%2C%2C%2C%2C%2C%2C%2C%2C%2C%2C1%2C%2C%2C%2C|22|536875008|-1|24|24|LINUX|LINUX%2F5.15.0%3B%20x86%3B%20%3B%20Chromium%2F114%3B%20114.0.5735.199|en-US|%5Bobject%20PluginArray%5D|Mozilla|0|PC|114|5.15.0|-1|0|0|0|%7B-1%7D|503|-1|0|3374|0|0|0|560|560|0|0|4.11.0_4f066a|4f16d3&lts=-1&d2=-1|-1|-1|1|100|0|0|0|0|0", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavSF;CavNVC", END_INLINE
    );

    ns_end_transaction("index_html", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("test_rum");
    ns_web_url("test_rum",
        "URL=https://10.10.30.9:8003/test_rum?s=001403978702638809130&p=1&m=0&op=el&pi=1&CavStore=-1&pid=-1&d=-1&lts=326889265&nvcounter=1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=CavVI;CavNVC;CavSF",
        BODY_BEGIN,
            "001403978702638809130|-1|1|326889271|Timing|{"tti":503}|
001403978702638809130|-1|1|326889323|Timing|{"fid":1}|",
        BODY_END
    );

    ns_end_transaction("test_rum", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("test_rum_2");
    ns_web_url("test_rum_2",
        "URL=https://10.10.30.9:8003/test_rum?s=001403978702638809130&p=1&m=0&op=useraction&pi=1&CavStore=-1&pid=-1&d=0|0&lts=326889366&nvcounter=2",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=CavVI;CavSF;CavNVC",
        BODY_BEGIN,
            "001403978702638809130|-1|1|326889324333|-1|2|:nth-child(2) > td > input|-3|username|INPUT|text|38|524|121|21|||-1|-1||{"id":":nth-child(2) > td > input"}|/html/body/table/tbody/tr[2]/td/table/tbody/tr/td/form/center/table/tbody/tr[2]/td/input
001403978702638809130|-1|1|326889330816|-1|2|:nth-child(2) > td > input|-3|username|INPUT|text|28|524|121|21|1.Zw==||-1|-1||{"id":":nth-child(2) > td > input"}|/html/body/table/tbody/tr[2]/td/table/tbody/tr/td/form/center/table/tbody/tr[2]/td/input
001403978702638809130|-1|1|326889341342|-1|2|:nth-child(2) > td > input|-3|username|INPUT|text|9|518|121|21|1.Zw==||-1|-1||{"id":":nth-child(2) > td > input"}|/html/body/table/tbody/tr[2]/td/table/tbody/tr/td/form/center/table/tbody/tr[2]/td/input
001403978702638809130|-1|1|326889342516|-1|2|:nth-child(2) > td > input|-3|username|INPUT|text|9|518|121|21|1.Zw==||-1|-1||{"id":":nth-child(2) > td > input"}|/html/body/table/tbody/tr[2]/td/table/tbody/tr/td/form/center/table/tbody/tr[2]/td/input
001403978702638809130|-1|1|326889343748|-1|2|:nth-child(2) > td > input|-3|username|INPUT|text|9|518|121|21|1.Zw==||-1|-1||{"id":":nth-child(2) > td > input"}|/html/body/table/tbody/tr[2]/td/table/tbody/tr/td/form/center/table/tbody/tr[2]/td/input
001403978702638809130|-1|1|326889363151|39610|3|:nth-child(2) > td > input|-3|username|INPUT|text|46|516|121|21|5.dHNldWc=||-1|-1||{"id":":nth-child(2) > td > input"}|
001403978702638809130|-1|1|326889365907|2746|3|:nth-child(4) > td > input|-3|password|INPUT|password|16|618|121|21| 5.KioqKio=||-1|-1||{"id":":nth-child(4) > td > input"}|",
        BODY_END
    );

    ns_end_transaction("test_rum_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("test_rum_3");
    ns_web_url("test_rum_3",
        "URL=https://10.10.30.9:8003/test_rum?s=001403978702638809130&p=1&m=0&op=useraction&pi=1&CavStore=-1&pid=-1&d=0|0&lts=326889366&nvcounter=3",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=CavVI;CavSF;CavNVC",
        BODY_BEGIN,
            "001403978702638809130|-1|1|326889366128|229|2|:nth-child(5) > td > input|-3|login|INPUT|image|47|676|95|25|||-1|-1||{"id":":nth-child(5) > td > input"}|/html/body/table/tbody/tr[2]/td/table/tbody/tr/td/form/center/table/tbody/tr[5]/td/input
001403978702638809130|-1|1|326889366145|0|1001||-2||||0|0|1521|527|||1000|1000||{}|",
        BODY_END
    );

    ns_end_transaction("test_rum_3", NS_AUTO_STATUS);
    ns_page_think_time(0.001);

    //Page Auto split for Image Link 'Login' Clicked by User
    ns_start_transaction("login");
    ns_web_url("login",
        "URL=https://10.10.30.9:8003/cgi-bin/login?userSession=75893.0884568651DQADHfApHDHfcDtccpfAttcf&username=guest&password=guest&login.x=55&login.y=10&JSFormSubmit=off",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=CavVI;CavSF;CavNVC"
    );

    ns_end_transaction("login", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("test_rum_4");
    ns_web_url("test_rum_4",
        "URL=https://10.10.30.9:8003/test_rum?s=001403978702638809130&p=1&m=0&op=useraction&pi=1&CavStore=-1&pid=-1&d=0|0&lts=326889366&nvcounter=4",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=CavVI;CavSF;CavNVC",
        BODY_BEGIN,
            "001403978702638809130|-1|1|326889366188|0|1001||-2||||0|0|1521|527|||1000|1000||{}|",
        BODY_END,
        INLINE_URLS,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/banner_animated.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/sun_swede.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/flights.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/home.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/signoff.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/images/banner_merctur.jpg", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/vep/images/velocigen.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/favicon.ico", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE
    );

    ns_end_transaction("test_rum_4", NS_AUTO_STATUS);
    ns_page_think_time(1.903);

    //Page Auto split for Image Link 'Search Flights Button' Clicked by User
    ns_start_transaction("reservation");
    ns_web_url("reservation",
        "URL=https://10.10.30.9:8003/cgi-bin/reservation",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=CavVI;CavSF;CavNVC",
        INLINE_URLS,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/banner_animated.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/sun_swede.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/flights.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/home.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/signoff.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/splash_Findflight.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/images/continue.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/vep/images/velocigen.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/favicon.ico", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE
    );

    ns_end_transaction("reservation", NS_AUTO_STATUS);
    ns_page_think_time(2.04);

    //Page Auto split for Image Link 'findFlights' Clicked by User
    ns_start_transaction("findflight");
    ns_web_url("findflight",
        "URL=https://10.10.30.9:8003/cgi-bin/findflight?depart=Acapulco&departDate=05-12-2024&arrive=Acapulco&returnDate=05-13-2024&numPassengers=1&seatPref=None&seatType=Coach&findFlights.x=45&findFlights.y=18",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=CavVI;CavSF;CavNVC",
        INLINE_URLS,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/banner_animated.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/sun_swede.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/flights.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/home.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/signoff.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/images/splash_Searchresults.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/images/continue.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/images/startover.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/vep/images/velocigen.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/favicon.ico", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE
    );

    ns_end_transaction("findflight", NS_AUTO_STATUS);
    ns_page_think_time(2.898);

    //Page Auto split for Image Link 'reserveFlights' Clicked by User
    ns_start_transaction("findflight_2");
    ns_web_url("findflight_2",
        "URL=https://10.10.30.9:8003/cgi-bin/findflight?hidden_outboundFlight_button0=000%7C0%7C05-12-2024&outboundFlight=button1&hidden_outboundFlight_button1=001%7C0%7C05-12-2024&hidden_outboundFlight_button2=002%7C0%7C05-12-2024&hidden_outboundFlight_button3=003%7C0%7C05-12-2024&numPassengers=1&advanceDiscount=&seatType=Coach&seatPref=None&reserveFlights.x=74&reserveFlights.y=4",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=CavVI;CavSF;CavNVC",
        INLINE_URLS,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/banner_animated.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/sun_swede.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/flights.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/home.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/signoff.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/images/splash_creditcard.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/images/purchaseflight.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/images/startover.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/vep/images/velocigen.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/favicon.ico", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE
    );

    ns_end_transaction("findflight_2", NS_AUTO_STATUS);
    ns_page_think_time(63.344);

    //Page Auto split for Image Link 'buyFlights' Clicked by User
    ns_start_transaction("findflight_3");
    ns_web_url("findflight_3",
        "URL=https://10.10.30.9:8003/cgi-bin/findflight?firstName=Tiger&lastName=Scott&address1=4261+Stevenson+Blvd.&address2=Fremont%2C+CA+94538&pass1=Scott+Tiger&creditCard=&expDate=&oldCCOption=&numPassengers=1&seatType=Coach&seatPref=None&outboundFlight=001%7C0%7C05-12-2024&advanceDiscount=&buyFlights.x=59&buyFlights.y=15&.cgifields=saveCC",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=CavVI;CavSF;CavNVC",
        INLINE_URLS,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/banner_animated.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/sun_swede.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/flights.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/home.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/signoff.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/images/splash_flightconfirm.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/images/bookanother.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/vep/images/velocigen.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/favicon.ico", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE
    );

    ns_end_transaction("findflight_3", NS_AUTO_STATUS);
    ns_page_think_time(25.622);

    //Page Auto split for Image Link 'SignOff Button' Clicked by User
    ns_start_transaction("welcome");
    ns_web_url("welcome",
        "URL=https://10.10.30.9:8003/cgi-bin/welcome",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=CavVI;CavSF;CavNVC",
        INLINE_URLS,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/banner_animated.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/sun_swede.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/Merc10-dev/images/login.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/images/banner_merctur.jpg", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/tours/vep/images/velocigen.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE,
            "URL=https://10.10.30.9:8003/favicon.ico", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=CavVI;CavSF;CavNVC", END_INLINE
    );

    ns_end_transaction("welcome", NS_AUTO_STATUS);
    ns_page_think_time(24.396);

}

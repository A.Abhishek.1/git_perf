--Request 
GET https://10.10.30.9:8003/tours/index.html
Host: 10.10.30.9:8003
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
sec-ch-ua-platform: \"Linux\"
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7
Sec-Fetch-Site: none
Sec-Fetch-Mode: navigate
Sec-Fetch-User: ?1
Sec-Fetch-Dest: document
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200 OK
Content-Length: 3074
Content-Type: text/html
Connection: Keep-Alive
----
--Request 
GET https://10.10.30.9:8003/tours/Merc10-dev/images/banner_animated.gif
Host: 10.10.30.9:8003
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://10.10.30.9:8003/tours/index.html
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200 OK
Content-Length: 16522
Content-Type: image/gif
Connection: Keep-Alive
----
--Request 
GET https://10.10.30.9:8003/tours/Merc10-dev/images/sun_swede.gif
Host: 10.10.30.9:8003
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://10.10.30.9:8003/tours/index.html
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200 OK
Content-Length: 4125
Content-Type: image/gif
Connection: Keep-Alive
----
--Request 
GET https://10.10.30.9:8003/tours/Merc10-dev/images/login.gif
Host: 10.10.30.9:8003
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://10.10.30.9:8003/tours/index.html
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200 OK
Content-Length: 1323
Content-Type: image/gif
Connection: Keep-Alive
----
--Request 
GET https://10.10.30.9:8003/tours/images/banner_merctur.jpg
Host: 10.10.30.9:8003
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://10.10.30.9:8003/tours/index.html
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200 OK
Content-Length: 22548
Content-Type: image/jpeg
Connection: Keep-Alive
----
--Request 
GET https://10.10.30.9:8003/nv/netvision/nv_bootstrap.js
Host: 10.10.30.9:8003
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://10.10.30.9:8003/tours/index.html
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200 OK
Content-Encoding: gzip
Content-Length: 678
Content-Type: text/javascript
cache-control: public, max-age=60480
----
--Request 
GET https://10.10.30.9:8003/nv/netvision/cav_nv.js
Host: 10.10.30.9:8003
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://10.10.30.9:8003/tours/index.html
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200 OK
Content-Encoding: gzip
Content-Length: 111626
Content-Type: text/javascript
cache-control: public, max-age=7200
----
--Request 
GET https://10.10.30.9:8003/favicon.ico
Host: 10.10.30.9:8003
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://10.10.30.9:8003/tours/index.html
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200 OK
Content-Length: 1406
Connection: Keep-Alive
----
--Request 
GET https://10.10.30.9:8003/nv/netvision/config.js
Host: 10.10.30.9:8003
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://10.10.30.9:8003/tours/index.html
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200 OK
Content-Encoding: gzip
Content-Length: 911
Content-Type: text/javascript
cache-control: public, max-age=604800
----
--Request 
GET https://10.10.30.9:8003/nv/netvision/jquery.min.js
Host: 10.10.30.9:8003
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://10.10.30.9:8003/tours/index.html
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200 OK
Content-Encoding: gzip
Content-Length: 24311
Content-Type: text/javascript
cache-control: public, max-age=60480
----
--Request 
GET https://10.10.30.9:4444/nv/netvision/%3CFEEDBACK-MODULE%3E/feedback.js
Host: 10.10.30.9:4444
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: same-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://10.10.30.9:8003/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
Cookie: CavSF=cavnvComplete,,,,,,,,,,,1,,,,
----
--Response 
HTTP/1.1 503
cache-control: no-cache
content-type: text/html
----
--Request 
GET https://10.10.30.9:8003/test_rum?s=000000000000000000000&p=1&op=timing&pi=1&CavStore=-1&pid=-1&d=1|0|-1|1|387|-1|0|383|407|2|-2|162|0|0|585|503|-1|503|0|1||https%3A%2F%2F10.10.30.9%3A8003%2Ftours%2Findex.html|10.10.30.9%3A8003|CavSF%3DcavnvComplete%2C%2C%2C%2C%2C%2C%2C%2C%2C%2C%2C1%2C%2C%2C%2C|22|536875008|-1|24|24|LINUX|LINUX%2F5.15.0%3B%20x86%3B%20%3B%20Chromium%2F114%3B%20114.0.5735.199|en-US|%5Bobject%20PluginArray%5D|Mozilla|0|PC|114|5.15.0|-1|0|0|0|%7B-1%7D|503|-1|0|3374|0|0|0|560|560|0|0|4.11.0_4f066a|4f16d3&lts=-1&d2=-1|-1|-1|1|100|0|0|0|0|0
Host: 10.10.30.9:8003
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://10.10.30.9:8003/tours/index.html
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
Cookie: CavSF=cavnvComplete,,1:0:0,,0|,,,,,,,1,,,,; CavNVC=-0--1-2
----
--Response 
HTTP/1.1 200 OK
Content-Length: 0000000313
Content-Type: text/plain
Access-Control-Allow-Origin: *
Timing-Allow-Origin: *
Access-Control-Allow-Credentials: true
Access-Control-Allow-Methods: POST, PUT, DELETE, GET, OPTIONS
Access-Control-Request-Headers: *
Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization, Content-Encoding
Access-Control-Max-Age: 300
cache-control: no-store
Connection: Keep-Alive
----


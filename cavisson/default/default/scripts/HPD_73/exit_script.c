/*-----------------------------------------------------------------------------
    Name: exit_script
    Created By: cavisson
    Date of creation: 4.13.0 (build# 111)
    Flow details:
    Build details: 05/16/2024 02:41:17
    Modification History:
-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include "ns_string.h"

int exit_script ()
{
    return 0;
}

--Request 
POST https://api.tatadigital.com/analytics-engine/events/v1
Host: api.tatadigital.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-platform: \"Linux\"
customer-hash: null
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
Content-Type: application/json
tdl-sso-version: 4.1.15
session: false
client_id: CROMA-WEB-APP
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://www.croma.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
 POSTDATA {"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tLyIsImV2ZW50RGF0YSI6W3sidGltZXN0YW1wIjoxNzE1ODM5MTQxODA2LCJkYXRhIjoie1wiZXZlbnRcIjpcInBhZ2VMb2FkXCIsXCJwYXlsb2FkXCI6e1widXJsXCI6XCJodHRwczovL3d3dy5jcm9tYS5jb20vXCIsXCJkYXRhU291cmNlXCI6XCJsb2FkXCIsXCJpc1RydXN0ZWRcIjp0cnVlfX0ifV0sImN1c3RvbWVySGFzaCI6bnVsbCwiZWNpZCI6IlVua25vd24iLCJjbGllbnRJZCI6IkNST01BLVdFQi1BUFAiLCJzZXNzaW9uSWQiOiJiNjVmNTRiNC01YzkzLTQ4MmUtOGNmOC1jMjEwZWNmZmE3OGYuOTIzYmI1N2YtOTUxYi00NzgwLThjYzMtM2U3Yzg5ZTkwNWZlIiwiYnJhbmQiOiJDUk9NQSIsInRpbWVzdGFtcCI6MTcxNTgzOTE0MTgwNiwiZGV2aWNlIjoiTW96aWxsYS81LjAgKFgxMTsgTGludXggeDg2XzY0KSBBcHBsZVdlYktpdC81MzcuMzYgKEtIVE1MLCBsaWtlIEdlY2tvKSBDaHJvbWUvMTE0LjAuMC4wIFNhZmFyaS81MzcuMzYiLCJqYXJ2aXNJZCI6IjRkNjA0NTcwLWJhZmItNDU1MS1iYWQwLTg1Mzg5NzFjN2VlMiIsImFkdmVydGlzaW5nSWQiOm51bGwsImFwcFZlcnNpb24iOiIwIn0="}
--Response 
HTTP/1.1 200
content-type: application/json; charset=utf-8
content-length: 19
etag: W/\"13-AU5FafGnjGQRcAzuw5htDSM9eaQ\"
x-frame-options: SAMEORIGIN
x-xss-protection: 1; mode=block
date: Thu, 16 May 2024 05:59:01 GMT
server-timing: cdn-cache; desc=MISS
server-timing: edge; dur=35
server-timing: origin; dur=9
server-timing: ak_p; desc=\"1715839141818_388943750_1558506937_4422_10763_3_0_219\";dur=1
set-cookie: _abck=349DCDFF2D5E53EC7A3EE8EA8A9A95B1~-1~YAAQhs8uF4wfu26PAQAA8of7fwvaBKrFqVW3y8lYwxDbioUR+c4a63wfeBKplpNKZDVsc9Sw6lwIKeThbcUjuNUhnwdjAsAQWd3rkrSWViP2qxd0q1p2ugd79CjPv9P1R99fp1Lns1LovqH9wNivh7JIab5TrBFIjv0PiUXUb7vneOSVhJPQ8KURXJ12xWRks6TDiivL+O4HWNMuOqA/LkVclFHmB573I0gDR2KmmpcecsXaDY2YFyaRakiCIn6EhNjDWCuPqZZn2K8ngRSKUUnx/eJmVUxlp/UjOMbmfajlxygM5rcp6sMREv/TpaKjMczqNwPrlHe/kncRy8D077diJHxWMdUTcDDHKI2ggDeBmVdMRGt3WPLM9EFmtTHH~-1~-1~-1; Domain=.tatadigital.com; Path=/; Expires=Fri, 16 May 2025 05:59:01 GMT; Max-Age=31536000; Secure
set-cookie: ak_bmsc=3D61B75129EEFEF1FC4A33B52486C8E9~000000000000000000000000000000~YAAQhs8uF40fu26PAQAA8of7fxdGFVuqvXv+JZRpdVSLuJe1mDsZiCyHB/E6yqLbp1mWj9PNN0X25mVkGSzMDsWZ8o0OBpN4WCzkBv1El+xnDUVe1oPxu/NzVh8y7me31oEE01Zr2I8qDoCnAu3KaneJxEHEnP8UjTeqZjxBqpO0sDJFlvw0WYg83zWQEsOEqcIyuCdB/d+QCpukl76csh5iyf1gHGaHtpGK6GIWexXlxPlfz/vymTn4GQc61k445XD+zkIss21hYafX7u4U+pwuffxIzdW7YyhAaJ0yKG4VCoB6KuvFu3jRlhP6qYznHRzbUEqQeiEZ9uHSw7y7wbmX64Jflq/O9fxLUVFu1xvUSitrCvWNiNE74ByQLrYrTrQFoQ7YfJEu6fmm64mAiQ==; Domain=.tatadigital.com; Path=/; Expires=Thu, 16 May 2024 07:59:01 GMT; Max-Age=7200
set-cookie: bm_sz=37C7D5D33FFDDAFD6BC39F04736F1CD5~YAAQhs8uF44fu26PAQAA8of7fxeTr1ZY5PmkQIyNGD7d35+pSpwQyxF6mkJ5wer2/UMJ0F0XM95MkBOXD3LSNsUqqcs7t5BRm2FCM2Ai7939rh53hqCWmaKvtnyJSiR1EiajmEnetkAeM81Gu2IUm0GAJE1N1rHxjfcP745Uo33lBcwmhcg8wbOxYC6zYME9qWh1NhawN4ayh/ll3PvAAPwI0rjMnTP6cDeDMrqCGZy9AugPCy5k1nr//cNFqD6pAg2E+sDZn2BoVtp2P4njD+CllDJtw7ZUVvZu85a9XY+R77z5BmqhtrpmYvXj+i/ERzSaUrFIOA0fQfWsO95h4APwl3fl8SLCk/jK5pspOU0TH7iIPTPb0MX6Zmc=~4604228~4474169; Domain=.tatadigital.com; Path=/; Expires=Thu, 16 May 2024 09:59:01 GMT; Max-Age=14400
----
--Request 
GET https://cdn.appdynamics.com/adrum-ext.a57fe9a4dfa0e1d6b2dc001466e4e21d.js
Host: cdn.appdynamics.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://www.croma.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: application/javascript
date: Tue, 16 Apr 2024 20:50:30 GMT
server: nginx/1.16.1
last-modified: Tue, 21 Nov 2023 16:11:07 GMT
etag: W/\"655cd69b-d667\"
access-control-allow-origin: *
access-control-allow-methods: GET, POST, OPTIONS
access-control-allow-headers: DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type
cache-control: public, max-age=2678400, s-max-age=14400
timing-allow-origin: *
content-encoding: gzip
vary: Accept-Encoding
x-cache: Hit from cloudfront
via: 1.1 6c4627b4ac2c0d26f97aa4c03194996e.cloudfront.net (CloudFront)
x-amz-cf-pop: DEL51-P3
x-amz-cf-id: LYlOCQEr-2EAubE2lQeK9hi3E8vjafv7Mz_O7rV4Gfr8DmazcKAp4w==
age: 2538511
----
--Request 
GET https://sslwidget.criteo.com/event?a=56256&v=5.23.0&otl=1&p0=e%3Dexd%26site_type%3Dd&p1=e%3Dvh%26tms%3Dgtm-template&p2=e%3Ddis&adce=1&tld=croma.com&dy=1&fu=https%253A%252F%252Fwww.croma.com%252F&ceid=c0906449-bf8d-4ffd-a8bd-5be728d7f957&dtycbr=64651
Host: sslwidget.criteo.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://www.croma.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: application/x-javascript
date: Thu, 16 May 2024 05:59:01 GMT
server: Kestrel
access-control-allow-origin: *
cache-control: no-cache
content-encoding: gzip
expires: 0
pragma: no-cache
cross-origin-resource-policy: cross-origin
p3p: NON DSP COR CURa PSA PSD OUR BUS NAV STA
timing-allow-origin: *
server-processing-duration-in-ticks: 5688196
strict-transport-security: max-age=31536000; preload;
----
--Request 
GET https://www.croma.com/assets/favicon.ico
Host: www.croma.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.croma.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
Cookie: AKA_A2=A; at_check=true; AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg=1; jarvis-id=4d604570-bafb-4551-bad0-8538971c7ee2; s_ecid=MCMID%7C23208888194201725061814923007976880958; mbox=session#895fcaa2d50341f0b4231873c20498c7#1715841000|PC#895fcaa2d50341f0b4231873c20498c7.41_0#1779083940; s_plt=NaN; s_pltp=undefined; s_cc=true; s_nr30=1715839139916-New; s_stop5=0; _gcl_au=1.1.1421799651.1715839140; AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg=179643557%7CMCIDTS%7C19860%7CMCMID%7C23208888194201725061814923007976880958%7CMCAAMLH-1716443939%7C12%7CMCAAMB-1716443939%7C6G1ynYcLPuiQxYZrsz_pkqfLG9yMXBpb2zX5dvJdYQJzPXImdj0y%7CMCOPTOUT-1715846339s%7CNONE%7CMCAID%7CNONE%7CMCSYNCSOP%7C411-19867%7CvVersion%7C5.5.0; _hjSessionUser_3400595=eyJpZCI6Ijk2YjE5NmY3LWRkMzMtNTkxYy1iY2IyLTIzMDNlMDRkNzEyNSIsImNyZWF0ZWQiOjE3MTU4MzkxNDA2MDUsImV4aXN0aW5nIjpmYWxzZX0=; _hjSession_3400595=eyJpZCI6ImFiMTJiMDJjLTFjMWYtNDJkOC1iOGYxLTAwY2Q2ZDYyMWM1ZSIsImMiOjE3MTU4MzkxNDA2MDksInMiOjAsInIiOjAsInNiIjowLCJzciI6MCwic2UiOjAsImZzIjoxLCJzcCI6MH0=; _fbp=fb.1.1715839140634.281453602; _ga=GA1.1.1944455297.1715839141; _uetsid=6467a920134911efa7d615e2de33fb03; _uetvid=6467fc40134911efa1829f1407dcc88b; WZRK_G=6f08a8df4ce04ff4b786855393b6d325; WZRK_S_679-6RK-976Z=%7B%22p%22%3A1%2C%22s%22%3A1715839141%2C%22t%22%3A1715839141%7D; s_ips=523; s_tp=3225; s_ppv=homepage%2C16%2C16%2C523%2C1%2C6; RT=\"z=1&dm=www.croma.com&si=a944a24b-fdf9-4959-b8b0-94a3120cdc98&ss=lw8ubjvb&sl=1&tt=38h&rl=1&ld=38k\"; _ga_DJ3RGVFHJN=GS1.1.1715839140.1.0.1715839141.59.0.0
----
--Response 
HTTP/1.1 200
content-type: image/x-icon
request-context: appId=cid-v1:21d4e0ae-a33c-4037-b5fb-14e6aa4460da
access-control-allow-origin: *
content-security-policy: script-src 'self' 'unsafe-inline' 'unsafe-eval' https://cdnjs.cloudflare.com http://assets.adobedtm.com https://www.googletagmanager.com *.facebook.net *.hotjar.com https://www.googleadservices.com *.doubleclick.net *.googleapis.com *.go-mpulse.net *.juspay.in https://youtube.com *.akstat.io *.cloudinary.com https://maps.gstatic.com *.cloudfront.net *.adobe.com  *.omniture.com *.asbmit.com *.admitad.com *.bing.com https://cromapt-res.cloudinary.com *.jwplatform.com *.jwpsrv.com *.jwpcdn.com *.cloudfront.net *.pointandplace.com https://croma.api.cashify.in/ *.omguk.com *.qualtrics.com *.fullstory.com *.tatadigital.com *.croma.com https://webtrafficsource.com/ https://cdn.taboola.com/ https://www.clarity.ms/ *.criteo.com *.criteo.net *.wzrkt.com *.clevertap-prod.com *.spiky.wzrkt.com *.spiky.clevertap-prod.com https://s3-eu-west-1.amazonaws.com/static.wizrocket.com/js/sw_webpush.js *.appdynamics.com *.eum-appdynamics.com;default-src 'self' *.croma.com *.demdex.net https://cm.everesttech.net *.tatadigital.com https://www.facebook.com https://www.google.com https://www.google-analytics.com https://analytics.google.com/ https://www.google.co.in *.omtrdc.net https://maps.googleapis.com http://www.yellowslice.us https://yellowslice.us/ *.facebook.net *.juspay.in https://youtube.com *.go-mpulse.net *.akstat.io *.cloudinary.com *.hotjar.com https://maps.gstatic.com *.cloudfront.net *.adobe.com  *.omniture.com *.asbmit.com *.admitad.com *.bing.com http://ysprod.croma.com *.hotjar.io wss://*.hotjar.com data: *.jwpsrv.com *.jwplatform.com *.jwpcdn.com *.cloudfront.net *.pointandplace.com blob: https://croma.api.cashify.in/ *.omguk.com *.qualtrics.com *.fullstory.com *.tatadigital.com *.croma.com https://ad.doubleclick.net/ https://googleads.g.doubleclick.net/ *.adobe.com  *.criteo.com *.criteo.net *.wzrkt.com *.clevertap-prod.com *.spiky.wzrkt.com *.spiky.clevertap-prod.com https://s3-eu-west-1.amazonaws.com/static.wizrocket.com/js/sw_webpush.js *.appdynamics.com *.eum-appdynamics.com;style-src 'self' 'unsafe-inline' http://www.yellowslice.us https://yellowslice.us/ https://fonts.googleapis.com *.croma.com data: *.jwpsrv.com *.jwplatform.com *.jwpcdn.com *.cloudfront.net *.pointandplace.com;style-src-elem 'self' 'unsafe-inline' https://fonts.googleapis.com http://www.yellowslice.us https://yellowslice.us/ *.croma.com *.jwplatform.com *.jwpsrv.com *.jwpcdn.com *.cloudfront.net *.pointandplace.com;font-src 'self' 'unsafe-inline' https://fonts.gstatic.com https://fonts.googleapis.com data: *.croma.com *.jwplatform.com *.jwpsrv.com *.jwpcdn.com *.cloudfront.net *.pointandplace.com;frame-src 'self' *.doubleclick.net https://www.facebook.com http://www.yellowslice.us https://yellowslice.us/ *.croma.com  *.demdex.net https://media.flixcar.com *.juspay.in https://youtube.com https://www.youtube.com https://docs.google.com *.hotjar.com https://stacins03hybdevcma01.z29.web.core.windows.net *.flixcar.com *.flixfacts.com *.flix360.com *.flix360.io https://livevideo.croma.com:8443/ intent://arvr.google.com *.cloudfront.net *.tatadigital.com mailto: *.croma.com https://zodiacupdates.com/ *.adobe.com *.criteo.com *.criteo.net https://www.croma-myfestivewish.com https://wap-ci-ecom-dev-chb-01.azurewebsites.net/ https://wap-ci-ecom-pt-chb-01.azurewebsites.net/ https://wap-ci-ecom-prd-chb-01.azurewebsites.net/;frame-ancestors 'self' https://livevideo.croma.com:8443/
x-dns-prefetch-control: off
expect-ct: max-age=0
x-frame-options: SAMEORIGIN
strict-transport-security: max-age=15724800; includeSubDomains
x-download-options: noopen
x-content-type-options: nosniff
x-permitted-cross-domain-policies: none
referrer-policy: no-referrer
x-xss-protection: 0
accept-ranges: bytes
last-modified: Mon, 06 May 2024 18:40:53 GMT
etag: W/\"12346-18f4f356f08\"
content-encoding: gzip
content-length: 1888
cache-control: max-age=600
expires: Thu, 16 May 2024 06:09:01 GMT
date: Thu, 16 May 2024 05:59:01 GMT
vary: Accept-Encoding
server-timing: cdn-cache; desc=HIT
server-timing: edge; dur=1
server-timing: ak_p; desc=\"1715839141913_388943743_867776019_37_21244_3_0_219\";dur=1
----


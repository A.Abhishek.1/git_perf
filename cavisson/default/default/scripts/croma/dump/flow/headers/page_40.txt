--Request 
POST https://api.tatadigital.com/analytics-engine/events/v1
Host: api.tatadigital.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-platform: \"Linux\"
customer-hash: null
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
Content-Type: application/json
tdl-sso-version: 4.1.15
session: false
client_id: CROMA-WEB-APP
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://www.croma.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
 POSTDATA {"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tL2NhbXBhaWduL29uZXBsdXMtci01Zy9jLzY0MDc/cT0lM0FwcmljZS1hc2MiLCJldmVudERhdGEiOlt7InRpbWVzdGFtcCI6MTcxNTgzOTE2NDM4MCwiZGF0YSI6IntcImV2ZW50XCI6XCJwYWdlTG9hZFwiLFwicGF5bG9hZFwiOntcInVybFwiOlwiaHR0cHM6Ly93d3cuY3JvbWEuY29tL2NhbXBhaWduL29uZXBsdXMtci01Zy9jLzY0MDc/cT0lM0FwcmljZS1hc2NcIixcImRhdGFTb3VyY2VcIjpcInB1c2hTdGF0ZVwifX0ifV0sImN1c3RvbWVySGFzaCI6bnVsbCwiZWNpZCI6IlVua25vd24iLCJjbGllbnRJZCI6IkNST01BLVdFQi1BUFAiLCJzZXNzaW9uSWQiOiJiNjVmNTRiNC01YzkzLTQ4MmUtOGNmOC1jMjEwZWNmZmE3OGYuOTIzYmI1N2YtOTUxYi00NzgwLThjYzMtM2U3Yzg5ZTkwNWZlIiwiYnJhbmQiOiJDUk9NQSIsInRpbWVzdGFtcCI6MTcxNTgzOTE2NDM4MCwiZGV2aWNlIjoiTW96aWxsYS81LjAgKFgxMTsgTGludXggeDg2XzY0KSBBcHBsZVdlYktpdC81MzcuMzYgKEtIVE1MLCBsaWtlIEdlY2tvKSBDaHJvbWUvMTE0LjAuMC4wIFNhZmFyaS81MzcuMzYiLCJqYXJ2aXNJZCI6IjRkNjA0NTcwLWJhZmItNDU1MS1iYWQwLTg1Mzg5NzFjN2VlMiIsImFkdmVydGlzaW5nSWQiOm51bGwsImFwcFZlcnNpb24iOiIwIn0="}
--Response 
HTTP/1.1 200
content-type: application/json; charset=utf-8
content-length: 19
etag: W/\"13-AU5FafGnjGQRcAzuw5htDSM9eaQ\"
x-frame-options: SAMEORIGIN
x-xss-protection: 1; mode=block
date: Thu, 16 May 2024 05:59:24 GMT
server-timing: cdn-cache; desc=MISS
server-timing: edge; dur=57
server-timing: origin; dur=9
server-timing: ak_p; desc=\"1715839164385_388943750_1558704075_7113_10973_7_0_109\";dur=1
set-cookie: _abck=10F814E1FF76704081258A8F2F12A09E~-1~YAAQhs8uF4Epu26PAQAAL+D7fwuqYHPKu5pBG5nsDAS6dCiuhqmMFE1WgdzKcadR28hQl5PQYNmGr+a+roo0zkALdfXFff9xk7WSTec1/3KYUDPGFSVTu/wkGyMWrVX+sfYYrjpEc6Isn8B/r9DezIxyQ6iZAM69RFpQP4y/BUQAYRRBZXyLpI8pLo7FhZcMKSn31ol3harTG+34zJ55apRsA/a5XhS/YRhjHc28gnYJnueavhGkomkUUllnSiGBqxSh/vmlhUnrPH0kGkbbPHtwS208eybv3nTws/U3R3MSXP7elpPfpxalROP73UCU7Y2D5cpKXjjlJXBxc6a7vUgFXlIBu0PQk9EEZV8WIwKMNtsoyXiua2CamGke0r19~-1~-1~-1; Domain=.tatadigital.com; Path=/; Expires=Fri, 16 May 2025 05:59:24 GMT; Max-Age=31536000; Secure
set-cookie: ak_bmsc=8542FF8093DC5EDB9801B8D30A8C8DDB~000000000000000000000000000000~YAAQhs8uF4Ipu26PAQAAL+D7fxcHnIQKWHiBqtXCCmtbdLsIb1swsCx9ArZWu/oSEE8vUm00glfRaEq4EUvOxkXu2SKTg8MlwsJZc3VUXhfKGwtdcx+F8GkFRoxMtpaRUObW3h62EpwlnhpEX3Fwnsgli2IL9ovSUFtHrlmFjWFkEpRUyj2y2pcthprPhYNWioIOld0TxB+CEwfibgoM4MD45CIPPsQgSVlxunWzC6gM64ftMUMREu6BXlTucLGGo4TC+f2eNpOH3O+r3KRkIEG7M+uCpd8GOTnzHX7HJXq5r/beJa2Ov8+5S2qhr8ySuhVScGWY3OIMqVeD7mps8MfEDr+wDmAFGCvtFmNo0fosgrtJRxPUWZkqfnZ679T462ATttpa5LnZVdEvZeEq/g==; Domain=.tatadigital.com; Path=/; Expires=Thu, 16 May 2024 07:59:24 GMT; Max-Age=7200
set-cookie: bm_sz=EBE35779441ED1E9FC26D236AB35DA55~YAAQhs8uF4Mpu26PAQAAL+D7fxceYrBNWc3+xTLunDCWC/mtWeqt8KerRp/6Vn5uYGOm0E2KMPWK2rVyeheN+/hF1lZ6WmdvWPnLG413EAJmE5IhIJbm9+4Mt8e+I6YYdW/UTVoCRMWT58pZftYT33iFYW5aNzzIoBUuvsAXvQg1NwWva6tlKmbBCuSznkWAvXxkDZDbvgWyt4btZ/pgcFQu1T1T86jX3UhNgdA82NgsmOVD4Q206eBbmBj1T80oH1xKsLYGi4YZeLqlTTEU625lC7FbfqVmu+xpKLjPLxu5qe287B5CxM2yoA5BLSANNdN6iDjEpGBMjmU07piQw9clv90PY0H9vVhMmgICscjGmLkEatBfmbrA46s=~3684163~3683138; Domain=.tatadigital.com; Path=/; Expires=Thu, 16 May 2024 09:59:24 GMT; Max-Age=14400
----


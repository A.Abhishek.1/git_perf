--Request 
POST https://api.tatadigital.com/analytics-engine/events/v1
Host: api.tatadigital.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-platform: \"Linux\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
Content-Type: text/plain;charset=UTF-8
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: empty
Referer: https://www.croma.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
 POSTDATA {"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tL3Bob25lcy13ZWFyYWJsZXMvbW9iaWxlLXBob25lcy9jLzEwIiwiZXZlbnREYXRhIjpbeyJ0aW1lc3RhbXAiOjE3MTU4MzkxNTYwNDYsImRhdGEiOiJ7XCJldmVudFwiOlwid2ViLXZpdGFsc1wiLFwiZGF0YVwiOntcIm5hbWVcIjpcIkZJRFwiLFwidmFsdWVcIjoxLFwicmF0aW5nXCI6XCJnb29kXCIsXCJkZWx0YVwiOjEsXCJlbnRyaWVzXCI6W3tcIm5hbWVcIjpcInBvaW50ZXJkb3duXCIsXCJlbnRyeVR5cGVcIjpcImZpcnN0LWlucHV0XCIsXCJzdGFydFRpbWVcIjoxNzkzMy4xOTk5OTg4NTU1OSxcImR1cmF0aW9uXCI6MzIsXCJwcm9jZXNzaW5nU3RhcnRcIjoxNzkzNC4xOTk5OTg4NTU1OSxcInByb2Nlc3NpbmdFbmRcIjoxNzkzNS4xOTk5OTg4NTU1OSxcImNhbmNlbGFibGVcIjp0cnVlfV0sXCJpZFwiOlwidjMtMTcxNTgzOTEzOTI3MC02ODA5MDg1NzQ2NjA4XCIsXCJuYXZpZ2F0aW9uVHlwZVwiOlwibmF2aWdhdGVcIn19In1dLCJjdXN0b21lckhhc2giOm51bGwsImVjaWQiOiJVbmtub3duIiwiY2xpZW50SWQiOiJDUk9NQS1XRUItQVBQIiwic2Vzc2lvbklkIjoiYjY1ZjU0YjQtNWM5My00ODJlLThjZjgtYzIxMGVjZmZhNzhmLjkyM2JiNTdmLTk1MWItNDc4MC04Y2MzLTNlN2M4OWU5MDVmZSIsImJyYW5kIjoiQ1JPTUEiLCJ0aW1lc3RhbXAiOjE3MTU4MzkxNTYwNDYsImRldmljZSI6Ik1vemlsbGEvNS4wIChYMTE7IExpbnV4IHg4Nl82NCkgQXBwbGVXZWJLaXQvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lLzExNC4wLjAuMCBTYWZhcmkvNTM3LjM2IiwiamFydmlzSWQiOiI0ZDYwNDU3MC1iYWZiLTQ1NTEtYmFkMC04NTM4OTcxYzdlZTIiLCJhZHZlcnRpc2luZ0lkIjpudWxsLCJhcHBWZXJzaW9uIjoiMCJ9"}
--Response 
HTTP/1.1 200
content-type: application/json; charset=utf-8
content-length: 19
etag: W/\"13-AU5FafGnjGQRcAzuw5htDSM9eaQ\"
x-frame-options: SAMEORIGIN
x-xss-protection: 1; mode=block
date: Thu, 16 May 2024 05:59:16 GMT
server-timing: cdn-cache; desc=MISS
server-timing: edge; dur=47
server-timing: origin; dur=19
server-timing: ak_p; desc=\"1715839155969_388943750_1558622965_7208_10892_5_0_109\";dur=1
set-cookie: _abck=28FC652176B31A0D2B1C7F77F618A296~-1~YAAQhs8uFwEmu26PAQAATr/7fwukii81uNnwM7g1AnSOVfhSGT4nXp1TQq57dYfmqsJ5OMGGj9vE/EXdGlIoy3NAjsoTxzGNcfKKnTFds1FyvyZPYSTIMj9/XYsAUDEOBj7+Jnj4PB7CxyM+uCylg88td9mtC2fJVzGNYwQPtTI+8jVHKTF+RXhC61ogmHvxOdk/Va6RmnL/IGsjxRxsb9sSJ2TJT/nkHgJpCH0TrvyczgYOBpWflg9c0AGKuAfzhXvb3ZPM+VpKqyE4GC9DHV5bjKLFS+VXQYn3BN59tdhexmx9WXfsWYw5IsQ2Jk8GoF5BYxq925WAEnFIu9Vr9YDAdwW9nkN9AtO08idNz08sqOYwEODlZPdCioVYw/g8~-1~-1~-1; Domain=.tatadigital.com; Path=/; Expires=Fri, 16 May 2025 05:59:16 GMT; Max-Age=31536000; Secure
set-cookie: ak_bmsc=09D758101A393E4D2C8DC9D8D7E97E6A~000000000000000000000000000000~YAAQhs8uFwImu26PAQAATr/7fxeUTVgyhHaeIkMOkAHHsld3wFbGcAdPeX8wdzixtsx6Q/IHF1ydRm4sfSo9nwWuFuFTjh02KpwtEqe7cmbXoUiuwTeOFgRZBBRd+/aD/tS4AmdDQzuO8+mwA7AtRc8K7ipxg1VBfxzSvGnAZfh/LShqABWDmSHodd0c00Atj7DF4tHgogoGyo6ShLVwea/B1Gkdnpuj6lqQalVXHnBJgyN/92ReqGC31xjT1ee13AOmaHeate28xU+t6dWy2xAZBW5byvVMB93yXEaNiGGZnZS0ADDLltqJSU/tcalHx4mBBvxCujuvdzUdlWbrhnGhAXb+jHq3YXWgaPeh2lVvBeugw+MZqZvjQs0YziG+zsfhvyDZ9/d36+nvb9HHQw==; Domain=.tatadigital.com; Path=/; Expires=Thu, 16 May 2024 07:59:15 GMT; Max-Age=7199
set-cookie: bm_sz=E62F381E757CF75C1FE3B86FF0A08BFE~YAAQhs8uFwMmu26PAQAATr/7fxc0LsUrFEmfHtV6QF0/jBo0oWA2sIHBOTX0KnykD95788nYp6+EElhc03iDqeCrn0HTCnbnroUX/nChlIkaIJGDNIQ9r6Ur736U428PUDtjOILwbkzEKwD2f5Alw8AP4Vge0siPoZAkgto9kP56xbqK13mCB9GKDOtAVX0/r3HvW7P8ETvm7SwTsNYmQu9ZmHiv/OZQewtkjnnWwT/Ci6BcFA4DNheD+f/ApJMRKRHHg9KZdlCrfx7LLqucDZPEIORepn+AZGlCxsdx4nY5QCz2LOw1rvNlF+3aBQrD89thjBH4zT/KvrO3Q4kPkTITUBGBiscRms/gxYFo4oPwS3c2t+x0yzK8jmE=~3749446~3748419; Domain=.tatadigital.com; Path=/; Expires=Thu, 16 May 2024 09:59:15 GMT; Max-Age=14399
----


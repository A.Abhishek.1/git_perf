--Request 
GET https://api.tatadigital.com/analytics-engine/config/v2
Host: api.tatadigital.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-platform: \"Linux\"
sec-ch-ua-mobile: ?0
Site-Origin: https://www.croma.com
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
Content-Type: application/json
Accept: application/json
Client-Id: CROMA-WEB-APP
client_id: CROMA-WEB-APP
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://www.croma.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: application/json; charset=utf-8
content-encoding: gzip
etag: W/\"5a5-Cf2cWs3WTu6l0/KnSUPHMaQi2XA\"
x-frame-options: SAMEORIGIN
x-xss-protection: 1; mode=block
content-length: 763
date: Thu, 16 May 2024 05:58:59 GMT
vary: Accept-Encoding
server-timing: cdn-cache; desc=MISS
server-timing: edge; dur=61
server-timing: origin; dur=15
server-timing: ak_p; desc=\"1715839139300_388943750_1558489699_7532_12693_3_16_219\";dur=1
set-cookie: _abck=403A9AA778E98C527F8CA01C4A0BD659~-1~YAAQhs8uF14eu26PAQAATn77fwuEM+QOyoxn/HV8P8vzia9IgecRNKVmxod5fuRBVj9Xq1Q0e0YEfetftM4QUOe+6SYcNh4mwM4mXmocjCW4/0pfzHoCgOP8zfO3pu3WXAXhge8kP4COcjlRfktwsD6AfvI6VKg3HWdZH8kE7nzNOWTlCUPSEXmx+GeLM18KqVUJyXRmaopQXZNwPza6FMTwP0WfGVQduYPGPiJMVq+6kSZuLhUrGYqvfwJNFdjISYfQyvZDGcbL6zYkj8Gu8Irz8QQt3qb5EerCkg1wDP26xxR/rC4AyBn4fAV8xizI4Xx2lOozoT0Dkh+mzOJEcPpnfpUrW7FriIw5V70YRU+DzA60E6UKa34DVpWKwsuj~-1~-1~-1; Domain=.tatadigital.com; Path=/; Expires=Fri, 16 May 2025 05:58:59 GMT; Max-Age=31536000; Secure
set-cookie: ak_bmsc=805DECB2A3E925BB1D5BE7C8EC6DE81C~000000000000000000000000000000~YAAQhs8uF18eu26PAQAATn77fxeBTtf0j0REKi81UdonM9BmiROdmPJGibAC0budzjMXpVt5bG6Y/FDS3NlFqiuIxyEMWrDk0BMvI+ocD8nJ2CL/wIHZX/P+PqeecYxYnE8COes68JRR05YwbDZnMs8UFObIzvh03/RrHOaQpEJnIuzRhEYq0O2ta7unaT0aRASHgma4TiwQF07WGwJ0J/bV6UlUQ6G0lzZmtW1apQJMvPVNn0H+U9AiyKtG5ZJZDHZsCBiFIU2oK4muSiPBk9w+1Qf3uBe0JcM+iQsblL3QkVA1GWOr5fU6RSL6TyFS1264XdsvFIachcGe/+lbUqEzMnE+Z9ec63OgwOTCsde1yOXRJbX8DivlbAFTRDqUQphHWNGP3j0vLJmiucGvIw==; Domain=.tatadigital.com; Path=/; Expires=Thu, 16 May 2024 07:58:59 GMT; Max-Age=7200
set-cookie: bm_sz=8B4938928B0A6F457350F989CB8584C3~YAAQhs8uF2Aeu26PAQAATn77fxdnpDFOnB9dTXkFT8zMNW2lpcKBw7OBf2aJzu0zJ5TUedgLxfXd/MnLQNoUGmmhSDRXk7qVVjjOxWjLkBfhULZqMnNp/83OhBws95wFYacyZWoTUJl7uY7WtbDA02vUezBg+GzE5NadR8f1PzSwxlmOJaEbsueKYY8OPhgqjOqbNuzzA45RIeEqeZJNZ+EuvdCCCBjXLBg54k3wlkBK3xC60v8rsK5cSZqfr+kwDa8WNpdNwsdDsQJcL0ZcCpKh4mS+JlA5OiFNPb+WyaHqgKU3k0jFKsH3omVN6256vMBwgvXunJNuvjL3Qv2SPnyQEcTgdYz31M4hFck7UZ2bTz9CgMEvlXScaTo=~4404034~3552577; Domain=.tatadigital.com; Path=/; Expires=Thu, 16 May 2024 09:58:59 GMT; Max-Age=14400
----
--Request 
GET https://www.googletagmanager.com/gtm.js?id=GTM-5X2VQJX
Host: www.googletagmanager.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://www.croma.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: application/javascript; charset=UTF-8
access-control-allow-origin: *
access-control-allow-credentials: true
access-control-allow-headers: Cache-Control
content-encoding: br
vary: Accept-Encoding
date: Thu, 16 May 2024 05:58:59 GMT
expires: Thu, 16 May 2024 05:58:59 GMT
cache-control: private, max-age=900
last-modified: Thu, 16 May 2024 03:00:00 GMT
strict-transport-security: max-age=31536000; includeSubDomains
cross-origin-resource-policy: cross-origin
server: Google Tag Manager
content-length: 110342
x-xss-protection: 0
alt-svc: h3=\":443\"; ma=2592000,h3-29=\":443\"; ma=2592000
----


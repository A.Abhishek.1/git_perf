--Request 
POST https://cromaretail.tt.omtrdc.net/rest/v1/delivery?client=cromaretail&sessionId=895fcaa2d50341f0b4231873c20498c7&version=2.9.0
Host: cromaretail.tt.omtrdc.net
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-platform: \"Linux\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
Content-Type: text/plain
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://www.croma.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
 POSTDATA {"requestId":"b4692c69a5d5494ab7476e1985eed6d6","context":{"userAgent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36","clientHints":{"mobile":false,"platform":"Linux","browserUAWithMajorVersion":"\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\""},"timeOffsetInMinutes":-300,"channel":"web","screen":{"width":1523,"height":650,"orientation":"landscape","colorDepth":24,"pixelRatio":1},"window":{"width":790,"height":473},"browser":{"host":"www.croma.com","webGLRenderer":"ANGLE (Google, Vulkan 1.3.0 (SwiftShader Device (Subzero) (0x0000C0DE)), SwiftShader driver)"},"address":{"url":"https://www.croma.com/","referringUrl":""}},"id":{"marketingCloudVisitorId":"23208888194201725061814923007976880958"},"experienceCloud":{"audienceManager":{"locationHint":12,"blob":"6G1ynYcLPuiQxYZrsz_pkqfLG9yMXBpb2zX5dvJdYQJzPXImdj0y"},"analytics":{"logging":"server_side","supplementalDataId":"209844575BC87636-67440160CD9108F0","trackingServer":"metrics.croma.com","trackingServerSecure":"smetrics.croma.com"}},"execute":{"pageLoad":{}},"prefetch":{"views":[{}]}}
--Response 
HTTP/1.1 200
date: Thu, 16 May 2024 05:58:59 GMT
content-type: application/json;charset=UTF-8
vary: origin,access-control-request-method,access-control-request-headers,accept-encoding
x-request-id: ecb1773d-4d9e-414d-be33-003b0296d683
timing-allow-origin: *
accept-ch: Sec-CH-UA, Sec-CH-UA-Arch, Sec-CH-UA-Model, Sec-CH-UA-Platform, Sec-CH-UA-Platform-Version, Sec-CH-UA-Mobile, Sec-CH-UA-Bitness, Sec-CH-UA-Full-Version-List
content-encoding: gzip
referrer-policy: strict-origin-when-cross-origin
server: jag
strict-transport-security: max-age=31536000; includeSubDomains
cache-control: no-cache, no-store, max-age=0, no-transform, private
x-xss-protection: 1; mode=block
x-content-type-options: nosniff
----


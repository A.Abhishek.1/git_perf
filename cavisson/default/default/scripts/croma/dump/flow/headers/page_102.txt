--Request 
POST https://bom-col.eum-appdynamics.com/eumcollector/beacons/browser/v1/BO-AAB-FBK/adrum
Host: bom-col.eum-appdynamics.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-platform: \"Linux\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
Content-type: text/plain
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://www.croma.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
 POSTDATA {"vr":"23.10.1.4359","dt":"R","rg":"0","es":[{"eg":"1","et":2,"eu":"0://1/2/3/4/5/6","ts":1715839279524,"mg":"0","au":"0://7/8/9/6","at":0,"pp":3,"mx":{"PLC":1,"FBT":123,"DDT":0,"DPT":1,"PLT":124,"ARE":0},"md":"GET","xs":200,"si":46},{"eg":"2","et":2,"eu":"0://1/2/3/4/10/6","ts":1715839279523,"mg":"0","au":"0://7/8/9/6","at":0,"pp":3,"mx":{"PLC":1,"FBT":147,"DDT":1,"DPT":0,"PLT":148,"ARE":0},"md":"GET","xs":200,"si":47},{"eg":"3","et":2,"eu":"0://1/11/3/4/12?13","ts":1715839279525,"mg":"0","au":"0://7/8/9/6","at":0,"pp":3,"mx":{"PLC":1,"FBT":353,"DDT":9,"DPT":1,"PLT":363,"ARE":0},"md":"GET","xs":200,"si":48}],"ai":"477d42aa_9f21_9144_1a76_688c9aa25e4b","gs":["06a6dd02_3c5f_2943_3169_f771971f39e1","97d2819d_b9a4_f548_267c_76359141c812","f3a1bce9_28ca_43f6_596b_6856325238bb","36b1910e_ec0d_f2dc_803e_1c21e37e6338"],"up":["https","api.croma.com","productdetails","allchannels","v1","bifurcationCount","304697","www.croma.com","oneplus-12r-5g-8gb-128gb-cool-blue-","p","reviewcount","cmstemplate","page","pageType=ContentPage&pageLabelOrId=304697&fields=FULL"]}
--Response 
HTTP/1.1 200
date: Thu, 16 May 2024 06:01:24 GMT
content-type: text/html
set-cookie: ADRUM_BTa=R:22|g:87a5a232-8dff-4e91-95bb-3797204c7e4b; Path=/; Expires=Thu, 16-May-2024 06:01:54 GMT; Max-Age=30
set-cookie: ADRUM_BTa=R:22|g:87a5a232-8dff-4e91-95bb-3797204c7e4b|n:appdynamics_46b45e6c-9ffd-4db5-9fe0-e4760639f012; Path=/; Expires=Thu, 16-May-2024 06:01:54 GMT; Max-Age=30
set-cookie: SameSite=None; Path=/; Expires=Thu, 16-May-2024 06:01:54 GMT; Max-Age=30; Secure
set-cookie: ADRUM_BT1=R:22|i:57892; Path=/; Expires=Thu, 16-May-2024 06:01:54 GMT; Max-Age=30
set-cookie: ADRUM_BT1=R:22|i:57892|e:2; Path=/; Expires=Thu, 16-May-2024 06:01:54 GMT; Max-Age=30
set-cookie: ADRUM_BT1=R:22|i:57892|e:2|t:1715839284975; Path=/; Expires=Thu, 16-May-2024 06:01:54 GMT; Max-Age=30
expires: 0
cache-control: private, no-cache, no-store, must-revalidate, max-age=0, proxy-revalidate, s-maxage=0
pragma: no-cache
vary: *
x-content-type-options: nosniff
strict-transport-security: max-age=31536010; includeSubDomains
access-control-allow-origin: *
access-control-allow-headers: origin, content-type, accept
x-envoy-upstream-service-time: 0
server: envoy
----


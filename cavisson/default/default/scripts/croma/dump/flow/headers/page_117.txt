--Request 
POST https://api.tatadigital.com/analytics-engine/events/v1
Host: api.tatadigital.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-platform: \"Linux\"
customer-hash: null
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
Content-Type: application/json
tdl-sso-version: 4.1.15
session: false
client_id: CROMA-WEB-APP
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://www.croma.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
 POSTDATA {"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tL29uZXBsdXMtMTJyLTVnLThnYi0xMjhnYi1jb29sLWJsdWUtL3AvMzA0Njk3IiwiZXZlbnREYXRhIjpbeyJ0aW1lc3RhbXAiOjE3MTU4MzkzNzMyNTcsImRhdGEiOiJ7XCJldmVudFwiOlwiZ3RtLWV2ZW50XCIsXCJkYXRhXCI6XCJ7XFxcImV2ZW50XFxcIjpcXFwiYWRkX3RvX2NhcnRcXFwiLFxcXCJwYWdldHlwZVxcXCI6XFxcInBkcFxcXCIsXFxcInNvdXJjZV9wYWdlX3VybFxcXCI6XFxcIi9vbmVwbHVzLTEyci01Zy04Z2ItMTI4Z2ItY29vbC1ibHVlLS9wLzMwNDY5N1xcXCIsXFxcInByZXZpb3VzX3BhZ2VfdXJsXFxcIjpcXFwiL2NhbXBhaWduL29uZXBsdXMtci01Zy9jLzY0MDdcXFwiLFxcXCJsb2dpbl90cmlnZ2VyXFxcIjpcXFwiTi9BXFxcIixcXFwibG9naW5fc3RhdHVzXFxcIjpmYWxzZSxcXFwidXNlcl9pZFxcXCI6ZmFsc2UsXFxcImNsaWNrX3RleHRcXFwiOlxcXCJBZGQgdG8gQ2FydFxcXCIsXFxcImlzX2Nyb3NzX3NlbGxcXFwiOlxcXCJOL0FcXFwiLFxcXCJlY29tbWVyY2VcXFwiOntcXFwiaXRlbXNcXFwiOlt7XFxcIml0ZW1faWRcXFwiOlxcXCIzMDQ2OTdcXFwiLFxcXCJpdGVtX25hbWVcXFwiOlxcXCJPbmVQbHVzIDEyUiA1RyAoOEdCLCAxMjhHQiwgQ29vbCBCbHVlKVxcXCIsXFxcImFmZmlsaWF0aW9uXFxcIjpcXFwiY3JvbWEuY29tXFxcIixcXFwiY291cG9uXFxcIjpcXFwiTi9BXFxcIixcXFwiY3VycmVuY3lcXFwiOlxcXCJOL0FcXFwiLFxcXCJkaXNjb3VudFxcXCI6XFxcIjAlXFxcIixcXFwiaW5kZXhcXFwiOlxcXCJOL0FcXFwiLFxcXCJpdGVtX2JyYW5kXFxcIjpcXFwiT25lUGx1c1xcXCIsXFxcIml0ZW1fY2F0ZWdvcnlcXFwiOlxcXCJQaG9uZXMgJiBXZWFyYWJsZXNcXFwiLFxcXCJpdGVtX2NhdGVnb3J5MlxcXCI6XFxcIk1vYmlsZSBQaG9uZXNcXFwiLFxcXCJpdGVtX2NhdGVnb3J5M1xcXCI6XFxcIkFuZHJvaWQgUGhvbmVzXFxcIixcXFwiaXRlbV9jYXRlZ29yeTRcXFwiOlxcXCJOL0FcXFwiLFxcXCJpdGVtX2NhdGVnb3J5NVxcXCI6XFxcIk4vQVxcXCIsXFxcIml0ZW1fbGlzdF9pZFxcXCI6XFxcIk4vQVxcXCIsXFxcIml0ZW1fbGlzdF9uYW1lXFxcIjpcXFwiTi9BXFxcIixcXFwiaXRlbV92YXJpYW50XFxcIjpcXFwiTi9BXFxcIixcXFwibG9jYXRpb25faWRcXFwiOlxcXCJOL0FcXFwiLFxcXCJwcmljZVxcXCI6XFxcIjM5OTk5XFxcIixcXFwicXVhbnRpdHlcXFwiOlxcXCIxXFxcIixcXFwicHJpY2VfbXJwXFxcIjpcXFwiMzk5OTlcXFwifV19LFxcXCJndG0udW5pcXVlRXZlbnRJZFxcXCI6NDR9XCJ9In1dLCJjdXN0b21lckhhc2giOm51bGwsImVjaWQiOiJVbmtub3duIiwiY2xpZW50SWQiOiJDUk9NQS1XRUItQVBQIiwic2Vzc2lvbklkIjoiYjY1ZjU0YjQtNWM5My00ODJlLThjZjgtYzIxMGVjZmZhNzhmLjkyM2JiNTdmLTk1MWItNDc4MC04Y2MzLTNlN2M4OWU5MDVmZSIsImJyYW5kIjoiQ1JPTUEiLCJ0aW1lc3RhbXAiOjE3MTU4MzkzNzMyNTcsImRldmljZSI6Ik1vemlsbGEvNS4wIChYMTE7IExpbnV4IHg4Nl82NCkgQXBwbGVXZWJLaXQvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lLzExNC4wLjAuMCBTYWZhcmkvNTM3LjM2IiwiamFydmlzSWQiOiI0ZDYwNDU3MC1iYWZiLTQ1NTEtYmFkMC04NTM4OTcxYzdlZTIiLCJhZHZlcnRpc2luZ0lkIjpudWxsLCJhcHBWZXJzaW9uIjoiMCJ9"}
--Response 
HTTP/1.1 200
content-type: application/json; charset=utf-8
content-length: 19
etag: W/\"13-AU5FafGnjGQRcAzuw5htDSM9eaQ\"
x-frame-options: SAMEORIGIN
x-xss-protection: 1; mode=block
date: Thu, 16 May 2024 06:02:53 GMT
server-timing: cdn-cache; desc=MISS
server-timing: edge; dur=38
server-timing: origin; dur=13
server-timing: ak_p; desc=\"1715839373280_388943750_1560446032_5053_11600_11_0_219\";dur=1
set-cookie: _abck=3C70162D882417DD83B6CC39F91EBD8B~-1~YAAQhs8uFy6uu26PAQAAHxD/fwuoHS8l2kmccvBJuFKXYrP7oJY0P48vHVYAz1dOIpFRjWd73uiDX43c3zsqMfEz/WhfkyAXJ1ogZCrk/XGkydPZPwc6YipTzTvmiM4fwA6UrJD/sCYvLUkKIfCL8S21ryTz54IYn8DLknOD0ESG4Sn3Dk6y65la6uQiMWTXtq9AKZMmJoktXYPSWpdoBE5hj9ytzPha7wMzX2/DghGfuWu5qkLN35mkgrJ/sZ+Gv0bbxjTgBFP69d6LKHqauKt6IuSqxawFw88tWK9P0R/L/ugWt+WeNepGZPUoZT3fMYcawO7qvTBVCNpGY9LtxcLF5MZOETTaKI99WJzUIEcBAbVus1iPMvj1UKl9hIzq~-1~-1~-1; Domain=.tatadigital.com; Path=/; Expires=Fri, 16 May 2025 06:02:53 GMT; Max-Age=31536000; Secure
set-cookie: ak_bmsc=A1D320298FDC6445CF1076BAE4916182~000000000000000000000000000000~YAAQhs8uFy+uu26PAQAAHxD/fxcNgC7eaMH1K57n//DkIzNeh7yqWAeVg3+8WSjy39AKcqa1qzvRHRaPRlw1p3KgvIfJTrhsvwG5GwEWZivlF39+lXVm0QJMPlfpuhk1NSJe0Ey46qs7HhpHtK+3z7A3KR5Bfg8do+YtfdYsuyNBepUivxMW4119qlIvf6DB1GUyZU7by57BFKyApP3ayzMVkP5FNlz9E85SjCMRK5ItG+N/p/eYi+P/UsGh/beXXwieVZ2GrA66rpZXBqhRRk8SD1aWYxl7vF+vvMz2KLMbjHC5zQ2Ic5taATtjEBI5Ghb3Jit7Krv/Yu/xtSFCEBVLmc3Nv+tTdzCB48c6xXPk1bTn3N1u+AGqRfyPRlPW8fZJjBQr9IIsOZUt7YrI+w==; Domain=.tatadigital.com; Path=/; Expires=Thu, 16 May 2024 08:02:53 GMT; Max-Age=7200
set-cookie: bm_sz=B43DF663EE5A1F2FADBD293DDA865A63~YAAQhs8uFzCuu26PAQAAHxD/fxdQsZf2ejvzIPuJJUC/oc2nILi/j2hc1sw169nGFdCd5FPkm5hdBqHqZVOOKBJxepxRHpxkOYmFULA6B3MZELa9hdZ952M86OrUMlPtWt191t4v8c6OffodcxYwJkKS3T4LnzGk+409+PzdB/GU5u+7KRwLw+ZPHFMmymOKGOyRgZH6VJae/Bfa5T/3C3XXFsn0IhtobIuV7etd+lxV5jk9I20jqfyMuLiFD7qKNnNitFywub4C/tGApbI13arwBHVW0/w88t0pYM9fT5SVKoF74e6D/FK06gybrV1Yy5fKo4fVaslXsXEBxbOdJBDWHuH+NaaAKCFMQpRqyvPCaTzTtCDOQDGuKos=~3290679~4404018; Domain=.tatadigital.com; Path=/; Expires=Thu, 16 May 2024 10:02:53 GMT; Max-Age=14400
----
--Request 
GET https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1706791128/Croma%20Assets/Communication/Mobiles/Images/304697_zp8rwp.png?tr=w-400
Host: media-ik.croma.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: same-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.croma.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
Cookie: AKA_A2=A; at_check=true; AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg=1; s_ecid=MCMID%7C23208888194201725061814923007976880958; s_plt=NaN; s_pltp=undefined; s_cc=true; _gcl_au=1.1.1421799651.1715839140; AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg=179643557%7CMCIDTS%7C19860%7CMCMID%7C23208888194201725061814923007976880958%7CMCAAMLH-1716443939%7C12%7CMCAAMB-1716443939%7C6G1ynYcLPuiQxYZrsz_pkqfLG9yMXBpb2zX5dvJdYQJzPXImdj0y%7CMCOPTOUT-1715846339s%7CNONE%7CMCAID%7CNONE%7CMCSYNCSOP%7C411-19867%7CvVersion%7C5.5.0; _hjSession_3400595=eyJpZCI6ImFiMTJiMDJjLTFjMWYtNDJkOC1iOGYxLTAwY2Q2ZDYyMWM1ZSIsImMiOjE3MTU4MzkxNDA2MDksInMiOjAsInIiOjAsInNiIjowLCJzciI6MCwic2UiOjAsImZzIjoxLCJzcCI6MH0=; _fbp=fb.1.1715839140634.281453602; _ga=GA1.1.1944455297.1715839141; WZRK_G=6f08a8df4ce04ff4b786855393b6d325; mbox=session#895fcaa2d50341f0b4231873c20498c7#1715841064|PC#895fcaa2d50341f0b4231873c20498c7.41_0#1779084004; s_ips=523; th_external_id=dd225ea0e0a549dff2458f2b188bd0b54a923171c4b30d006a0ce3ff08d8fa8c; _uetsid=6467a920134911efa7d615e2de33fb03; _uetvid=6467fc40134911efa1829f1407dcc88b; _hjSessionUser_3400595=eyJpZCI6Ijk2YjE5NmY3LWRkMzMtNTkxYy1iY2IyLTIzMDNlMDRkNzEyNSIsImNyZWF0ZWQiOjE3MTU4MzkxNDA2MDUsImV4aXN0aW5nIjp0cnVlfQ==; cto_bundle=WwpUr19ZQjN2TTdzaTNPd0Q0dlByN3VaMFZMOEIwMTVJWjE0ZXI3bEFGMGJFZUxUZ2pPQ2F0Vk5FdlY1cXpBTXJ6bWF1WlppM040TnV2MU1IMnJNbjFlMTQ1UllVN2hSbHpzJTJGT1J0eFNnZ1FnekxaakRmcGh3THpVMnd2VnJVMzMlMkJFU2U; s_tp=6437; s_ppv=pdp%2C44%2C8%2C2860%2C5%2C12; WZRK_S_679-6RK-976Z=%7B%22p%22%3A2%2C%22s%22%3A1715839141%2C%22t%22%3A1715839327%7D; s_nr30=1715839373133-New; s_sq=%5B%5BB%5D%5D; _ga_DJ3RGVFHJN=GS1.1.1715839140.1.1.1715839373.12.0.0
----
--Response 
HTTP/1.1 200
content-type: image/webp
content-length: 12028
age: 6907347
access-control-allow-origin: *
access-control-allow-methods: GET
access-control-allow-headers: *
timing-allow-origin: *
x-server: ImageKit.io
x-request-id: 01384482-4e38-4401-9a91-06946edb428e
cache-control: public, s-maxage=31536000, max-age=31536000, must-revalidate
etag: \"0b451f658908cbfeb88e39fd0cb64e26\"
last-modified: Wed, 14 Feb 2024 12:14:29 GMT
date: Mon, 26 Feb 2024 07:16:58 GMT
vary: Accept
x-cache: Hit from cloudfront
via: 1.1 d02e6230e17bdc1d6594ead12d216e02.cloudfront.net (CloudFront)
x-amz-cf-pop: DEL54-P5
alt-svc: h3=\":443\"; ma=86400
x-amz-cf-id: 6Qux8D-1JJQgucregjnut75UpPKCn9jvZ8h_l6eSw3J8LniM1fcF5g==
----
--Request 
GET https://www.facebook.com/tr/?cd[content_ids]=%5B%22304697%22%5D&cd[content_name]=OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)&cd[content_type]=product&cd[currency]=INR&cd[value]=39999.00&cdl=API_unavailable&coo=false&dl=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ec=4&eid=11146789180925&ev=AddToCart&fbp=fb.1.1715839140634.281453602&id=1369867960112522&if=false&it=1715839205604&ler=empty&o=4126&r=stable&redirect=0&rl=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&rqm=GET&sh=650&sw=1523&tm=1&ts=1715839373184&ud[external_id]=36ce52f2c012a86c9264c805b8cff82e07250268c3b1663e92a631306623bfc6&v=2.9.156
Host: www.facebook.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.croma.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: text/plain
access-control-allow-credentials: true
strict-transport-security: max-age=31536000; includeSubDomains
cross-origin-resource-policy: cross-origin
content-length: 0
server: proxygen-bolt
x-fb-connection-quality: EXCELLENT; q=0.9, rtt=12, rtx=0, c=22, mss=1232, tbw=4841, tp=17, tpl=0, uplat=0, ullat=0
alt-svc: h3=\":443\"; ma=86400
priority: u=3,i
date: Thu, 16 May 2024 06:02:53 GMT
----


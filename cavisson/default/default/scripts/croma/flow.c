/*-----------------------------------------------------------------------------
    Name: flow
    Created By: cavisson
    Date of creation: 05/16/2024 01:03:12
    Flow details:
    Build details: 4.13.0 (build# 111)
    Modification History:
-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include "ns_string.h"

void flow()
{
    ns_start_transaction("index");
    ns_web_url("index",
        "URL=https://www.croma.com/",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://assets.croma.com/assets/fonts/croma.woff2", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=Origin:https://www.croma.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2", END_INLINE,
            "URL=https://www.croma.com/assets/fonts/Switzer-Bold.woff2", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=Origin:https://www.croma.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2", END_INLINE,
            "URL=https://www.croma.com/assets/fonts/Switzer-Regular.woff2", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=Origin:https://www.croma.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2", END_INLINE,
            "URL=https://www.croma.com/assets/fonts/Switzer-Medium.woff2", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=Origin:https://www.croma.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2", END_INLINE,
            "URL=https://assets.adobedtm.com/a83cfb422665/6969f0a69b1e/launch-883ee2cb26fd.min.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.croma.com/static/css/19.7113b688.chunk.css", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2", END_INLINE,
            "URL=https://www.croma.com/static/css/main.50a49cb7.chunk.css", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2", END_INLINE,
            "URL=https://www.croma.com/static/js/19.b5f9868a.chunk.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2", END_INLINE,
            "URL=https://www.croma.com/static/js/main.705c574a.chunk.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2", END_INLINE,
            "URL=https://accounts.tatadigital.com/v2/tdl-sso-auth.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("index", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("id");
    ns_web_url("id",
        "URL=https://dpm.demdex.net/id?d_visid_ver=5.5.0&d_fieldgroup=MC&d_rtbd=json&d_ver=2&d_orgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&d_nsid=0&ts=1715839138605",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://s.go-mpulse.net/boomerang/Y64HZ-S7F3X-X4LT4-6FRFB-MK4UE", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://maps.googleapis.com/maps/api/js?key=AIzaSyBALhlQSaWB7dWn--IJ95ozhB3_3Zzlcdc&libraries=places&callback=initMap", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1637759004/Croma%20Assets/CMS/Category%20icon/Final%20icon/Croma_Logo_acrkvn.svg", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;mbox;at_check", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1713554114/Croma%20Assets/CMS/LP%20Page%20Banners/2024/Summer%20LP%20-New/HP_SummerCampaign_19April24_daunkc.png", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;mbox;at_check", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1715773569/Croma%20Assets/CMS/Homepage%20Banners/HP%20Rotating/2024/May/16052024/Desktop/HP_Rotating_AC_16May24_wrtteo.jpg", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;mbox;at_check", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1715773570/Croma%20Assets/CMS/Homepage%20Banners/HP%20Rotating/2024/May/16052024/Desktop/HP_Rotating_iPhone13_16May24_gl2cyl.jpg", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;mbox;at_check", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1715773570/Croma%20Assets/CMS/Homepage%20Banners/HP%20Rotating/2024/May/16052024/Desktop/HP_Rotating_M_G_16May24_pz63px.jpg", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;mbox;at_check", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1715773570/Croma%20Assets/CMS/Homepage%20Banners/HP%20Rotating/2024/May/16052024/Desktop/HP_Rotating_Laptops_16May24_dbg2br.jpg", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;mbox;at_check", END_INLINE,
            "URL=https://cdn.appdynamics.com/adrum/adrum-23.10.1.4359.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.croma.com/assets/js/cromaUtility.min.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;mbox;at_check", END_INLINE,
            "URL=https://www.croma.com/assets/js/cromaSso.min.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;mbox;at_check", END_INLINE,
            "URL=https://assets.adobedtm.com/extensions/EPef068a8d6dd34a43866d9a80cc98baab/AppMeasurement.min.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/extensions/EPef068a8d6dd34a43866d9a80cc98baab/AppMeasurement_Module_ActivityMap.min.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://edge.fullstory.com/s/fs.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=Origin:https://www.croma.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cromaretail.demdex.net/dest5.html?d_nsid=0#https%3A%2F%2Fwww.croma.com", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://smetrics.croma.com/id?d_visid_ver=5.5.0&d_fieldgroup=A&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&mid=23208888194201725061814923007976880958&ts=1715839139112", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Content-Type:application/x-www-form-urlencoded", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;mbox;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1715773570/Croma%20Assets/CMS/Homepage%20Banners/HP%20Rotating/2024/May/16052024/Desktop/HP_Rotating_WM_16May24_quhcdh.jpg", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;mbox;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1715773570/Croma%20Assets/CMS/Homepage%20Banners/HP%20Rotating/2024/May/16052024/Desktop/HP_Rotating_SW_16May24_kxcuht.jpg", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;mbox;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1715257658/Croma%20Assets/CMS/Homepage%20Banners/HP%20Rotating/2024/May/10052024/Desktop/HP_OLCampaign_8May2024_smzduv.jpg", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;mbox;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg", END_INLINE,
            "URL=https://cm.everesttech.net/cm/dd?d_uuid=30479175953135214261393567414247057654", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=411&dpuuid=ZkWgowAAAK-iyQMg", END_INLINE
    );

    ns_end_transaction("id", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("delivery");
    ns_web_url("delivery",
        "URL=https://cromaretail.tt.omtrdc.net/rest/v1/delivery?client=cromaretail&sessionId=895fcaa2d50341f0b4231873c20498c7&version=2.9.0",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"requestId":"b4692c69a5d5494ab7476e1985eed6d6","context":{"userAgent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36","clientHints":{"mobile":false,"platform":"Linux","browserUAWithMajorVersion":"\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\""},"timeOffsetInMinutes":-300,"channel":"web","screen":{"width":1523,"height":650,"orientation":"landscape","colorDepth":24,"pixelRatio":1},"window":{"width":790,"height":473},"browser":{"host":"www.croma.com","webGLRenderer":"ANGLE (Google, Vulkan 1.3.0 (SwiftShader Device (Subzero) (0x0000C0DE)), SwiftShader driver)"},"address":{"url":"https://www.croma.com/","referringUrl":""}},"id":{"marketingCloudVisitorId":"23208888194201725061814923007976880958"},"experienceCloud":{"audienceManager":{"locationHint":12,"blob":"6G1ynYcLPuiQxYZrsz_pkqfLG9yMXBpb2zX5dvJdYQJzPXImdj0y"},"analytics":{"logging":"server_side","supplementalDataId":"209844575BC87636-67440160CD9108F0","trackingServer":"metrics.croma.com","trackingServerSecure":"smetrics.croma.com"}},"execute":{"pageLoad":{}},"prefetch":{"views":[{}]}}",
        BODY_END
    );

    ns_end_transaction("delivery", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("v2");
    ns_web_url("v2",
        "URL=https://api.tatadigital.com/analytics-engine/config/v2",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Site-Origin:https://www.croma.com",
        "HEADER=Content-Type:application/json",
        "HEADER=Client-Id:CROMA-WEB-APP",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.googletagmanager.com/gtm.js?id=GTM-5X2VQJX", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("v2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("events");
    ns_web_url("events",
        "URL=https://api.tatadigital.com/api/v1.1/msd/events",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Program-Id:01eae2ec-0576-1000-bbea-86e16dcb4b79",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"page_type":"home","content_type":"product","action_name":"view","event_name":"page_view","correlation_id":"sssaasss","medium":"pixel","metadata":{},"source":"CROMA","user_id":["08vHUbbT7lzWpSwZ"],"user_id_type":["CROMA"],"mad_uuid":"08vHUbbT7lzWpSwZ","utm":{"utm_source":"","utm_medium":"","utm_campaign":""},"epoch":"1715839139633","pincode":""}",
        BODY_END,
        INLINE_URLS,
            "URL=https://dpm.demdex.net/ibs:dpid=411&dpuuid=ZkWgowAAAK-iyQMg", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.croma.com/static/js/58.85035225.chunk.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;mbox;jarvis-id;s_ecid;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg", END_INLINE
    );

    ns_end_transaction("events", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("gen_204");
    ns_web_url("gen_204",
        "URL=https://maps.googleapis.com/maps/api/mapsjs/gen_204?csp_test=true",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://maps.googleapis.com/maps-api-v3/api/js/56/12a/common.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://maps.googleapis.com/maps-api-v3/api/js/56/12a/util.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://maps.googleapis.com/maps-api-v3/api/js/56/12a/geocoder.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s38168602798756?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%200%3A58%3A59%204%20300&sdid=209844575BC87636-67440160CD9108F0&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=homepage&g=https%3A%2F%2Fwww.croma.com%2F&c.&getNewRepeat=3.0.1&getPageLoadTime=2.0.2&performanceWriteFull=n%2Fa&performanceWritePart=n%2Fa&performanceCheck=n%2Fa&p_fo=3.0&inList=3.0&apl=4.0&getPreviousValue=3.0.1&.c&cc=INR&ch=croma%3Ahomepage&server=www.croma.com&events=event1%3D1.9&aamb=6G1ynYcLPuiQxYZrsz_pkqfLG9yMXBpb2zX5dvJdYQJzPXImdj0y&c1=homepage&v1=D%3Dmid&c2=www.croma.com%2F&v2=new&c3=2024-5-16%200%3A58%3A59&v4=guest%20user&v39=RL425eb72221864237b3ecc22b172469cc&v51=1.9&v76=%2F&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5Bpageload%5D%20%3A%20Send%20Beacon&v198=www.croma.com%2F&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30", END_INLINE,
            "URL=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=411&dpuuid=ZkWgowAAAK-iyQMg", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("gen_204", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("web");
    ns_web_url("web",
        "URL=https://edge.fullstory.com/s/settings/11EGJ5/v1/web",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("web", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("check_session");
    ns_web_url("check_session",
        "URL=https://api.tatadigital.com/api/v2/sso/check-session",
        "METHOD=POST",
        "HEADER=Access-Control-Allow-Origin:https://api.tatadigital.com",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"codeChallenge":"B264LXPMwqBj9XnOVELcbdfC8tkpbJVaEolkeoq2-aA","redirectUrl":"https://www.croma.com/"}",
        BODY_END,
        INLINE_URLS,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s37107826663653?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%200%3A58%3A59%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=homepage&g=https%3A%2F%2Fwww.croma.com%2F&cc=INR&server=www.croma.com&events=event55%2Cevent50%3DNaN&c1=homepage&v1=D%3Dmid&c2=www.croma.com%2F&v2=new&c3=2024-5-16%200%3A58%3A59&l3=hero%20banner%3Asummer%20campaign%20n%3Ahomepage&v4=guest%20user&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5BImpression%5D%20%3A%20impression&v156=hero%20banner%3Asummer%20campaign%20n%3Ahomepage&pe=lnk_o&pev2=impression%3A%20tracking&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30", END_INLINE,
            "URL=https://www.googletagmanager.com/gtag/js?id=G-DJ3RGVFHJN&l=dataLayer&cx=c", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googletagmanager.com/gtag/destination?id=AW-609902077&l=dataLayer&cx=c", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://bat.bing.com/bat.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dynamic.criteo.com/js/ld/ld.js?a=56256", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static.hotjar.com/c/hotjar-3400595.js?sv=7", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googletagmanager.com/gtag/destination?id=AW-1006316414&l=dataLayer&cx=c", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googletagmanager.com/gtag/destination?id=DC-917807264&l=dataLayer&cx=c", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googletagmanager.com/gtag/destination?id=DC-13135721&l=dataLayer&cx=c", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://connect.facebook.net/en_US%20/fbevents.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://capi.croma.com/static/DhPixel.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au", END_INLINE,
            "URL=https://d2r1yp2w7bby2u.cloudfront.net/js/clevertap.min.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("check_session", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("page");
    ns_web_url("page",
        "URL=https://api.croma.com/cmstemplate/allchannels/v1/page?pageType=ContentPage&pageLabelOrId=pwaHomePage1&fields=FULL",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=httpsAgent:[object Object]",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au",
        INLINE_URLS,
            "URL=https://script.hotjar.com/modules.e5979922753cf3b8b069.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://in1.clevertap-prod.com/a?t=96&type=push&d=N4IgLgngDgpiBcIoCcD2AzAlgGzgGiTS1wVAGMwB9VKMVAVzAXQENsBnGAXwMwBMEIAGwB2AJwBaIQCUA0hLEihALRAEoAcwQBGAi3SkQ2TACNBAdxgmJ7PgGsJAN20A6ES4DMawqjplU2IIAFmBgUOwgXFxAA%3D%3D&optOut=false&rn=1&i=1715839140&sn=0&tries=1&useIP=true&r=1715839140448", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://connect.facebook.net/signals/config/1369867960112522?v=2.9.156&r=stable&domain=www.croma.com&hme=c3a545c63044e8e9102d4f32d84a1137594d024f28e801d670bc76dc5c075575&ex_m=67%2C112%2C99%2C103%2C58%2C3%2C93%2C66%2C15%2C91%2C84%2C49%2C51%2C158%2C161%2C172%2C168%2C169%2C171%2C28%2C94%2C50%2C73%2C170%2C153%2C156%2C165%2C166%2C173%2C121%2C14%2C48%2C178%2C177%2C123%2C17%2C33%2C38%2C1%2C41%2C62%2C63%2C64%2C68%2C88%2C16%2C13%2C90%2C87%2C86%2C100%2C102%2C37%2C101%2C29%2C25%2C154%2C157%2C130%2C27%2C10%2C11%2C12%2C5%2C6%2C24%2C21%2C22%2C54%2C59%2C61%2C71%2C95%2C26%2C72%2C8%2C7%2C76%2C46%2C20%2C97%2C96%2C9%2C19%2C18%2C81%2C53%2C79%2C32%2C70%2C0%2C89%2C31%2C78%2C83%2C45%2C44%2C82%2C36%2C4%2C85%2C77%2C42%2C39%2C34%2C80%2C2%2C35%2C60%2C40%2C98%2C43%2C75%2C65%2C104%2C57%2C56%2C30%2C92%2C55%2C52%2C47%2C74%2C69%2C23%2C105", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.facebook.com/tr/?id=1369867960112522&ev=PageView&dl=https%3A%2F%2Fwww.croma.com%2F&rl=&if=false&ts=1715839140635&sw=1523&sh=650&v=2.9.156&r=stable&ec=0&o=4126&fbp=fb.1.1715839140634.281453602&ler=empty&cdl=API_unavailable&it=1715839140500&coo=false&eid=85333188842523&tm=1&rqm=GET", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.facebook.com/tr/?id=1369867960112522&ev=PageView&dl=https%3A%2F%2Fwww.croma.com%2F&rl=&if=false&ts=1715839140641&sw=1523&sh=650&v=2.9.156&r=stable&ec=1&o=4126&fbp=fb.1.1715839140634.281453602&ler=empty&cdl=API_unavailable&it=1715839140500&coo=false&eid=56995789061150&tm=1&rqm=GET", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googleadservices.com/pagead/conversion/609902077/?random=1715839140699&cv=11&fst=1715839140699&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2F&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=Croma%20Electronics%20%7C%20Online%20Electronics%20Shopping%20%7C%20Buy%20Electronics%20Online&value=0&bttype=purchase&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&rfmt=3&fmt=4", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googleadservices.com/pagead/conversion/609902077/?random=1715839140714&cv=11&fst=1715839140714&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2F&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=Croma%20Electronics%20%7C%20Online%20Electronics%20Shopping%20%7C%20Buy%20Electronics%20Online&value=0&bttype=purchase&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&rfmt=3&fmt=4", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googleadservices.com/pagead/conversion/1006316414/?random=1715839140760&cv=11&fst=1715839140760&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2F&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Croma%20Electronics%20%7C%20Online%20Electronics%20Shopping%20%7C%20Buy%20Electronics%20Online&value=0&bttype=purchase&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&rfmt=3&fmt=4", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googleadservices.com/pagead/conversion/1006316414/?random=1715839140778&cv=11&fst=1715839140778&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2F&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Croma%20Electronics%20%7C%20Online%20Electronics%20Shopping%20%7C%20Buy%20Electronics%20Online&value=0&bttype=purchase&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&rfmt=3&fmt=4", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://917807264.fls.doubleclick.net/activityi;src=917807264;type=invmedia;cat=croma0;ord=7521621243952;npa=0;auiddc=1421799651.1715839140;uaa=x86;uab=64;uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199;uamb=0;uam=;uap=Linux;uapv=5.15.0;uaw=0;pscdl=noapi;frm=0;gtm=45fe45f0z8889407033za201;gcd=13l3l3l3l1;dma=0;epver=2;~oref=https%3A%2F%2Fwww.croma.com%2F?", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("page", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect");
    ns_web_url("collect",
        "URL=https://analytics.google.com/g/collect?v=2&tid=G-DJ3RGVFHJN&gtm=45je45f0v893318240z8889407033za200&_p=1715839139273&_gaz=1&gcd=13l3l3l3l1&npa=0&dma=0&cid=1944455297.1715839141&ul=en-us&sr=1523x650&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&frm=0&pscdl=noapi&_s=1&sid=1715839140&sct=1&seg=0&dl=https%3A%2F%2Fwww.croma.com%2F&dt=Croma%20Electronics%20%7C%20Online%20Electronics%20Shopping%20%7C%20Buy%20Electronics%20Online&en=page_view&_fv=1&_nsi=1&_ss=1&tfd=3338",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.google.co.in/ads/ga-audiences?v=1&t=sr&slf_rd=1&_r=4&tid=G-DJ3RGVFHJN&cid=1944455297.1715839141&gtm=45je45f0v893318240z8889407033za200&aip=1&dma=0&gcd=13l3l3l3l1&npa=0&frm=0&z=1746051070", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://bat.bing.com/p/action/25149556.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://bat.bing.com/action/0?ti=25149556&tm=gtm002&Ver=2&mid=4eaa6314-3ab7-442a-848e-01064f9baf94&sid=6467a920134911efa7d615e2de33fb03&vid=6467fc40134911efa1829f1407dcc88b&vids=1&msclkid=N&pi=918639831&lg=en-US&sw=1523&sh=650&sc=24&tl=Croma%20Electronics%20%7C%20Online%20Electronics%20Shopping%20%7C%20Buy%20Electronics%20Online&p=https%3A%2F%2Fwww.croma.com%2F&r=&lt=2331&evt=pageLoad&sv=1&rn=212839", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://in1.clevertap-prod.com/a?t=96&type=page&d=N4IglgJiBcIGwHYCcBaOAlA0ipC4C0QAaEABwHMYBGEgQwDMZQAbMAIxhAHcBTNlAM4QA1igBuVAHQJJAZmJkATgHsALsoDGy5pwAWq1aQEgAviQ0U9Bo9AD0tro8kaVAW1rPlr2woDq%2BLAB9AGEAQQBZAAUmbldSGAAGEi4wOMSSVWYNGABtAF1ksEzs6HyTEyAAA%3D%3D&rn=2&i=1715839140&sn=0&gc=6f08a8df4ce04ff4b786855393b6d325&tries=1&useIP=true&r=1715839141005", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://in1.clevertap-prod.com/a?t=96&type=push&d=N4IgLgngDgpiBcIYDcYDswgDROWAcgIYC2ciA7gF4BOA1gPoBmMYAxgBba5gAihYhBKEzwALAF8cASwAmCEADYA7AE4AtAoBKAaTUqlCgFpcoAcwQBGHIUZCQAGykAjeeRhO1AZxm01yCwB0SgEAzCbUAPZgEawR9vLsYGBQniCSIADqhjr0AMIAggCyAAp25MRQCAAMOORSFdU4YPasCADaALq1Us2t8J3i4kAA&rn=3&i=1715839140&sn=0&gc=6f08a8df4ce04ff4b786855393b6d325&tries=1&useIP=true&r=1715839141006", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("collect", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("config_json");
    ns_web_url("config_json",
        "URL=https://c.go-mpulse.net/api/config.json?key=Y64HZ-S7F3X-X4LT4-6FRFB-MK4UE&d=www.croma.com&t=5719464&v=1.720.0&sl=0&si=a944a24b-fdf9-4959-b8b0-94a3120cdc98-sdkdya&plugins=AK,ConfigOverride,Continuity,PageParams,IFrameDelay,AutoXHR,SPA,History,Angular,Backbone,Ember,RT,CrossDomain,BW,PaintTiming,NavigationTiming,ResourceTiming,Memory,CACHE_RELOAD,Errors,TPAnalytics,UserTiming,Akamai,Early,EventTiming,LOGN&acao=&ak.ai=690600",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://13135721.fls.doubleclick.net/activityi;src=13135721;type=croma01;cat=croma0;ord=2122113974442;npa=0;auiddc=1421799651.1715839140;uaa=x86;uab=64;uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199;uamb=0;uam=;uap=Linux;uapv=5.15.0;uaw=0;pscdl=noapi;frm=0;gtm=45fe45f0z8889407033za201;gcd=13l3l3l3l1;dma=0;epver=2;~oref=https%3A%2F%2Fwww.croma.com%2F?", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1715259394/Croma%20Assets/CMS/LP%20Page%20Banners/2024/HP%20Widgets/May/Blog%20Banners/10052024/Desktop/HP_Blogs_1_9May2024_pu04cw.png", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1715259395/Croma%20Assets/CMS/LP%20Page%20Banners/2024/HP%20Widgets/May/Blog%20Banners/10052024/Desktop/HP_Blogs_2_9May2024_ftrjdz.png", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1715259397/Croma%20Assets/CMS/LP%20Page%20Banners/2024/HP%20Widgets/May/Blog%20Banners/10052024/Desktop/HP_Blogs_3_9May2024_gnm2bi.png", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1715259397/Croma%20Assets/CMS/LP%20Page%20Banners/2024/HP%20Widgets/May/Blog%20Banners/10052024/Desktop/HP_Blogs_4_9May2024_zkqtmr.png", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://917807264.fls.doubleclick.net/activityi;dc_pre=CP2q_PO-kYYDFTiKrAIdarMJ0g;src=917807264;type=invmedia;cat=croma0;ord=7521621243952;npa=0;auiddc=1421799651.1715839140;uaa=x86;uab=64;uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199;uamb=0;uam=;uap=Linux;uapv=5.15.0;uaw=0;pscdl=noapi;frm=0;gtm=45fe45f0z8889407033za201;gcd=13l3l3l3l1;dma=0;epver=2;~oref=https%3A%2F%2Fwww.croma.com%2F?", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/609902077/?random=1991333045&cv=11&fst=1715839140699&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2F&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=Croma%20Electronics%20%7C%20Online%20Electronics%20Shopping%20%7C%20Buy%20Electronics%20Online&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtTUEc0YQgVM2sFKo6YV1I0yNDJ_hDPNJyw&pscrd=IhMIp-vt876RhgMVaVSdCR2ZUAZOMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media-ik.croma.com/prod/undefined?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1709186812/Croma%20Assets/Test_Nikhil/Hybrid%20Homepage/catnav/2902/Desktop/Mobiles_ueht5o.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1709186810/Croma%20Assets/Test_Nikhil/Hybrid%20Homepage/catnav/2902/Desktop/Air_Conditioners_dmes8r.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1709186812/Croma%20Assets/Test_Nikhil/Hybrid%20Homepage/catnav/2902/Desktop/Televisions_hilbar.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1709186816/Croma%20Assets/Test_Nikhil/Hybrid%20Homepage/catnav/2902/Desktop/Refrigerators_jezg47.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1709186810/Croma%20Assets/Test_Nikhil/Hybrid%20Homepage/catnav/2902/Desktop/Laptops_g9ymac.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1709186809/Croma%20Assets/Test_Nikhil/Hybrid%20Homepage/catnav/2902/Desktop/Headphones_Earphones_w7ibqa.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1713365545/Croma%20Assets/CMS/LP%20Page%20Banners/2024/Testing/Hybrid%20Homepage/Cat%20Nav/Desktop/Cooler_dx0xg8.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1709186810/Croma%20Assets/Test_Nikhil/Hybrid%20Homepage/catnav/2902/Desktop/Home_Theatres_Soundbars_nqyuff.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1709186815/Croma%20Assets/Test_Nikhil/Hybrid%20Homepage/catnav/2902/Desktop/Washing_Machines_t1r7fe.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1709186813/Croma%20Assets/Test_Nikhil/Hybrid%20Homepage/catnav/2902/Desktop/Tablets_knivhx.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1713365551/Croma%20Assets/CMS/LP%20Page%20Banners/2024/Testing/Hybrid%20Homepage/Cat%20Nav/Desktop/Wearable_1_gstxxr.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1709186816/Croma%20Assets/Test_Nikhil/Hybrid%20Homepage/catnav/2902/Desktop/Speakers_Media_Players_jfp7dc.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1709186811/Croma%20Assets/Test_Nikhil/Hybrid%20Homepage/catnav/2902/Desktop/Kitchen_Appliances_mjwb3l.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1709186810/Croma%20Assets/Test_Nikhil/Hybrid%20Homepage/catnav/2902/Desktop/Grooming_x2isvq.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1713365542/Croma%20Assets/CMS/LP%20Page%20Banners/2024/Testing/Hybrid%20Homepage/Cat%20Nav/Desktop/Accessories_kafthb.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1713365546/Croma%20Assets/CMS/LP%20Page%20Banners/2024/Testing/Hybrid%20Homepage/Cat%20Nav/Desktop/Fans_oq5qhv.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1713365547/Croma%20Assets/CMS/LP%20Page%20Banners/2024/Testing/Hybrid%20Homepage/Cat%20Nav/Desktop/Microwave_pgj63e.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1713365543/Croma%20Assets/CMS/LP%20Page%20Banners/2024/Testing/Hybrid%20Homepage/Cat%20Nav/Desktop/Camera_e8rnh5.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1713455299/Croma%20Assets/CMS/LP%20Page%20Banners/2024/More%20For%20Your%20Money/April/BAU/Homepage%20New%20Layout/BAU/Thu%20-%20ICICI/LP_2Split_MFYMP_1_17April2024_cxs9d9.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1713455299/Croma%20Assets/CMS/LP%20Page%20Banners/2024/More%20For%20Your%20Money/April/BAU/Homepage%20New%20Layout/BAU/Thu%20-%20ICICI/LP_2Split_MFYMP_2_17April2024_amm6ce.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1714655578/Croma%20Assets/CMS/LP%20Page%20Banners/2024/More%20For%20Your%20Money/April/BAU/Homepage%20New%20Layout/BAU/2%20split/BrandxBank/Revised/02052024/HP_2Split_MFYMP_Brands_HDFC_ICICI_2May2024_mltbxp.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1713269877/Croma%20Assets/CMS/LP%20Page%20Banners/2024/Testing/Hybrid%20Homepage/16th%20April/Croma_Collections_aer8cq.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1713278421/Croma%20Assets/CMS/LP%20Page%20Banners/2024/Testing/Hybrid%20Homepage/16th%20April/04_6_go4wv6.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1713278435/Croma%20Assets/CMS/LP%20Page%20Banners/2024/Testing/Hybrid%20Homepage/16th%20April/03_30_fhwssf.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1713278460/Croma%20Assets/CMS/LP%20Page%20Banners/2024/Testing/Hybrid%20Homepage/16th%20April/01_50_sa0hk3.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1713278428/Croma%20Assets/CMS/LP%20Page%20Banners/2024/Testing/Hybrid%20Homepage/16th%20April/02_17_epxyzy.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1713273508/Croma%20Assets/CMS/LP%20Page%20Banners/2024/Testing/Hybrid%20Homepage/16th%20April/E-waste_disposal_-_Desktop_wewkot.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1713273472/Croma%20Assets/CMS/LP%20Page%20Banners/2024/Testing/Hybrid%20Homepage/16th%20April/Deals_of_the_week_-_Desktop_cdvjjx.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_ips;s_tp;s_ppv;s_cc;s_nr30;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga_DJ3RGVFHJN;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://13135721.fls.doubleclick.net/activityi;dc_pre=CO-Qh_S-kYYDFVyQrAIdv1YCbg;src=13135721;type=croma01;cat=croma0;ord=2122113974442;npa=0;auiddc=1421799651.1715839140;uaa=x86;uab=64;uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199;uamb=0;uam=;uap=Linux;uapv=5.15.0;uaw=0;pscdl=noapi;frm=0;gtm=45fe45f0z8889407033za201;gcd=13l3l3l3l1;dma=0;epver=2;~oref=https%3A%2F%2Fwww.croma.com%2F?", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://gumi.criteo.com/syncframe?topUrl=www.croma.com&origin=onetag#{%22bundle%22:{%22origin%22:0,%22value%22:null},%22cw%22:true,%22optout%22:{%22origin%22:0,%22value%22:null},%22origin%22:%22onetag%22,%22sid%22:{%22origin%22:0,%22value%22:null},%22tld%22:%22croma.com%22,%22topUrl%22:%22www.croma.com%22,%22version%22:%225_23_0%22,%22ifa%22:{%22origin%22:0,%22value%22:null},%22lsw%22:true,%22pm%22:0}", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/609902077/?random=156409110&cv=11&fst=1715839140714&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2F&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=Croma%20Electronics%20%7C%20Online%20Electronics%20Shopping%20%7C%20Buy%20Electronics%20Online&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtQHdOLvdqKoHq1mPlqmsIbM9z-0x4S6RdA&pscrd=IhMI2Nrt876RhgMV7k2dCR0_lj1lMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1006316414/?random=2092121373&cv=11&fst=1715839140760&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2F&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Croma%20Electronics%20%7C%20Online%20Electronics%20Shopping%20%7C%20Buy%20Electronics%20Online&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtSlHyEu4XtXhFXwUBHk7OWqinJtV0TKKpQ&pscrd=IhMIvNnt876RhgMV6FCdCR14vg_5MgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1006316414/?random=1128595619&cv=11&fst=1715839140778&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2F&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Croma%20Electronics%20%7C%20Online%20Electronics%20Shopping%20%7C%20Buy%20Electronics%20Online&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtdaGq-uWu0Hhv6GySU-4Ma0nEAKDce6SQg&pscrd=IhMIwsXt876RhgMVMEedCR2VZQZkMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://adservice.google.com/ddm/fls/z/dc_pre=CP2q_PO-kYYDFTiKrAIdarMJ0g;src=917807264;type=invmedia;cat=croma0;ord=7521621243952;npa=0;auiddc=*;uaa=x86;uab=64;uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199;uamb=0;uam=;uap=Linux;uapv=5.15.0;uaw=0;pscdl=noapi;frm=0;gtm=45fe45f0z8889407033za201;gcd=13l3l3l3l1;dma=0;epver=2;~oref=https%3A%2F%2Fwww.croma.com%2F", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-conversion/609902077/?random=1991333045&cv=11&fst=1715839140699&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2F&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=Croma%20Electronics%20%7C%20Online%20Electronics%20Shopping%20%7C%20Buy%20Electronics%20Online&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMIp-vt876RhgMVaVSdCR2ZUAZOMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtqe86NbZTXQTCCx51MZYprUsthkrXSsA&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtQMf8msyi4j5fvdKL4Fil75-CNU57-zKPg&random=3019716273", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-conversion/609902077/?random=156409110&cv=11&fst=1715839140714&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2F&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=Croma%20Electronics%20%7C%20Online%20Electronics%20Shopping%20%7C%20Buy%20Electronics%20Online&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMI2Nrt876RhgMV7k2dCR0_lj1lMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtqoa1abdha05TK3h8WZ7DDnoKmjJg4dw&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtSlBjP_JWT967dPZYgFXy49VQUWV9XdC6Q&random=2705534473", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://adservice.google.com/ddm/fls/z/dc_pre=CO-Qh_S-kYYDFVyQrAIdv1YCbg;src=13135721;type=croma01;cat=croma0;ord=2122113974442;npa=0;auiddc=*;uaa=x86;uab=64;uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199;uamb=0;uam=;uap=Linux;uapv=5.15.0;uaw=0;pscdl=noapi;frm=0;gtm=45fe45f0z8889407033za201;gcd=13l3l3l3l1;dma=0;epver=2;~oref=https%3A%2F%2Fwww.croma.com%2F", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-conversion/1006316414/?random=2092121373&cv=11&fst=1715839140760&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2F&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Croma%20Electronics%20%7C%20Online%20Electronics%20Shopping%20%7C%20Buy%20Electronics%20Online&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMIvNnt876RhgMV6FCdCR14vg_5MgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtqSepIK2Pqc5jThfQINeivjK8ZA0V7lw&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtVcy2074OnF2zIm7__IImfeEKuO4jHkbzw&random=3871982175", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-conversion/1006316414/?random=1128595619&cv=11&fst=1715839140778&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2F&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Croma%20Electronics%20%7C%20Online%20Electronics%20Shopping%20%7C%20Buy%20Electronics%20Online&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMIwsXt876RhgMVMEedCR2VZQZkMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtq2kxiYivEtsbnZ151M8JExO3T3t91Fg&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtRAuaQQCJJdyTBaj6GT8JNujoua6TQotRg&random=22544844", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-conversion/1006316414/?random=2092121373&cv=11&fst=1715839140760&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2F&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Croma%20Electronics%20%7C%20Online%20Electronics%20Shopping%20%7C%20Buy%20Electronics%20Online&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMIvNnt876RhgMV6FCdCR14vg_5MgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtqSepIK2Pqc5jThfQINeivjK8ZA0V7lw&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtVcy2074OnF2zIm7__IImfeEKuO4jHkbzw&random=3871982175&ipr=y", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-conversion/1006316414/?random=1128595619&cv=11&fst=1715839140778&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2F&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Croma%20Electronics%20%7C%20Online%20Electronics%20Shopping%20%7C%20Buy%20Electronics%20Online&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMIwsXt876RhgMVMEedCR2VZQZkMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtq2kxiYivEtsbnZ151M8JExO3T3t91Fg&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtRAuaQQCJJdyTBaj6GT8JNujoua6TQotRg&random=22544844&ipr=y", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-conversion/609902077/?random=1991333045&cv=11&fst=1715839140699&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2F&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=Croma%20Electronics%20%7C%20Online%20Electronics%20Shopping%20%7C%20Buy%20Electronics%20Online&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMIp-vt876RhgMVaVSdCR2ZUAZOMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtqe86NbZTXQTCCx51MZYprUsthkrXSsA&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtQMf8msyi4j5fvdKL4Fil75-CNU57-zKPg&random=3019716273&ipr=y", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-conversion/609902077/?random=156409110&cv=11&fst=1715839140714&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2F&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=Croma%20Electronics%20%7C%20Online%20Electronics%20Shopping%20%7C%20Buy%20Electronics%20Online&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMI2Nrt876RhgMV7k2dCR0_lj1lMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtqoa1abdha05TK3h8WZ7DDnoKmjJg4dw&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtSlBjP_JWT967dPZYgFXy49VQUWV9XdC6Q&random=2705534473&ipr=y", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("config_json", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("json");
    ns_web_url("json",
        "URL=https://gumi.criteo.com/sid/json?origin=onetag&domain=croma.com&sn=ChromeSyncframe&so=0&topUrl=www.croma.com&cw=1&lsw=1&topicsavail=0&fledgeavail=0",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("json", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1");
    ns_web_url("v1",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=customer-hash:null",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=tdl-sso-version:4.1.15",
        "HEADER=session:false",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tLyIsImV2ZW50RGF0YSI6W3sidGltZXN0YW1wIjoxNzE1ODM5MTQxODA2LCJkYXRhIjoie1wiZXZlbnRcIjpcInBhZ2VMb2FkXCIsXCJwYXlsb2FkXCI6e1widXJsXCI6XCJodHRwczovL3d3dy5jcm9tYS5jb20vXCIsXCJkYXRhU291cmNlXCI6XCJsb2FkXCIsXCJpc1RydXN0ZWRcIjp0cnVlfX0ifV0sImN1c3RvbWVySGFzaCI6bnVsbCwiZWNpZCI6IlVua25vd24iLCJjbGllbnRJZCI6IkNST01BLVdFQi1BUFAiLCJzZXNzaW9uSWQiOiJiNjVmNTRiNC01YzkzLTQ4MmUtOGNmOC1jMjEwZWNmZmE3OGYuOTIzYmI1N2YtOTUxYi00NzgwLThjYzMtM2U3Yzg5ZTkwNWZlIiwiYnJhbmQiOiJDUk9NQSIsInRpbWVzdGFtcCI6MTcxNTgzOTE0MTgwNiwiZGV2aWNlIjoiTW96aWxsYS81LjAgKFgxMTsgTGludXggeDg2XzY0KSBBcHBsZVdlYktpdC81MzcuMzYgKEtIVE1MLCBsaWtlIEdlY2tvKSBDaHJvbWUvMTE0LjAuMC4wIFNhZmFyaS81MzcuMzYiLCJqYXJ2aXNJZCI6IjRkNjA0NTcwLWJhZmItNDU1MS1iYWQwLTg1Mzg5NzFjN2VlMiIsImFkdmVydGlzaW5nSWQiOm51bGwsImFwcFZlcnNpb24iOiIwIn0="}",
        BODY_END,
        INLINE_URLS,
            "URL=https://cdn.appdynamics.com/adrum-ext.a57fe9a4dfa0e1d6b2dc001466e4e21d.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://sslwidget.criteo.com/event?a=56256&v=5.23.0&otl=1&p0=e%3Dexd%26site_type%3Dd&p1=e%3Dvh%26tms%3Dgtm-template&p2=e%3Ddis&adce=1&tld=croma.com&dy=1&fu=https%253A%252F%252Fwww.croma.com%252F&ceid=c0906449-bf8d-4ffd-a8bd-5be728d7f957&dtycbr=64651", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.croma.com/assets/favicon.ico", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;mbox;s_plt;s_pltp;s_cc;s_nr30;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;s_tp;s_ppv;RT;_ga_DJ3RGVFHJN", END_INLINE
    );

    ns_end_transaction("v1", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_2");
    ns_web_url("v1_2",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=customer-hash:null",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=tdl-sso-version:4.1.15",
        "HEADER=session:false",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tLyIsImV2ZW50RGF0YSI6W3sidGltZXN0YW1wIjoxNzE1ODM5MTQxODA4LCJkYXRhIjoie1wiZXZlbnRcIjpcIm5ldHdvcmstaW5mb1wiLFwiZGF0YVwiOntcImRvd25saW5rXCI6MTAsXCJlZmZlY3RpdmVUeXBlXCI6XCI0Z1wifX0ifV0sImN1c3RvbWVySGFzaCI6bnVsbCwiZWNpZCI6IlVua25vd24iLCJjbGllbnRJZCI6IkNST01BLVdFQi1BUFAiLCJzZXNzaW9uSWQiOiJiNjVmNTRiNC01YzkzLTQ4MmUtOGNmOC1jMjEwZWNmZmE3OGYuOTIzYmI1N2YtOTUxYi00NzgwLThjYzMtM2U3Yzg5ZTkwNWZlIiwiYnJhbmQiOiJDUk9NQSIsInRpbWVzdGFtcCI6MTcxNTgzOTE0MTgwOSwiZGV2aWNlIjoiTW96aWxsYS81LjAgKFgxMTsgTGludXggeDg2XzY0KSBBcHBsZVdlYktpdC81MzcuMzYgKEtIVE1MLCBsaWtlIEdlY2tvKSBDaHJvbWUvMTE0LjAuMC4wIFNhZmFyaS81MzcuMzYiLCJqYXJ2aXNJZCI6IjRkNjA0NTcwLWJhZmItNDU1MS1iYWQwLTg1Mzg5NzFjN2VlMiIsImFkdmVydGlzaW5nSWQiOm51bGwsImFwcFZlcnNpb24iOiIwIn0="}",
        BODY_END
    );

    ns_end_transaction("v1_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("page_2");
    ns_web_url("page_2",
        "URL=https://rs.fullstory.com/rec/page",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"OrgId":"11EGJ5","UserId":"","Url":"https://www.croma.com/","Base":"https://www.croma.com/","Width":1508,"Height":523,"ScreenWidth":1523,"ScreenHeight":650,"SnippetVersion":"1.3.0","Referrer":"","Preroll":2517,"Doctype":"<!DOCTYPE html>","CompiledVersion":"2df08c2f7ddf811d40d34bea72f08ca3d8851de8","CompiledTimestamp":1715639152,"TabId":"c93cc22a5192a0"}",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.croma.com/assets/js/shopWithVideo.min.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;mbox;s_plt;s_pltp;s_cc;s_nr30;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;s_tp;s_ppv;RT;_ga_DJ3RGVFHJN;cto_bundle", END_INLINE
    );

    ns_end_transaction("page_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("adrum");
    ns_web_url("adrum",
        "URL=https://bom-col.eum-appdynamics.com/eumcollector/beacons/browser/v1/BO-AAB-FBK/adrum",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:text/plain",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/adrum_url_0_1_1715839393333.body",
        BODY_END
    );

    ns_end_transaction("adrum", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_2");
    ns_web_url("collect_2",
        "URL=https://analytics.google.com/g/collect?v=2&tid=G-DJ3RGVFHJN&gtm=45je45f0v893318240z8889407033za200&_p=1715839139273&gcd=13l3l3l3l1&npa=0&dma=0&cid=1944455297.1715839141&ul=en-us&sr=1523x650&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&frm=0&pscdl=noapi&_s=2&sid=1715839140&sct=1&seg=0&dl=https%3A%2F%2Fwww.croma.com%2F&dt=Croma%20Electronics%20%7C%20Online%20Electronics%20Shopping%20%7C%20Buy%20Electronics%20Online&en=scroll_depth&epn.scroll_percentage=10&tfd=9263",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s39829118024275?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%200%3A59%3A7%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=homepage&g=https%3A%2F%2Fwww.croma.com%2F&cc=INR&server=www.croma.com&events=event55%2Cevent50%3Dundefined&c1=homepage&v1=D%3Dmid&c2=www.croma.com%2F&v2=new&c3=2024-5-16%200%3A59%3A7&l3=hero%20banner%3Aac%20n%3Ahomepage&v4=guest%20user&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5BImpression%5D%20%3A%20impression&v156=hero%20banner%3Aac%20n%3Ahomepage&pe=lnk_o&pev2=impression%3A%20tracking&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&lrt=127&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;s_tp;s_ppv;_ga_DJ3RGVFHJN;cto_bundle;s_nr30", END_INLINE,
            "URL=https://www.croma.com/static/css/40.5effb02f.chunk.css", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;mbox;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;s_tp;RT;_ga_DJ3RGVFHJN;cto_bundle;s_nr30;s_ppv", END_INLINE,
            "URL=https://www.croma.com/static/js/40.93cbd39b.chunk.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;mbox;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;s_tp;RT;_ga_DJ3RGVFHJN;cto_bundle;s_nr30;s_ppv", END_INLINE,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s3478868529192?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%200%3A59%3A9%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=homepage&g=https%3A%2F%2Fwww.croma.com%2F&cc=INR&server=www.croma.com&events=event55%2Cevent50%3Dundefined&c1=homepage&v1=D%3Dmid&c2=www.croma.com%2F&v2=new&c3=2024-5-16%200%3A59%3A9&l3=cat%20nav%3Aref%3Ahomepage&v4=guest%20user&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5BImpression%5D%20%3A%20impression&v156=cat%20nav%3Aref%3Ahomepage&pe=lnk_o&pev2=impression%3A%20tracking&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&lrt=136&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;s_tp;cto_bundle;s_ppv;_ga_DJ3RGVFHJN;s_nr30", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1689252663/Croma%20Assets/CMS/LP%20Page%20Banners/2023/Indulge/Sanity/Blank%20Banners/D_CromaIndulge_Blank_45H_snvzsu.png?tr=w-1024", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;s_tp;cto_bundle;s_ppv;_ga_DJ3RGVFHJN;s_nr30", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1715778682/Croma%20Assets/CMS/LP%20Page%20Banners/2024/BAU/neu_offers_desk_lwe5pf.png?tr=w-1024", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;s_tp;cto_bundle;s_ppv;_ga_DJ3RGVFHJN;s_nr30", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1713849331/Croma%20Assets/CMS/LP%20Page%20Banners/2024/Sanity/other/April/23042024/HP_TNRL_22April2024_ekmdao.jpg?tr=w-1024", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;s_tp;cto_bundle;s_ppv;_ga_DJ3RGVFHJN;s_nr30", END_INLINE
    );

    ns_end_transaction("collect_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("page_3");
    ns_web_url("page_3",
        "URL=https://api.croma.com/cmstemplate/allchannels/v1/page?pageType=ContentPage&pageLabelOrId=pwaHomePageFooter&fields=FULL",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;ADRUM_BTa;SameSite;ADRUM_BT1;WZRK_S_679-6RK-976Z;s_ips;s_tp;cto_bundle;s_ppv;_ga_DJ3RGVFHJN;s_nr30",
        INLINE_URLS,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s37875846766674?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%200%3A59%3A9%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=homepage&g=https%3A%2F%2Fwww.croma.com%2F&cc=INR&server=www.croma.com&events=event55%2Cevent50%3Dundefined&c1=homepage&v1=D%3Dmid&c2=www.croma.com%2F&v2=new&c3=2024-5-16%200%3A59%3A9&l3=cat%20nav%3Ahome%20theatre%3Ahomepage&v4=guest%20user&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5BImpression%5D%20%3A%20impression&v156=cat%20nav%3Ahome%20theatre%3Ahomepage&pe=lnk_o&pev2=impression%3A%20tracking&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;s_tp;cto_bundle;s_ppv;_ga_DJ3RGVFHJN;s_nr30", END_INLINE,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s37496570525739?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%200%3A59%3A9%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=homepage&g=https%3A%2F%2Fwww.croma.com%2F&cc=INR&server=www.croma.com&events=event55%2Cevent50%3Dundefined&c1=homepage&v1=D%3Dmid&c2=www.croma.com%2F&v2=new&c3=2024-5-16%200%3A59%3A9&l3=cat%20nav%3Aac%3Ahomepage&v4=guest%20user&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5BImpression%5D%20%3A%20impression&v156=cat%20nav%3Aac%3Ahomepage&pe=lnk_o&pev2=impression%3A%20tracking&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;s_tp;cto_bundle;s_ppv;_ga_DJ3RGVFHJN;s_nr30", END_INLINE,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s35668456475135?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%200%3A59%3A9%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=homepage&g=https%3A%2F%2Fwww.croma.com%2F&cc=INR&server=www.croma.com&events=event55%2Cevent50%3Dundefined&c1=homepage&v1=D%3Dmid&c2=www.croma.com%2F&v2=new&c3=2024-5-16%200%3A59%3A9&l3=cat%20nav%3Awm%3Ahomepage&v4=guest%20user&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5BImpression%5D%20%3A%20impression&v156=cat%20nav%3Awm%3Ahomepage&pe=lnk_o&pev2=impression%3A%20tracking&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;s_tp;cto_bundle;s_ppv;_ga_DJ3RGVFHJN;s_nr30", END_INLINE,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s39668287569515?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%200%3A59%3A9%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=homepage&g=https%3A%2F%2Fwww.croma.com%2F&cc=INR&server=www.croma.com&events=event55%2Cevent50%3Dundefined&c1=homepage&v1=D%3Dmid&c2=www.croma.com%2F&v2=new&c3=2024-5-16%200%3A59%3A9&l3=cat%20nav%3Atablet%3Ahomepage&v4=guest%20user&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5BImpression%5D%20%3A%20impression&v156=cat%20nav%3Atablet%3Ahomepage&pe=lnk_o&pev2=impression%3A%20tracking&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;s_tp;cto_bundle;s_ppv;_ga_DJ3RGVFHJN;s_nr30", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1639657222/insta_before_v2nnfx.png", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;s_tp;cto_bundle;s_ppv;_ga_DJ3RGVFHJN;s_nr30", END_INLINE,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s33871708574340?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%200%3A59%3A9%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=homepage&g=https%3A%2F%2Fwww.croma.com%2F&cc=INR&server=www.croma.com&events=event55%2Cevent50%3Dundefined&c1=homepage&v1=D%3Dmid&c2=www.croma.com%2F&v2=new&c3=2024-5-16%200%3A59%3A9&l3=cat%20nav%3Atv%3Ahomepage&v4=guest%20user&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5BImpression%5D%20%3A%20impression&v156=cat%20nav%3Atv%3Ahomepage&pe=lnk_o&pev2=impression%3A%20tracking&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;s_tp;cto_bundle;s_ppv;_ga_DJ3RGVFHJN;s_nr30", END_INLINE,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s31581887705084?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%200%3A59%3A9%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=homepage&g=https%3A%2F%2Fwww.croma.com%2F&cc=INR&server=www.croma.com&events=event55%2Cevent50%3Dundefined&c1=homepage&v1=D%3Dmid&c2=www.croma.com%2F&v2=new&c3=2024-5-16%200%3A59%3A9&l3=neu%20offerings%3Atnrl%20promotion%3Ahomepage&v4=guest%20user&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5BImpression%5D%20%3A%20impression&v156=neu%20offerings%3Atnrl%20promotion%3Ahomepage&pe=lnk_o&pev2=impression%3A%20tracking&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;s_tp;cto_bundle;s_ppv;_ga_DJ3RGVFHJN;s_nr30", END_INLINE,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s32807714728089?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%200%3A59%3A9%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=homepage&g=https%3A%2F%2Fwww.croma.com%2F&cc=INR&server=www.croma.com&events=event55%2Cevent50%3Dundefined&c1=homepage&v1=D%3Dmid&c2=www.croma.com%2F&v2=new&c3=2024-5-16%200%3A59%3A9&l3=cat%20nav%3Alaptop%3Ahomepage&v4=guest%20user&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5BImpression%5D%20%3A%20impression&v156=cat%20nav%3Alaptop%3Ahomepage&pe=lnk_o&pev2=impression%3A%20tracking&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;s_tp;cto_bundle;s_ppv;_ga_DJ3RGVFHJN;s_nr30", END_INLINE,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s39324067501073?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%200%3A59%3A9%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=homepage&g=https%3A%2F%2Fwww.croma.com%2F&cc=INR&server=www.croma.com&events=event55%2Cevent50%3Dundefined&c1=homepage&v1=D%3Dmid&c2=www.croma.com%2F&v2=new&c3=2024-5-16%200%3A59%3A9&l3=neu%20offerings%3Aneu%20hdfc%20credit%20card%3Ahomepage&v4=guest%20user&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5BImpression%5D%20%3A%20impression&v156=neu%20offerings%3Aneu%20hdfc%20credit%20card%3Ahomepage&pe=lnk_o&pev2=impression%3A%20tracking&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;s_tp;cto_bundle;s_ppv;_ga_DJ3RGVFHJN;s_nr30", END_INLINE,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s330855785883?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%200%3A59%3A9%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=homepage&g=https%3A%2F%2Fwww.croma.com%2F&cc=INR&server=www.croma.com&events=event55%2Cevent50%3Dundefined&c1=homepage&v1=D%3Dmid&c2=www.croma.com%2F&v2=new&c3=2024-5-16%200%3A59%3A9&l3=blank%20space%3Ablank%3Ahomepage&v4=guest%20user&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5BImpression%5D%20%3A%20impression&v156=blank%20space%3Ablank%3Ahomepage&pe=lnk_o&pev2=impression%3A%20tracking&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;s_tp;cto_bundle;s_ppv;_ga_DJ3RGVFHJN;s_nr30", END_INLINE,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s32457887431629?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%200%3A59%3A9%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=homepage&g=https%3A%2F%2Fwww.croma.com%2F&cc=INR&server=www.croma.com&events=event55%2Cevent50%3Dundefined&c1=homepage&v1=D%3Dmid&c2=www.croma.com%2F&v2=new&c3=2024-5-16%200%3A59%3A9&l3=cat%20nav%3Aheadphone%3Ahomepage&v4=guest%20user&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5BImpression%5D%20%3A%20impression&v156=cat%20nav%3Aheadphone%3Ahomepage&pe=lnk_o&pev2=impression%3A%20tracking&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;s_tp;cto_bundle;s_ppv;_ga_DJ3RGVFHJN;s_nr30", END_INLINE,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s38273232392537?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%200%3A59%3A9%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=homepage&g=https%3A%2F%2Fwww.croma.com%2F&cc=INR&server=www.croma.com&events=event55%2Cevent50%3Dundefined&c1=homepage&v1=D%3Dmid&c2=www.croma.com%2F&v2=new&c3=2024-5-16%200%3A59%3A9&l3=cat%20nav%3Amobiles%3Ahomepage&v4=guest%20user&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5BImpression%5D%20%3A%20impression&v156=cat%20nav%3Amobiles%3Ahomepage&pe=lnk_o&pev2=impression%3A%20tracking&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;s_tp;cto_bundle;s_ppv;_ga_DJ3RGVFHJN;s_nr30", END_INLINE,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s3520160156161?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%200%3A59%3A9%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=homepage&g=https%3A%2F%2Fwww.croma.com%2F&cc=INR&server=www.croma.com&events=event55%2Cevent50%3Dundefined&c1=homepage&v1=D%3Dmid&c2=www.croma.com%2F&v2=new&c3=2024-5-16%200%3A59%3A9&l3=cat%20nav%3Acooler%3Ahomepage&v4=guest%20user&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5BImpression%5D%20%3A%20impression&v156=cat%20nav%3Acooler%3Ahomepage&pe=lnk_o&pev2=impression%3A%20tracking&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;s_tp;cto_bundle;s_ppv;_ga_DJ3RGVFHJN;s_nr30", END_INLINE
    );

    ns_end_transaction("page_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_3");
    ns_web_url("collect_3",
        "URL=https://analytics.google.com/g/collect?v=2&tid=G-DJ3RGVFHJN&gtm=45je45f0v893318240z8889407033za200&_p=1715839139273&gcd=13l3l3l3l1&npa=0&dma=0&cid=1944455297.1715839141&ul=en-us&sr=1523x650&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&frm=0&pscdl=noapi&sid=1715839140&sct=1&seg=0&dl=https%3A%2F%2Fwww.croma.com%2F&dt=Croma%20Electronics%20%7C%20Online%20Electronics%20Shopping%20%7C%20Buy%20Electronics%20Online&_s=3&tfd=16558",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "en=section_card_impressions&ep.pagetype=homePage&ep.source_page_url=%2F&ep.previous_page_url=&ep.destination_page_url=%2Fhome-appliances%2Frefrigerators-freezers%2Fc%2F47&ep.click_text=N%2FA&ep.interaction_type=N%2FA&epn.sequence=4&epn.total_elements=18&ep.section=N%2FA&ep.item_name=REF&ep.item_category=N%2FA&ep.item_category2=N%2FA&ep.item_category3=N%2FA&ep.brand=N%2FA&ep.platform=croma.com&ep.creative_id=https%3A%2F%2Fmedia.croma.com%2Fimage%2Fupload%2Fv1709186816%2FCroma%2520Assets%2FTest_Nikhil%2FHybrid%2520Homepage%2Fcatnav%2F2902%2FDesktop%2FRefrigerators_jezg47.png&ep.login_trigger=N%2FA&ep.login_status=false&uid=false
en=section_card_impressions&ep.pagetype=homePage&ep.source_page_url=%2F&ep.previous_page_url=&ep.destination_page_url=%2Faudio-video%2Fhome-theatres-soundbars%2Fc%2F44&ep.click_text=N%2FA&ep.interaction_type=N%2FA&epn.sequence=8&epn.total_elements=18&ep.section=N%2FA&ep.item_name=Home%20Theatre&ep.item_category=N%2FA&ep.item_category2=N%2FA&ep.item_category3=N%2FA&ep.brand=N%2FA&ep.platform=croma.com&ep.creative_id=https%3A%2F%2Fmedia.croma.com%2Fimage%2Fupload%2Fv1709186810%2FCroma%2520Assets%2FTest_Nikhil%2FHybrid%2520Homepage%2Fcatnav%2F2902%2FDesktop%2FHome_Theatres_Soundbars_nqyuff.png&ep.login_trigger=N%2FA&ep.login_status=false&uid=false
en=section_card_impressions&ep.pagetype=homePage&ep.source_page_url=%2F&ep.previous_page_url=&ep.destination_page_url=%2Fhome-appliances%2Fair-conditioners%2Fc%2F46&ep.click_text=N%2FA&ep.interaction_type=N%2FA&epn.sequence=2&epn.total_elements=18&ep.section=N%2FA&ep.item_name=AC&ep.item_category=N%2FA&ep.item_category2=N%2FA&ep.item_category3=N%2FA&ep.brand=N%2FA&ep.platform=croma.com&ep.creative_id=https%3A%2F%2Fmedia.croma.com%2Fimage%2Fupload%2Fv1709186810%2FCroma%2520Assets%2FTest_Nikhil%2FHybrid%2520Homepage%2Fcatnav%2F2902%2FDesktop%2FAir_Conditioners_dmes8r.png&ep.login_trigger=N%2FA&ep.login_status=false&uid=false
en=section_card_impressions&ep.pagetype=homePage&ep.source_page_url=%2F&ep.previous_page_url=&ep.destination_page_url=%2Fhome-appliances%2Fwashing-machines-dryers%2Fc%2F48&ep.click_text=N%2FA&ep.interaction_type=N%2FA&epn.sequence=9&epn.total_elements=18&ep.section=N%2FA&ep.item_name=WM&ep.item_category=N%2FA&ep.item_category2=N%2FA&ep.item_category3=N%2FA&ep.brand=N%2FA&ep.platform=croma.com&ep.creative_id=https%3A%2F%2Fmedia.croma.com%2Fimage%2Fupload%2Fv1709186815%2FCroma%2520Assets%2FTest_Nikhil%2FHybrid%2520Homepage%2Fcatnav%2F2902%2FDesktop%2FWashing_Machines_t1r7fe.png&ep.login_trigger=N%2FA&ep.login_status=false&uid=false
en=section_card_impressions&ep.pagetype=homePage&ep.source_page_url=%2F&ep.previous_page_url=&ep.destination_page_url=%2Fcomputers-tablets%2Ftablets-ereaders%2Fc%2F22&ep.click_text=N%2FA&ep.interaction_type=N%2FA&epn.sequence=10&epn.total_elements=18&ep.section=N%2FA&ep.item_name=Tablet&ep.item_category=N%2FA&ep.item_category2=N%2FA&ep.item_category3=N%2FA&ep.brand=N%2FA&ep.platform=croma.com&ep.creative_id=https%3A%2F%2Fmedia.croma.com%2Fimage%2Fupload%2Fv1709186813%2FCroma%2520Assets%2FTest_Nikhil%2FHybrid%2520Homepage%2Fcatnav%2F2902%2FDesktop%2FTablets_knivhx.png&ep.login_trigger=N%2FA&ep.login_status=false&uid=false
en=section_card_impressions&ep.pagetype=homePage&ep.source_page_url=%2F&ep.previous_page_url=&ep.destination_page_url=%2Ftelevisions-accessories%2Fled-tvs%2Fc%2F392&ep.click_text=N%2FA&ep.interaction_type=N%2FA&epn.sequence=3&epn.total_elements=18&ep.section=N%2FA&ep.item_name=TV&ep.item_category=N%2FA&ep.item_category2=N%2FA&ep.item_category3=N%2FA&ep.brand=N%2FA&ep.platform=croma.com&ep.creative_id=https%3A%2F%2Fmedia.croma.com%2Fimage%2Fupload%2Fv1709186812%2FCroma%2520Assets%2FTest_Nikhil%2FHybrid%2520Homepage%2Fcatnav%2F2902%2FDesktop%2FTelevisions_hilbar.png&ep.login_trigger=N%2FA&ep.login_status=false&uid=false
en=section_card_impressions&ep.pagetype=homePage&ep.source_page_url=%2F&ep.previous_page_url=&ep.destination_page_url=https%3A%2F%2Fwww.tatadigital.com%2Ftnrl-campaign&ep.click_text=N%2FA&ep.interaction_type=N%2FA&epn.sequence=2&epn.total_elements=2&ep.section=N%2FA&ep.item_name=TNRL%20promotion&ep.item_category=N%2FA&ep.item_category2=N%2FA&ep.item_category3=N%2FA&ep.brand=N%2FA&ep.platform=croma.com&ep.creative_id=https%3A%2F%2Fmedia.croma.com%2Fimage%2Fupload%2Fv1713849331%2FCroma%2520Assets%2FCMS%2FLP%2520Page%2520Banners%2F2024%2FSanity%2Fother%2FApril%2F23042024%2FHP_TNRL_22April2024_ekmdao.jpg&ep.login_trigger=N%2FA&ep.login_status=false&uid=false
en=section_card_impressions&ep.pagetype=homePage&ep.source_page_url=%2F&ep.previous_page_url=&ep.destination_page_url=%2Fcomputers-tablets%2Flaptops%2Fc%2F20&ep.click_text=N%2FA&ep.interaction_type=N%2FA&epn.sequence=5&epn.total_elements=18&ep.section=N%2FA&ep.item_name=Laptop&ep.item_category=N%2FA&ep.item_category2=N%2FA&ep.item_category3=N%2FA&ep.brand=N%2FA&ep.platform=croma.com&ep.creative_id=https%3A%2F%2Fmedia.croma.com%2Fimage%2Fupload%2Fv1709186810%2FCroma%2520Assets%2FTest_Nikhil%2FHybrid%2520Homepage%2Fcatnav%2F2902%2FDesktop%2FLaptops_g9ymac.png&ep.login_trigger=N%2FA&ep.login_status=false&uid=false
en=section_card_impressions&ep.pagetype=homePage&ep.source_page_url=%2F&ep.previous_page_url=&ep.destination_page_url=https%3A%2F%2Fwww.tatadigital.com%2Fcreditcard%2F&ep.click_text=N%2FA&ep.interaction_type=N%2FA&epn.sequence=1&epn.total_elements=2&ep.section=N%2FA&ep.item_name=Neu%20HDFC%20Credit%20Card&ep.item_category=N%2FA&ep.item_category2=N%2FA&ep.item_category3=N%2FA&ep.brand=N%2FA&ep.platform=croma.com&ep.creative_id=https%3A%2F%2Fmedia.croma.com%2Fimage%2Fupload%2Fv1715778682%2FCroma%2520Assets%2FCMS%2FLP%2520Page%2520Banners%2F2024%2FBAU%2Fneu_offers_desk_lwe5pf.png&ep.login_trigger=N%2FA&ep.login_status=false&uid=false
en=section_card_impressions&ep.pagetype=homePage&ep.source_page_url=%2F&ep.previous_page_url=&ep.destination_page_url=N%2FA&ep.click_text=N%2FA&ep.interaction_type=N%2FA&epn.sequence=1&epn.total_elements=1&ep.section=N%2FA&ep.item_name=Blank&ep.item_category=N%2FA&ep.item_category2=N%2FA&ep.item_category3=N%2FA&ep.brand=N%2FA&ep.platform=croma.com&ep.creative_id=https%3A%2F%2Fmedia.croma.com%2Fimage%2Fupload%2Fv1689252663%2FCroma%2520Assets%2FCMS%2FLP%2520Page%2520Banners%2F2023%2FIndulge%2FSanity%2FBlank%2520Banners%2FD_CromaIndulge_Blank_45H_snvzsu.png&ep.login_trigger=N%2FA&ep.login_status=false&uid=false
en=section_card_impressions&ep.pagetype=homePage&ep.source_page_url=%2F&ep.previous_page_url=&ep.destination_page_url=%2Faudio-video%2Fheadphones-earphones%2Fc%2F1012&ep.click_text=N%2FA&ep.interaction_type=N%2FA&epn.sequence=6&epn.total_elements=18&ep.section=N%2FA&ep.item_name=Headphone&ep.item_category=N%2FA&ep.item_category2=N%2FA&ep.item_category3=N%2FA&ep.brand=N%2FA&ep.platform=croma.com&ep.creative_id=https%3A%2F%2Fmedia.croma.com%2Fimage%2Fupload%2Fv1709186809%2FCroma%2520Assets%2FTest_Nikhil%2FHybrid%2520Homepage%2Fcatnav%2F2902%2FDesktop%2FHeadphones_Earphones_w7ibqa.png&ep.login_trigger=N%2FA&ep.login_status=false&uid=false
en=section_card_impressions&ep.pagetype=homePage&ep.source_page_url=%2F&ep.previous_page_url=&ep.destination_page_url=%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&ep.click_text=N%2FA&ep.interaction_type=N%2FA&epn.sequence=1&epn.total_elements=18&ep.section=N%2FA&ep.item_name=Mobiles&ep.item_category=N%2FA&ep.item_category2=N%2FA&ep.item_category3=N%2FA&ep.brand=N%2FA&ep.platform=croma.com&ep.creative_id=https%3A%2F%2Fmedia.croma.com%2Fimage%2Fupload%2Fv1709186812%2FCroma%2520Assets%2FTest_Nikhil%2FHybrid%2520Homepage%2Fcatnav%2F2902%2FDesktop%2FMobiles_ueht5o.png&ep.login_trigger=N%2FA&ep.login_status=false&uid=false
en=scroll_depth&epn.scroll_percentage=25
en=section_card_impressions&ep.pagetype=homePage&ep.source_page_url=%2F&ep.previous_page_url=&ep.destination_page_url=%2Fhome-appliances%2Fair-coolers%2Fc%2F2005&ep.click_text=N%2FA&ep.interaction_type=N%2FA&epn.sequence=7&epn.total_elements=18&ep.section=N%2FA&ep.item_name=Cooler&ep.item_category=N%2FA&ep.item_category2=N%2FA&ep.item_category3=N%2FA&ep.brand=N%2FA&ep.platform=croma.com&ep.creative_id=https%3A%2F%2Fmedia.croma.com%2Fimage%2Fupload%2Fv1713365545%2FCroma%2520Assets%2FCMS%2FLP%2520Page%2520Banners%2F2024%2FTesting%2FHybrid%2520Homepage%2FCat%2520Nav%2FDesktop%2FCooler_dx0xg8.png&ep.login_trigger=N%2FA&ep.login_status=false&uid=false",
        BODY_END
    );

    ns_end_transaction("collect_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("adrum_2");
    ns_web_url("adrum_2",
        "URL=https://bom-col.eum-appdynamics.com/eumcollector/beacons/browser/v1/BO-AAB-FBK/adrum",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:text/plain",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"vr":"23.10.1.4359","dt":"R","rg":"0","es":[{"eg":"1","et":2,"eu":"0://1/2/3/4/5?6","ts":1715839149537,"mg":"0","au":"0://7/","at":0,"pp":3,"mx":{"PLC":1,"FBT":176,"DDT":0,"DPT":0,"PLT":176,"ARE":0},"md":"GET","xs":200,"si":9}],"ai":"477d42aa_9f21_9144_1a76_688c9aa25e4b","gs":["94fc8c22_8b4e_9951_5dd4_a346d07ee981","4f8be8b7_0f84_1b63_a0f0_34da08f8850b"],"up":["https","api.croma.com","cmstemplate","allchannels","v1","page","pageType=ContentPage&pageLabelOrId=pwaHomePageFooter&fields=FULL","www.croma.com"]}",
        BODY_END
    );

    ns_end_transaction("adrum_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_3");
    ns_web_url("v1_3",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tLyIsImV2ZW50RGF0YSI6W3sidGltZXN0YW1wIjoxNzE1ODM5MTU1Nzg0LCJkYXRhIjoie1xuICBcInVybFwiOiB7XG4gICAgXCJhbmNlc3Rvck9yaWdpbnNcIjoge30sXG4gICAgXCJocmVmXCI6IFwiaHR0cHM6Ly93d3cuY3JvbWEuY29tL1wiLFxuICAgIFwib3JpZ2luXCI6IFwiaHR0cHM6Ly93d3cuY3JvbWEuY29tXCIsXG4gICAgXCJwcm90b2NvbFwiOiBcImh0dHBzOlwiLFxuICAgIFwiaG9zdFwiOiBcInd3dy5jcm9tYS5jb21cIixcbiAgICBcImhvc3RuYW1lXCI6IFwid3d3LmNyb21hLmNvbVwiLFxuICAgIFwicG9ydFwiOiBcIlwiLFxuICAgIFwicGF0aG5hbWVcIjogXCIvXCIsXG4gICAgXCJzZWFyY2hcIjogXCJcIixcbiAgICBcImhhc2hcIjogXCJcIlxuICB9LFxuICBcInBhdGhcIjogW1xuICAgIHtcbiAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJpbWdcIixcbiAgICAgIFwiaWRcIjogXCJcIixcbiAgICAgIFwiY2xhc3NcIjogXCJcIlxuICAgIH0sXG4gICAge1xuICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImFcIixcbiAgICAgIFwiaWRcIjogXCJcIixcbiAgICAgIFwiY2xhc3NcIjogXCJcIlxuICAgIH0sXG4gICAge1xuICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImRpdlwiLFxuICAgICAgXCJpZFwiOiBcIlwiLFxuICAgICAgXCJjbGFzc1wiOiBcInNsaWRlLWltZy13cmFwXCJcbiAgICB9LFxuICAgIHtcbiAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJkaXZcIixcbiAgICAgIFwiaWRcIjogXCJcIixcbiAgICAgIFwiY2xhc3NcIjogXCJzd2lwZXItc2xpZGUgc3dpcGVyLXNsaWRlLWFjdGl2ZVwiXG4gICAgfSxcbiAgICB7XG4gICAgICBcImVsZW1lbnRUeXBlXCI6IFwiZGl2XCIsXG4gICAgICBcImlkXCI6IFwiXCIsXG4gICAgICBcImNsYXNzXCI6IFwic3dpcGVyLXdyYXBwZXJcIlxuICAgIH0sXG4gICAge1xuICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImRpdlwiLFxuICAgICAgXCJpZFwiOiBcIlwiLFxuICAgICAgXCJjbGFzc1wiOiBcInN3aXBlci1jb250YWluZXIgY29tcF8wMDAwQk9LUTAtY2Fyb3VzZWwtc2xpZGVyIHNjcm9sbC1iYW5uZXItY29udGFpbmVyIHN3aXBlci1jb250YWluZXItaW5pdGlhbGl6ZWQgc3dpcGVyLWNvbnRhaW5lci1ob3Jpem9udGFsXCJcbiAgICB9LFxuICAgIHtcbiAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJkaXZcIixcbiAgICAgIFwiaWRcIjogXCJcIixcbiAgICAgIFwiY2xhc3NcIjogXCJzd2lwZXItbWFpbi1jb250IHdpZHRoLWZ1bGwgc3dpcGVyLW1haW4tY29tcF8wMDAwQk9LUTBcIlxuICAgIH0sXG4gICAge1xuICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImRpdlwiLFxuICAgICAgXCJpZFwiOiBcIlwiLFxuICAgICAgXCJjbGFzc1wiOiBcInNlYy1jb250XCJcbiAgICB9LFxuICAgIHtcbiAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJkaXZcIixcbiAgICAgIFwiaWRcIjogXCJcIixcbiAgICAgIFwiY2xhc3NcIjogXCJjb250YWluZXIgc2Nyb2xsLWJhbm5lclwiXG4gICAgfSxcbiAgICB7XG4gICAgICBcImVsZW1lbnRUeXBlXCI6IFwiZGl2XCIsXG4gICAgICBcImlkXCI6IFwiY29tcF8wMDAwQk9LUVwiLFxuICAgICAgXCJjbGFzc1wiOiBcIiBjcC1zZWN0aW9uLWhvbWUgYmFubmVyLXNwYWNpbmdcIlxuICAgIH0sXG4gICAge1xuICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImRpdlwiLFxuICAgICAgXCJpZFwiOiBcIlwiLFxuICAgICAgXCJjbGFzc1wiOiBcIlwiXG4gICAgfSxcbiAgICB7XG4gICAgICBcImVsZW1lbnRUeXBlXCI6IFwiZGl2XCIsXG4gICAgICBcImlkXCI6IFwiaG9tZWRhdGFlbFwiLFxuICAgICAgXCJjbGFzc1wiOiBcImJzLWNvbnRlbnQgdHlwLW5vLXBhZGRpbmcgdHlwLW1hcmdpblwiXG4gICAgfSxcbiAgICB7XG4gICAgICBcImVsZW1lbnRUeXBlXCI6IFwiZGl2XCIsXG4gICAgICBcImlkXCI6IFwiXCIsXG4gICAgICBcImNsYXNzXCI6IFwibWFpbkNvbnRhaW5lclwiXG4gICAgfSxcbiAgICB7XG4gICAgICBcImVsZW1lbnRUeXBlXCI6IFwiZGl2XCIsXG4gICAgICBcImlkXCI6IFwiXCIsXG4gICAgICBcImNsYXNzXCI6IFwiXCJcbiAgICB9LFxuICAgIHtcbiAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJtYWluXCIsXG4gICAgICBcImlkXCI6IFwiY29udGFpbmVyXCIsXG4gICAgICBcImNsYXNzXCI6IFwiZGFyay10aGVtZVwiXG4gICAgfVxuICBdLFxuICBcInRhcmdldFwiOiBcIjxpbWcgc3JjPVxcXCJodHRwczovL21lZGlhLWlrLmNyb21hLmNvbS9wcm9kL2h0dHBzOi8vbWVkaWEuY3JvbWEuY29tL2ltYWdlL3VwbG9hZC92MTcwOTE4NjgxMi9Dcm9tYSUyMEFzc2V0cy9UZXN0X05pa2hpbC9IeWJyaWQlMjBIb21lcGFnZS9jYXRuYXYvMjkwMi9EZXNrdG9wL01vYmlsZXNfdWVodDVvLnBuZz90cj13LTEwMDBcXFwiIHNyY3NldD1cXFwiaHR0cHM6Ly9tZWRpYS1pay5jcm9tYS5jb20vcHJvZC9odHRwczovL21lZGlhLmNyb21hLmNvbS9pbWFnZS91cGxvYWQvdjE3MDkxODY4MTIvQ3JvbWElMjBBc3NldHMvVGVzdF9OaWtoaWwvSHlicmlkJTIwSG9tZXBhZ2UvY2F0bmF2LzI5MDIvRGVza3RvcC9Nb2JpbGVzX3VlaHQ1by5wbmc/dHI9dy0yNDAgMjQwdyxodHRwczovL21lZGlhLWlrLmNyb21hLmNvbS9wcm9kL2h0dHBzOi8vbWVkaWEuY3JvbWEuY29tL2ltYWdlL3VwbG9hZC92MTcwOTE4NjgxMi9Dcm9tYSUyMEFzc2V0cy9UZXN0X05pa2hpbC9IeWJyaWQlMjBIb21lcGFnZS9jYXRuYXYvMjkwMi9EZXNrdG9wL01vYmlsZXNfdWVodDVvLnBuZz90cj13LTM2MCAzNjB3LGh0dHBzOi8vbWVkaWEtaWsuY3JvbWEuY29tL3Byb2QvaHR0cHM6Ly9tZWRpYS5jcm9tYS5jb20vaW1hZ2UvdXBsb2FkL3YxNzA5MTg2ODEyL0Nyb21hJTIwQXNzZXRzL1Rlc3RfTmlraGlsL0h5YnJpZCUyMEhvbWVwYWdlL2NhdG5hdi8yOTAyL0Rlc2t0b3AvTW9iaWxlc191ZWh0NW8ucG5nP3RyPXctNDgwIDQ4MHcsaHR0cHM6Ly9tZWRpYS1pay5jcm9tYS5jb20vcHJvZC9odHRwczovL21lZGlhLmNyb21hLmNvbS9pbWFnZS91cGxvYWQvdjE3MDkxODY4MTIvQ3JvbWElMjBBc3NldHMvVGVzdF9OaWtoaWwvSHlicmlkJTIwSG9tZXBhZ2UvY2F0bmF2LzI5MDIvRGVza3RvcC9Nb2JpbGVzX3VlaHQ1by5wbmc/dHI9dy03MjAgNzIwdyxodHRwczovL21lZGlhLWlrLmNyb21hLmNvbS9wcm9kL2h0dHBzOi8vbWVkaWEuY3JvbWEuY29tL2ltYWdlL3VwbG9hZC92MTcwOTE4NjgxMi9Dcm9tYSUyMEFzc2V0cy9UZXN0X05pa2hpbC9IeWJyaWQlMjBIb21lcGFnZS9jYXRuYXYvMjkwMi9EZXNrdG9wL01vYmlsZXNfdWVodDVvLnBuZz90cj13LTEwMjQgMTAyNHcsaHR0cHM6Ly9tZWRpYS1pay5jcm9tYS5jb20vcHJvZC9odHRwczovL21lZGlhLmNyb21hLmNvbS9pbWFnZS91cGxvYWQvdjE3MDkxODY4MTIvQ3JvbWElMjBBc3NldHMvVGVzdF9OaWtoaWwvSHlicmlkJTIwSG9tZXBhZ2UvY2F0bmF2LzI5MDIvRGVza3RvcC9Nb2JpbGVzX3VlaHQ1by5wbmc/dHI9dy0xNDQwIDE0NDB3XFxcIiBzaXplcz1cXFwiKG1heC13aWR0aDogNzY4cHgpIDUwdncsIDMzdndcXFwiIGFsdD1cXFwidW5kZWZpbmVkXFxcIiBsb2FkaW5nPVxcXCJsYXp5XFxcIj5cIixcbiAgXCJ0aW1lU3RhbXBcIjogMTcxNTgzOTE1NTc4NCxcbiAgXCJ0eXBlXCI6IFwiY2xpY2tcIlxufSJ9XSwiY3VzdG9tZXJIYXNoIjpudWxsLCJlY2lkIjoiVW5rbm93biIsImNsaWVudElkIjoiQ1JPTUEtV0VCLUFQUCIsInNlc3Npb25JZCI6ImI2NWY1NGI0LTVjOTMtNDgyZS04Y2Y4LWMyMTBlY2ZmYTc4Zi45MjNiYjU3Zi05NTFiLTQ3ODAtOGNjMy0zZTdjODllOTA1ZmUiLCJicmFuZCI6IkNST01BIiwidGltZXN0YW1wIjoxNzE1ODM5MTU1Nzg1LCJkZXZpY2UiOiJNb3ppbGxhLzUuMCAoWDExOyBMaW51eCB4ODZfNjQpIEFwcGxlV2ViS2l0LzUzNy4zNiAoS0hUTUwsIGxpa2UgR2Vja28pIENocm9tZS8xMTQuMC4wLjAgU2FmYXJpLzUzNy4zNiIsImphcnZpc0lkIjoiNGQ2MDQ1NzAtYmFmYi00NTUxLWJhZDAtODUzODk3MWM3ZWUyIiwiYWR2ZXJ0aXNpbmdJZCI6bnVsbCwiYXBwVmVyc2lvbiI6IjAifQ=="}",
        BODY_END
    );

    ns_end_transaction("v1_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_4");
    ns_web_url("v1_4",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=customer-hash:null",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=tdl-sso-version:4.1.15",
        "HEADER=session:false",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tL3Bob25lcy13ZWFyYWJsZXMvbW9iaWxlLXBob25lcy9jLzEwIiwiZXZlbnREYXRhIjpbeyJ0aW1lc3RhbXAiOjE3MTU4MzkxNTU4NDcsImRhdGEiOiJ7XCJldmVudFwiOlwicGFnZUxvYWRcIixcInBheWxvYWRcIjp7XCJ1cmxcIjpcImh0dHBzOi8vd3d3LmNyb21hLmNvbS9waG9uZXMtd2VhcmFibGVzL21vYmlsZS1waG9uZXMvYy8xMFwiLFwiZGF0YVNvdXJjZVwiOlwicHVzaFN0YXRlXCJ9fSJ9XSwiY3VzdG9tZXJIYXNoIjpudWxsLCJlY2lkIjoiVW5rbm93biIsImNsaWVudElkIjoiQ1JPTUEtV0VCLUFQUCIsInNlc3Npb25JZCI6ImI2NWY1NGI0LTVjOTMtNDgyZS04Y2Y4LWMyMTBlY2ZmYTc4Zi45MjNiYjU3Zi05NTFiLTQ3ODAtOGNjMy0zZTdjODllOTA1ZmUiLCJicmFuZCI6IkNST01BIiwidGltZXN0YW1wIjoxNzE1ODM5MTU1ODQ3LCJkZXZpY2UiOiJNb3ppbGxhLzUuMCAoWDExOyBMaW51eCB4ODZfNjQpIEFwcGxlV2ViS2l0LzUzNy4zNiAoS0hUTUwsIGxpa2UgR2Vja28pIENocm9tZS8xMTQuMC4wLjAgU2FmYXJpLzUzNy4zNiIsImphcnZpc0lkIjoiNGQ2MDQ1NzAtYmFmYi00NTUxLWJhZDAtODUzODk3MWM3ZWUyIiwiYWR2ZXJ0aXNpbmdJZCI6bnVsbCwiYXBwVmVyc2lvbiI6IjAifQ=="}",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.croma.com/phones-wearables/mobile-phones/c/10", END_INLINE
    );

    ns_end_transaction("v1_4", NS_AUTO_STATUS);
    ns_page_think_time(42.617);

    //Page Auto split for Image Link 'undefined' Clicked by User
    ns_start_transaction("index_2");
    ns_web_url("index_2",
        "URL=https://www.facebook.com/tr/?id=1369867960112522&ev=PageView&dl=https%3A%2F%2Fwww.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&rl=&if=false&ts=1715839155877&sw=1523&sh=650&v=2.9.156&r=stable&ec=2&o=4126&fbp=fb.1.1715839140634.281453602&ler=empty&cdl=API_unavailable&it=1715839140500&coo=false&rqm=GET",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:image",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s31676959719108?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%200%3A59%3A15%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=homepage&g=https%3A%2F%2Fwww.croma.com%2F&cc=INR&server=www.croma.com&events=event4&c1=homepage&v1=D%3Dmid&c2=www.croma.com%2F&v2=new&c3=2024-5-16%200%3A59%3A15&l3=cat%20nav%3Amobiles%3Ahomepage&v4=guest%20user&v15=banner%3Amobiles&v16=banner_clicked&v17=homepage%3Acat%20nav&v39=RL9bd43344037f476081fbf32b9dc7acb2&v53=https%3A%2F%2Fwww.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&v71=banner%3Amobiles%7Chomepage%3Acat%20nav%7Cbanner_clicked&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5BotherClick%5D%20%3A%20Other%20Click&v156=cat%20nav%3Amobiles%3Ahomepage&pe=lnk_o&pev2=banner%3Amobiles&c.&a.&activitymap.&page=homepage&link=undefined&region=comp_0000BOKQ&pageIDType=1&.activitymap&.a&.c&pid=homepage&pidt=1&oid=function%28c%29%7Bvarq%3Db.Qa%28f%2Cc%2Ce%2Cl%29%3Ba.conf.ja%26%26%28q.oa%3Dx%29%3Breturnb.sh%28n%2Cq%2Cthis%2Carguments%29%7D&oidt=2&ot=IMG&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&lrt=50&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;s_tp;cto_bundle;s_ppv;_ga_DJ3RGVFHJN;s_nr30;s_sq", END_INLINE
    );

    ns_end_transaction("index_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_5");
    ns_web_url("v1_5",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tL3Bob25lcy13ZWFyYWJsZXMvbW9iaWxlLXBob25lcy9jLzEwIiwiZXZlbnREYXRhIjpbeyJ0aW1lc3RhbXAiOjE3MTU4MzkxNTU5NDAsImRhdGEiOiJ7XCJldmVudFwiOlwid2ViLXZpdGFsc1wiLFwiZGF0YVwiOntcIm5hbWVcIjpcIkxDUFwiLFwidmFsdWVcIjoyMTM4Ljg5OTk5OTYxODUzMDMsXCJyYXRpbmdcIjpcImdvb2RcIixcImRlbHRhXCI6MjEzOC44OTk5OTk2MTg1MzAzLFwiZW50cmllc1wiOlt7XCJuYW1lXCI6XCJcIixcImVudHJ5VHlwZVwiOlwibGFyZ2VzdC1jb250ZW50ZnVsLXBhaW50XCIsXCJzdGFydFRpbWVcIjoyMTM4Ljg5OTk5OTYxODUzMDMsXCJkdXJhdGlvblwiOjAsXCJzaXplXCI6MjkxNDAwLFwicmVuZGVyVGltZVwiOjAsXCJsb2FkVGltZVwiOjIxMzguODk5LFwiZmlyc3RBbmltYXRlZEZyYW1lVGltZVwiOjAsXCJpZFwiOlwiXCIsXCJ1cmxcIjpcImh0dHBzOi8vbWVkaWEtaWsuY3JvbWEuY29tL3Byb2QvaHR0cHM6Ly9tZWRpYS5jcm9tYS5jb20vaW1hZ2UvdXBsb2FkL3YxNzE1MjU3NjU4L0Nyb21hJTIwQXNzZXRzL0NNUy9Ib21lcGFnZSUyMEJhbm5lcnMvSFAlMjBSb3RhdGluZy8yMDI0L01heS8xMDA1MjAyNC9EZXNrdG9wL0hQX09MQ2FtcGFpZ25fOE1heTIwMjRfc216ZHV2LmpwZ1wifV0sXCJpZFwiOlwidjMtMTcxNTgzOTEzOTI3MS0xMTUzMTE0ODg3ODU5XCIsXCJuYXZpZ2F0aW9uVHlwZVwiOlwibmF2aWdhdGVcIn19In1dLCJjdXN0b21lckhhc2giOm51bGwsImVjaWQiOiJVbmtub3duIiwiY2xpZW50SWQiOiJDUk9NQS1XRUItQVBQIiwic2Vzc2lvbklkIjoiYjY1ZjU0YjQtNWM5My00ODJlLThjZjgtYzIxMGVjZmZhNzhmLjkyM2JiNTdmLTk1MWItNDc4MC04Y2MzLTNlN2M4OWU5MDVmZSIsImJyYW5kIjoiQ1JPTUEiLCJ0aW1lc3RhbXAiOjE3MTU4MzkxNTU5NDAsImRldmljZSI6Ik1vemlsbGEvNS4wIChYMTE7IExpbnV4IHg4Nl82NCkgQXBwbGVXZWJLaXQvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lLzExNC4wLjAuMCBTYWZhcmkvNTM3LjM2IiwiamFydmlzSWQiOiI0ZDYwNDU3MC1iYWZiLTQ1NTEtYmFkMC04NTM4OTcxYzdlZTIiLCJhZHZlcnRpc2luZ0lkIjpudWxsLCJhcHBWZXJzaW9uIjoiMCJ9"}",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.croma.com/assets/favicon.ico", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;mbox;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;RT;cto_bundle;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv", END_INLINE
    );

    ns_end_transaction("v1_5", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("events_2");
    ns_web_url("events_2",
        "URL=https://api.tatadigital.com/api/v1.1/msd/events",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Program-Id:01eae2ec-0576-1000-bbea-86e16dcb4b79",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"page_type":"clp","content_type":"product","action_name":"view","event_name":"page_view","correlation_id":"sssaasss","medium":"pixel","metadata":{},"source":"CROMA","user_id_type":["CROMA"],"user_id":["08vHUbbT7lzWpSwZ"],"mad_uuid":"08vHUbbT7lzWpSwZ","utm":{"utm_source":"","utm_medium":"","utm_campaign":""},"epoch":"1715839155944","content_info":{"source_id":"10"},"pincode":""}",
        BODY_END
    );

    ns_end_transaction("events_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("X0_fields_FULL");
    ns_web_url("X0_fields_FULL",
        "URL=https://api.croma.com/cmstemplate/allchannels/v1/page?pageType=ContentPage&pageLabelOrId=/c/10&fields=FULL",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=httpsAgent:[object Object]",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;SameSite;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;_ga_DJ3RGVFHJN;ADRUM_BTa;ADRUM_BT1;s_nr30;s_sq;s_tp;s_ppv"
    );

    ns_end_transaction("X0_fields_FULL", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("X0");
    ns_web_url("X0",
        "URL=https://api.croma.com/searchservices/v1/category/10?currentPage=0&query=%3Arelevance&fields=FULL&channel=WEB&channelCode=",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=httpsAgent:[object Object]",
        "HEADER=eventsSequenceId:31122020221022020331100322",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;SameSite;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;_ga_DJ3RGVFHJN;ADRUM_BTa;ADRUM_BT1;s_nr30;s_sq;s_tp;s_ppv"
    );

    ns_end_transaction("X0", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("page_4");
    ns_web_url("page_4",
        "URL=https://api.croma.com/cmstemplate/allchannels/v1/page?pageType=CategoryPage&code=10&fields=DEFAULT",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=eventsSequenceId:31122020221022020331100322",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;SameSite;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;_ga_DJ3RGVFHJN;ADRUM_BTa;ADRUM_BT1;s_nr30;s_sq;s_tp;s_ppv"
    );

    ns_end_transaction("page_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_4");
    ns_web_url("collect_4",
        "URL=https://analytics.google.com/g/collect?v=2&tid=G-DJ3RGVFHJN&gtm=45je45f0v893318240z8889407033za200&_p=1715839139273&gcd=13l3l3l3l1&npa=0&dma=0&cid=1944455297.1715839141&ul=en-us&sr=1523x650&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&frm=0&pscdl=noapi&sid=1715839140&sct=1&seg=0&dl=https%3A%2F%2Fwww.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&dt=Croma%20Electronics%20%7C%20Online%20Electronics%20Shopping%20%7C%20Buy%20Electronics%20Online&_s=4&tfd=18396",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "en=scroll_depth&epn.scroll_percentage=50&_et=412
en=scroll_depth&epn.scroll_percentage=75&_et=15",
        BODY_END
    );

    ns_end_transaction("collect_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_5");
    ns_web_url("collect_5",
        "URL=https://analytics.google.com/g/collect?v=2&tid=G-DJ3RGVFHJN&gtm=45je45f0v893318240za200&_p=1715839139273&gcd=13l3l3l3l1&npa=0&dma=0&cid=1944455297.1715839141&ul=en-us&sr=1523x650&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&frm=0&pscdl=noapi&_eu=AEA&_s=5&sid=1715839140&sct=1&seg=0&dl=https%3A%2F%2Fwww.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&dt=Croma%20Electronics%20%7C%20Online%20Electronics%20Shopping%20%7C%20Buy%20Electronics%20Online&en=scroll&epn.percent_scrolled=90&_et=5&tfd=18415",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("collect_5", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_6");
    ns_web_url("v1_6",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tL3Bob25lcy13ZWFyYWJsZXMvbW9iaWxlLXBob25lcy9jLzEwIiwiZXZlbnREYXRhIjpbeyJ0aW1lc3RhbXAiOjE3MTU4MzkxNTYwNDYsImRhdGEiOiJ7XCJldmVudFwiOlwid2ViLXZpdGFsc1wiLFwiZGF0YVwiOntcIm5hbWVcIjpcIkZJRFwiLFwidmFsdWVcIjoxLFwicmF0aW5nXCI6XCJnb29kXCIsXCJkZWx0YVwiOjEsXCJlbnRyaWVzXCI6W3tcIm5hbWVcIjpcInBvaW50ZXJkb3duXCIsXCJlbnRyeVR5cGVcIjpcImZpcnN0LWlucHV0XCIsXCJzdGFydFRpbWVcIjoxNzkzMy4xOTk5OTg4NTU1OSxcImR1cmF0aW9uXCI6MzIsXCJwcm9jZXNzaW5nU3RhcnRcIjoxNzkzNC4xOTk5OTg4NTU1OSxcInByb2Nlc3NpbmdFbmRcIjoxNzkzNS4xOTk5OTg4NTU1OSxcImNhbmNlbGFibGVcIjp0cnVlfV0sXCJpZFwiOlwidjMtMTcxNTgzOTEzOTI3MC02ODA5MDg1NzQ2NjA4XCIsXCJuYXZpZ2F0aW9uVHlwZVwiOlwibmF2aWdhdGVcIn19In1dLCJjdXN0b21lckhhc2giOm51bGwsImVjaWQiOiJVbmtub3duIiwiY2xpZW50SWQiOiJDUk9NQS1XRUItQVBQIiwic2Vzc2lvbklkIjoiYjY1ZjU0YjQtNWM5My00ODJlLThjZjgtYzIxMGVjZmZhNzhmLjkyM2JiNTdmLTk1MWItNDc4MC04Y2MzLTNlN2M4OWU5MDVmZSIsImJyYW5kIjoiQ1JPTUEiLCJ0aW1lc3RhbXAiOjE3MTU4MzkxNTYwNDYsImRldmljZSI6Ik1vemlsbGEvNS4wIChYMTE7IExpbnV4IHg4Nl82NCkgQXBwbGVXZWJLaXQvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lLzExNC4wLjAuMCBTYWZhcmkvNTM3LjM2IiwiamFydmlzSWQiOiI0ZDYwNDU3MC1iYWZiLTQ1NTEtYmFkMC04NTM4OTcxYzdlZTIiLCJhZHZlcnRpc2luZ0lkIjpudWxsLCJhcHBWZXJzaW9uIjoiMCJ9"}",
        BODY_END
    );

    ns_end_transaction("v1_6", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_7");
    ns_web_url("v1_7",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=customer-hash:null",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=tdl-sso-version:4.1.15",
        "HEADER=session:false",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tL3Bob25lcy13ZWFyYWJsZXMvbW9iaWxlLXBob25lcy9jLzEwIiwiZXZlbnREYXRhIjpbeyJ0aW1lc3RhbXAiOjE3MTU4MzkxNTY3NDksImRhdGEiOiJ7XCJldmVudFwiOlwicGFnZUxvYWRcIixcInBheWxvYWRcIjp7XCJ1cmxcIjpcImh0dHBzOi8vd3d3LmNyb21hLmNvbS9waG9uZXMtd2VhcmFibGVzL21vYmlsZS1waG9uZXMvYy8xMFwiLFwiZGF0YVNvdXJjZVwiOlwicmVwbGFjZVN0YXRlXCJ9fSJ9XSwiY3VzdG9tZXJIYXNoIjpudWxsLCJlY2lkIjoiVW5rbm93biIsImNsaWVudElkIjoiQ1JPTUEtV0VCLUFQUCIsInNlc3Npb25JZCI6ImI2NWY1NGI0LTVjOTMtNDgyZS04Y2Y4LWMyMTBlY2ZmYTc4Zi45MjNiYjU3Zi05NTFiLTQ3ODAtOGNjMy0zZTdjODllOTA1ZmUiLCJicmFuZCI6IkNST01BIiwidGltZXN0YW1wIjoxNzE1ODM5MTU2NzQ5LCJkZXZpY2UiOiJNb3ppbGxhLzUuMCAoWDExOyBMaW51eCB4ODZfNjQpIEFwcGxlV2ViS2l0LzUzNy4zNiAoS0hUTUwsIGxpa2UgR2Vja28pIENocm9tZS8xMTQuMC4wLjAgU2FmYXJpLzUzNy4zNiIsImphcnZpc0lkIjoiNGQ2MDQ1NzAtYmFmYi00NTUxLWJhZDAtODUzODk3MWM3ZWUyIiwiYWR2ZXJ0aXNpbmdJZCI6bnVsbCwiYXBwVmVyc2lvbiI6IjAifQ=="}",
        BODY_END,
        INLINE_URLS,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1684242615/Croma%20Assets/CMS/CAtegory/Mobile%20phone%20-%20C10/16-05-23/Desktop/Main%20Banner/D_main-banner_hat0zq.png?tr=w-2048", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://www.croma.com/phones-wearables/mobile-phones/c/item?.media?.url", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;mbox;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;RT;cto_bundle;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/undefined?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1689855040/Croma%20Assets/CMS/CAtegory/Mobile%20phone%20-%20C10/blog/Mobile-Buying-Guide_20July2023_gbfrzl.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1684242611/Croma%20Assets/CMS/CAtegory/Mobile%20phone%20-%20C10/16-05-23/Desktop/Your%20Smartphone%2C%20Your%20Use/4split_PCP_smartphonesfor_content-creation_15May2023_zczhv8.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1698219482/Croma%20Assets/CMS/Bugs/01012023/040423/mob%20pcp/4split_PCP_smartphonesfor_Gaming_25Oct2023_w5stjf.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1698219485/Croma%20Assets/CMS/Bugs/01012023/040423/mob%20pcp/4split_PCP_smartphonesfor_Productivity_25Oct2023_seszo7.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1698219477/Croma%20Assets/CMS/Bugs/01012023/040423/mob%20pcp/4split_PCP_smartphonesfor_5GNetwork_25Oct2023_i1z2wc.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1698219482/Croma%20Assets/CMS/Bugs/01012023/040423/mob%20pcp/4split_PCP_smartphonesfor_Daylonguse_25Oct2023_hilmcr.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1698219480/Croma%20Assets/CMS/Bugs/01012023/040423/mob%20pcp/4split_PCP_smartphonesfor_Bingewatching_25Oct2023_ofkw6y.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1687517134/Croma%20Assets/CMS/PCP/23rd%20June/4spli_PCP_shopbybrand_apple_23june2023_sk5llx.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1666000242/Croma%20Assets/CMS/CAtegory/Mobile%20phone%20-%20C10/NEW%20PCP%20DESIGN%20-%20OCT/Updated/4spli_PCP_shopbybrand_vivo_27sep2022_knaawj.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1666000241/Croma%20Assets/CMS/CAtegory/Mobile%20phone%20-%20C10/NEW%20PCP%20DESIGN%20-%20OCT/Updated/4spli_PCP_shopbybrand_oneplus_27sep2022_bdlvsd.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1697631771/Croma%20Assets/CMS/PCP/Mobile%20PCP/18-10-2023/4spli_PCP_shopbybrand_nothing_180ct2023_xy128n.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1666000241/Croma%20Assets/CMS/CAtegory/Mobile%20phone%20-%20C10/NEW%20PCP%20DESIGN%20-%20OCT/Updated/4spli_PCP_shopbybrand_oppo_27sep2022_aga789.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1667542217/Croma%20Assets/CMS/CAtegory/Mobile%20phone%20-%20C10/4spli_PCP_shopbybrand_realme_3nov2022_u4p8mv.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1666000242/Croma%20Assets/CMS/CAtegory/Mobile%20phone%20-%20C10/NEW%20PCP%20DESIGN%20-%20OCT/Updated/4spli_PCP_shopbybrand_tecno_27sep2022_pi8az1.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1666000242/Croma%20Assets/CMS/CAtegory/Mobile%20phone%20-%20C10/NEW%20PCP%20DESIGN%20-%20OCT/Updated/4spli_PCP_shopbybrand_mi_27sep2022_tpwcdb.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1693910684/Croma%20Assets/CMS/Brand%20Page%20Banners/2023/Apple/4split_PCP_shopbybrand_5sep2023_mfoqdq.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1684242609/Croma%20Assets/CMS/CAtegory/Mobile%20phone%20-%20C10/16-05-23/Desktop/For%20Your%20Budget/4Split_PCP_shopbyprice_below5K_15May2023_k8wvhd.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1684242610/Croma%20Assets/CMS/CAtegory/Mobile%20phone%20-%20C10/16-05-23/Desktop/For%20Your%20Budget/4Split_PCP_shopbyprice_5Kto10K_15May2023_xm9iqq.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1684242606/Croma%20Assets/CMS/CAtegory/Mobile%20phone%20-%20C10/16-05-23/Desktop/For%20Your%20Budget/4Split_PCP_shopbyprice_10Kto20K_15May2023_fuop6n.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1684242607/Croma%20Assets/CMS/CAtegory/Mobile%20phone%20-%20C10/16-05-23/Desktop/For%20Your%20Budget/4Split_PCP_shopbyprice_20Kto30K_15May2023_yrfh2g.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1684242609/Croma%20Assets/CMS/CAtegory/Mobile%20phone%20-%20C10/16-05-23/Desktop/For%20Your%20Budget/4Split_PCP_shopbyprice_30Kto50K_15May2023_qspz8w.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1684242607/Croma%20Assets/CMS/CAtegory/Mobile%20phone%20-%20C10/16-05-23/Desktop/For%20Your%20Budget/4Split_PCP_shopbyprice_above50K_15May2023_bznsjt.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE
    );

    ns_end_transaction("v1_7", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_8");
    ns_web_url("v1_8",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tL3Bob25lcy13ZWFyYWJsZXMvbW9iaWxlLXBob25lcy9jLzEwIiwiZXZlbnREYXRhIjpbeyJ0aW1lc3RhbXAiOjE3MTU4MzkxNjQzNjksImRhdGEiOiJ7XG4gIFwidXJsXCI6IHtcbiAgICBcImFuY2VzdG9yT3JpZ2luc1wiOiB7fSxcbiAgICBcImhyZWZcIjogXCJodHRwczovL3d3dy5jcm9tYS5jb20vcGhvbmVzLXdlYXJhYmxlcy9tb2JpbGUtcGhvbmVzL2MvMTBcIixcbiAgICBcIm9yaWdpblwiOiBcImh0dHBzOi8vd3d3LmNyb21hLmNvbVwiLFxuICAgIFwicHJvdG9jb2xcIjogXCJodHRwczpcIixcbiAgICBcImhvc3RcIjogXCJ3d3cuY3JvbWEuY29tXCIsXG4gICAgXCJob3N0bmFtZVwiOiBcInd3dy5jcm9tYS5jb21cIixcbiAgICBcInBvcnRcIjogXCJcIixcbiAgICBcInBhdGhuYW1lXCI6IFwiL3Bob25lcy13ZWFyYWJsZXMvbW9iaWxlLXBob25lcy9jLzEwXCIsXG4gICAgXCJzZWFyY2hcIjogXCJcIixcbiAgICBcImhhc2hcIjogXCJcIlxuICB9LFxuICBcInBhdGhcIjogW1xuICAgIHtcbiAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJpbWdcIixcbiAgICAgIFwiaWRcIjogXCJcIixcbiAgICAgIFwiY2xhc3NcIjogXCJcIlxuICAgIH0sXG4gICAge1xuICAgICAgXCJlbGVtZW50VHlwZVwiOiBcInNwYW5cIixcbiAgICAgIFwiaWRcIjogXCJcIixcbiAgICAgIFwiY2xhc3NcIjogXCIgbGF6eS1sb2FkLWltYWdlLWJhY2tncm91bmQgYmx1ciBsYXp5LWxvYWQtaW1hZ2UtbG9hZGVkXCJcbiAgICB9LFxuICAgIHtcbiAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJhXCIsXG4gICAgICBcImlkXCI6IFwiXCIsXG4gICAgICBcImNsYXNzXCI6IFwiXCJcbiAgICB9LFxuICAgIHtcbiAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJkaXZcIixcbiAgICAgIFwiaWRcIjogXCJcIixcbiAgICAgIFwiY2xhc3NcIjogXCJcIlxuICAgIH0sXG4gICAge1xuICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImRpdlwiLFxuICAgICAgXCJpZFwiOiBcIlwiLFxuICAgICAgXCJjbGFzc1wiOiBcIlwiXG4gICAgfSxcbiAgICB7XG4gICAgICBcImVsZW1lbnRUeXBlXCI6IFwiZGl2XCIsXG4gICAgICBcImlkXCI6IFwiXCIsXG4gICAgICBcImNsYXNzXCI6IFwiU3RhY2tCYW5uZXJcIlxuICAgIH0sXG4gICAge1xuICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImRpdlwiLFxuICAgICAgXCJpZFwiOiBcIlwiLFxuICAgICAgXCJjbGFzc1wiOiBcIlwiXG4gICAgfSxcbiAgICB7XG4gICAgICBcImVsZW1lbnRUeXBlXCI6IFwiZGl2XCIsXG4gICAgICBcImlkXCI6IFwiXCIsXG4gICAgICBcImNsYXNzXCI6IFwiXCJcbiAgICB9LFxuICAgIHtcbiAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJkaXZcIixcbiAgICAgIFwiaWRcIjogXCJcIixcbiAgICAgIFwiY2xhc3NcIjogXCJzZWMtY29udFwiXG4gICAgfSxcbiAgICB7XG4gICAgICBcImVsZW1lbnRUeXBlXCI6IFwiZGl2XCIsXG4gICAgICBcImlkXCI6IFwiXCIsXG4gICAgICBcImNsYXNzXCI6IFwiY29udGFpbmVyIHN0YWNrLWJhbm5lclwiXG4gICAgfSxcbiAgICB7XG4gICAgICBcImVsZW1lbnRUeXBlXCI6IFwiZGl2XCIsXG4gICAgICBcImlkXCI6IFwiY29tcF8wMDAwNzlNUFwiLFxuICAgICAgXCJjbGFzc1wiOiBcImNwLXNlY3Rpb24taG9tZSBiYW5uZXItc3BhY2luZ1wiXG4gICAgfSxcbiAgICB7XG4gICAgICBcImVsZW1lbnRUeXBlXCI6IFwiZGl2XCIsXG4gICAgICBcImlkXCI6IFwiXCIsXG4gICAgICBcImNsYXNzXCI6IFwiXCJcbiAgICB9LFxuICAgIHtcbiAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJkaXZcIixcbiAgICAgIFwiaWRcIjogXCJcIixcbiAgICAgIFwiY2xhc3NcIjogXCJicy1jb250ZW50XCJcbiAgICB9LFxuICAgIHtcbiAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJkaXZcIixcbiAgICAgIFwiaWRcIjogXCJcIixcbiAgICAgIFwiY2xhc3NcIjogXCJtYWluQ29udGFpbmVyXCJcbiAgICB9LFxuICAgIHtcbiAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJkaXZcIixcbiAgICAgIFwiaWRcIjogXCJcIixcbiAgICAgIFwiY2xhc3NcIjogXCJcIlxuICAgIH0sXG4gICAge1xuICAgICAgXCJlbGVtZW50VHlwZVwiOiBcIm1haW5cIixcbiAgICAgIFwiaWRcIjogXCJjb250YWluZXJcIixcbiAgICAgIFwiY2xhc3NcIjogXCJkYXJrLXRoZW1lXCJcbiAgICB9XG4gIF0sXG4gIFwidGFyZ2V0XCI6IFwiPGltZyBzcmM9XFxcImh0dHBzOi8vbWVkaWEtaWsuY3JvbWEuY29tL3Byb2QvaHR0cHM6Ly9tZWRpYS5jcm9tYS5jb20vaW1hZ2UvdXBsb2FkL3YxNzA3ODI3MTQ1L0Nyb21hJTIwQXNzZXRzL0NNUy9MUCUyMFBhZ2UlMjBCYW5uZXJzLzIwMjQvUENQL01vYmlsZS9GZWIvMTN0aC9wY3Bfc21hcnRwaG9uZV9uY19uZXdhdGNyb21hX09uZVBsdXMxMlJfMTNGZWIyMDI0X2hudnh4Yy5wbmc/dHI9dy0xMDAwXFxcIiBzcmNzZXQ9XFxcImh0dHBzOi8vbWVkaWEtaWsuY3JvbWEuY29tL3Byb2QvaHR0cHM6Ly9tZWRpYS5jcm9tYS5jb20vaW1hZ2UvdXBsb2FkL3YxNzA3ODI3MTQ1L0Nyb21hJTIwQXNzZXRzL0NNUy9MUCUyMFBhZ2UlMjBCYW5uZXJzLzIwMjQvUENQL01vYmlsZS9GZWIvMTN0aC9wY3Bfc21hcnRwaG9uZV9uY19uZXdhdGNyb21hX09uZVBsdXMxMlJfMTNGZWIyMDI0X2hudnh4Yy5wbmc/dHI9dy0yNDAgMjQwdyxodHRwczovL21lZGlhLWlrLmNyb21hLmNvbS9wcm9kL2h0dHBzOi8vbWVkaWEuY3JvbWEuY29tL2ltYWdlL3VwbG9hZC92MTcwNzgyNzE0NS9Dcm9tYSUyMEFzc2V0cy9DTVMvTFAlMjBQYWdlJTIwQmFubmVycy8yMDI0L1BDUC9Nb2JpbGUvRmViLzEzdGgvcGNwX3NtYXJ0cGhvbmVfbmNfbmV3YXRjcm9tYV9PbmVQbHVzMTJSXzEzRmViMjAyNF9obnZ4eGMucG5nP3RyPXctMzYwIDM2MHcsaHR0cHM6Ly9tZWRpYS1pay5jcm9tYS5jb20vcHJvZC9odHRwczovL21lZGlhLmNyb21hLmNvbS9pbWFnZS91cGxvYWQvdjE3MDc4MjcxNDUvQ3JvbWElMjBBc3NldHMvQ01TL0xQJTIwUGFnZSUyMEJhbm5lcnMvMjAyNC9QQ1AvTW9iaWxlL0ZlYi8xM3RoL3BjcF9zbWFydHBob25lX25jX25ld2F0Y3JvbWFfT25lUGx1czEyUl8xM0ZlYjIwMjRfaG52eHhjLnBuZz90cj13LTQ4MCA0ODB3LGh0dHBzOi8vbWVkaWEtaWsuY3JvbWEuY29tL3Byb2QvaHR0cHM6Ly9tZWRpYS5jcm9tYS5jb20vaW1hZ2UvdXBsb2FkL3YxNzA3ODI3MTQ1L0Nyb21hJTIwQXNzZXRzL0NNUy9MUCUyMFBhZ2UlMjBCYW5uZXJzLzIwMjQvUENQL01vYmlsZS9GZWIvMTN0aC9wY3Bfc21hcnRwaG9uZV9uY19uZXdhdGNyb21hX09uZVBsdXMxMlJfMTNGZWIyMDI0X2hudnh4Yy5wbmc/dHI9dy03MjAgNzIwdyxodHRwczovL21lZGlhLWlrLmNyb21hLmNvbS9wcm9kL2h0dHBzOi8vbWVkaWEuY3JvbWEuY29tL2ltYWdlL3VwbG9hZC92MTcwNzgyNzE0NS9Dcm9tYSUyMEFzc2V0cy9DTVMvTFAlMjBQYWdlJTIwQmFubmVycy8yMDI0L1BDUC9Nb2JpbGUvRmViLzEzdGgvcGNwX3NtYXJ0cGhvbmVfbmNfbmV3YXRjcm9tYV9PbmVQbHVzMTJSXzEzRmViMjAyNF9obnZ4eGMucG5nP3RyPXctMTAyNCAxMDI0d1xcXCIgc2l6ZXM9XFxcIjUwdndcXFwiIGFsdD1cXFwidW5kZWZpbmVkXFxcIj5cIixcbiAgXCJ0aW1lU3RhbXBcIjogMTcxNTgzOTE2NDM2OSxcbiAgXCJ0eXBlXCI6IFwiY2xpY2tcIlxufSJ9XSwiY3VzdG9tZXJIYXNoIjpudWxsLCJlY2lkIjoiVW5rbm93biIsImNsaWVudElkIjoiQ1JPTUEtV0VCLUFQUCIsInNlc3Npb25JZCI6ImI2NWY1NGI0LTVjOTMtNDgyZS04Y2Y4LWMyMTBlY2ZmYTc4Zi45MjNiYjU3Zi05NTFiLTQ3ODAtOGNjMy0zZTdjODllOTA1ZmUiLCJicmFuZCI6IkNST01BIiwidGltZXN0YW1wIjoxNzE1ODM5MTY0MzcwLCJkZXZpY2UiOiJNb3ppbGxhLzUuMCAoWDExOyBMaW51eCB4ODZfNjQpIEFwcGxlV2ViS2l0LzUzNy4zNiAoS0hUTUwsIGxpa2UgR2Vja28pIENocm9tZS8xMTQuMC4wLjAgU2FmYXJpLzUzNy4zNiIsImphcnZpc0lkIjoiNGQ2MDQ1NzAtYmFmYi00NTUxLWJhZDAtODUzODk3MWM3ZWUyIiwiYWR2ZXJ0aXNpbmdJZCI6bnVsbCwiYXBwVmVyc2lvbiI6IjAifQ=="}",
        BODY_END,
        INLINE_URLS,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s31264951814282?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%200%3A59%3A17%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=plp&g=https%3A%2F%2Fwww.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&c.&getNewRepeat=3.0.1&getPageLoadTime=2.0.2&performanceWriteFull=n%2Fa&performanceWritePart=n%2Fa&performanceCheck=n%2Fa&p_fo=3.0&inList=3.0&apl=4.0&getPreviousValue=3.0.1&.c&cc=INR&ch=croma%3Aplp&server=www.croma.com&events=event2%2Cevent20%2Cevent21%2Cevent49%2Cevent1%3D1.9&aamb=6G1ynYcLPuiQxYZrsz_pkqfLG9yMXBpb2zX5dvJdYQJzPXImdj0y&c1=plp&v1=D%3Dmid&c2=www.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&v2=new&c3=2024-5-16%200%3A59%3A17&c4=homepage&v4=guest%20user&c9=TypeError%3A%20Cannot%20read%20properties%20of%20undefined%20%28reading%20%27productSKU%27%29&v38=homepage%20%7C%20highestPercentViewed%3D124%20%7C%20initialPercentViewed%3D58&v51=1.9&v76=%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5Bpageload%5D%20%3A%20Send%20Beacon&v198=www.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&lrt=123&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_sq;_ga_DJ3RGVFHJN;s_nr30;s_tp;s_ppv", END_INLINE,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s39062410652046?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%200%3A59%3A17%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=plp&g=https%3A%2F%2Fwww.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&cc=INR&server=www.croma.com&events=event55%2Cevent72%2Cevent20%2Cevent21%2Cevent50%3Dundefined&c1=plp&v1=D%3Dmid&c2=www.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&v2=new&c3=2024-5-16%200%3A59%3A17&l3=hero%20banner%3Abuy%20mobile%20phones%20online%3Aplp&c4=homepage&v4=guest%20user&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5BImpression%5D%20%3A%20impression&v156=hero%20banner%3Abuy%20mobile%20phones%20online%3Aplp&pe=lnk_o&pev2=impression%3A%20tracking&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&lrt=49&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_sq;_ga_DJ3RGVFHJN;s_tp;s_ppv;s_nr30", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1708090137/Croma%20Assets/CMS/LP%20Page%20Banners/2024/PCP/Mobile/Feb/16th/PCP_3split_flagship_OP_12Feb2024_eakj7y.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_sq;_ga_DJ3RGVFHJN;s_tp;s_ppv;s_nr30", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1708090249/Croma%20Assets/CMS/LP%20Page%20Banners/2024/PCP/Mobile/Feb/16th/PCP_3split_flagship_Samsung_12Feb2024_acryjq.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_sq;_ga_DJ3RGVFHJN;s_tp;s_ppv;s_nr30", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1708090250/Croma%20Assets/CMS/LP%20Page%20Banners/2024/PCP/Mobile/Feb/16th/PCP_3split_flagship_Vivo_12Feb2024_rrtetd.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_sq;_ga_DJ3RGVFHJN;s_tp;s_ppv;s_nr30", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1708090128/Croma%20Assets/CMS/LP%20Page%20Banners/2024/PCP/Mobile/Feb/16th/PCP_3split_flagship_iphone_12Feb2024_ynpfpe.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;s_ips;cto_bundle;s_sq;_ga_DJ3RGVFHJN;s_tp;s_ppv;s_nr30", END_INLINE
    );

    ns_end_transaction("v1_8", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_6");
    ns_web_url("collect_6",
        "URL=https://analytics.google.com/g/collect?v=2&tid=G-DJ3RGVFHJN&gtm=45je45f0v893318240z8889407033za200&_p=1715839139273&gcd=13l3l3l3l1&npa=0&dma=0&cid=1944455297.1715839141&ul=en-us&sr=1523x650&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&frm=0&pscdl=noapi&_s=6&sid=1715839140&sct=1&seg=0&dl=https%3A%2F%2Fwww.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&dt=Croma%20Electronics%20%7C%20Online%20Electronics%20Shopping%20%7C%20Buy%20Electronics%20Online&en=scroll_depth&epn.scroll_percentage=100&_et=16&tfd=20171",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.facebook.com/tr/?id=1369867960112522&ev=PageView&dl=https%3A%2F%2Fwww.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&rl=&if=false&ts=1715839157808&sw=1523&sh=650&v=2.9.156&r=stable&ec=3&o=4126&fbp=fb.1.1715839140634.281453602&ler=empty&cdl=API_unavailable&it=1715839140500&coo=false&eid=12656602678985&tm=1&rqm=GET", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googleadservices.com/pagead/conversion/609902077/?random=1715839157810&cv=11&fst=1715839157810&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=Mobile%20Phones%20%7C%20Buy%20Latest%20Mobiles%20Online%20at%20Best%20Prices%20in%20India%20%7C%20Croma&value=0&bttype=purchase&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&rfmt=3&fmt=4", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googleadservices.com/pagead/conversion/1006316414/?random=1715839157814&cv=11&fst=1715839157814&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Mobile%20Phones%20%7C%20Buy%20Latest%20Mobiles%20Online%20at%20Best%20Prices%20in%20India%20%7C%20Croma&value=0&bttype=purchase&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&rfmt=3&fmt=4", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1006316414/?random=1190929830&cv=11&fst=1715839157814&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Mobile%20Phones%20%7C%20Buy%20Latest%20Mobiles%20Online%20at%20Best%20Prices%20in%20India%20%7C%20Croma&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtbqPZQSO9q5Jhe_q4-FzVmwl9O6JLyfsSg&pscrd=IhMI3-uE_L6RhgMVWkedCR0Q1wo_MgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/609902077/?random=2033588916&cv=11&fst=1715839157810&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=Mobile%20Phones%20%7C%20Buy%20Latest%20Mobiles%20Online%20at%20Best%20Prices%20in%20India%20%7C%20Croma&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtcixrlYERGA2e-1Jcmmcq4rLw1di07A7XA&pscrd=IhMIhPGE_L6RhgMVAmCdCR18dgNKMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-conversion/609902077/?random=2033588916&cv=11&fst=1715839157810&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=Mobile%20Phones%20%7C%20Buy%20Latest%20Mobiles%20Online%20at%20Best%20Prices%20in%20India%20%7C%20Croma&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMIhPGE_L6RhgMVAmCdCR18dgNKMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtq7N_cSEV5EZCngR7_lQFcPIMf8gn2sg&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtX_eI3xPit5zj1vVB-w1bAVVpwXLwLxoaQ&random=2899178660", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-conversion/1006316414/?random=1190929830&cv=11&fst=1715839157814&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Mobile%20Phones%20%7C%20Buy%20Latest%20Mobiles%20Online%20at%20Best%20Prices%20in%20India%20%7C%20Croma&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMI3-uE_L6RhgMVWkedCR0Q1wo_MgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtqe05yfFddCv6EcW8H8HuuRSXJYzjNwQ&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtTaERTiehzGJ3X4sveJM30hLxCKKHHNkBg&random=1847846371", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-conversion/609902077/?random=2033588916&cv=11&fst=1715839157810&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=Mobile%20Phones%20%7C%20Buy%20Latest%20Mobiles%20Online%20at%20Best%20Prices%20in%20India%20%7C%20Croma&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMIhPGE_L6RhgMVAmCdCR18dgNKMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtq7N_cSEV5EZCngR7_lQFcPIMf8gn2sg&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtX_eI3xPit5zj1vVB-w1bAVVpwXLwLxoaQ&random=2899178660&ipr=y", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-conversion/1006316414/?random=1190929830&cv=11&fst=1715839157814&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Mobile%20Phones%20%7C%20Buy%20Latest%20Mobiles%20Online%20at%20Best%20Prices%20in%20India%20%7C%20Croma&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMI3-uE_L6RhgMVWkedCR0Q1wo_MgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtqe05yfFddCv6EcW8H8HuuRSXJYzjNwQ&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtTaERTiehzGJ3X4sveJM30hLxCKKHHNkBg&random=1847846371&ipr=y", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1689855039/Croma%20Assets/CMS/CAtegory/Mobile%20phone%20-%20C10/blog/10-things-your-smartphone-can-do_20July2023_bz6mmo.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_sq;s_nr30;_ga_DJ3RGVFHJN;s_ips;s_tp;s_ppv", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1689855040/Croma%20Assets/CMS/CAtegory/Mobile%20phone%20-%20C10/blog/A-checklist-for-setting-up-new-phone_20July2023_yhr8gt.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_sq;s_nr30;_ga_DJ3RGVFHJN;s_ips;s_tp;s_ppv", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1689855041/Croma%20Assets/CMS/CAtegory/Mobile%20phone%20-%20C10/blog/How-to-reset-your-mobile-phone_20July2023_fzhbcg.png?tr=w-720", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_sq;s_nr30;_ga_DJ3RGVFHJN;s_ips;s_tp;s_ppv", END_INLINE
    );

    ns_end_transaction("collect_6", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_7");
    ns_web_url("collect_7",
        "URL=https://analytics.google.com/g/collect?v=2&tid=G-DJ3RGVFHJN&gtm=45je45f0v893318240za200&_p=1715839139273&gcd=13l3l3l3l1&npa=0&dma=0&cid=1944455297.1715839141&ul=en-us&sr=1523x650&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&frm=0&pscdl=noapi&_eu=AEA&_s=7&dl=https%3A%2F%2Fwww.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&dr=https%3A%2F%2Fwww.croma.com%2F&sid=1715839140&sct=1&seg=1&dt=Mobile%20Phones%20%7C%20Buy%20Latest%20Mobiles%20Online%20at%20Best%20Prices%20in%20India%20%7C%20Croma&en=page_view&_et=1736&tfd=21885",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1715261042/Croma%20Assets/CMS/LP%20Page%20Banners/2024/PCP/Mobile/May/Vivo%20v30/PCP_2Split_Mobile_VivoV30e5G-_7May2024_agovqh.png?tr=w-1024", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_sq;s_nr30;s_ips;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1715333852/Croma%20Assets/CMS/LP%20Page%20Banners/2024/PCP/Mobile/May/10052024/iphone/Desktop/PCP_2Split_Mobile_iPhone13_48590_10May2024_pk4skw.png?tr=w-1024", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_sq;s_nr30;s_ips;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1707827145/Croma%20Assets/CMS/LP%20Page%20Banners/2024/PCP/Mobile/Feb/13th/pcp_smartphone_nc_newatcroma_OnePlus12R_13Feb2024_hnvxxc.png?tr=w-1024", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_sq;s_nr30;s_ips;s_tp;_ga_DJ3RGVFHJN;s_ppv", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1707827145/Croma%20Assets/CMS/LP%20Page%20Banners/2024/PCP/Mobile/Feb/13th/pcp_smartphone_nc_newatcroma_Oppo_13Feb2024_wshq8v.png?tr=w-1024", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_sq;s_nr30;s_ips;s_tp;_ga_DJ3RGVFHJN;s_ppv", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1704879050/Croma%20Assets/CMS/CAtegory/Mobile%20phone%20-%20C10/10012024/Redmi%20note%2013/pcp/pcp_smartphone_newatcroma_Redmi_9Jan2024.png_1_wwfcho.png?tr=w-1024", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_sq;s_nr30;s_ips;s_tp;_ga_DJ3RGVFHJN;s_ppv", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1697468339/Croma%20Assets/CMS/PCP/Mobile%20PCP/16-10-2023/pcp_smartphone_nc_newatcroma_NothingPhone2_16Oct2023_nrx7iu.png?tr=w-1024", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_sq;s_nr30;s_ips;s_tp;_ga_DJ3RGVFHJN;s_ppv", END_INLINE
    );

    ns_end_transaction("collect_7", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("X00049");
    ns_web_url("X00049",
        "URL=https://api.croma.com/pwagoogle/v1/getgeocode/400049?components=%27country:IN|postal_code:400049%27",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;SameSite;WZRK_S_679-6RK-976Z;cto_bundle;s_sq;ADRUM_BTa;ADRUM_BT1;s_nr30;s_ips;_ga_DJ3RGVFHJN;s_tp;s_ppv"
    );

    ns_end_transaction("X00049", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("X0_2");
    ns_web_url("X0_2",
        "URL=https://api.croma.com/searchservices/v1/category/10?currentPage=0&query=:relevance&fields=FULL&channel=WEB&channelCode=400049",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=httpsAgent:[object Object]",
        "HEADER=eventsSequenceId:31122020221022020331100322",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;SameSite;WZRK_S_679-6RK-976Z;cto_bundle;s_sq;ADRUM_BTa;ADRUM_BT1;s_nr30;s_ips;_ga_DJ3RGVFHJN;s_tp;s_ppv"
    );

    ns_end_transaction("X0_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("adrum_3");
    ns_web_url("adrum_3",
        "URL=https://bom-col.eum-appdynamics.com/eumcollector/beacons/browser/v1/BO-AAB-FBK/adrum",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:text/plain",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"vr":"23.10.1.4359","dt":"R","rg":"0","es":[{"eg":"1","et":2,"eu":"0://1/2/3/4","ts":1715839155848,"mg":"0","au":"0://5/6/7/8/9","at":0,"pp":3,"mx":{"PLC":1,"FBT":201,"DDT":1,"DPT":0,"PLT":202,"ARE":0},"md":"POST","xs":200,"si":10},{"eg":"2","et":2,"eu":"0://1/10/11/12/3","ts":1715839155945,"mg":"0","au":"0://5/6/7/8/9","at":0,"pp":3,"mx":{"PLC":1,"FBT":133,"DDT":1,"DPT":0,"PLT":134,"ARE":0},"md":"POST","xs":200,"si":11},{"eg":"3","et":2,"eu":"0://13/14/15/4/16?17","ts":1715839155956,"mg":"0","au":"0://5/6/7/8/9","at":0,"pp":3,"mx":{"PLC":1,"FBT":209,"DDT":1,"DPT":1,"PLT":211,"ARE":0},"md":"GET","xs":200,"si":12},{"eg":"4","et":2,"eu":"0://13/18/4/19/9?20","ts":1715839155955,"mg":"0","au":"0://5/6/7/8/9","at":0,"pp":3,"mx":{"PLC":1,"FBT":478,"DDT":1,"DPT":0,"PLT":479,"ARE":0},"md":"GET","xs":200,"si":13},{"eg":"5","et":2,"eu":"0://13/14/15/4/16?21","ts":1715839155954,"mg":"0","au":"0://5/6/7/8/9","at":0,"pp":3,"mx":{"PLC":1,"FBT":677,"DDT":36,"DPT":0,"PLT":713,"ARE":0},"md":"GET","xs":200,"si":14},{"eg":"6","et":2,"eu":"0://1/2/3/4","ts":1715839156750,"mg":"0","au":"0://5/6/7/8/9","at":0,"pp":3,"mx":{"PLC":1,"FBT":128,"DDT":1,"DPT":0,"PLT":129,"ARE":0},"md":"POST","xs":200,"si":15},{"eg":"7","et":2,"eu":"0://13/22/4/23/24?25","ts":1715839160963,"mg":"0","au":"0://5/6/7/8/9","at":0,"pp":3,"mx":{"PLC":1,"FBT":84,"DDT":1,"DPT":1,"PLT":86,"ARE":0},"md":"GET","xs":200,"si":16}],"ai":"477d42aa_9f21_9144_1a76_688c9aa25e4b","gs":["94fc8c22_8b4e_9951_5dd4_a346d07ee981","6172206b_5bf9_3cc0_6247_6dba3b23fe95","b3aef869_f829_ab62_a071_13c19809400b","720aadbb_348b_da64_5329_eaf7a136d0d6","8a93d879_8492_c1ad_7eb2_7a5e9d9276d2","48a2237a_777c_a24b_5abd_edfadf6cc7af","9d1dfa09_66e9_6c7e_38fc_6bf6fe57c676","2ed2ffcb_aaf0_4789_8869_12fda5184fae"],"up":["https","api.tatadigital.com","analytics-engine","events","v1","www.croma.com","phones-wearables","mobile-phones","c","10","api","v1.1","msd","api.croma.com","cmstemplate","allchannels","page","pageType=CategoryPage&code=10&fields=DEFAULT","searchservices","category","currentPage=0&query=%3Arelevance&fields=FULL&channel=WEB&channelCode=","pageType=ContentPage&pageLabelOrId=/c/10&fields=FULL","pwagoogle","getgeocode","400049","components='country:IN|postal_code:400049'"]}",
        BODY_END,
        INLINE_URLS,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s39703410366785?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%200%3A59%3A21%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=plp&g=https%3A%2F%2Fwww.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&cc=INR&server=www.croma.com&events=event55%2Cevent72%2Cevent20%2Cevent21%2Cevent50%3Dundefined&c1=plp&v1=D%3Dmid&c2=www.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&v2=new&c3=2024-5-16%200%3A59%3A21&l3=%3Aoppo%20a59%205g%3Aplp&c4=homepage&v4=guest%20user&v5=400049&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5BImpression%5D%20%3A%20impression&v156=%3Aoppo%20a59%205g%3Aplp&pe=lnk_o&pev2=impression%3A%20tracking&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&lrt=48&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_sq;s_ips;s_tp;s_ppv;_ga_DJ3RGVFHJN;s_nr30", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1684242611/Croma%20Assets/CMS/CAtegory/Mobile%20phone%20-%20C10/16-05-23/Desktop/Choose%20Your%20Type/3split_PCP_righttype_iPhone_15May2023_dfaihn.png?tr=w-1024", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_sq;s_ips;s_tp;s_ppv;_ga_DJ3RGVFHJN;s_nr30", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1684242611/Croma%20Assets/CMS/CAtegory/Mobile%20phone%20-%20C10/16-05-23/Desktop/Choose%20Your%20Type/3split_PCP_righttype_Android_15May2023_m5mclk.png?tr=w-1024", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_sq;s_ips;s_tp;s_ppv;_ga_DJ3RGVFHJN;s_nr30", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1684242608/Croma%20Assets/CMS/CAtegory/Mobile%20phone%20-%20C10/16-05-23/Desktop/Choose%20Your%20Type/3split_PCP_righttype_Feature_15May2023_say3ow.png?tr=w-1024", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_sq;s_ips;s_tp;s_ppv;_ga_DJ3RGVFHJN;s_nr30", END_INLINE
    );

    ns_end_transaction("adrum_3", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("X00049_2");
    ns_web_url("X00049_2",
        "URL=https://api.croma.com/pwagoogle/v1/getgeocode/400049?components=%27country:IN|postal_code:400049%27",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;SameSite;WZRK_S_679-6RK-976Z;cto_bundle;s_sq;ADRUM_BTa;ADRUM_BT1;s_ips;s_tp;_ga_DJ3RGVFHJN;s_nr30;s_ppv",
        INLINE_URLS,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s34106277371687?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%200%3A59%3A21%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=plp&g=https%3A%2F%2Fwww.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&cc=INR&server=www.croma.com&events=event55%2Cevent72%2Cevent20%2Cevent21%2Cevent50%3Dundefined&c1=plp&v1=D%3Dmid&c2=www.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&v2=new&c3=2024-5-16%200%3A59%3A21&l3=%3Aoneplus%2012r%205g%3Aplp&c4=homepage&v4=guest%20user&v5=400049&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5BImpression%5D%20%3A%20impression&v156=%3Aoneplus%2012r%205g%3Aplp&pe=lnk_o&pev2=impression%3A%20tracking&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_sq;s_ips;s_tp;_ga_DJ3RGVFHJN;s_nr30;s_ppv", END_INLINE,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s39349590579082?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%200%3A59%3A21%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=plp&g=https%3A%2F%2Fwww.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&cc=INR&server=www.croma.com&events=event55%2Cevent72%2Cevent20%2Cevent21%2Cevent50%3Dundefined&c1=plp&v1=D%3Dmid&c2=www.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&v2=new&c3=2024-5-16%200%3A59%3A21&l3=%3Aredmi%20note%2013%3Aplp&c4=homepage&v4=guest%20user&v5=400049&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5BImpression%5D%20%3A%20impression&v156=%3Aredmi%20note%2013%3Aplp&pe=lnk_o&pev2=impression%3A%20tracking&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_sq;s_ips;s_tp;_ga_DJ3RGVFHJN;s_nr30;s_ppv", END_INLINE,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s39621028528443?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%200%3A59%3A21%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=plp&g=https%3A%2F%2Fwww.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&cc=INR&server=www.croma.com&events=event55%2Cevent72%2Cevent20%2Cevent21%2Cevent50%3Dundefined&c1=plp&v1=D%3Dmid&c2=www.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&v2=new&c3=2024-5-16%200%3A59%3A21&l3=%3Anothing%20phone%202%205g%2012gb256gb%3Aplp&c4=homepage&v4=guest%20user&v5=400049&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5BImpression%5D%20%3A%20impression&v156=%3Anothing%20phone%202%205g%2012gb256gb%3Aplp&pe=lnk_o&pev2=impression%3A%20tracking&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_sq;s_ips;s_tp;_ga_DJ3RGVFHJN;s_nr30;s_ppv", END_INLINE,
            "URL=https://www.croma.com/campaign/oneplus-r-5g/c/6407?q=%3Aprice-asc", END_INLINE
    );

    ns_end_transaction("X00049_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_9");
    ns_web_url("v1_9",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=customer-hash:null",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=tdl-sso-version:4.1.15",
        "HEADER=session:false",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tL2NhbXBhaWduL29uZXBsdXMtci01Zy9jLzY0MDc/cT0lM0FwcmljZS1hc2MiLCJldmVudERhdGEiOlt7InRpbWVzdGFtcCI6MTcxNTgzOTE2NDM4MCwiZGF0YSI6IntcImV2ZW50XCI6XCJwYWdlTG9hZFwiLFwicGF5bG9hZFwiOntcInVybFwiOlwiaHR0cHM6Ly93d3cuY3JvbWEuY29tL2NhbXBhaWduL29uZXBsdXMtci01Zy9jLzY0MDc/cT0lM0FwcmljZS1hc2NcIixcImRhdGFTb3VyY2VcIjpcInB1c2hTdGF0ZVwifX0ifV0sImN1c3RvbWVySGFzaCI6bnVsbCwiZWNpZCI6IlVua25vd24iLCJjbGllbnRJZCI6IkNST01BLVdFQi1BUFAiLCJzZXNzaW9uSWQiOiJiNjVmNTRiNC01YzkzLTQ4MmUtOGNmOC1jMjEwZWNmZmE3OGYuOTIzYmI1N2YtOTUxYi00NzgwLThjYzMtM2U3Yzg5ZTkwNWZlIiwiYnJhbmQiOiJDUk9NQSIsInRpbWVzdGFtcCI6MTcxNTgzOTE2NDM4MCwiZGV2aWNlIjoiTW96aWxsYS81LjAgKFgxMTsgTGludXggeDg2XzY0KSBBcHBsZVdlYktpdC81MzcuMzYgKEtIVE1MLCBsaWtlIEdlY2tvKSBDaHJvbWUvMTE0LjAuMC4wIFNhZmFyaS81MzcuMzYiLCJqYXJ2aXNJZCI6IjRkNjA0NTcwLWJhZmItNDU1MS1iYWQwLTg1Mzg5NzFjN2VlMiIsImFkdmVydGlzaW5nSWQiOm51bGwsImFwcFZlcnNpb24iOiIwIn0="}",
        BODY_END
    );

    ns_end_transaction("v1_9", NS_AUTO_STATUS);
    ns_page_think_time(0.086);

    //Page Auto split for Image Link 'undefined' Clicked by User
    ns_start_transaction("index_3");
    ns_web_url("index_3",
        "URL=https://www.facebook.com/tr/?id=1369867960112522&ev=PageView&dl=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&rl=&if=false&ts=1715839164388&sw=1523&sh=650&v=2.9.156&r=stable&ec=4&o=4126&fbp=fb.1.1715839140634.281453602&ler=empty&cdl=API_unavailable&it=1715839140500&coo=false&rqm=GET",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:image",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s32393395197483?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%200%3A59%3A24%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=plp&g=https%3A%2F%2Fwww.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&cc=INR&server=www.croma.com&events=event4&c1=plp&v1=D%3Dmid&l1=q%3D%253Aprice-asc&c2=www.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&v2=new&c3=2024-5-16%200%3A59%3A24&l3=%3Aoneplus%2012r%205g%3Aplp&c4=homepage&v4=guest%20user&v5=400049&v15=banner%3Aoneplus%2012r%205g&v16=banner_clicked&v17=phones-wearables%2Fmobile-phones%2Fc%2F10%3A&v39=RL9bd43344037f476081fbf32b9dc7acb2&v53=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&v71=banner%3Aoneplus%2012r%205g%7Cphones-wearables%2Fmobile-phones%2Fc%2F10%3A%7Cbanner_clicked&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5BotherClick%5D%20%3A%20Other%20Click&v156=%3Aoneplus%2012r%205g%3Aplp&pe=lnk_o&pev2=banner%3Aoneplus%2012r%205g&c.&a.&activitymap.&page=plp&link=undefined&region=comp_000079MP&pageIDType=1&.activitymap&.a&.c&pid=plp&pidt=1&oid=function%28c%29%7Bvarq%3Db.Qa%28f%2Cc%2Ce%2Cl%29%3Ba.conf.ja%26%26%28q.oa%3Dx%29%3Breturnb.sh%28n%2Cq%2Cthis%2Carguments%29%7D&oidt=2&ot=IMG&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&lrt=37&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;s_tp;_ga_DJ3RGVFHJN;s_ppv;s_nr30;s_sq", END_INLINE,
            "URL=https://www.croma.com/assets/favicon.ico", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;mbox;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;RT;s_nr30;s_sq;s_tp;s_ppv", END_INLINE
    );

    ns_end_transaction("index_3", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("X00049_3");
    ns_web_url("X00049_3",
        "URL=https://api.croma.com/pwagoogle/v1/getgeocode/400049?components=%27country:IN|postal_code:400049%27",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;SameSite;WZRK_S_679-6RK-976Z;cto_bundle;ADRUM_BTa;ADRUM_BT1;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv"
    );

    ns_end_transaction("X00049_3", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("X407_fields_FULL");
    ns_web_url("X407_fields_FULL",
        "URL=https://api.croma.com/cmstemplate/allchannels/v1/page?pageType=ContentPage&pageLabelOrId=/c/6407&fields=FULL",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=httpsAgent:[object Object]",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;SameSite;WZRK_S_679-6RK-976Z;cto_bundle;ADRUM_BTa;ADRUM_BT1;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv"
    );

    ns_end_transaction("X407_fields_FULL", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("X407");
    ns_web_url("X407",
        "URL=https://api.croma.com/searchservices/v1/category/6407?currentPage=0&query=%3Aprice-asc&fields=FULL&channel=WEB&channelCode=400049",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=httpsAgent:[object Object]",
        "HEADER=eventsSequenceId:31122020221022020331100322",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;SameSite;WZRK_S_679-6RK-976Z;cto_bundle;ADRUM_BTa;ADRUM_BT1;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv"
    );

    ns_end_transaction("X407", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("page_5");
    ns_web_url("page_5",
        "URL=https://api.croma.com/cmstemplate/allchannels/v1/page?pageType=CategoryPage&code=6407&fields=DEFAULT",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=eventsSequenceId:31122020221022020331100322",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;SameSite;WZRK_S_679-6RK-976Z;cto_bundle;ADRUM_BTa;ADRUM_BT1;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv"
    );

    ns_end_transaction("page_5", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("storelocation");
    ns_web_url("storelocation",
        "URL=https://api.croma.com/lookup/mobile-app/v1/storelocation?latitude=19.1047153&longitude=72.826827&",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;SameSite;WZRK_S_679-6RK-976Z;cto_bundle;ADRUM_BTa;ADRUM_BT1;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv"
    );

    ns_end_transaction("storelocation", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("events_3");
    ns_web_url("events_3",
        "URL=https://api.tatadigital.com/api/v1.1/msd/events",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Program-Id:01eae2ec-0576-1000-bbea-86e16dcb4b79",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"page_type":"clp","content_type":"product","action_name":"view","event_name":"page_view","correlation_id":"sssaasss","medium":"pixel","metadata":{},"source":"CROMA","user_id_type":["CROMA"],"user_id":["08vHUbbT7lzWpSwZ"],"mad_uuid":"08vHUbbT7lzWpSwZ","utm":{"utm_source":"","utm_medium":"","utm_campaign":""},"epoch":"1715839164407","content_info":{"source_id":"6407"},"pincode":"400049"}",
        BODY_END
    );

    ns_end_transaction("events_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_8");
    ns_web_url("collect_8",
        "URL=https://analytics.google.com/g/collect?v=2&tid=G-DJ3RGVFHJN&gtm=45je45f0v893318240z8889407033za200&_p=1715839139273&gcd=13l3l3l3l1&npa=0&dma=0&cid=1944455297.1715839141&ul=en-us&sr=1523x650&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&frm=0&pscdl=noapi&sid=1715839140&sct=1&seg=1&dl=https%3A%2F%2Fwww.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&dr=https%3A%2F%2Fwww.croma.com%2F&dt=Mobile%20Phones%20%7C%20Buy%20Latest%20Mobiles%20Online%20at%20Best%20Prices%20in%20India%20%7C%20Croma&uid=false&_s=8&tfd=26887",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "en=section_impression&ep.pagetype=plp&ep.source_page_url=%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&ep.previous_page_url=%2F&ep.destination_page_url=N%2FA&ep.click_text=N%2FA&ep.interaction_type=N%2FA&ep.sequence=N%2FA&epn.total_elements=9&ep.section=Know%20Your%20Mobile%20Phones&ep.item_name=N%2FA&ep.item_category=N%2FA&ep.item_category2=N%2FA&ep.item_category3=N%2FA&ep.brand=N%2FA&ep.creative_id=N%2FA&ep.login_trigger=N%2FA&ep.login_status=false&ep.form_field=N%2FA&ep.platform=croma.com&_et=1711
en=section_card_impressions&ep.pagetype=plp&ep.source_page_url=%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&ep.previous_page_url=%2F&ep.destination_page_url=https%3A%2F%2Fwww.croma.com%2Funboxed%2Fthings-you-didnt-know-your-smartphone-could-do&ep.click_text=N%2FA&ep.interaction_type=N%2FA&epn.sequence=2&epn.total_elements=9&ep.section=Know%20Your%20Mobile%20Phones&ep.item_name=1&ep.item_category=N%2FA&ep.item_category2=N%2FA&ep.item_category3=N%2FA&ep.brand=N%2FA&ep.platform=croma.com&ep.creative_id=https%3A%2F%2Fmedia.croma.com%2Fimage%2Fupload%2Fv1689855039%2FCroma%2520Assets%2FCMS%2FCAtegory%2FMobile%2520phone%2520-%2520C10%2Fblog%2F10-things-your-smartphone-can-do_20July2023_bz6mmo.png&ep.login_trigger=N%2FA&ep.login_status=false&_et=335
en=section_card_impressions&ep.pagetype=plp&ep.source_page_url=%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&ep.previous_page_url=%2F&ep.destination_page_url=https%3A%2F%2Fwww.croma.com%2Funboxed%2F7-things-to-do-while-setting-up-your-new-phone&ep.click_text=N%2FA&ep.interaction_type=N%2FA&epn.sequence=3&epn.total_elements=9&ep.section=Know%20Your%20Mobile%20Phones&ep.item_name=2&ep.item_category=N%2FA&ep.item_category2=N%2FA&ep.item_category3=N%2FA&ep.brand=N%2FA&ep.platform=croma.com&ep.creative_id=https%3A%2F%2Fmedia.croma.com%2Fimage%2Fupload%2Fv1689855040%2FCroma%2520Assets%2FCMS%2FCAtegory%2FMobile%2520phone%2520-%2520C10%2Fblog%2FA-checklist-for-setting-up-new-phone_20July2023_yhr8gt.png&ep.login_trigger=N%2FA&ep.login_status=false&_et=6
en=section_card_impressions&ep.pagetype=plp&ep.source_page_url=%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&ep.previous_page_url=%2F&ep.destination_page_url=https%3A%2F%2Fwww.croma.com%2Funboxed%2Fhow-to-reset-your-mobile-phone&ep.click_text=N%2FA&ep.interaction_type=N%2FA&epn.sequence=4&epn.total_elements=9&ep.section=Know%20Your%20Mobile%20Phones&ep.item_name=3&ep.item_category=N%2FA&ep.item_category2=N%2FA&ep.item_category3=N%2FA&ep.brand=N%2FA&ep.platform=croma.com&ep.creative_id=https%3A%2F%2Fmedia.croma.com%2Fimage%2Fupload%2Fv1689855041%2FCroma%2520Assets%2FCMS%2FCAtegory%2FMobile%2520phone%2520-%2520C10%2Fblog%2FHow-to-reset-your-mobile-phone_20July2023_fzhbcg.png&ep.login_trigger=N%2FA&ep.login_status=false&_et=5
en=section_card_impressions&ep.pagetype=plp&ep.source_page_url=%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&ep.previous_page_url=%2F&ep.destination_page_url=%2Flp-buying-guide%2Fmobile-phone&ep.click_text=N%2FA&ep.interaction_type=N%2FA&epn.sequence=1&epn.total_elements=9&ep.section=Know%20Your%20Mobile%20Phones&ep.item_name=BG&ep.item_category=N%2FA&ep.item_category2=N%2FA&ep.item_category3=N%2FA&ep.brand=N%2FA&ep.platform=croma.com&ep.creative_id=https%3A%2F%2Fmedia.croma.com%2Fimage%2Fupload%2Fv1689855040%2FCroma%2520Assets%2FCMS%2FCAtegory%2FMobile%2520phone%2520-%2520C10%2Fblog%2FMobile-Buying-Guide_20July2023_gbfrzl.png&ep.login_trigger=N%2FA&ep.login_status=false&_et=10
en=section_impression&ep.pagetype=plp&ep.source_page_url=%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&ep.previous_page_url=%2F&ep.destination_page_url=N%2FA&ep.click_text=N%2FA&ep.interaction_type=N%2FA&ep.sequence=N%2FA&ep.total_elements=N%2FA&ep.section=Most%20Popular%20Smartphones&ep.item_name=N%2FA&ep.item_category=N%2FA&ep.item_category2=N%2FA&ep.item_category3=N%2FA&ep.brand=N%2FA&ep.creative_id=N%2FA&ep.login_trigger=N%2FA&ep.login_status=false&ep.form_field=N%2FA&ep.platform=croma.com&_et=767
en=section_card_impressions&ep.pagetype=plp&ep.source_page_url=%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&ep.previous_page_url=%2F&ep.destination_page_url=%2Fcampaign%2Foppo-a59-5g%2Fc%2F6371&ep.click_text=N%2FA&ep.interaction_type=N%2FA&epn.sequence=2&epn.total_elements=4&ep.section=N%2FA&ep.item_name=Oppo%20A59%205G&ep.item_category=N%2FA&ep.item_category2=N%2FA&ep.item_category3=N%2FA&ep.brand=N%2FA&ep.platform=croma.com&ep.creative_id=https%3A%2F%2Fmedia.croma.com%2Fimage%2Fupload%2Fv1707827145%2FCroma%2520Assets%2FCMS%2FLP%2520Page%2520Banners%2F2024%2FPCP%2FMobile%2FFeb%2F13th%2Fpcp_smartphone_nc_newatcroma_Oppo_13Feb2024_wshq8v.png&ep.login_trigger=N%2FA&ep.login_status=false&_et=741
en=section_card_impressions&ep.pagetype=plp&ep.source_page_url=%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&ep.previous_page_url=%2F&ep.destination_page_url=%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&ep.click_text=N%2FA&ep.interaction_type=N%2FA&epn.sequence=1&epn.total_elements=4&ep.section=N%2FA&ep.item_name=OnePlus%2012R%205G&ep.item_category=N%2FA&ep.item_category2=N%2FA&ep.item_category3=N%2FA&ep.brand=N%2FA&ep.platform=croma.com&ep.creative_id=https%3A%2F%2Fmedia.croma.com%2Fimage%2Fupload%2Fv1707827145%2FCroma%2520Assets%2FCMS%2FLP%2520Page%2520Banners%2F2024%2FPCP%2FMobile%2FFeb%2F13th%2Fpcp_smartphone_nc_newatcroma_OnePlus12R_13Feb2024_hnvxxc.png&ep.login_trigger=N%2FA&ep.login_status=false&_et=29
en=section_card_impressions&ep.pagetype=plp&ep.source_page_url=%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&ep.previous_page_url=%2F&ep.destination_page_url=%2Fcampaign%2Fredmi-note%2Fc%2F6386%3Fq%3D%253Aprice-asc%253Adelivery_mode%253AHome%2BDelivery&ep.click_text=N%2FA&ep.interaction_type=N%2FA&epn.sequence=3&epn.total_elements=4&ep.section=N%2FA&ep.item_name=Redmi%20note%2013&ep.item_category=N%2FA&ep.item_category2=N%2FA&ep.item_category3=N%2FA&ep.brand=N%2FA&ep.platform=croma.com&ep.creative_id=https%3A%2F%2Fmedia.croma.com%2Fimage%2Fupload%2Fv1704879050%2FCroma%2520Assets%2FCMS%2FCAtegory%2FMobile%2520phone%2520-%2520C10%2F10012024%2FRedmi%2520note%252013%2Fpcp%2Fpcp_smartphone_newatcroma_Redmi_9Jan2024.png_1_wwfcho.png&ep.login_trigger=N%2FA&ep.login_status=false&_et=11
en=section_card_impressions&ep.pagetype=plp&ep.source_page_url=%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&ep.previous_page_url=%2F&ep.destination_page_url=%2FsearchB%3Fq%3Dnothing%2520phone%25202%253Arelevance%26text%3Dnothing%2520phone%25202&ep.click_text=N%2FA&ep.interaction_type=N%2FA&epn.sequence=4&epn.total_elements=4&ep.section=N%2FA&ep.item_name=Nothing%20Phone%202%205G%2012GB%7C256GB&ep.item_category=N%2FA&ep.item_category2=N%2FA&ep.item_category3=N%2FA&ep.brand=N%2FA&ep.platform=croma.com&ep.creative_id=https%3A%2F%2Fmedia.croma.com%2Fimage%2Fupload%2Fv1697468339%2FCroma%2520Assets%2FCMS%2FPCP%2FMobile%2520PCP%2F16-10-2023%2Fpcp_smartphone_nc_newatcroma_NothingPhone2_16Oct2023_nrx7iu.png&ep.login_trigger=N%2FA&ep.login_status=false&_et=19",
        BODY_END
    );

    ns_end_transaction("collect_8", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_10");
    ns_web_url("v1_10",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=customer-hash:null",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=tdl-sso-version:4.1.15",
        "HEADER=session:false",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tL2NhbXBhaWduL29uZXBsdXMtci01Zy9jLzY0MDc/cT0lM0FwcmljZS1hc2MiLCJldmVudERhdGEiOlt7InRpbWVzdGFtcCI6MTcxNTgzOTE2NTYwMSwiZGF0YSI6IntcImV2ZW50XCI6XCJwYWdlTG9hZFwiLFwicGF5bG9hZFwiOntcInVybFwiOlwiaHR0cHM6Ly93d3cuY3JvbWEuY29tL2NhbXBhaWduL29uZXBsdXMtci01Zy9jLzY0MDc/cT0lM0FwcmljZS1hc2NcIixcImRhdGFTb3VyY2VcIjpcInJlcGxhY2VTdGF0ZVwifX0ifV0sImN1c3RvbWVySGFzaCI6bnVsbCwiZWNpZCI6IlVua25vd24iLCJjbGllbnRJZCI6IkNST01BLVdFQi1BUFAiLCJzZXNzaW9uSWQiOiJiNjVmNTRiNC01YzkzLTQ4MmUtOGNmOC1jMjEwZWNmZmE3OGYuOTIzYmI1N2YtOTUxYi00NzgwLThjYzMtM2U3Yzg5ZTkwNWZlIiwiYnJhbmQiOiJDUk9NQSIsInRpbWVzdGFtcCI6MTcxNTgzOTE2NTYwMSwiZGV2aWNlIjoiTW96aWxsYS81LjAgKFgxMTsgTGludXggeDg2XzY0KSBBcHBsZVdlYktpdC81MzcuMzYgKEtIVE1MLCBsaWtlIEdlY2tvKSBDaHJvbWUvMTE0LjAuMC4wIFNhZmFyaS81MzcuMzYiLCJqYXJ2aXNJZCI6IjRkNjA0NTcwLWJhZmItNDU1MS1iYWQwLTg1Mzg5NzFjN2VlMiIsImFkdmVydGlzaW5nSWQiOm51bGwsImFwcFZlcnNpb24iOiIwIn0="}",
        BODY_END,
        INLINE_URLS,
            "URL=https://sslwidget.criteo.com/event?a=56256&v=5.23.0&otl=1&p0=e%3Dexd%26site_type%3Dd&p1=e%3Dvl%26tms%3Dgtm-template%26p%3D%255B303651%252C243459%252C300652%255D&p2=e%3Ddis&adce=1&bundle=EPlPXl9ZQjN2TTdzaTNPd0Q0dlByN3VaMFZKWVpFMmRIUXg4WFZHd0t1eUFlY3ZJZHhZOGdWZU1pa0FqeGs0VVBSczlOOXJabXAxU3g5bkRtVEh3Vm9aTE1CMWJnUDJkJTJCNFN1WWxUWmoxN0wybzBtaXVnSyUyQnVpNHh5VTBXQ1BUZHFrQmY&tld=croma.com&dy=1&fu=https%253A%252F%252Fwww.croma.com%252Fcampaign%252Foneplus-r-5g%252Fc%252F6407%253Fq%253D%25253Aprice-asc&ceid=35a1dcdd-27eb-4460-affd-fc22ee82e8d9&dtycbr=94907", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://13135721.fls.doubleclick.net/activityi;src=13135721;type=croma03;cat=croma0;ord=3902782723694;u=%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407;npa=0;auiddc=1421799651.1715839140;uaa=x86;uab=64;uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199;uamb=0;uam=;uap=Linux;uapv=5.15.0;uaw=0;pscdl=noapi;frm=0;gtm=45fe45f0z8889407033za201;gcd=13l3l3l3l1;dma=0;epver=2;~oref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc?", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1704436896/Croma%20Assets/Communication/Mobiles/Images/303651_vedl2i.png?tr=w-400", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1708670560/Croma%20Assets/Communication/Mobiles/Images/243459_0_cvj2b5.png?tr=w-400", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1708673189/Croma%20Assets/Communication/Mobiles/Images/300652_0_ncocr2.png?tr=w-400", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1704435473/Croma%20Assets/Communication/Mobiles/Images/303650_wl4sl4.png?tr=w-400", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media.croma.com/image/upload/f_auto,q_auto,d_Croma%20Assets:No_image.png/Croma%20Assets/UI%20Assets/lazyLoading.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            //URL commented due to error code : CACHE_MISS
            //"URL=https://www.croma.com/assets/fonts/Switzer-Light.woff2", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=Origin:https://www.croma.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;mbox;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;RT;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://13135721.fls.doubleclick.net/activityi;dc_pre=CNCtxv--kYYDFdCkrAIdt6oNvg;src=13135721;type=croma03;cat=croma0;ord=3902782723694;u=%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407;npa=0;auiddc=1421799651.1715839140;uaa=x86;uab=64;uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199;uamb=0;uam=;uap=Linux;uapv=5.15.0;uaw=0;pscdl=noapi;frm=0;gtm=45fe45f0z8889407033za201;gcd=13l3l3l3l1;dma=0;epver=2;~oref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc?", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1706791158/Croma%20Assets/Communication/Mobiles/Images/304696_m3xah2.png?tr=w-400", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1706791128/Croma%20Assets/Communication/Mobiles/Images/304697_zp8rwp.png?tr=w-400", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1706790940/Croma%20Assets/Communication/Mobiles/Images/304698_hagerl.png?tr=w-400", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1706790957/Croma%20Assets/Communication/Mobiles/Images/304699_c7shme.png?tr=w-400", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE
    );

    ns_end_transaction("v1_10", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("details_pwa");
    ns_web_url("details_pwa",
        "URL=https://api.croma.com/inventory/oms/v2/tms/details-pwa/",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=eventsSequenceId:31122020221022020331100322",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=oms-apim-subscription-key:1131858141634e2abe2efb2b3a2a2a5d",
        "HEADER=Content-Type:application/json",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;SameSite;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;s_nr30;s_sq;s_tp;s_ppv;ADRUM_BTa;ADRUM_BT1;_ga_DJ3RGVFHJN",
        BODY_BEGIN,
            "{"promise":{"allocationRuleID":"SYSTEM","checkInventory":"Y","organizationCode":"CROMA","sourcingClassification":"EC","promiseLines":{"promiseLine":[{"fulfillmentType":"HDEL","itemID":"304696","lineId":"1","reEndDate":"2500-01-01","reqStartDate":"","requiredQty":"1","shipToAddress":{"company":"","country":"","city":"","mobilePhone":"","state":"","zipCode":"400049","extn":{"irlAddressLine1":"","irlAddressLine2":""}},"extn":{"widerStoreFlag":"N"}},{"fulfillmentType":"STOR","itemID":"304696","lineId":"2","reEndDate":"2500-01-01","reqStartDate":"","requiredQty":"1","shipToAddress":{"company":"","country":"","city":"","mobilePhone":"","state":"","zipCode":"400049","extn":{"irlAddressLine1":"","irlAddressLine2":""}},"extn":{"widerStoreFlag":"N"}},{"fulfillmentType":"SDEL","itemID":"304696","lineId":"3","reEndDate":"2500-01-01","reqStartDate":"","requiredQty":"1","shipToAddress":{"company":"","country":"","city":"","mobilePhone":"","state":"","zipCode":"400049","extn":{"irlAddressLine1":"","irlAddressLine2":""}},"extn":{"widerStoreFlag":"N"}},{"fulfillmentType":"HDEL","itemID":"304697","lineId":"1","reEndDate":"2500-01-01","reqStartDate":"","requiredQty":"1","shipToAddress":{"company":"","country":"","city":"","mobilePhone":"","state":"","zipCode":"400049","extn":{"irlAddressLine1":"","irlAddressLine2":""}},"extn":{"widerStoreFlag":"N"}},{"fulfillmentType":"STOR","itemID":"304697","lineId":"2","reEndDate":"2500-01-01","reqStartDate":"","requiredQty":"1","shipToAddress":{"company":"","country":"","city":"","mobilePhone":"","state":"","zipCode":"400049","extn":{"irlAddressLine1":"","irlAddressLine2":""}},"extn":{"widerStoreFlag":"N"}},{"fulfillmentType":"SDEL","itemID":"304697","lineId":"3","reEndDate":"2500-01-01","reqStartDate":"","requiredQty":"1","shipToAddress":{"company":"","country":"","city":"","mobilePhone":"","state":"","zipCode":"400049","extn":{"irlAddressLine1":"","irlAddressLine2":""}},"extn":{"widerStoreFlag":"N"}},{"fulfillmentType":"HDEL","itemID":"304698","lineId":"1","reEndDate":"2500-01-01","reqStartDate":"","requiredQty":"1","shipToAddress":{"company":"","country":"","city":"","mobilePhone":"","state":"","zipCode":"400049","extn":{"irlAddressLine1":"","irlAddressLine2":""}},"extn":{"widerStoreFlag":"N"}},{"fulfillmentType":"STOR","itemID":"304698","lineId":"2","reEndDate":"2500-01-01","reqStartDate":"","requiredQty":"1","shipToAddress":{"company":"","country":"","city":"","mobilePhone":"","state":"","zipCode":"400049","extn":{"irlAddressLine1":"","irlAddressLine2":""}},"extn":{"widerStoreFlag":"N"}},{"fulfillmentType":"SDEL","itemID":"304698","lineId":"3","reEndDate":"2500-01-01","reqStartDate":"","requiredQty":"1","shipToAddress":{"company":"","country":"","city":"","mobilePhone":"","state":"","zipCode":"400049","extn":{"irlAddressLine1":"","irlAddressLine2":""}},"extn":{"widerStoreFlag":"N"}},{"fulfillmentType":"HDEL","itemID":"304699","lineId":"1","reEndDate":"2500-01-01","reqStartDate":"","requiredQty":"1","shipToAddress":{"company":"","country":"","city":"","mobilePhone":"","state":"","zipCode":"400049","extn":{"irlAddressLine1":"","irlAddressLine2":""}},"extn":{"widerStoreFlag":"N"}},{"fulfillmentType":"STOR","itemID":"304699","lineId":"2","reEndDate":"2500-01-01","reqStartDate":"","requiredQty":"1","shipToAddress":{"company":"","country":"","city":"","mobilePhone":"","state":"","zipCode":"400049","extn":{"irlAddressLine1":"","irlAddressLine2":""}},"extn":{"widerStoreFlag":"N"}},{"fulfillmentType":"SDEL","itemID":"304699","lineId":"3","reEndDate":"2500-01-01","reqStartDate":"","requiredQty":"1","shipToAddress":{"company":"","country":"","city":"","mobilePhone":"","state":"","zipCode":"400049","extn":{"irlAddressLine1":"","irlAddressLine2":""}},"extn":{"widerStoreFlag":"N"}}]}}}",
        BODY_END,
        INLINE_URLS,
            "URL=https://sslwidget.criteo.com/event?a=56256&v=5.23.0&otl=1&p0=e%3Dexd%26site_type%3Dd&p1=e%3Dvl%26tms%3Dgtm-template%26p%3D%255B304696%252C304697%252C304698%255D&p2=e%3Ddis&adce=1&bundle=EPlPXl9ZQjN2TTdzaTNPd0Q0dlByN3VaMFZKWVpFMmRIUXg4WFZHd0t1eUFlY3ZJZHhZOGdWZU1pa0FqeGs0VVBSczlOOXJabXAxU3g5bkRtVEh3Vm9aTE1CMWJnUDJkJTJCNFN1WWxUWmoxN0wybzBtaXVnSyUyQnVpNHh5VTBXQ1BUZHFrQmY&tld=croma.com&dy=1&fu=https%253A%252F%252Fwww.croma.com%252Fcampaign%252Foneplus-r-5g%252Fc%252F6407%253Fq%253D%25253Aprice-asc&ceid=7161ee12-5cad-412a-8545-b8f582d88d86&dtycbr=18775", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://13135721.fls.doubleclick.net/activityi;src=13135721;type=croma03;cat=croma0;ord=2792844007821;u=%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407;npa=0;auiddc=1421799651.1715839140;uaa=x86;uab=64;uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199;uamb=0;uam=;uap=Linux;uapv=5.15.0;uaw=0;pscdl=noapi;frm=0;gtm=45fe45f0z8889407033za201;gcd=13l3l3l3l1;dma=0;epver=2;~oref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc?", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("details_pwa", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("X00049_4");
    ns_web_url("X00049_4",
        "URL=https://api.croma.com/pwagoogle/v1/getgeocode/400049?components=%27country:IN|postal_code:400049%27",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;SameSite;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;s_nr30;s_sq;s_tp;s_ppv;ADRUM_BTa;ADRUM_BT1;_ga_DJ3RGVFHJN",
        INLINE_URLS,
            "URL=https://adservice.google.com/ddm/fls/z/dc_pre=CNCtxv--kYYDFdCkrAIdt6oNvg;src=13135721;type=croma03;cat=croma0;ord=3902782723694;u=%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407;npa=0;auiddc=*;uaa=x86;uab=64;uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199;uamb=0;uam=;uap=Linux;uapv=5.15.0;uaw=0;pscdl=noapi;frm=0;gtm=45fe45f0z8889407033za201;gcd=13l3l3l3l1;dma=0;epver=2;~oref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("X00049_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_11");
    ns_web_url("v1_11",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=customer-hash:null",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=tdl-sso-version:4.1.15",
        "HEADER=session:false",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/v1_11_url_0_1_1715839393355.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://c.bing.com/c.gif?Red3=CTOMS_pd&cbid=k-0tyrVmhJLxG-nAKY3h_cmH_uE4L3d_G-DpKVpINPhVSXxwS3", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("v1_11", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("storelocation_2");
    ns_web_url("storelocation_2",
        "URL=https://api.croma.com/lookup/mobile-app/v1/storelocation?latitude=19.1047153&longitude=72.826827&",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;SameSite;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;s_nr30;s_sq;s_tp;s_ppv;ADRUM_BTa;ADRUM_BT1;_ga_DJ3RGVFHJN",
        INLINE_URLS,
            "URL=https://13135721.fls.doubleclick.net/activityi;dc_pre=CLqT2_--kYYDFUqNrAIds_sORw;src=13135721;type=croma03;cat=croma0;ord=2792844007821;u=%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407;npa=0;auiddc=1421799651.1715839140;uaa=x86;uab=64;uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199;uamb=0;uam=;uap=Linux;uapv=5.15.0;uaw=0;pscdl=noapi;frm=0;gtm=45fe45f0z8889407033za201;gcd=13l3l3l3l1;dma=0;epver=2;~oref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc?", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("storelocation_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_12");
    ns_web_url("v1_12",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tL2NhbXBhaWduL29uZXBsdXMtci01Zy9jLzY0MDc/cT0lM0FwcmljZS1hc2MiLCJldmVudERhdGEiOlt7InRpbWVzdGFtcCI6MTcxNTgzOTIwMTcxNywiZGF0YSI6IntcbiAgXCJ1cmxcIjoge1xuICAgIFwiYW5jZXN0b3JPcmlnaW5zXCI6IHt9LFxuICAgIFwiaHJlZlwiOiBcImh0dHBzOi8vd3d3LmNyb21hLmNvbS9jYW1wYWlnbi9vbmVwbHVzLXItNWcvYy82NDA3P3E9JTNBcHJpY2UtYXNjXCIsXG4gICAgXCJvcmlnaW5cIjogXCJodHRwczovL3d3dy5jcm9tYS5jb21cIixcbiAgICBcInByb3RvY29sXCI6IFwiaHR0cHM6XCIsXG4gICAgXCJob3N0XCI6IFwid3d3LmNyb21hLmNvbVwiLFxuICAgIFwiaG9zdG5hbWVcIjogXCJ3d3cuY3JvbWEuY29tXCIsXG4gICAgXCJwb3J0XCI6IFwiXCIsXG4gICAgXCJwYXRobmFtZVwiOiBcIi9jYW1wYWlnbi9vbmVwbHVzLXItNWcvYy82NDA3XCIsXG4gICAgXCJzZWFyY2hcIjogXCI/cT0lM0FwcmljZS1hc2NcIixcbiAgICBcImhhc2hcIjogXCJcIlxuICB9LFxuICBcInBhdGhcIjogW1xuICAgIHtcbiAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJpbWdcIixcbiAgICAgIFwiaWRcIjogXCJcIixcbiAgICAgIFwiY2xhc3NcIjogXCJcIlxuICAgIH0sXG4gICAge1xuICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImRpdlwiLFxuICAgICAgXCJpZFwiOiBcIlwiLFxuICAgICAgXCJjbGFzc1wiOiBcInByb2R1Y3QtaW1nIHBscC1jYXJkLXRodW1ibmFpbCBwbHBuZXdzZWFyY2hcIlxuICAgIH0sXG4gICAge1xuICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImFcIixcbiAgICAgIFwiaWRcIjogXCJcIixcbiAgICAgIFwiY2xhc3NcIjogXCJcIlxuICAgIH0sXG4gICAge1xuICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImRpdlwiLFxuICAgICAgXCJpZFwiOiBcIlwiLFxuICAgICAgXCJjbGFzc1wiOiBcIlwiXG4gICAgfSxcbiAgICB7XG4gICAgICBcImVsZW1lbnRUeXBlXCI6IFwiZGl2XCIsXG4gICAgICBcImlkXCI6IFwiMzA0Njk3XCIsXG4gICAgICBcImNsYXNzXCI6IFwiY3AtcHJvZHVjdCB0eXAtcGxwIHBscC1zcnAtdHlwXCJcbiAgICB9LFxuICAgIHtcbiAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJsaVwiLFxuICAgICAgXCJpZFwiOiBcIlwiLFxuICAgICAgXCJjbGFzc1wiOiBcInByb2R1Y3QtaXRlbVwiXG4gICAgfSxcbiAgICB7XG4gICAgICBcImVsZW1lbnRUeXBlXCI6IFwidWxcIixcbiAgICAgIFwiaWRcIjogXCJwcm9kdWN0LWxpc3QtYmFja1wiLFxuICAgICAgXCJjbGFzc1wiOiBcInByb2R1Y3QtbGlzdFwiXG4gICAgfSxcbiAgICB7XG4gICAgICBcImVsZW1lbnRUeXBlXCI6IFwiZGl2XCIsXG4gICAgICBcImlkXCI6IFwiXCIsXG4gICAgICBcImNsYXNzXCI6IFwiY29udGVudC13cmFwXCJcbiAgICB9LFxuICAgIHtcbiAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJkaXZcIixcbiAgICAgIFwiaWRcIjogXCJcIixcbiAgICAgIFwiY2xhc3NcIjogXCJ0d28tY29sLXJpZ250IHBscC1zcnAtbmV3ZGVzaWduXCJcbiAgICB9LFxuICAgIHtcbiAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJkaXZcIixcbiAgICAgIFwiaWRcIjogXCJcIixcbiAgICAgIFwiY2xhc3NcIjogXCJjb250YWluZXJcIlxuICAgIH0sXG4gICAge1xuICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImRpdlwiLFxuICAgICAgXCJpZFwiOiBcIlwiLFxuICAgICAgXCJjbGFzc1wiOiBcImNwLWxpc3RpbmdcIlxuICAgIH0sXG4gICAge1xuICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImRpdlwiLFxuICAgICAgXCJpZFwiOiBcIlwiLFxuICAgICAgXCJjbGFzc1wiOiBcImJzLWNvbnRlbnRcIlxuICAgIH0sXG4gICAge1xuICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImRpdlwiLFxuICAgICAgXCJpZFwiOiBcIlwiLFxuICAgICAgXCJjbGFzc1wiOiBcIm1haW5Db250YWluZXJcIlxuICAgIH0sXG4gICAge1xuICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImRpdlwiLFxuICAgICAgXCJpZFwiOiBcIlwiLFxuICAgICAgXCJjbGFzc1wiOiBcIlwiXG4gICAgfSxcbiAgICB7XG4gICAgICBcImVsZW1lbnRUeXBlXCI6IFwibWFpblwiLFxuICAgICAgXCJpZFwiOiBcImNvbnRhaW5lclwiLFxuICAgICAgXCJjbGFzc1wiOiBcImRhcmstdGhlbWVcIlxuICAgIH1cbiAgXSxcbiAgXCJ0YXJnZXRcIjogXCI8aW1nIGNsYXNzPVxcXCJcXFwiIGRhdGEtc3JjPVxcXCJodHRwczovL21lZGlhLWlrLmNyb21hLmNvbS9wcm9kL2h0dHBzOi8vbWVkaWEuY3JvbWEuY29tL2ltYWdlL3VwbG9hZC92MTcwNjc5MTEyOC9Dcm9tYSUyMEFzc2V0cy9Db21tdW5pY2F0aW9uL01vYmlsZXMvSW1hZ2VzLzMwNDY5N196cDhyd3AucG5nP3RyPXctNDAwXFxcIiBzcmM9XFxcImh0dHBzOi8vbWVkaWEtaWsuY3JvbWEuY29tL3Byb2QvaHR0cHM6Ly9tZWRpYS5jcm9tYS5jb20vaW1hZ2UvdXBsb2FkL3YxNzA2NzkxMTI4L0Nyb21hJTIwQXNzZXRzL0NvbW11bmljYXRpb24vTW9iaWxlcy9JbWFnZXMvMzA0Njk3X3pwOHJ3cC5wbmc/dHI9dy00MDBcXFwiIGRhdGEtc3Jjc2V0PVxcXCJodHRwczovL21lZGlhLWlrLmNyb21hLmNvbS9wcm9kL2h0dHBzOi8vbWVkaWEuY3JvbWEuY29tL2ltYWdlL3VwbG9hZC92MTcwNjc5MTEyOC9Dcm9tYSUyMEFzc2V0cy9Db21tdW5pY2F0aW9uL01vYmlsZXMvSW1hZ2VzLzMwNDY5N196cDhyd3AucG5nP3RyPXctMjAwIDIwMHcsaHR0cHM6Ly9tZWRpYS1pay5jcm9tYS5jb20vcHJvZC9odHRwczovL21lZGlhLmNyb21hLmNvbS9pbWFnZS91cGxvYWQvdjE3MDY3OTExMjgvQ3JvbWElMjBBc3NldHMvQ29tbXVuaWNhdGlvbi9Nb2JpbGVzL0ltYWdlcy8zMDQ2OTdfenA4cndwLnBuZz90cj13LTQwMCA0MDB3LGh0dHBzOi8vbWVkaWEtaWsuY3JvbWEuY29tL3Byb2QvaHR0cHM6Ly9tZWRpYS5jcm9tYS5jb20vaW1hZ2UvdXBsb2FkL3YxNzA2NzkxMTI4L0Nyb21hJTIwQXNzZXRzL0NvbW11bmljYXRpb24vTW9iaWxlcy9JbWFnZXMvMzA0Njk3X3pwOHJ3cC5wbmc/dHI9dy02MDAgNjAwd1xcXCIgc2l6ZXM9XFxcIihtYXgtd2lkdGg6IDQ4MHB4KSAxMTVweCwgMjEycHhcXFwiIGFsdD1cXFwiT25lUGx1cyAxMlIgNUcgKDhHQiwgMTI4R0IsIENvb2wgQmx1ZSlcXFwiIHRpdGxlPVxcXCJPbmVQbHVzIDEyUiA1RyAoOEdCLCAxMjhHQiwgQ29vbCBCbHVlKVxcXCIgc3R5bGU9XFxcImhlaWdodDogMjYwcHg7IHdpZHRoOiAyNjBweDtcXFwiPlwiLFxuICBcInRpbWVTdGFtcFwiOiAxNzE1ODM5MjAxNzE3LFxuICBcInR5cGVcIjogXCJjbGlja1wiXG59In1dLCJjdXN0b21lckhhc2giOm51bGwsImVjaWQiOiJVbmtub3duIiwiY2xpZW50SWQiOiJDUk9NQS1XRUItQVBQIiwic2Vzc2lvbklkIjoiYjY1ZjU0YjQtNWM5My00ODJlLThjZjgtYzIxMGVjZmZhNzhmLjkyM2JiNTdmLTk1MWItNDc4MC04Y2MzLTNlN2M4OWU5MDVmZSIsImJyYW5kIjoiQ1JPTUEiLCJ0aW1lc3RhbXAiOjE3MTU4MzkyMDE3MTgsImRldmljZSI6Ik1vemlsbGEvNS4wIChYMTE7IExpbnV4IHg4Nl82NCkgQXBwbGVXZWJLaXQvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lLzExNC4wLjAuMCBTYWZhcmkvNTM3LjM2IiwiamFydmlzSWQiOiI0ZDYwNDU3MC1iYWZiLTQ1NTEtYmFkMC04NTM4OTcxYzdlZTIiLCJhZHZlcnRpc2luZ0lkIjpudWxsLCJhcHBWZXJzaW9uIjoiMCJ9"}",
        BODY_END,
        INLINE_URLS,
            "URL=https://adservice.google.com/ddm/fls/z/dc_pre=CLqT2_--kYYDFUqNrAIds_sORw;src=13135721;type=croma03;cat=croma0;ord=2792844007821;u=%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407;npa=0;auiddc=*;uaa=x86;uab=64;uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199;uamb=0;uam=;uap=Linux;uapv=5.15.0;uaw=0;pscdl=noapi;frm=0;gtm=45fe45f0z8889407033za201;gcd=13l3l3l3l1;dma=0;epver=2;~oref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://c.bing.com/c.gif?Red3=CTOMS_pd&cbid=k-0tyrVmhJLxG-nAKY3h_cmH_uE4L3d_G-DpKVpINPhVSXxwS3", END_INLINE
    );

    ns_end_transaction("v1_12", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("s31935144306421");
    ns_web_url("s31935144306421",
        "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s31935144306421",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN;s_nr30",
        BODY_BEGIN,
            "AQB=1&ndh=1&pf=1&t=16%2F4%2F2024+0%3A59%3A26+4+300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=plp&g=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253aprice-asc&c.=&getNewRepeat=3.0.1&getPageLoadTime=2.0.2&performanceWriteFull=n%2Fa&performanceWritePart=n%2Fa&performanceCheck=n%2Fa&p_fo=3.0&inList=3.0&apl=4.0&getPreviousValue=3.0.1&.c=&cc=INR&ch=croma%3Aplp&server=www.croma.com&events=event2%2Cevent20%2Cevent21%2Cevent49%2Cevent22%3D4%2Cevent1%3D1.9&products=%3B304696%3B%3B%3Bevent20%3D39999%7Cevent21%3D39999%3BeVar10%3D304696%7CeVar13%3D39999%7CeVar14%3D39999%7CeVar43%3DCampaign%3AOnePlus+12R+5G%7CeVar26%3DYes%7CeVar12%3D1%7CeVar41%3DOnePlus+12R+5G+%288GB+128GB+Iron+Grey%29%2C%3B304697%3B%3B%3Bevent20%3D39999%7Cevent21%3D39999%3BeVar10%3D304697%7CeVar13%3D39999%7CeVar14%3D39999%7CeVar43%3Dundefined%7CeVar26%3DYes%7CeVar12%3D2%7CeVar41%3DOnePlus+12R+5G+%288GB+128GB+Cool+Blue%29%2C%3B304698%3B%3B%3Bevent20%3D45999%7Cevent21%3D45999%3BeVar10%3D304698%7CeVar13%3D45999%7CeVar14%3D45999%7CeVar43%3Dundefined%7CeVar26%3DYes%7CeVar12%3D3%7CeVar41%3DOnePlus+12R+5G+%2816GB+256GB+Iron+Grey%29%2C%3B304699%3B%3B%3Bevent20%3D45999%7Cevent21%3D45999%3BeVar10%3D304699%7CeVar13%3D45999%7CeVar14%3D45999%7CeVar43%3Dundefined%7CeVar26%3DYes%7CeVar12%3D4%7CeVar41%3DOnePlus+12R+5G+%2816GB+256GB+Cool+Blue%29&aamb=6G1ynYcLPuiQxYZrsz_pkqfLG9yMXBpb2zX5dvJdYQJzPXImdj0y&c1=plp&v1=D%3Dmid&l1=q%3D%253Aprice-asc&c2=www.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407&v2=new&l2=sortby%3Aprice-asc%7D&c3=2024-5-16+0%3A59%3A26&c4=plp&v4=guest+user&v5=400049&v22=4&v38=plp+%7C+highestPercentViewed%3D219+%7C+initialPercentViewed%3D67&v39=RL425eb72221864237b3ecc22b172469cc&v51=1.9&v76=%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&v121=no&v131=Direct+Call+%2850%29+%3A+%5Bpageload%5D+%3A+Send+Beacon&v198=www.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&lrt=34&AQE=1",
        BODY_END
    );

    ns_end_transaction("s31935144306421", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("adrum_4");
    ns_web_url("adrum_4",
        "URL=https://bom-col.eum-appdynamics.com/eumcollector/beacons/browser/v1/BO-AAB-FBK/adrum",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:text/plain",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"vr":"23.10.1.4359","dt":"R","rg":"0","es":[{"eg":"1","et":2,"eu":"0://1/2/3/4/5?6","ts":1715839160967,"mg":"0","au":"0://7/8/9/10/5","at":0,"pp":3,"mx":{"PLC":1,"FBT":534,"DDT":0,"DPT":1,"PLT":535,"ARE":0},"md":"GET","xs":200,"si":17},{"eg":"2","et":2,"eu":"0://1/11/3/12/13?14","ts":1715839161499,"mg":"0","au":"0://7/8/9/10/5","at":0,"pp":3,"mx":{"PLC":1,"FBT":18,"DDT":1,"DPT":0,"PLT":19,"ARE":0},"md":"GET","xs":200,"si":18},{"eg":"3","et":2,"eu":"0://1/11/3/12/13?14","ts":1715839161500,"mg":"0","au":"0://7/8/9/10/5","at":0,"pp":3,"mx":{"PLC":1,"FBT":21,"DDT":2,"DPT":0,"PLT":23,"ARE":0},"md":"GET","xs":200,"si":19},{"eg":"4","et":2,"eu":"0://1/11/3/12/13?14","ts":1715839164413,"mg":"0","au":"0://7/15/16/10/17?18","at":0,"pp":3,"mx":{"PLC":1,"FBT":16,"DDT":1,"DPT":0,"PLT":17,"ARE":0},"md":"GET","xs":200,"si":20},{"eg":"5","et":2,"eu":"0://19/20/21/22/23","ts":1715839164407,"mg":"0","au":"0://7/15/16/10/17?18","at":0,"pp":3,"mx":{"PLC":1,"FBT":108,"DDT":1,"DPT":0,"PLT":109,"ARE":0},"md":"POST","xs":200,"si":21},{"eg":"6","et":2,"eu":"0://1/24/25/3/26?27","ts":1715839164431,"mg":"0","au":"0://7/15/16/10/17?18","at":0,"pp":3,"mx":{"PLC":1,"FBT":114,"DDT":1,"DPT":1,"PLT":116,"ARE":0},"md":"GET","xs":200,"si":22},{"eg":"7","et":2,"eu":"0://1/28/29/3/30?31","ts":1715839164413,"mg":"0","au":"0://7/15/16/10/17?18","at":0,"pp":3,"mx":{"PLC":1,"FBT":145,"DDT":1,"DPT":0,"PLT":146,"ARE":0},"md":"GET","xs":200,"si":23},{"eg":"8","et":2,"eu":"0://1/28/29/3/30?32","ts":1715839164414,"mg":"0","au":"0://7/15/16/10/17?18","at":0,"pp":3,"mx":{"PLC":1,"FBT":1130,"DDT":0,"DPT":0,"PLT":1130,"ARE":0},"md":"GET","xs":200,"si":24},{"eg":"9","et":2,"eu":"0://1/2/3/4/17?33","ts":1715839164414,"mg":"0","au":"0://7/15/16/10/17?18","at":0,"pp":3,"mx":{"PLC":1,"FBT":1144,"DDT":0,"DPT":0,"PLT":1144,"ARE":0},"md":"GET","xs":200,"si":25},{"eg":"10","et":2,"eu":"0://19/34/23/3","ts":1715839164381,"mg":"0","au":"0://7/15/16/10/17?18","at":0,"pp":3,"mx":{"PLC":1,"FBT":1304,"DDT":3,"DPT":0,"PLT":1307,"ARE":0},"md":"POST","xs":200,"si":26},{"eg":"11","et":2,"eu":"0://1/11/3/12/13?14","ts":1715839165666,"mg":"0","au":"0://7/15/16/10/17?18","at":0,"pp":3,"mx":{"PLC":1,"FBT":90,"DDT":1,"DPT":0,"PLT":91,"ARE":0},"md":"GET","xs":200,"si":27},{"eg":"12","et":2,"eu":"0://1/35/36/37/38/39/","ts":1715839165627,"mg":"0","au":"0://7/15/16/10/17?18","at":0,"pp":3,"mx":{"PLC":1,"FBT":143,"DDT":1,"DPT":0,"PLT":144,"ARE":0},"md":"POST","xs":200,"si":28},{"eg":"13","et":2,"eu":"0://1/24/25/3/26?27","ts":1715839165758,"mg":"0","au":"0://7/15/16/10/17?18","at":0,"pp":3,"mx":{"PLC":1,"FBT":56,"DDT":1,"DPT":0,"PLT":57,"ARE":0},"md":"GET","xs":200,"si":29},{"eg":"14","et":2,"eu":"0://19/34/23/3","ts":1715839165602,"mg":"0","au":"0://7/15/16/10/17?18","at":0,"pp":3,"mx":{"PLC":1,"FBT":214,"DDT":10,"DPT":0,"PLT":224,"ARE":0},"md":"POST","xs":200,"si":30},{"eg":"15","et":2,"eu":"0://19/34/23/3","ts":1715839165809,"mg":"0","au":"0://7/15/16/10/17?18","at":0,"pp":3,"mx":{"PLC":1,"FBT":137,"DDT":1,"DPT":0,"PLT":138,"ARE":0},"md":"POST","xs":200,"si":31},{"eg":"16","et":2,"eu":"0://40/41/42/43/44/45/46","ts":1715839166216,"mg":"0","au":"0://7/15/16/10/17?18","at":0,"pp":3,"mx":{"PLC":1,"FBT":49,"DDT":0,"DPT":1,"PLT":50,"ARE":0},"md":"POST","xs":200,"si":32}],"ai":"477d42aa_9f21_9144_1a76_688c9aa25e4b","gs":["94fc8c22_8b4e_9951_5dd4_a346d07ee981","6a42d6b9_2797_297b_1612_0800c03eb8a2","d26202eb_6bb6_f2bb_f668_db591890f7fd","caaffb9a_f223_4580_ea2d_899de78c9a36","0f037088_919d_b852_48c3_a4af46a4f0c2","1639e880_a170_cf84_eb25_3a3c0b770517","ca94ae3a_1d25_36be_864d_60f45aee6963","c57dabf2_f89c_815f_df26_4d44dd3549cd","8abc7548_655e_baa5_004f_77630a5fcbf0","025754c7_ecb1_7261_6832_7160fea005a4","7bd3f82c_0a94_6ccf_4e9e_e19825d0d1a5","f8b60bbb_6f66_89cc_9494_109284cf5ac7","1778f939_8836_89de_1abc_86ac91ce41ae","db21d4f6_5e43_7542_571d_9a152fbab03d","42045af3_697d_2e36_7509_9bff3b3d5fd4","c2ea6d31_3a1c_4403_9cbc_01ba852bd7a0","39b7de8c_f929_f868_d927_fdb70abb7f9f"],"up":["https","api.croma.com","searchservices","v1","category","10","currentPage=0&query=:relevance&fields=FULL&channel=WEB&channelCode=400049","www.croma.com","phones-wearables","mobile-phones","c","pwagoogle","getgeocode","400049","components='country:IN|postal_code:400049'","campaign","oneplus-r-5g","6407","q=%3Aprice-asc","api.tatadigital.com","api","v1.1","msd","events","lookup","mobile-app","storelocation","latitude=19.1047153&longitude=72.826827&","cmstemplate","allchannels","page","pageType=ContentPage&pageLabelOrId=/c/6407&fields=FULL","pageType=CategoryPage&code=6407&fields=DEFAULT","currentPage=0&query=%3Aprice-asc&fields=FULL&channel=WEB&channelCode=400049","analytics-engine","inventory","oms","v2","tms","details-pwa","smetrics.croma.com","b","ss","infinitipwa","1","JS-2.25.0-LDQM","s31935144306421"]}",
        BODY_END
    );

    ns_end_transaction("adrum_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_9");
    ns_web_url("collect_9",
        "URL=https://analytics.google.com/g/collect?v=2&tid=G-DJ3RGVFHJN&gtm=45je45f0v893318240z8889407033za200&_p=1715839139273&gcd=13l3l3l3l1&npa=0&dma=0&cid=1944455297.1715839141&ul=en-us&sr=1523x650&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&frm=0&pscdl=noapi&_eu=IA&sid=1715839140&sct=1&seg=1&dl=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&dr=https%3A%2F%2Fwww.croma.com%2F&dt=Mobile%20Phones%20%7C%20Buy%20Latest%20Mobiles%20Online%20at%20Best%20Prices%20in%20India%20%7C%20Croma&uid=false&_s=9&tfd=29019",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "en=view_item_list&pr1=id303651~nmvivo%20Y28%205G%20(4GB%20RAM%2C%20128GB%2C%20Glitter%20Aqua)~afcroma.com~cpN%2FA~k0currency~v0N%2FA~ds7%25~lpN%2FA~brVivo~caPhones%20%26%20Wearables~c2Mobile%20Phones~c3N%2FA~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr13999~qt1&pr2=id243459~nmApple%20iPhone%2013%20(128GB%2C%20Midnight)~afcroma.com~cpN%2FA~k0currency~v0N%2FA~ds10%25~lpN%2FA~brApple~caPhones%20%26%20Wearables~c2Mobile%20Phones~c3N%2FA~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr53990~qt1&pr3=id300652~nmApple%20iPhone%2015%20(128GB%2C%20Black)~afcroma.com~cpN%2FA~k0currency~v0N%2FA~ds11%25~lpN%2FA~brApple~caPhones%20%26%20Wearables~c2Mobile%20Phones~c3N%2FA~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr71290~qt1&pr4=id303650~nmvivo%20Y28%205G%20(4GB%20RAM%2C%20128GB%2C%20Crystal%20Purple)~afcroma.com~cpN%2FA~k0currency~v0N%2FA~ds7%25~lpN%2FA~brVivo~caPhones%20%26%20Wearables~c2Mobile%20Phones~c3N%2FA~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr13999~qt1&pr5=id243460~nmApple%20iPhone%2013%20(128GB%2C%20Starlight%20White)~afcroma.com~cpN%2FA~k0currency~v0N%2FA~ds10%25~lpN%2FA~brApple~caPhones%20%26%20Wearables~c2Mobile%20Phones~c3N%2FA~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr53990~qt1&pr6=id270656~nmOnePlus%20Nord%20CE%203%20Lite%205G%20(8GB%20RAM%2C%20256GB%2C%20Chromatic%20Gray)~afcroma.com~cpN%2FA~k0currency~v0N%2FA~ds9%25~lpN%2FA~brOnePlus~caPhones%20%26%20Wearables~c2Mobile%20Phones~c3N%2FA~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr19999~qt1&pr7=id306355~nmvivo%20T3x%205G%20(6GB%20RAM%2C%20128GB%2C%20Crimson%20Bliss)~afcroma.com~cpN%2FA~k0currency~v0N%2FA~ds17%25~lpN%2FA~brVivo~caPhones%20%26%20Wearables~c2Mobile%20Phones~c3N%2FA~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr14999~qt1&pr8=id306594~nmNothing%20Phone%202a%205G%20(12GB%20RAM%2C%20256GB%2C%20Blue)~afcroma.com~cpN%2FA~k0currency~v0N%2FA~ds7%25~lpN%2FA~brNothing~caPhones%20%26%20Wearables~c2Mobile%20Phones~c3N%2FA~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr27999~qt1&pr9=id300684~nmApple%20iPhone%2015%20(128GB%2C%20Blue)~afcroma.com~cpN%2FA~k0currency~v0N%2FA~ds11%25~lpN%2FA~brApple~caPhones%20%26%20Wearables~c2Mobile%20Phones~c3N%2FA~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr71290~qt1&pr10=id306353~nmvivo%20T3x%205G%20(4GB%20RAM%2C%20128GB%2C%20Celestial%20Green)~afcroma.com~cpN%2FA~k0currency~v0N%2FA~ds18%25~lpN%2FA~brVivo~caPhones%20%26%20Wearables~c2Mobile%20Phones~c3N%2FA~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr13499~qt1&pr11=id306352~nmvivo%20T3x%205G%20(4GB%20RAM%2C%20128GB%2C%20Crimson%20Bliss)~afcroma.com~cpN%2FA~k0currency~v0N%2FA~ds18%25~lpN%2FA~brVivo~caPhones%20%26%20Wearables~c2Mobile%20Phones~c3N%2FA~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr13499~qt1&pr12=id270657~nmOnePlus%20Nord%20CE%203%20Lite%205G%20(8GB%20RAM%2C%20256GB%2C%20Pastel%20Lime)~afcroma.com~cpN%2FA~k0currency~v0N%2FA~ds9%25~lpN%2FA~brOnePlus~caPhones%20%26%20Wearables~c2Mobile%20Phones~c3N%2FA~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr19999~qt1&pr13=id305654~nmvivo%20T3%205G%20(8GB%20RAM%2C%20256GB%2C%20Crystal%20Flake)~afcroma.com~cpN%2FA~k0currency~v0N%2FA~ds12%25~lpN%2FA~brVivo~caPhones%20%26%20Wearables~c2Mobile%20Phones~c3N%2FA~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr21999~qt1&pr14=id306358~nmvivo%20T3x%205G%20(8GB%20RAM%2C%20128GB%2C%20Crimson%20Bliss)~afcroma.com~cpN%2FA~k0currency~v0N%2FA~ds15%25~lpN%2FA~brVivo~caPhones%20%26%20Wearables~c2Mobile%20Phones~c3N%2FA~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr16499~qt1&pr15=id303531~nmvivo%20Y28%205G%20(6GB%20RAM%2C%20128GB%2C%20Glitter%20Aqua)~afcroma.com~cpN%2FA~k0currency~v0N%2FA~ds23%25~lpN%2FA~brVivo~caPhones%20%26%20Wearables~c2Mobile%20Phones~c3N%2FA~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr15499~qt1&pr16=id243463~nmApple%20iPhone%2013%20(128GB%2C%20Blue)~afcroma.com~cpN%2FA~k0currency~v0N%2FA~ds10%25~lpN%2FA~brApple~caPhones%20%26%20Wearables~c2Mobile%20Phones~c3N%2FA~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr53990~qt1&pr17=id305655~nmvivo%20T3%205G%20(8GB%20RAM%2C%20128GB%2C%20Cosmic%20Blue)~afcroma.com~cpN%2FA~k0currency~v0N%2FA~ds13%25~lpN%2FA~brVivo~caPhones%20%26%20Wearables~c2Mobile%20Phones~c3N%2FA~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr19999~qt1&pr18=id305870~nmOnePlus%20Nord%20CE4%205G%20(8GB%20RAM%2C%20128GB%2C%20Marble)~afcroma.com~cpN%2FA~k0currency~v0N%2FA~ds0%25~lpN%2FA~brOnePlus~caPhones%20%26%20Wearables~c2Mobile%20Phones~c3N%2FA~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr24999~qt1&pr19=id271022~nmOnePlus%20Nord%20CE%203%20Lite%205G%20(8GB%20RAM%2C%20128GB%2C%20Pastel%20Lime)~afcroma.com~cpN%2FA~k0currency~v0N%2FA~ds10%25~lpN%2FA~brOnePlus~caPhones%20%26%20Wearables~c2Mobile%20Phones~c3N%2FA~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr17999~qt1&pr20=id305653~nmvivo%20T3%205G%20(8GB%20RAM%2C%20256GB%2C%20Cosmic%20Blue)~afcroma.com~cpN%2FA~k0currency~v0N%2FA~ds12%25~lpN%2FA~brVivo~caPhones%20%26%20Wearables~c2Mobile%20Phones~c3N%2FA~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr21999~qt1&pr21=id300738~nmApple%20iPhone%2015%20(256GB%2C%20Blue)~afcroma.com~cpN%2FA~k0currency~v0N%2FA~ds10%25~lpN%2FA~brApple~caPhones%20%26%20Wearables~c2Mobile%20Phones~c3N%2FA~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr80590~qt1&ep.pagetype=plp&ep.source_page_url=%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407&ep.previous_page_url=%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&ep.destination_page_url=%2FsearchB%3Fq%3Dnothing%2520phone%25202%253Arelevance%26text%3Dnothing%2520phone%25202&ep.login_trigger=N%2FA&ep.login_status=false&ep.click_text=N%2FA&ep.is_cross_sell=N%2FA&_et=3680
en=view_item_list&pr1=id304696~nmOnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Iron%20Grey)~afcroma.com~cpN%2FA~k0currency~v0N%2FA~ds0%25~lpN%2FA~brOnePlus~caCampaign~c2OnePlus%2012R%205G~c3N%2FA~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr39999~qt1&pr2=id304697~nmOnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)~afcroma.com~cpN%2FA~k0currency~v0N%2FA~ds0%25~lpN%2FA~brOnePlus~caCampaign~c2OnePlus%2012R%205G~c3N%2FA~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr39999~qt1&pr3=id304698~nmOnePlus%2012R%205G%20(16GB%2C%20256GB%2C%20Iron%20Grey)~afcroma.com~cpN%2FA~k0currency~v0N%2FA~ds0%25~lpN%2FA~brOnePlus~caCampaign~c2OnePlus%2012R%205G~c3N%2FA~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr45999~qt1&pr4=id304699~nmOnePlus%2012R%205G%20(16GB%2C%20256GB%2C%20Cool%20Blue)~afcroma.com~cpN%2FA~k0currency~v0N%2FA~ds0%25~lpN%2FA~brOnePlus~caCampaign~c2OnePlus%2012R%205G~c3N%2FA~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr45999~qt1&ep.pagetype=plp&ep.source_page_url=%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407&ep.previous_page_url=%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&ep.destination_page_url=%2FsearchB%3Fq%3Dnothing%2520phone%25202%253Arelevance%26text%3Dnothing%2520phone%25202&ep.login_trigger=N%2FA&ep.login_status=false&ep.click_text=N%2FA&ep.is_cross_sell=N%2FA&_et=490&dt=OnePlus%2012R%205G",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.facebook.com/tr/?id=1369867960112522&ev=PageView&dl=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&rl=&if=false&ts=1715839166668&sw=1523&sh=650&v=2.9.156&r=stable&ec=5&o=4126&fbp=fb.1.1715839140634.281453602&ler=empty&cdl=API_unavailable&it=1715839140500&coo=false&eid=91344459232060&tm=1&rqm=GET", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googleadservices.com/pagead/conversion/609902077/?random=1715839166672&cv=11&fst=1715839166672&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=OnePlus%2012R%205G&value=0&bttype=purchase&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&rfmt=3&fmt=4", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googleadservices.com/pagead/conversion/1006316414/?random=1715839166677&cv=11&fst=1715839166677&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=OnePlus%2012R%205G&value=0&bttype=purchase&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&rfmt=3&fmt=4", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/609902077/?random=202293622&cv=11&fst=1715839166672&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=OnePlus%2012R%205G&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtWC8ri3cDtgd0aANdBJmK5O8FrRvEWWPJw&pscrd=IhMIlNKYgL-RhgMVg0idCR3wSACyMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1006316414/?random=1274511321&cv=11&fst=1715839166677&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=OnePlus%2012R%205G&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtczRnsrN01F0f51r9LGMGODH8HOE6zQY-Q&pscrd=IhMI6ZSZgL-RhgMVDEqdCR1Dwg5iMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-conversion/609902077/?random=202293622&cv=11&fst=1715839166672&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=OnePlus%2012R%205G&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMIlNKYgL-RhgMVg0idCR3wSACyMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtq0x3GFd3ZDjkAVLQBbybC9GXhBFq1oQ&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtetxENeOyRBuFeW9E128f1ermIWlPLPrgw&random=1175472525", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-conversion/1006316414/?random=1274511321&cv=11&fst=1715839166677&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=OnePlus%2012R%205G&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMI6ZSZgL-RhgMVDEqdCR1Dwg5iMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtqIgfqPk03rbSWmrM-zfODEYsT-s5xCg&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtY_SrdDGAHDcuktK6Nc5XUJh6X_9GJLFxA&random=382498057", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-conversion/609902077/?random=202293622&cv=11&fst=1715839166672&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=OnePlus%2012R%205G&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMIlNKYgL-RhgMVg0idCR3wSACyMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtq0x3GFd3ZDjkAVLQBbybC9GXhBFq1oQ&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtetxENeOyRBuFeW9E128f1ermIWlPLPrgw&random=1175472525&ipr=y", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-conversion/1006316414/?random=1274511321&cv=11&fst=1715839166677&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=OnePlus%2012R%205G&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMI6ZSZgL-RhgMVDEqdCR1Dwg5iMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtqIgfqPk03rbSWmrM-zfODEYsT-s5xCg&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtY_SrdDGAHDcuktK6Nc5XUJh6X_9GJLFxA&random=382498057&ipr=y", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s34590877932019?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%200%3A59%3A28%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=plp&g=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253aprice-asc&cc=INR&server=www.croma.com&events=event55%2Cevent72%2Cevent20%2Cevent21%2Cevent50%3Dundefined&products=%3B%3B%3B%3Bevent20%3D%7Cevent21%3D%3BeVar10%3D%7CeVar12%3D%7CeVar14%3D%7CeVar13%3D%7CeVar26%3D%7CeVar27%3D%7CeVar28%3D%7CeVar29%3D%7CeVar41%3D&c1=plp&v1=D%3Dmid&l1=q%3D%253Aprice-asc&c2=www.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407&v2=new&c3=2024-5-16%200%3A59%3A28&l3=hero%20banner%3A%3Aplp&c4=plp&v4=guest%20user&v5=400049&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5BImpression%5D%20%3A%20impression&v156=hero%20banner%3A%3Aplp&pe=lnk_o&pev2=impression%3A%20tracking&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&lrt=50&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN;s_nr30", END_INLINE
    );

    ns_end_transaction("collect_9", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("iev");
    ns_web_url("iev",
        "URL=https://csm.sg1.as.criteo.net/iev?entry=c~Gum.ChromeSyncframe.SidReadSuccess~1&entry=h~Gum.ChromeSyncframe.SidReadSuccessDuration~143",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("iev", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_10");
    ns_web_url("collect_10",
        "URL=https://analytics.google.com/g/collect?v=2&tid=G-DJ3RGVFHJN&gtm=45je45f0v893318240za200&_p=1715839139273&gcd=13l3l3l3l1&npa=0&dma=0&cid=1944455297.1715839141&ul=en-us&sr=1523x650&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&frm=0&pscdl=noapi&_eu=AEA&_s=10&dl=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&dr=https%3A%2F%2Fwww.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&sid=1715839140&sct=1&seg=1&dt=OnePlus%2012R%205G&en=page_view&_et=971&tfd=34023",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("collect_10", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_11");
    ns_web_url("collect_11",
        "URL=https://analytics.google.com/g/collect?v=2&tid=G-DJ3RGVFHJN&gtm=45je45f0v893318240z8889407033za200&_p=1715839139273&_gaz=1&gcd=13l3l3l3l1&npa=0&dma=0&cid=1944455297.1715839141&ul=en-us&sr=1523x650&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&frm=0&pscdl=noapi&_eu=IA&_s=11&sid=1715839140&sct=1&seg=1&dl=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&dr=https%3A%2F%2Fwww.croma.com%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&dt=OnePlus%2012R%205G&uid=false&en=select_item&pr1=id304697~nmOnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)~afcroma.com~ds0%25~lp1~brOnePlus~caCampaign~c2OnePlus%2012R%205G~c4~c5~li~ln~va~lo~pr39999&ep.pagetype=plp&ep.source_page_url=%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407&ep.previous_page_url=%2Fphones-wearables%2Fmobile-phones%2Fc%2F10&ep.login_trigger=N%2FA&ep.login_status=false&ep.click_text=N%2FA&_et=35130&tfd=64154",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("collect_11", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_13");
    ns_web_url("v1_13",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tL2NhbXBhaWduL29uZXBsdXMtci01Zy9jLzY0MDc/cT0lM0FwcmljZS1hc2MiLCJldmVudERhdGEiOlt7InRpbWVzdGFtcCI6MTcxNTgzOTIwMTk4MSwiZGF0YSI6IntcImV2ZW50XCI6XCJ3ZWItdml0YWxzXCIsXCJkYXRhXCI6e1wibmFtZVwiOlwiQ0xTXCIsXCJ2YWx1ZVwiOjAuNDI0OTQzMzIzMzA4MTk0NCxcInJhdGluZ1wiOlwicG9vclwiLFwiZGVsdGFcIjowLjQyNDk0MzMyMzMwODE5NDQsXCJlbnRyaWVzXCI6W3tcIm5hbWVcIjpcIlwiLFwiZW50cnlUeXBlXCI6XCJsYXlvdXQtc2hpZnRcIixcInN0YXJ0VGltZVwiOjE5MTg3LjE5OTk5ODg1NTU5LFwiZHVyYXRpb25cIjowLFwidmFsdWVcIjowLjQyNDk0MzMyMzMwODE5NDQsXCJoYWRSZWNlbnRJbnB1dFwiOmZhbHNlLFwibGFzdElucHV0VGltZVwiOjE4MTU4Ljg5OTk5OTYxODUzLFwic291cmNlc1wiOlt7XCJwcmV2aW91c1JlY3RcIjp7XCJ4XCI6MTU5LFwieVwiOjk3LFwid2lkdGhcIjoxMTcwLFwiaGVpZ2h0XCI6MTUsXCJ0b3BcIjo5NyxcInJpZ2h0XCI6MTMyOSxcImJvdHRvbVwiOjExMixcImxlZnRcIjoxNTl9LFwiY3VycmVudFJlY3RcIjp7XCJ4XCI6MTU5LFwieVwiOjk3LFwid2lkdGhcIjoxMTcwLFwiaGVpZ2h0XCI6MTUsXCJ0b3BcIjo5NyxcInJpZ2h0XCI6MTMyOSxcImJvdHRvbVwiOjExMixcImxlZnRcIjoxNTl9fSx7XCJwcmV2aW91c1JlY3RcIjp7XCJ4XCI6MTg0LFwieVwiOjE5OCxcIndpZHRoXCI6MTE0MCxcImhlaWdodFwiOjE4LFwidG9wXCI6MTk4LFwicmlnaHRcIjoxMzI0LFwiYm90dG9tXCI6MjE2LFwibGVmdFwiOjE4NH0sXCJjdXJyZW50UmVjdFwiOntcInhcIjowLFwieVwiOjAsXCJ3aWR0aFwiOjAsXCJoZWlnaHRcIjowLFwidG9wXCI6MCxcInJpZ2h0XCI6MCxcImJvdHRvbVwiOjAsXCJsZWZ0XCI6MH19LHtcInByZXZpb3VzUmVjdFwiOntcInhcIjowLFwieVwiOjIxOSxcIndpZHRoXCI6MTUwOCxcImhlaWdodFwiOjE0NSxcInRvcFwiOjIxOSxcInJpZ2h0XCI6MTUwOCxcImJvdHRvbVwiOjM2NCxcImxlZnRcIjowfSxcImN1cnJlbnRSZWN0XCI6e1wieFwiOjAsXCJ5XCI6MCxcIndpZHRoXCI6MCxcImhlaWdodFwiOjAsXCJ0b3BcIjowLFwicmlnaHRcIjowLFwiYm90dG9tXCI6MCxcImxlZnRcIjowfX0se1wicHJldmlvdXNSZWN0XCI6e1wieFwiOjAsXCJ5XCI6NDcxLFwid2lkdGhcIjoxNTA4LFwiaGVpZ2h0XCI6NTIsXCJ0b3BcIjo0NzEsXCJyaWdodFwiOjE1MDgsXCJib3R0b21cIjo1MjMsXCJsZWZ0XCI6MH0sXCJjdXJyZW50UmVjdFwiOntcInhcIjowLFwieVwiOjAsXCJ3aWR0aFwiOjAsXCJoZWlnaHRcIjowLFwidG9wXCI6MCxcInJpZ2h0XCI6MCxcImJvdHRvbVwiOjAsXCJsZWZ0XCI6MH19XX1dLFwiaWRcIjpcInYzLTE3MTU4MzkxNDAzODUtNTA1MTM2MDY1MTIxMlwiLFwibmF2aWdhdGlvblR5cGVcIjpcIm5hdmlnYXRlXCJ9fSJ9XSwiY3VzdG9tZXJIYXNoIjpudWxsLCJlY2lkIjoiVW5rbm93biIsImNsaWVudElkIjoiQ1JPTUEtV0VCLUFQUCIsInNlc3Npb25JZCI6ImI2NWY1NGI0LTVjOTMtNDgyZS04Y2Y4LWMyMTBlY2ZmYTc4Zi45MjNiYjU3Zi05NTFiLTQ3ODAtOGNjMy0zZTdjODllOTA1ZmUiLCJicmFuZCI6IkNST01BIiwidGltZXN0YW1wIjoxNzE1ODM5MjAxOTgyLCJkZXZpY2UiOiJNb3ppbGxhLzUuMCAoWDExOyBMaW51eCB4ODZfNjQpIEFwcGxlV2ViS2l0LzUzNy4zNiAoS0hUTUwsIGxpa2UgR2Vja28pIENocm9tZS8xMTQuMC4wLjAgU2FmYXJpLzUzNy4zNiIsImphcnZpc0lkIjoiNGQ2MDQ1NzAtYmFmYi00NTUxLWJhZDAtODUzODk3MWM3ZWUyIiwiYWR2ZXJ0aXNpbmdJZCI6bnVsbCwiYXBwVmVyc2lvbiI6IjAifQ=="}",
        BODY_END
    );

    ns_end_transaction("v1_13", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("s42804754789358");
    ns_web_url("s42804754789358",
        "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s42804754789358",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;s_tp;s_ppv;_ga_DJ3RGVFHJN;s_nr30;s_sq",
        BODY_BEGIN,
            "AQB=1&ndh=1&pf=1&t=16%2F4%2F2024+1%3A0%3A1+4+300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=plp&g=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253aprice-asc&cc=INR&server=www.croma.com&events=event4%2Cevent20%2Cevent21%2Cevent49%2Cevent54%2Cevent19%3D2%2Cevent50%3Dundefined&products=%3B304697%3B%3B%3Bevent20%3D39999%7Cevent21%3D39999%7Cevent54%3Dundefined%7Cevent49%3D%3BeVar10%3D304697%7CeVar14%3D39999%7CeVar13%3D39999%7CeVar23%3D%7CeVar123%3Dundefined%7CeVar12%3D2%7CeVar24%3D%7CeVar144%3Dundefined%7CeVar143%3Dundefined%7CeVar25%3D%7CeVar26%3Dyes%7CeVar27%3Dno%3Anot+serviceable%7CeVar28%3Dyes%3A0%7CeVar29%3Dno%3Anot+serviceable%7CeVar111%3Dundefined%7CeVar117%3Dundefined%7CeVar115%3Dundefined%7CeVar148%3Dundefined%7CeVar149%3Dundefined%7CeVar116%3Dundefined%7CeVar114%3Dundefined%7CeVar118%3Dundefined%7CeVar119%3Dundefined%7CeVar120%3Dundefined%7CeVar41%3DOnePlus+12R+5G+%288GB+128GB+Cool+Blue%29%7CeVar43%3DCampaign%3AOnePlus+12R+5G&c1=plp&v1=D%3Dmid&l1=q%3D%253Aprice-asc&c2=www.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407&v2=new&c3=2024-5-16+1%3A0%3A1&l3=plpresult%3Aoneplus+12r+5g+%288gb+128gb+cool+blue%29%3Aplp&c4=plp&v4=guest+user&v5=400049&v15=productimage&v16=clicked&v17=body&v39=RL9bd43344037f476081fbf32b9dc7acb2&v53=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&v71=productimage%7Cbody%7Cclicked&v121=no&v131=Direct+Call+%2850%29+%3A+%5BotherClick%5D+%3A+Other+Click&v156=plpresult%3Aoneplus+12r+5g+%288gb+128gb+cool+blue%29%3Aplp&pe=lnk_o&pev2=productimage&c.=&a.=&activitymap.=&page=plp&link=OnePlus+12R+5G+%288GB%2C+128GB%2C+Cool+Blue%29&region=304697&pageIDType=1&.activitymap=&.a=&.c=&pid=plp&pidt=1&oid=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ot=A&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&lrt=39&AQE=1",
        BODY_END
    );

    ns_end_transaction("s42804754789358", NS_AUTO_STATUS);
    ns_page_think_time(0.106);

    //Page Auto split for Image Link 'OnePlus 12R 5G (8GB, 128GB, Cool Blue)' Clicked by User
    ns_start_transaction("ga_audiences");
    ns_web_url("ga_audiences",
        "URL=https://www.google.co.in/ads/ga-audiences?v=1&t=sr&slf_rd=1&_r=4&tid=G-DJ3RGVFHJN&cid=1944455297.1715839141&gtm=45je45f0v893318240z8889407033za200&aip=1&uid=false&dma=0&gcd=13l3l3l3l1&npa=0&frm=0&z=1282073993",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:image",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("ga_audiences", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("events_4");
    ns_web_url("events_4",
        "URL=https://api.tatadigital.com/api/v1.1/msd/events",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Program-Id:01eae2ec-0576-1000-bbea-86e16dcb4b79",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"page_type":"clp","content_type":"product","action_name":"click","event_name":"view_details","correlation_id":"sssaasss","medium":"pixel","metadata":{},"source":"CROMA","user_id":["08vHUbbT7lzWpSwZ"],"user_id_type":["CROMA"],"mad_uuid":"08vHUbbT7lzWpSwZ","utm":{"utm_source":"","utm_medium":"","utm_campaign":""},"epoch":"1715839201749","content_info":{"source_id":"304697","value":"39999","catalog_value":"39999"},"pincode":"400049"}",
        BODY_END
    );

    ns_end_transaction("events_4", NS_AUTO_STATUS);
    ns_page_think_time(0.082);

    ns_start_transaction("X04697");
    ns_web_url("X04697",
        "URL=https://www.croma.com/oneplus-12r-5g-8gb-128gb-cool-blue-/p/304697",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;mbox;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;RT;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv"
    );

    ns_end_transaction("X04697", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_14");
    ns_web_url("v1_14",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        INLINE_URLS,
            "URL=https://assets.croma.com/assets/fonts/croma.woff2", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=Origin:https://www.croma.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;mbox;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv", END_INLINE,
            "URL=https://www.croma.com/assets/fonts/Switzer-Bold.woff2", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=Origin:https://www.croma.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;mbox;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;RT;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv", END_INLINE,
            "URL=https://www.croma.com/assets/fonts/Switzer-Regular.woff2", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=Origin:https://www.croma.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;mbox;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;RT;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv", END_INLINE,
            "URL=https://www.croma.com/assets/fonts/Switzer-Medium.woff2", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=Origin:https://www.croma.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;mbox;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;RT;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv", END_INLINE
    );

    ns_end_transaction("v1_14", NS_AUTO_STATUS);
    ns_page_think_time(0.008);

    ns_start_transaction("activityi_dc_pre_CO3frpO_kYY");
    ns_web_url("activityi_dc_pre_CO3frpO_kYY",
        "URL=https://assets.adobedtm.com/a83cfb422665/6969f0a69b1e/launch-883ee2cb26fd.min.js",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:script",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.croma.com/static/css/19.7113b688.chunk.css", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;mbox;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;RT;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv", END_INLINE,
            "URL=https://www.croma.com/static/css/main.50a49cb7.chunk.css", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;mbox;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;RT;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv", END_INLINE,
            "URL=https://accounts.tatadigital.com/v2/tdl-sso-auth.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("activityi_dc_pre_CO3frpO_kYY", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("delivery_2");
    ns_web_url("delivery_2",
        "URL=https://cromaretail.tt.omtrdc.net/rest/v1/delivery?client=cromaretail&sessionId=895fcaa2d50341f0b4231873c20498c7&version=2.9.0",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"requestId":"d90533a704884002b3f7334e92012066","context":{"userAgent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36","clientHints":{"mobile":false,"platform":"Linux","browserUAWithMajorVersion":"\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\""},"timeOffsetInMinutes":-300,"channel":"web","screen":{"width":1523,"height":650,"orientation":"landscape","colorDepth":24,"pixelRatio":1},"window":{"width":1523,"height":523},"browser":{"host":"www.croma.com","webGLRenderer":"ANGLE (Google, Vulkan 1.3.0 (SwiftShader Device (Subzero) (0x0000C0DE)), SwiftShader driver)"},"address":{"url":"https://www.croma.com/oneplus-12r-5g-8gb-128gb-cool-blue-/p/304697","referringUrl":"https://www.croma.com/campaign/oneplus-r-5g/c/6407?q=%3Aprice-asc"}},"id":{"tntId":"895fcaa2d50341f0b4231873c20498c7.41_0","marketingCloudVisitorId":"23208888194201725061814923007976880958"},"experienceCloud":{"audienceManager":{"locationHint":12,"blob":"6G1ynYcLPuiQxYZrsz_pkqfLG9yMXBpb2zX5dvJdYQJzPXImdj0y"},"analytics":{"logging":"server_side","supplementalDataId":"7892C226D399CF1A-7543E32B5F623B41","trackingServer":"metrics.croma.com","trackingServerSecure":"smetrics.croma.com"}},"execute":{"pageLoad":{}},"prefetch":{"views":[{}]},"telemetry":{"entries":[]}}",
        BODY_END,
        INLINE_URLS,
            "URL=https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://s.go-mpulse.net/boomerang/Y64HZ-S7F3X-X4LT4-6FRFB-MK4UE", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://maps.googleapis.com/maps/api/js?key=AIzaSyBALhlQSaWB7dWn--IJ95ozhB3_3Zzlcdc&libraries=places&callback=initMap", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1637759004/Croma%20Assets/CMS/Category%20icon/Final%20icon/Croma_Logo_acrkvn.svg", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1706791128/Croma%20Assets/Communication/Mobiles/Images/304697_zp8rwp.png?tr=w-100", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/f_auto,q_auto,d_Croma%20Assets:No_image.png/Croma%20Assets/UI%20Assets/video_thumbnail.png", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1707378062/Croma%20Assets/Communication/Mobiles/Images/304697_1_vkmmz6.png?tr=w-100", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1707378061/Croma%20Assets/Communication/Mobiles/Images/304697_2_lheiul.png?tr=w-100", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1706791125/Croma%20Assets/Communication/Mobiles/Images/304697_3_htrwa0.png?tr=w-100", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1706791120/Croma%20Assets/Communication/Mobiles/Images/304697_4_uaovpj.png?tr=w-100", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1706791110/Croma%20Assets/Communication/Mobiles/Images/304697_5_xlaa6u.png?tr=w-100", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1706791113/Croma%20Assets/Communication/Mobiles/Images/304697_6_vb8mx6.png?tr=w-100", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1706791128/Croma%20Assets/Communication/Mobiles/Images/304697_zp8rwp.png?tr=w-640", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1707378062/Croma%20Assets/Communication/Mobiles/Images/304697_1_vkmmz6.png?tr=w-640", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1707378061/Croma%20Assets/Communication/Mobiles/Images/304697_2_lheiul.png?tr=w-640", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1706791125/Croma%20Assets/Communication/Mobiles/Images/304697_3_htrwa0.png?tr=w-640", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1706791120/Croma%20Assets/Communication/Mobiles/Images/304697_4_uaovpj.png?tr=w-640", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1706791110/Croma%20Assets/Communication/Mobiles/Images/304697_5_xlaa6u.png?tr=w-640", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1706791113/Croma%20Assets/Communication/Mobiles/Images/304697_6_vb8mx6.png?tr=w-640", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox", END_INLINE,
            "URL=https://assets.adobedtm.com/extensions/EPef068a8d6dd34a43866d9a80cc98baab/AppMeasurement.min.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/extensions/EPef068a8d6dd34a43866d9a80cc98baab/AppMeasurement_Module_ActivityMap.min.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://edge.fullstory.com/s/fs.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=Origin:https://www.croma.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("delivery_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("v2_2");
    ns_web_url("v2_2",
        "URL=https://api.tatadigital.com/analytics-engine/config/v2",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Site-Origin:https://www.croma.com",
        "HEADER=Content-Type:application/json",
        "HEADER=Client-Id:CROMA-WEB-APP",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.croma.com/assets/images/cts-white.svg", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;RT;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox", END_INLINE,
            //URL commented due to error code : ABORTED
            //"URL=https://media-ik.croma.com/prod/https://media.croma.com/video/upload/v1713436641/Croma%20Assets/Videos/2024%20Videos/304697_w9hxoc.mp4?tr=q-70", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Range:bytes=0-", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:video", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox", END_INLINE,
            "URL=https://www.googletagmanager.com/gtm.js?id=GTM-5X2VQJX", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cdn.appdynamics.com/adrum/adrum-23.10.1.4359.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.croma.com/assets/js/cromaUtility.min.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;RT;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox", END_INLINE,
            "URL=https://www.croma.com/assets/js/cromaSso.min.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;RT;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox", END_INLINE,
            "URL=https://www.croma.com/static/js/19.b5f9868a.chunk.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;RT;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox", END_INLINE,
            "URL=https://www.croma.com/static/js/main.705c574a.chunk.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;RT;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox", END_INLINE
    );

    ns_end_transaction("v2_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("web_2");
    ns_web_url("web_2",
        "URL=https://edge.fullstory.com/s/settings/11EGJ5/v1/web",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.googletagmanager.com/gtag/js?id=G-DJ3RGVFHJN&l=dataLayer&cx=c", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googletagmanager.com/gtag/destination?id=AW-609902077&l=dataLayer&cx=c", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://bat.bing.com/bat.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dynamic.criteo.com/js/ld/ld.js?a=56256", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static.hotjar.com/c/hotjar-3400595.js?sv=7", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googletagmanager.com/gtag/destination?id=AW-1006316414&l=dataLayer&cx=c", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googletagmanager.com/gtag/destination?id=DC-917807264&l=dataLayer&cx=c", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googletagmanager.com/gtag/destination?id=DC-13135721&l=dataLayer&cx=c", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://connect.facebook.net/en_US%20/fbevents.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://capi.croma.com/static/DhPixel.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox", END_INLINE,
            "URL=https://d2r1yp2w7bby2u.cloudfront.net/js/clevertap.min.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("web_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("check_session_2");
    ns_web_url("check_session_2",
        "URL=https://api.tatadigital.com/api/v2/sso/check-session",
        "METHOD=POST",
        "HEADER=Access-Control-Allow-Origin:https://api.tatadigital.com",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"codeChallenge":"YUP2MXZ3_ChIMTL2BBpD9OBt-gD7k1hFzdwydw8KvYs","redirectUrl":"https://www.croma.com/oneplus-12r-5g-8gb-128gb-cool-blue-/p/304697"}",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.croma.com/static/js/52.3a9a1fa1.chunk.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox;RT", END_INLINE,
            "URL=https://www.croma.com/static/js/59.7c9407cc.chunk.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox;RT", END_INLINE,
            "URL=https://www.croma.com/static/js/51.9d947cc8.chunk.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox;RT", END_INLINE,
            "URL=https://www.croma.com/static/js/60.71510179.chunk.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox;RT", END_INLINE,
            "URL=https://www.croma.com/static/js/62.71e43375.chunk.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox;RT", END_INLINE
    );

    ns_end_transaction("check_session_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("data");
    ns_web_url("data",
        "URL=https://api.tatadigital.com/api/v1.1/msd/data",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Program-Id:01eae2ec-0576-1000-bbea-86e16dcb4b79",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"correlation_id":"sssaasss","client":"CROMA","catalog":"croma_products","user_id":"08vHUbbT7lzWpSwZ","org_user_id":"","mad_uuid":"08vHUbbT7lzWpSwZ","fields":["no_of_views","product_image_url","product_title","product_price","product_image_text","product_detail_page_url","rating"],"data_params":{"catalog_item_id":"304697"}}",
        BODY_END
    );

    ns_end_transaction("data", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("getApplicationPromotionsForI");
    ns_web_url("getApplicationPromotionsForI",
        "URL=https://api.tatadigital.com/getApplicablePromotion/getApplicationPromotionsForItemOffer",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=client_id:CROMA",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Authorization:8Tksadcs85ad4vsasfasgf4sJHvfs4NiKNKLHKLH582546f646",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "%7B%22getApplicablePromotionsForItemRequest%22%3A%7B%22itemId%22%3A%22304697%22%2C%22programId%22%3A%2201eae2ec-0576-1000-bbea-86e16dcb4b79%22%2C%22channelIds%22%3A%5B%22TCPCHS0003%22%5D%2C%22status%22%3A%22ACTIVE%22%7D%7D=",
        BODY_END,
        INLINE_URLS,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1706791128/Croma%20Assets/Communication/Mobiles/Images/304697_zp8rwp.png", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox", END_INLINE
    );

    ns_end_transaction("getApplicationPromotionsForI", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("details_pwa_2");
    ns_web_url("details_pwa_2",
        "URL=https://api.croma.com/inventory/oms/v2/tms/details-pwa/",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=Content-Type:application/json",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=oms-apim-subscription-key:1131858141634e2abe2efb2b3a2a2a5d",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox",
        BODY_BEGIN,
            "{"promise":{"allocationRuleID":"SYSTEM","checkInventory":"Y","organizationCode":"CROMA","sourcingClassification":"EC","promiseLines":{"promiseLine":[{"fulfillmentType":"HDEL","mch":"","itemID":"304697","lineId":"1","categoryType":"mobile","reEndDate":"2500-01-01","reqStartDate":"","requiredQty":"1","shipToAddress":{"company":"","country":"","city":"","mobilePhone":"","state":"","zipCode":"400049","extn":{"irlAddressLine1":"","irlAddressLine2":""}},"extn":{"widerStoreFlag":"N"}},{"fulfillmentType":"STOR","mch":"","itemID":"304697","lineId":"2","categoryType":"mobile","reqEndDate":"","reqStartDate":"","requiredQty":"1","shipToAddress":{"company":"","country":"","city":"","mobilePhone":"","state":"","zipCode":"400049","extn":{"irlAddressLine1":"","irlAddressLine2":""}},"extn":{"widerStoreFlag":"N"}},{"fulfillmentType":"SDEL","mch":"","itemID":"304697","lineId":"3","categoryType":"mobile","reqEndDate":"","reqStartDate":"","requiredQty":"1","shipToAddress":{"company":"","country":"","city":"","mobilePhone":"","state":"","zipCode":"400049","extn":{"irlAddressLine1":"","irlAddressLine2":""}},"extn":{"widerStoreFlag":"N"}}]}}}",
        BODY_END
    );

    ns_end_transaction("details_pwa_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("essentialcombo");
    ns_web_url("essentialcombo",
        "URL=https://api.croma.com/sku/v1/essentialcombo?pinCode=400049&ProductSkus=304697",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox"
    );

    ns_end_transaction("essentialcombo", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("price");
    ns_web_url("price",
        "URL=https://api.croma.com/pricing-services/v1/price?productList=304697",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=channel:EC",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox",
        INLINE_URLS,
            "URL=https://www.croma.com/assets/js/Drift.min.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox;RT", END_INLINE,
            "URL=https://www.croma.com/static/css/40.5effb02f.chunk.css", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox;RT", END_INLINE,
            "URL=https://www.croma.com/static/js/40.93cbd39b.chunk.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox;RT", END_INLINE
    );

    ns_end_transaction("price", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("gen_204_2");
    ns_web_url("gen_204_2",
        "URL=https://maps.googleapis.com/maps/api/mapsjs/gen_204?csp_test=true",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://maps.googleapis.com/maps-api-v3/api/js/56/12a/common.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://maps.googleapis.com/maps-api-v3/api/js/56/12a/util.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://maps.googleapis.com/maps-api-v3/api/js/56/12a/geocoder.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://connect.facebook.net/signals/config/1369867960112522?v=2.9.156&r=stable&domain=www.croma.com&hme=c3a545c63044e8e9102d4f32d84a1137594d024f28e801d670bc76dc5c075575&ex_m=67%2C112%2C99%2C103%2C58%2C3%2C93%2C66%2C15%2C91%2C84%2C49%2C51%2C158%2C161%2C172%2C168%2C169%2C171%2C28%2C94%2C50%2C73%2C170%2C153%2C156%2C165%2C166%2C173%2C121%2C14%2C48%2C178%2C177%2C123%2C17%2C33%2C38%2C1%2C41%2C62%2C63%2C64%2C68%2C88%2C16%2C13%2C90%2C87%2C86%2C100%2C102%2C37%2C101%2C29%2C25%2C154%2C157%2C130%2C27%2C10%2C11%2C12%2C5%2C6%2C24%2C21%2C22%2C54%2C59%2C61%2C71%2C95%2C26%2C72%2C8%2C7%2C76%2C46%2C20%2C97%2C96%2C9%2C19%2C18%2C81%2C53%2C79%2C32%2C70%2C0%2C89%2C31%2C78%2C83%2C45%2C44%2C82%2C36%2C4%2C85%2C77%2C42%2C39%2C34%2C80%2C2%2C35%2C60%2C40%2C98%2C43%2C75%2C65%2C104%2C57%2C56%2C30%2C92%2C55%2C52%2C47%2C74%2C69%2C23%2C105", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("gen_204_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("events_5");
    ns_web_url("events_5",
        "URL=https://api.tatadigital.com/api/v1.1/msd/events",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Program-Id:01eae2ec-0576-1000-bbea-86e16dcb4b79",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"page_type":"cdp","content_type":"product","action_name":"view","event_name":"page_view","correlation_id":"sssaasss","medium":"pixel","metadata":{},"source":"CROMA","user_id":["08vHUbbT7lzWpSwZ"],"user_id_type":["CROMA"],"mad_uuid":"08vHUbbT7lzWpSwZ","utm":{"utm_source":"","utm_medium":"","utm_campaign":""},"epoch":"1715839205727","content_info":{"source_id":"304697","value":"39999","catalog_value":"39999"},"pincode":"400049"}",
        BODY_END
    );

    ns_end_transaction("events_5", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("emidata");
    ns_web_url("emidata",
        "URL=https://api.croma.com/product/allchannels/v1/emidata?fields=FULL&amount=39999",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;_ga_DJ3RGVFHJN;s_nr30;s_sq;s_tp;s_ppv;mbox"
    );

    ns_end_transaction("emidata", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_15");
    ns_web_url("v1_15",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=customer-hash:null",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=tdl-sso-version:4.1.15",
        "HEADER=session:false",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tL29uZXBsdXMtMTJyLTVnLThnYi0xMjhnYi1jb29sLWJsdWUtL3AvMzA0Njk3IiwiZXZlbnREYXRhIjpbeyJ0aW1lc3RhbXAiOjE3MTU4MzkyMDU3NjIsImRhdGEiOiJ7XCJldmVudFwiOlwicmVxdWVzdFwiLFwicGF5bG9hZFwiOntcInJlcXVlc3RIZWFkZXJzXCI6e1wiQWNjZXB0XCI6XCJhcHBsaWNhdGlvbi9qc29uLCB0ZXh0L3BsYWluLCAqLypcIixcImNoYW5uZWxcIjpcIkVDXCJ9LFwicmVxdWVzdFR5cGVcIjpcIkdFVFwiLFwicmVxdWVzdFVybFwiOlwiaHR0cHM6Ly9hcGkuY3JvbWEuY29tL3ByaWNpbmctc2VydmljZXMvdjEvcHJpY2U/cHJvZHVjdExpc3Q9MzA0Njk3XCIsXCJyZXNwb25zZVwiOlwie1xcXCJwcmljZWxpc3RcXFwiOlt7XFxcIml0ZW1JZFxcXCI6XFxcIjMwNDY5N1xcXCIsXFxcImN1cnJlbmN5SXNvXFxcIjpcXFwiSU5SXFxcIixcXFwiZGlzY291bnRWYWx1ZVxcXCI6XFxcIjAuMDBcXFwiLFxcXCJkaXNjb3VudFBlcmNlbnRhZ2VcXFwiOlxcXCIwLjAwJVxcXCIsXFxcIm1ycFxcXCI6XFxcIuKCuTM5LDk5OS4wMFxcXCIsXFxcIm1ycFZhbHVlXFxcIjpcXFwiMzk5OTlcXFwiLFxcXCJzZWxsaW5nUHJpY2VcXFwiOlxcXCLigrkzOSw5OTkuMDBcXFwiLFxcXCJzZWxsaW5nUHJpY2VWYWx1ZVxcXCI6XFxcIjM5OTk5XFxcIn1dfVwiLFwic3RhdHVzXCI6MjAwfX0ifV0sImN1c3RvbWVySGFzaCI6bnVsbCwiZWNpZCI6IlVua25vd24iLCJjbGllbnRJZCI6IkNST01BLVdFQi1BUFAiLCJzZXNzaW9uSWQiOiJiNjVmNTRiNC01YzkzLTQ4MmUtOGNmOC1jMjEwZWNmZmE3OGYuOTIzYmI1N2YtOTUxYi00NzgwLThjYzMtM2U3Yzg5ZTkwNWZlIiwiYnJhbmQiOiJDUk9NQSIsInRpbWVzdGFtcCI6MTcxNTgzOTIwNTc2MiwiZGV2aWNlIjoiTW96aWxsYS81LjAgKFgxMTsgTGludXggeDg2XzY0KSBBcHBsZVdlYktpdC81MzcuMzYgKEtIVE1MLCBsaWtlIEdlY2tvKSBDaHJvbWUvMTE0LjAuMC4wIFNhZmFyaS81MzcuMzYiLCJqYXJ2aXNJZCI6IjRkNjA0NTcwLWJhZmItNDU1MS1iYWQwLTg1Mzg5NzFjN2VlMiIsImFkdmVydGlzaW5nSWQiOm51bGwsImFwcFZlcnNpb24iOiIwIn0="}",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.googleadservices.com/pagead/conversion/1006316414/?random=1715839206129&cv=11&fst=1715839206129&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&value=0&bttype=purchase&npa=0&ec_m=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)*LI%3Atrue%3A23%3Afalse*1&ec_sel=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)&ec_meta=LI%3Atrue%3A23%3Afalse&ec_lat=19&ec_s=1&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&rfmt=3&fmt=4", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googleadservices.com/pagead/conversion/1006316414/?random=1715839206187&cv=11&fst=1715839206187&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&value=0&bttype=purchase&npa=0&ec_m=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)*LI%3Atrue%3A23%3Afalse*1&ec_sel=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)&ec_meta=LI%3Atrue%3A23%3Afalse&ec_lat=0&ec_s=1&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&rfmt=3&fmt=4", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://script.hotjar.com/modules.e5979922753cf3b8b069.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("v1_15", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_12");
    ns_web_url("collect_12",
        "URL=https://analytics.google.com/g/collect?v=2&tid=G-DJ3RGVFHJN&gtm=45je45f0v893318240z8889407033za200&_p=1715839203848&gcd=13l3l3l3l1&npa=0&dma=0&cid=1944455297.1715839141&ul=en-us&sr=1523x650&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&frm=0&pscdl=noapi&_s=1&sid=1715839140&sct=1&seg=1&dl=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&dr=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&dt=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&en=page_view&tfd=4302",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("collect_12", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_13");
    ns_web_url("collect_13",
        "URL=https://analytics.google.com/g/collect?v=2&tid=G-DJ3RGVFHJN&gtm=45je45f0v893318240z8889407033za200&_p=1715839203848&gcd=13l3l3l3l1&npa=0&dma=0&cid=1944455297.1715839141&ul=en-us&sr=1523x650&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&frm=0&pscdl=noapi&_eu=IA&_s=2&sid=1715839140&sct=1&seg=1&dl=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&dr=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&dt=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&uid=false&en=view_item&_c=1&pr1=id304697~nmOnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)~afcroma.com~cpN%2FA~k0currency~v0INR~ds0.00%25~lpN%2FA~brOnePlus~caPhones%20%26%20Wearables~c2Mobile%20Phones~c3Android%20Phones~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr39999~qt1~k1stock_status~v1in_stock~k2price_mrp~v239999&ep.pagetype=pdp&ep.source_page_url=%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ep.previous_page_url=%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407&ep.login_trigger=N%2FA&ep.login_status=false&ep.click_text=OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)&_et=432&tfd=4303",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.googleadservices.com/pagead/conversion/1006316414/?random=1715839206494&cv=11&fst=1715839206494&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=-5upCIv7u6MYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&value=0&bttype=purchase&npa=0&ec_m=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)*LI%3Atrue%3A23%3Afalse*1&ec_sel=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)&ec_meta=LI%3Atrue%3A23%3Afalse&ec_lat=3&ec_s=1&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&rfmt=3&fmt=4", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.croma.com/assets/fonts/Switzer-Light.woff2", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=Origin:https://www.croma.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;s_nr30;s_sq;s_tp;s_ppv;mbox;RT;_ga_DJ3RGVFHJN", END_INLINE
    );

    ns_end_transaction("collect_13", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_16");
    ns_web_url("v1_16",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=customer-hash:null",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=tdl-sso-version:4.1.15",
        "HEADER=session:false",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tL29uZXBsdXMtMTJyLTVnLThnYi0xMjhnYi1jb29sLWJsdWUtL3AvMzA0Njk3IiwiZXZlbnREYXRhIjpbeyJ0aW1lc3RhbXAiOjE3MTU4MzkyMDYzMzYsImRhdGEiOiJ7XCJldmVudFwiOlwicmVxdWVzdFwiLFwicGF5bG9hZFwiOntcInJlcXVlc3RIZWFkZXJzXCI6e1wiQWNjZXB0XCI6XCJhcHBsaWNhdGlvbi9qc29uLCB0ZXh0L3BsYWluLCAqLypcIixcIkNvbnRlbnQtVHlwZVwiOlwiYXBwbGljYXRpb24vanNvblwiLFwib21zLWFwaW0tc3Vic2NyaXB0aW9uLWtleVwiOlwiMTEzMTg1ODE0MTYzNGUyYWJlMmVmYjJiM2EyYTJhNWRcIn0sXCJyZXF1ZXN0VHlwZVwiOlwiUE9TVFwiLFwicmVxdWVzdFVybFwiOlwiaHR0cHM6Ly9hcGkuY3JvbWEuY29tL2ludmVudG9yeS9vbXMvdjIvdG1zL2RldGFpbHMtcHdhL1wiLFwicmVxdWVzdEJvZHlcIjpcIntcXFwicHJvbWlzZVxcXCI6e1xcXCJhbGxvY2F0aW9uUnVsZUlEXFxcIjpcXFwiU1lTVEVNXFxcIixcXFwiY2hlY2tJbnZlbnRvcnlcXFwiOlxcXCJZXFxcIixcXFwib3JnYW5pemF0aW9uQ29kZVxcXCI6XFxcIkNST01BXFxcIixcXFwic291cmNpbmdDbGFzc2lmaWNhdGlvblxcXCI6XFxcIkVDXFxcIixcXFwicHJvbWlzZUxpbmVzXFxcIjp7XFxcInByb21pc2VMaW5lXFxcIjpbe1xcXCJmdWxmaWxsbWVudFR5cGVcXFwiOlxcXCJIREVMXFxcIixcXFwibWNoXFxcIjpcXFwiXFxcIixcXFwiaXRlbUlEXFxcIjpcXFwiMzA0Njk3XFxcIixcXFwibGluZUlkXFxcIjpcXFwiMVxcXCIsXFxcImNhdGVnb3J5VHlwZVxcXCI6XFxcIm1vYmlsZVxcXCIsXFxcInJlRW5kRGF0ZVxcXCI6XFxcIjI1MDAtMDEtMDFcXFwiLFxcXCJyZXFTdGFydERhdGVcXFwiOlxcXCJcXFwiLFxcXCJyZXF1aXJlZFF0eVxcXCI6XFxcIjFcXFwiLFxcXCJzaGlwVG9BZGRyZXNzXFxcIjp7XFxcImNvbXBhbnlcXFwiOlxcXCJcXFwiLFxcXCJjb3VudHJ5XFxcIjpcXFwiXFxcIixcXFwiY2l0eVxcXCI6XFxcIlxcXCIsXFxcIm1vYmlsZVBob25lXFxcIjpcXFwiXFxcIixcXFwic3RhdGVcXFwiOlxcXCJcXFwiLFxcXCJ6aXBDb2RlXFxcIjpcXFwiNDAwMDQ5XFxcIixcXFwiZXh0blxcXCI6e1xcXCJpcmxBZGRyZXNzTGluZTFcXFwiOlxcXCJcXFwiLFxcXCJpcmxBZGRyZXNzTGluZTJcXFwiOlxcXCJcXFwifX0sXFxcImV4dG5cXFwiOntcXFwid2lkZXJTdG9yZUZsYWdcXFwiOlxcXCJOXFxcIn19LHtcXFwiZnVsZmlsbG1lbnRUeXBlXFxcIjpcXFwiU1RPUlxcXCIsXFxcIm1jaFxcXCI6XFxcIlxcXCIsXFxcIml0ZW1JRFxcXCI6XFxcIjMwNDY5N1xcXCIsXFxcImxpbmVJZFxcXCI6XFxcIjJcXFwiLFxcXCJjYXRlZ29yeVR5cGVcXFwiOlxcXCJtb2JpbGVcXFwiLFxcXCJyZXFFbmREYXRlXFxcIjpcXFwiXFxcIixcXFwicmVxU3RhcnREYXRlXFxcIjpcXFwiXFxcIixcXFwicmVxdWlyZWRRdHlcXFwiOlxcXCIxXFxcIixcXFwic2hpcFRvQWRkcmVzc1xcXCI6e1xcXCJjb21wYW55XFxcIjpcXFwiXFxcIixcXFwiY291bnRyeVxcXCI6XFxcIlxcXCIsXFxcImNpdHlcXFwiOlxcXCJcXFwiLFxcXCJtb2JpbGVQaG9uZVxcXCI6XFxcIlxcXCIsXFxcInN0YXRlXFxcIjpcXFwiXFxcIixcXFwiemlwQ29kZVxcXCI6XFxcIjQwMDA0OVxcXCIsXFxcImV4dG5cXFwiOntcXFwiaXJsQWRkcmVzc0xpbmUxXFxcIjpcXFwiXFxcIixcXFwiaXJsQWRkcmVzc0xpbmUyXFxcIjpcXFwiXFxcIn19LFxcXCJleHRuXFxcIjp7XFxcIndpZGVyU3RvcmVGbGFnXFxcIjpcXFwiTlxcXCJ9fSx7XFxcImZ1bGZpbGxtZW50VHlwZVxcXCI6XFxcIlNERUxcXFwiLFxcXCJtY2hcXFwiOlxcXCJcXFwiLFxcXCJpdGVtSURcXFwiOlxcXCIzMDQ2OTdcXFwiLFxcXCJsaW5lSWRcXFwiOlxcXCIzXFxcIixcXFwiY2F0ZWdvcnlUeXBlXFxcIjpcXFwibW9iaWxlXFxcIixcXFwicmVxRW5kRGF0ZVxcXCI6XFxcIlxcXCIsXFxcInJlcVN0YXJ0RGF0ZVxcXCI6XFxcIlxcXCIsXFxcInJlcXVpcmVkUXR5XFxcIjpcXFwiMVxcXCIsXFxcInNoaXBUb0FkZHJlc3NcXFwiOntcXFwiY29tcGFueVxcXCI6XFxcIlxcXCIsXFxcImNvdW50cnlcXFwiOlxcXCJcXFwiLFxcXCJjaXR5XFxcIjpcXFwiXFxcIixcXFwibW9iaWxlUGhvbmVcXFwiOlxcXCJcXFwiLFxcXCJzdGF0ZVxcXCI6XFxcIlxcXCIsXFxcInppcENvZGVcXFwiOlxcXCI0MDAwNDlcXFwiLFxcXCJleHRuXFxcIjp7XFxcImlybEFkZHJlc3NMaW5lMVxcXCI6XFxcIlxcXCIsXFxcImlybEFkZHJlc3NMaW5lMlxcXCI6XFxcIlxcXCJ9fSxcXFwiZXh0blxcXCI6e1xcXCJ3aWRlclN0b3JlRmxhZ1xcXCI6XFxcIk5cXFwifX1dfX19XCIsXCJyZXNwb25zZVwiOlwie1xcclxcbiAgXFxcInByb21pc2VcXFwiOiB7XFxyXFxuICAgIFxcXCJzdWdnZXN0ZWRPcHRpb25cXFwiOiB7XFxyXFxuICAgICAgXFxcIm9wdGlvblxcXCI6IHtcXHJcXG4gICAgICAgIFxcXCJwcm9taXNlTGluZXNcXFwiOiB7XFxyXFxuICAgICAgICAgIFxcXCJwcm9taXNlTGluZVxcXCI6IFtcXHJcXG4gICAgICAgICAgICB7XFxyXFxuICAgICAgICAgICAgICBcXFwic291cmNpbmdDbGFzc2lmaWNhdGlvblxcXCI6IFxcXCJFQ1xcXCIsXFxyXFxuICAgICAgICAgICAgICBcXFwibGluZUlkXFxcIjogXFxcIjFcXFwiLFxcclxcbiAgICAgICAgICAgICAgXFxcIml0ZW1JRFxcXCI6IFxcXCIzMDQ2OTdcXFwiLFxcclxcbiAgICAgICAgICAgICAgXFxcInJlcXVpcmVkUXR5XFxcIjogXFxcIjFcXFwiLFxcclxcbiAgICAgICAgICAgICAgXFxcInJlcXVpcmVkQ2FwYWNpdHlcXFwiOiBcXFwiMVxcXCIsXFxyXFxuICAgICAgICAgICAgICBcXFwiZXh0ZXJuYWxSZWZlcmVuY2VJZFxcXCI6IFxcXCI2ODY0M1xcXCIsXFxyXFxuICAgICAgICAgICAgICBcXFwib3JkZXJUeXBlXFxcIjogXFxcIm5vcm1hbFxcXCIsXFxyXFxuICAgICAgICAgICAgICBcXFwidW5pdE9mTWVhc3VyZVxcXCI6IFxcXCJFQVxcXCIsXFxyXFxuICAgICAgICAgICAgICBcXFwiY2FycmllclNlcnZpY2VDb2RlXFxcIjogXFxcIkJsdWVEYXJ0IC0gNyBEYXkgRnJpZWdodFxcXCIsXFxyXFxuICAgICAgICAgICAgICBcXFwic2NhY1xcXCI6IFxcXCJCbHVlRGFydFxcXCIsXFxyXFxuICAgICAgICAgICAgICBcXFwiemlwRW5hYmxlZEluVE1TXFxcIjogdHJ1ZSxcXHJcXG4gICAgICAgICAgICAgIFxcXCJpc1Rtc0VuYWJsZVxcXCI6IHRydWUsXFxyXFxuICAgICAgICAgICAgICBcXFwiZnVsZmlsbG1lbnRUeXBlXFxcIjogXFxcIkhERUxcXFwiLFxcclxcbiAgICAgICAgICAgICAgXFxcImFzc2lnbm1lbnRzXFxcIjoge1xcclxcbiAgICAgICAgICAgICAgICBcXFwidG90YWxOdW1iZXJPZlJlY29yZHNcXFwiOiBcXFwiMVxcXCIsXFxyXFxuICAgICAgICAgICAgICAgIFxcXCJhc3NpZ25tZW50XFxcIjogW1xcclxcbiAgICAgICAgICAgICAgICAgIHtcXHJcXG4gICAgICAgICAgICAgICAgICAgIFxcXCJxdWFudGl0eVxcXCI6IFxcXCIxXFxcIixcXHJcXG4gICAgICAgICAgICAgICAgICAgIFxcXCJzaGlwTm9kZVxcXCI6IFxcXCJEMDYxXFxcIixcXHJcXG4gICAgICAgICAgICAgICAgICAgIFxcXCJsbWRGY0lkXFxcIjogXFxcIlgwMDMtR09SRUdBT05cXFwiLFxcclxcbiAgICAgICAgICAgICAgICAgICAgXFxcIm9mZnNldFxcXCI6IFxcXCI3MjBcXFwiLFxcclxcbiAgICAgICAgICAgICAgICAgICAgXFxcImRlbGl2ZXJ5RGF0ZVxcXCI6IFxcXCIyMDI0LTA1LTE4VDA4OjAwOjA1LjM4MiswMDowMFxcXCIsXFxyXFxuICAgICAgICAgICAgICAgICAgICBcXFwiemlwQ29kZVxcXCI6IFxcXCI0MDAwNDlcXFwiLFxcclxcbiAgICAgICAgICAgICAgICAgICAgXFxcInByb2R1Y3RBdmFpbERhdGVcXFwiOiBcXFwiMjAyNC0wNS0xNlQxMTozMDowNS4zODJcXFwiLFxcclxcbiAgICAgICAgICAgICAgICAgICAgXFxcInBvZEluZGljYXRvclxcXCI6IFxcXCJOXFxcIixcXHJcXG4gICAgICAgICAgICAgICAgICAgIFxcXCJpc1RyYWRlaW5JbmRpY2F0b3JcXFwiOiBcXFwiWVxcXCIsXFxyXFxuICAgICAgICAgICAgICAgICAgICBcXFwiZGVsaXZlcnlEYXRlc1xcXCI6IFtdXFxyXFxuICAgICAgICAgICAgICAgICAgfVxcclxcbiAgICAgICAgICAgICAgICBdXFxyXFxuICAgICAgICAgICAgICB9LFxcclxcbiAgICAgICAgICAgICAgXFxcImF2YWlsYWJsZURlbGl2ZXJ5XFxcIjogW10sXFxyXFxuICAgICAgICAgICAgICBcXFwiZXh0blxcXCI6IHtcXHJcXG4gICAgICAgICAgICAgICAgXFxcInByZU9yZGVySXRlbVxcXCI6IFxcXCJcXFwiLFxcclxcbiAgICAgICAgICAgICAgICBcXFwid2lkZXJTdG9yZUZsYWdcXFwiOiBcXFwiTlxcXCJcXHJcXG4gICAgICAgICAgICAgIH1cXHJcXG4gICAgICAgICAgICB9LFxcclxcbiAgICAgICAgICAgIHtcXHJcXG4gICAgICAgICAgICAgIFxcXCJzb3VyY2luZ0NsYXNzaWZpY2F0aW9uXFxcIjogXFxcIkVDXFxcIixcXHJcXG4gICAgICAgICAgICAgIFxcXCJsaW5lSWRcXFwiOiBcXFwiM1xcXCIsXFxyXFxuICAgICAgICAgICAgICBcXFwiaXRlbUlEXFxcIjogXFxcIjMwNDY5N1xcXCIsXFxyXFxuICAgICAgICAgICAgICBcXFwicmVxdWlyZWRRdHlcXFwiOiBcXFwiMVxcXCIsXFxyXFxuICAgICAgICAgICAgICBcXFwicmVxdWlyZWRDYXBhY2l0eVxcXCI6IFxcXCIxXFxcIixcXHJcXG4gICAgICAgICAgICAgIFxcXCJleHRlcm5hbFJlZmVyZW5jZUlkXFxcIjogXFxcIjY4NjQ0XFxcIixcXHJcXG4gICAgICAgICAgICAgIFxcXCJvcmRlclR5cGVcXFwiOiBcXFwibm9ybWFsXFxcIixcXHJcXG4gICAgICAgICAgICAgIFxcXCJ1bml0T2ZNZWFzdXJlXFxcIjogXFxcIkVBXFxcIixcXHJcXG4gICAgICAgICAgICAgIFxcXCJjYXJyaWVyU2VydmljZUNvZGVcXFwiOiBcXFwiQ3JvbWFUcmFuc3BvcnRTZXJ2aWNlIC0gRXhwcmVzcyBEZWxpdmVyeVxcXCIsXFxyXFxuICAgICAgICAgICAgICBcXFwic2NhY1xcXCI6IFxcXCJDcm9tYVRyYW5zcG9ydFNlcnZpY2VcXFwiLFxcclxcbiAgICAgICAgICAgICAgXFxcInppcEVuYWJsZWRJblRNU1xcXCI6IGZhbHNlLFxcclxcbiAgICAgICAgICAgICAgXFxcImlzVG1zRW5hYmxlXFxcIjogdHJ1ZSxcXHJcXG4gICAgICAgICAgICAgIFxcXCJmdWxmaWxsbWVudFR5cGVcXFwiOiBcXFwiU0RFTFxcXCIsXFxyXFxuICAgICAgICAgICAgICBcXFwiYXNzaWdubWVudHNcXFwiOiB7XFxyXFxuICAgICAgICAgICAgICAgIFxcXCJ0b3RhbE51bWJlck9mUmVjb3Jkc1xcXCI6IFxcXCIxXFxcIixcXHJcXG4gICAgICAgICAgICAgICAgXFxcImFzc2lnbm1lbnRcXFwiOiBbXFxyXFxuICAgICAgICAgICAgICAgICAge1xcclxcbiAgICAgICAgICAgICAgICAgICAgXFxcInF1YW50aXR5XFxcIjogXFxcIjFcXFwiLFxcclxcbiAgICAgICAgICAgICAgICAgICAgXFxcInNoaXBOb2RlXFxcIjogXFxcIkEwMjFcXFwiLFxcclxcbiAgICAgICAgICAgICAgICAgICAgXFxcImxtZEZjSWRcXFwiOiBcXFwiXFxcIixcXHJcXG4gICAgICAgICAgICAgICAgICAgIFxcXCJvZmZzZXRcXFwiOiBcXFwiXFxcIixcXHJcXG4gICAgICAgICAgICAgICAgICAgIFxcXCJkZWxpdmVyeURhdGVcXFwiOiBcXFwiMjAyNC0wNS0xNlQyMTowMCswNTozMFxcXCIsXFxyXFxuICAgICAgICAgICAgICAgICAgICBcXFwiemlwQ29kZVxcXCI6IFxcXCI0MDAwNDlcXFwiLFxcclxcbiAgICAgICAgICAgICAgICAgICAgXFxcInByb2R1Y3RBdmFpbERhdGVcXFwiOiBcXFwiMjAyNC0wNS0xNlQxMTozMDowNS4zODNcXFwiLFxcclxcbiAgICAgICAgICAgICAgICAgICAgXFxcInBvZEluZGljYXRvclxcXCI6IFxcXCJOXFxcIixcXHJcXG4gICAgICAgICAgICAgICAgICAgIFxcXCJpc1RyYWRlaW5JbmRpY2F0b3JcXFwiOiBcXFwiTlxcXCJcXHJcXG4gICAgICAgICAgICAgICAgICB9XFxyXFxuICAgICAgICAgICAgICAgIF1cXHJcXG4gICAgICAgICAgICAgIH0sXFxyXFxuICAgICAgICAgICAgICBcXFwiYXZhaWxhYmxlRGVsaXZlcnlcXFwiOiBbXSxcXHJcXG4gICAgICAgICAgICAgIFxcXCJleHRuXFxcIjoge1xcclxcbiAgICAgICAgICAgICAgICBcXFwicHJlT3JkZXJJdGVtXFxcIjogXFxcIlxcXCIsXFxyXFxuICAgICAgICAgICAgICAgIFxcXCJ3aWRlclN0b3JlRmxhZ1xcXCI6IFxcXCJOXFxcIlxcclxcbiAgICAgICAgICAgICAgfVxcclxcbiAgICAgICAgICAgIH1cXHJcXG4gICAgICAgICAgXVxcclxcbiAgICAgICAgfVxcclxcbiAgICAgIH0sXFxyXFxuICAgICAgXFxcInVuYXZhaWxhYmxlTGluZXNcXFwiOiB7XFxyXFxuICAgICAgICBcXFwidW5hdmFpbGFibGVMaW5lXFxcIjogW1xcclxcbiAgICAgICAgICB7XFxyXFxuICAgICAgICAgICAgXFxcIml0ZW1JRFxcXCI6IFxcXCIzMDQ2OTdcXFwiLFxcclxcbiAgICAgICAgICAgIFxcXCJsaW5lSWRcXFwiOiBcXFwiMlxcXCIsXFxyXFxuICAgICAgICAgICAgXFxcInVuYXZhaWxhYmxlUmVhc29uXFxcIjogXFxcIlNPVVJDSU5HX1JVTEVfTk9UX0RFRklORURcXFwiXFxyXFxuICAgICAgICAgIH1cXHJcXG4gICAgICAgIF1cXHJcXG4gICAgICB9XFxyXFxuICAgIH0sXFxyXFxuICAgIFxcXCJvcHRpb25zXFxcIjoge31cXHJcXG4gIH1cXHJcXG59XCIsXCJzdGF0dXNcIjoyMDB9fSJ9XSwiY3VzdG9tZXJIYXNoIjpudWxsLCJlY2lkIjoiVW5rbm93biIsImNsaWVudElkIjoiQ1JPTUEtV0VCLUFQUCIsInNlc3Npb25JZCI6ImI2NWY1NGI0LTVjOTMtNDgyZS04Y2Y4LWMyMTBlY2ZmYTc4Zi45MjNiYjU3Zi05NTFiLTQ3ODAtOGNjMy0zZTdjODllOTA1ZmUiLCJicmFuZCI6IkNST01BIiwidGltZXN0YW1wIjoxNzE1ODM5MjA2MzM3LCJkZXZpY2UiOiJNb3ppbGxhLzUuMCAoWDExOyBMaW51eCB4ODZfNjQpIEFwcGxlV2ViS2l0LzUzNy4zNiAoS0hUTUwsIGxpa2UgR2Vja28pIENocm9tZS8xMTQuMC4wLjAgU2FmYXJpLzUzNy4zNiIsImphcnZpc0lkIjoiNGQ2MDQ1NzAtYmFmYi00NTUxLWJhZDAtODUzODk3MWM3ZWUyIiwiYWR2ZXJ0aXNpbmdJZCI6bnVsbCwiYXBwVmVyc2lvbiI6IjAifQ=="}",
        BODY_END,
        INLINE_URLS,
            "URL=https://917807264.fls.doubleclick.net/activityi;src=917807264;type=invmedia;cat=croma0;ord=9304351560192;npa=0;auiddc=1421799651.1715839140;uaa=x86;uab=64;uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199;uamb=0;uam=;uap=Linux;uapv=5.15.0;uaw=0;pscdl=noapi;frm=0;gtm=45fe45f0z8889407033za201;gcd=13l3l3l3l1;dma=0;epver=2;~oref=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697?", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://13135721.fls.doubleclick.net/activityi;src=13135721;type=croma01;cat=croma0;ord=2609973983841;npa=0;auiddc=1421799651.1715839140;uaa=x86;uab=64;uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199;uamb=0;uam=;uap=Linux;uapv=5.15.0;uaw=0;pscdl=noapi;frm=0;gtm=45fe45f0z8889407033za201;gcd=13l3l3l3l1;dma=0;epver=2;~oref=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697?", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://13135721.fls.doubleclick.net/activityi;src=13135721;type=croma02;cat=croma0;ord=1682216487709;u=%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697;npa=0;auiddc=1421799651.1715839140;uaa=x86;uab=64;uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199;uamb=0;uam=;uap=Linux;uapv=5.15.0;uaw=0;pscdl=noapi;frm=0;gtm=45fe45f0z8889407033za201;gcd=13l3l3l3l1;dma=0;epver=2;~oref=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697?", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://917807264.fls.doubleclick.net/activityi;dc_pre=CO3frpO_kYYDFZTyTAIdp4YMmw;src=917807264;type=invmedia;cat=croma0;ord=9304351560192;npa=0;auiddc=1421799651.1715839140;uaa=x86;uab=64;uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199;uamb=0;uam=;uap=Linux;uapv=5.15.0;uaw=0;pscdl=noapi;frm=0;gtm=45fe45f0z8889407033za201;gcd=13l3l3l3l1;dma=0;epver=2;~oref=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697?", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://sslwidget.criteo.com/event?a=56256&v=5.23.0&otl=1&p0=e%3Dexd%26site_type%3Dd&p1=e%3Dvp%26tms%3Dgtm-template%26p%3D304697&p2=e%3Ddis&adce=1&bundle=EPlPXl9ZQjN2TTdzaTNPd0Q0dlByN3VaMFZKWVpFMmRIUXg4WFZHd0t1eUFlY3ZJZHhZOGdWZU1pa0FqeGs0VVBSczlOOXJabXAxU3g5bkRtVEh3Vm9aTE1CMWJnUDJkJTJCNFN1WWxUWmoxN0wybzBtaXVnSyUyQnVpNHh5VTBXQ1BUZHFrQmY&tld=croma.com&dy=1&fu=https%253A%252F%252Fwww.croma.com%252Foneplus-12r-5g-8gb-128gb-cool-blue-%252Fp%252F304697&pu=https%253A%252F%252Fwww.croma.com%252Fcampaign%252Foneplus-r-5g%252Fc%252F6407%253Fq%253D%25253Aprice-asc&ceid=1ac25852-2b27-48d0-995e-6ded1b58d7e8&dtycbr=21272", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("v1_16", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("adrum_5");
    ns_web_url("adrum_5",
        "URL=https://bom-col.eum-appdynamics.com/eumcollector/beacons/browser/v1/BO-AAB-FBK/adrum",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:text/plain",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"vr":"23.10.1.4359","dt":"R","rg":"0","es":[{"eg":"1","et":2,"eu":"0://1/2/3/4/5/6/7","ts":1715839201822,"mg":"0","au":"0://8/9/10/11/12?13","at":0,"pp":3,"mx":{"PLC":1,"FBT":136,"DDT":1,"DPT":0,"PLT":137,"ARE":0},"md":"POST","xs":200,"si":33},{"eg":"2","et":2,"eu":"0://14/15/16/17/18","ts":1715839201750,"mg":"0","au":"0://8/9/10/11/12?13","at":0,"pp":3,"mx":{"PLC":1,"FBT":210,"DDT":0,"DPT":0,"PLT":210,"ARE":0},"md":"POST","xs":200,"si":34}],"ai":"477d42aa_9f21_9144_1a76_688c9aa25e4b","gs":["94fc8c22_8b4e_9951_5dd4_a346d07ee981","245fb7e1_b2c9_a32e_a7da_f4fcaa3f541c","36973e15_d658_30f1_d393_fbe8b6d460b8"],"up":["https","smetrics.croma.com","b","ss","infinitipwa","1","JS-2.25.0-LDQM","s42804754789358","www.croma.com","campaign","oneplus-r-5g","c","6407","q=%3Aprice-asc","api.tatadigital.com","api","v1.1","msd","events"]}",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.googleadservices.com/pagead/conversion/609902077/?random=1715839207284&cv=11&fst=1715839207284&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&value=0&bttype=purchase&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&rfmt=3&fmt=4", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googleadservices.com/pagead/conversion/609902077/?random=1715839207382&cv=11&fst=1715839207382&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&value=0&bttype=purchase&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&rfmt=3&fmt=4", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/video/upload/v1713436641/Croma%20Assets/Videos/2024%20Videos/304697_w9hxoc.mp4?tr=q-70", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Range:bytes=3932160-", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:video", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_ips;s_nr30;s_sq;s_tp;s_ppv;mbox;_ga_DJ3RGVFHJN", END_INLINE
    );

    ns_end_transaction("adrum_5", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("s47313657844741");
    ns_web_url("s47313657844741",
        "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s47313657844741",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_sq;mbox;_ga_DJ3RGVFHJN;s_nr30;s_ips;s_tp;s_ppv",
        BODY_BEGIN,
            "AQB=1&ndh=1&pf=1&t=16%2F4%2F2024+1%3A0%3A7+4+300&sdid=7892C226D399CF1A-7543E32B5F623B41&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=pdp&g=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&r=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&c.=&getNewRepeat=3.0.1&getPageLoadTime=2.0.2&performanceWriteFull=n%2Fa&performanceWritePart=n%2Fa&performanceCheck=n%2Fa&p_fo=3.0&inList=3.0&apl=4.0&getPreviousValue=3.0.1&.c=&cc=INR&ch=croma%3Apdp&server=www.croma.com&events=prodView%2Cevent20%2Cevent21%2Cevent99%2Cevent1%3D5.5&products=%3B304697%3B%3B%3Bevent20%3D39999%7Cevent21%3D39999%3BeVar10%3D304697%7CeVar14%3D39999%7CeVar13%3D39999%7CeVar23%3Dapi_delay%7CeVar24%3Dyes%7CeVar25%3Dapi_delay%7CeVar26%3Dyes%7CeVar27%3Dno%3Anot+serviceable%3Ana%3Ana%3Ana%7CeVar28%3Dyes%3A0%3Ad061%3Abluedart%3Abluedart+-+7+day+frieght%7CeVar29%3Dno%3Anot+serviceable%3Ana%3Ana%3Ana%7CeVar111%3Dundefined%7CeVar114%3Dyes%7CeVar117%3DNo%7CeVar41%3DOnePlus+12R+5G+%288GB+128GB+Cool+Blue%29%7CeVar136%3Dna%7CeVar43%3DPhones+%26+Wearables%3AMobile+Phones%3AAndroid+Phones%7CeVar89%3Dno%7CeVar124%3D0%7CeVar122%3Dapi_delay%7CeVar123%3Doneplus%7CeVar18%3D&aamb=6G1ynYcLPuiQxYZrsz_pkqfLG9yMXBpb2zX5dvJdYQJzPXImdj0y&c1=pdp&v1=D%3Dmid&c2=www.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&v2=new&c3=2024-5-16+1%3A0%3A7&c4=campaign&v4=guest+user&v5=400049&v9=Phones+%26+Wearables%3AMobile+Phones%3AAndroid+Phones&c14=p%2F304697&v38=plp+%7C+highestPercentViewed%3D106+%7C+initialPercentViewed%3D33&v39=RL425eb72221864237b3ecc22b172469cc&v51=5.5&v76=%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&v121=no&v131=Direct+Call+%2850%29+%3A+%5Bpageload%5D+%3A+Send+Beacon&v198=www.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=1523&bh=523&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&AQE=1",
        BODY_END,
        INLINE_URLS,
            "URL=https://13135721.fls.doubleclick.net/activityi;dc_pre=CJSB0JO_kYYDFXrvTAIdPL8Hgg;src=13135721;type=croma01;cat=croma0;ord=2609973983841;npa=0;auiddc=1421799651.1715839140;uaa=x86;uab=64;uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199;uamb=0;uam=;uap=Linux;uapv=5.15.0;uaw=0;pscdl=noapi;frm=0;gtm=45fe45f0z8889407033za201;gcd=13l3l3l3l1;dma=0;epver=2;~oref=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697?", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://13135721.fls.doubleclick.net/activityi;dc_pre=CMDD0pO_kYYDFcPvTAIdWeACBw;src=13135721;type=croma02;cat=croma0;ord=1682216487709;u=%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697;npa=0;auiddc=1421799651.1715839140;uaa=x86;uab=64;uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199;uamb=0;uam=;uap=Linux;uapv=5.15.0;uaw=0;pscdl=noapi;frm=0;gtm=45fe45f0z8889407033za201;gcd=13l3l3l3l1;dma=0;epver=2;~oref=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697?", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("s47313657844741", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("tr");
    ns_web_url("tr",
        "URL=https://capi.croma.com/tr?id=1369867960112522&ev=ViewContent&et=1715839207&es=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&referrer=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&eid=53824125171208&ua=Mozilla%2F5.0%20(X11%3B%20Linux%20x86_64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F114.0.0.0%20Safari%2F537.36&fbp=fb.1.1715839140634.281453602&external_id=dd225ea0e0a549dff2458f2b188bd0b54a923171c4b30d006a0ce3ff08d8fa8c&ud%5Bexternal_id%5D=36ce52f2c012a86c9264c805b8cff82e07250268c3b1663e92a631306623bfc6&cd%5Bvalue%5D=39999.00&cd%5Bcurrency%5D=INR&cd%5Bcontent_ids%5D=%5B%22304697%22%5D&cd%5Bcontent_type%5D=product&cd%5Bcontent_name%5D=OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;_uetsid;_uetvid;WZRK_G;WZRK_S_679-6RK-976Z;cto_bundle;s_sq;mbox;_ga_DJ3RGVFHJN;s_nr30;s_ips;s_tp;s_ppv;th_external_id",
        INLINE_URLS,
            "URL=https://in1.clevertap-prod.com/a?t=96&type=push&d=N4IgLgngDgpiBcIoCcD2AzAlgGzgGiTS1wVAGMwB9VKMVAVzAXQENsBnGAXwMwBMEIAGwB2AJwBaIQCUA0hLEihALRAEA5oKHoADAA4WevugAsZGDpPpTAIxF6hegKxOAzGNc2hfVwCYnaiDsCACMIiFOeu4hJiEEUJrwcSAs6KQg2Jg2ggDuMDYS7HwA1hIAbiEAdCKVroEoqHRkqNiCABZgYFDBXFxAAA%3D&optOut=false&rn=1&i=1715839207&sn=0&gc=6f08a8df4ce04ff4b786855393b6d325&arp=N4IgVg%2BgdiBcIC0DuBeFIA0ICW06IFsBOKAMwEEBVAYXPIDVtMQATCAFwGc4AGLFgBa8sANzgAmLJG7xgAX2bYW%2BAGwB2IgFoVAJQDSmompUJmAJw4yAjGqsBWABwBmIlYAsPOUA&tries=1&useIP=true&r=1715839207635", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://in1.clevertap-prod.com/a?t=96&type=push&d=N4IgLgngDgpiBcIYDcYDswgDROWAcgIYC2ciAxgM4D6ACgE4D2AJgK7lgAEAagJYwB3GM2y4wAEUJhCCULQA2UgGaN6xBCHJNihAHTlG6nAxbsuASRGIAzAAYALADYAnAHZRJth05FSGgPJoMAqslJwAjABMAEqcAKwA4pwAFAAcCQBCWBGR6VmcAMKMjPKcGfKsMACUogCy0bQa1s4tznX%2BjTatbTjmYDDEnOK8lAasGBq2ura2AKSiAMpgjOQA1pxLUqEavGjUlMtrIAC%2BOLxWII6uzgC0jtEA0jdujgBaogDmGo5KtqmEqWYSns5BgDiUwIARq5Uo5UnE4s1rJDHMxrJE4qJKAhwq5wnFUs1wvZwjgoF94KSQIQlLIQPJeJCNEJITdKMxVjdkOFdK5dNZRFAmIcShoABZgMBQbGnEAAdVej2oBQAgrVOqABMQoAhbDgBLxtbqcGB5OQEABtAC6%2Bt4pvN8Gtx2OQAA&rn=2&i=1715839207&sn=0&gc=6f08a8df4ce04ff4b786855393b6d325&arp=N4IgVg%2BgdiBcIC0DuBeFIA0ICW06IFsBOKAMwEEBVAYXPIDVtMQATCAFwGc4AGLFgBa8sANzgAmLJG7xgAX2bYW%2BAGwB2IgFoVAJQDSmompUJmAJw4yAjGqsBWABwBmIlYAsPOUA&tries=1&useIP=true&r=1715839207654", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://in1.clevertap-prod.com/a?t=96&type=page&d=N4IglgJiBcIGwHYCcBaOAlA0ipC4C0QAaEAcxngDMAGADgENaJKAWAYwFNqXLWAjBLTi0ArCIDMScXzgRxAJhHEQAZxgBGBOpG1J6lupIAHctHkl6lGKAA2YPhQDuHPihUQA1igBu6gHQIfuLKRgBOAPYALuFs4TYUABaRkUZqAL4kbCaJyanQAPT5jsV%2BbBEAtvSl4eX54QB2HEY2AK4qKOryoSgipCi0pK6dA66xcSh8rRwo%2BUb54txwuMoA6vhYAPoAwgCCALIACtYgjuVGMNQkjmBnFySRNmwwANoAuldgD0%2FQb2lpQA&rn=3&i=1715839207&sn=1&gc=6f08a8df4ce04ff4b786855393b6d325&arp=N4IgVg%2BgdiBcIC0DuBeFIA0ICW06IFsBOKAMwEEBVAYXPIDVtMQATCAFwGc4AGLFgBa8sANzgAmLJG7xgAX2bYW%2BAGwB2IgFoVAJQDSmompUJmAJw4yAjGqsBWABwBmIlYAsPOUA&tries=1&useIP=true&r=1715839207667", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://bat.bing.com/p/action/25149556.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://bat.bing.com/action/0?ti=25149556&tm=gtm002&Ver=2&mid=fb6c219e-30e1-49ce-9d0d-675f3d36ae36&sid=6467a920134911efa7d615e2de33fb03&vid=6467fc40134911efa1829f1407dcc88b&vids=0&msclkid=N&pi=918639831&lg=en-US&sw=1523&sh=650&sc=24&tl=Buy%20OnePlus%2012R%205G%20(8GB,%20128GB,%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details,%20reviews%20%26%20more.%20Shop%20now!&p=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&r=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&lt=3624&evt=pageLoad&sv=1&rn=144061", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://gumi.criteo.com/syncframe?topUrl=www.croma.com&origin=onetag#{%22bundle%22:{%22origin%22:3,%22value%22:%22EPlPXl9ZQjN2TTdzaTNPd0Q0dlByN3VaMFZKWVpFMmRIUXg4WFZHd0t1eUFlY3ZJZHhZOGdWZU1pa0FqeGs0VVBSczlOOXJabXAxU3g5bkRtVEh3Vm9aTE1CMWJnUDJkJTJCNFN1WWxUWmoxN0wybzBtaXVnSyUyQnVpNHh5VTBXQ1BUZHFrQmY%22},%22cw%22:true,%22optout%22:{%22origin%22:0,%22value%22:null},%22origin%22:%22onetag%22,%22sid%22:{%22origin%22:0,%22value%22:null},%22tld%22:%22croma.com%22,%22topUrl%22:%22www.croma.com%22,%22version%22:%225_23_0%22,%22ifa%22:{%22origin%22:0,%22value%22:null},%22lsw%22:true,%22pm%22:0}", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("tr", NS_AUTO_STATUS);
    ns_page_think_time(0.053);

    ns_start_transaction("activityi_dc_pre_CJSB0JO_kYY");
    ns_web_url("activityi_dc_pre_CJSB0JO_kYY",
        "URL=https://adservice.google.com/ddm/fls/z/dc_pre=CO3frpO_kYYDFZTyTAIdp4YMmw;src=917807264;type=invmedia;cat=croma0;ord=9304351560192;npa=0;auiddc=*;uaa=x86;uab=64;uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199;uamb=0;uam=;uap=Linux;uapv=5.15.0;uaw=0;pscdl=noapi;frm=0;gtm=45fe45f0z8889407033za201;gcd=13l3l3l3l1;dma=0;epver=2;~oref=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:image",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://13135721.fls.doubleclick.net/activityi;dc_pre=CJSB0JO_kYYDFXrvTAIdPL8Hgg;src=13135721;type=croma01;cat=croma0;ord=2609973983841;npa=0;auiddc=1421799651.1715839140;uaa=x86;uab=64;uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199;uamb=0;uam=;uap=Linux;uapv=5.15.0;uaw=0;pscdl=noapi;frm=0;gtm=45fe45f0z8889407033za201;gcd=13l3l3l3l1;dma=0;epver=2;~oref=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697?", END_INLINE
    );

    ns_end_transaction("activityi_dc_pre_CJSB0JO_kYY", NS_AUTO_STATUS);
    ns_page_think_time(0.424);

    ns_start_transaction("syncframe");
    ns_web_url("syncframe",
        "URL=https://adservice.google.com/ddm/fls/z/dc_pre=CJSB0JO_kYYDFXrvTAIdPL8Hgg;src=13135721;type=croma01;cat=croma0;ord=2609973983841;npa=0;auiddc=*;uaa=x86;uab=64;uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199;uamb=0;uam=;uap=Linux;uapv=5.15.0;uaw=0;pscdl=noapi;frm=0;gtm=45fe45f0z8889407033za201;gcd=13l3l3l3l1;dma=0;epver=2;~oref=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:image",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("syncframe", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("page_6");
    ns_web_url("page_6",
        "URL=https://api.croma.com/cmstemplate/allchannels/v1/page?pageType=ContentPage&pageLabelOrId=pwaHomePageFooter&fields=FULL",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSessionUser_3400595;_hjSession_3400595;_fbp;_ga;WZRK_G;cto_bundle;s_sq;mbox;ADRUM_BTa;SameSite;ADRUM_BT1;_ga_DJ3RGVFHJN;s_nr30;s_ips;s_tp;s_ppv;th_external_id;WZRK_S_679-6RK-976Z;_uetsid;_uetvid",
        INLINE_URLS,
            "URL=https://gumi.criteo.com/syncframe?topUrl=www.croma.com&origin=onetag#{%22bundle%22:{%22origin%22:3,%22value%22:%22EPlPXl9ZQjN2TTdzaTNPd0Q0dlByN3VaMFZKWVpFMmRIUXg4WFZHd0t1eUFlY3ZJZHhZOGdWZU1pa0FqeGs0VVBSczlOOXJabXAxU3g5bkRtVEh3Vm9aTE1CMWJnUDJkJTJCNFN1WWxUWmoxN0wybzBtaXVnSyUyQnVpNHh5VTBXQ1BUZHFrQmY%22},%22cw%22:true,%22optout%22:{%22origin%22:0,%22value%22:null},%22origin%22:%22onetag%22,%22sid%22:{%22origin%22:0,%22value%22:null},%22tld%22:%22croma.com%22,%22topUrl%22:%22www.croma.com%22,%22version%22:%225_23_0%22,%22ifa%22:{%22origin%22:0,%22value%22:null},%22lsw%22:true,%22pm%22:0}", END_INLINE
    );

    ns_end_transaction("page_6", NS_AUTO_STATUS);
    ns_page_think_time(0.069);

    ns_start_transaction("activityi_dc_pre_CMDD0pO_kYY");
    ns_web_url("activityi_dc_pre_CMDD0pO_kYY",
        "URL=https://www.facebook.com/tr/?id=1369867960112522&ev=PageView&dl=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&rl=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&if=false&ts=1715839208051&sw=1523&sh=650&ud[external_id]=36ce52f2c012a86c9264c805b8cff82e07250268c3b1663e92a631306623bfc6&v=2.9.156&r=stable&ec=0&o=4126&fbp=fb.1.1715839140634.281453602&ler=empty&cdl=API_unavailable&it=1715839205604&coo=false&eid=41509406383058&tm=1&rqm=GET",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:image",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.facebook.com/tr/?id=1369867960112522&ev=PageView&dl=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&rl=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&if=false&ts=1715839208068&sw=1523&sh=650&ud[external_id]=36ce52f2c012a86c9264c805b8cff82e07250268c3b1663e92a631306623bfc6&v=2.9.156&r=stable&ec=1&o=4126&fbp=fb.1.1715839140634.281453602&ler=empty&cdl=API_unavailable&it=1715839205604&coo=false&eid=82997599224919&tm=1&rqm=GET", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.facebook.com/tr/?id=1369867960112522&ev=ViewContent&dl=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&rl=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&if=false&ts=1715839208070&cd[value]=39999.00&cd[currency]=INR&cd[content_ids]=%5B%22304697%22%5D&cd[content_type]=product&cd[content_name]=OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)&sw=1523&sh=650&ud[external_id]=36ce52f2c012a86c9264c805b8cff82e07250268c3b1663e92a631306623bfc6&v=2.9.156&r=stable&ec=2&o=4126&fbp=fb.1.1715839140634.281453602&ler=empty&cdl=API_unavailable&it=1715839205604&coo=false&eid=53824125171208&tm=1&rqm=GET", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("activityi_dc_pre_CMDD0pO_kYY", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("json_2");
    ns_web_url("json_2",
        "URL=https://gumi.criteo.com/sid/json?origin=onetag&domain=croma.com&sn=ChromeSyncframe&so=3&topUrl=www.croma.com&bundle=EPlPXl9ZQjN2TTdzaTNPd0Q0dlByN3VaMFZKWVpFMmRIUXg4WFZHd0t1eUFlY3ZJZHhZOGdWZU1pa0FqeGs0VVBSczlOOXJabXAxU3g5bkRtVEh3Vm9aTE1CMWJnUDJkJTJCNFN1WWxUWmoxN0wybzBtaXVnSyUyQnVpNHh5VTBXQ1BUZHFrQmY&cw=1&lsw=1&topicsavail=0&fledgeavail=0",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1006316414/?random=36128514&cv=11&fst=1715839206129&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&value=0&npa=0&ec_m=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)*LI%3Atrue%3A23%3Afalse*1&ec_sel=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)&ec_meta=LI%3Atrue%3A23%3Afalse&ec_lat=19&ec_s=1&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtfgtjrb_K-_nre2IDZgUpb4hTRiXKt6bkg&pscrd=IhMI_46Uk7-RhgMVCUWdCR0hSw-VMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1006316414/?random=1995214093&cv=11&fst=1715839206187&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&value=0&npa=0&ec_m=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)*LI%3Atrue%3A23%3Afalse*1&ec_sel=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)&ec_meta=LI%3Atrue%3A23%3Afalse&ec_lat=0&ec_s=1&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtWRrHOUWjst_FUnU7COO76qpoJ1C-TikUQ&pscrd=IhMI-ouUk7-RhgMVMEedCR2VZQZkMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1006316414/?random=1114128768&cv=11&fst=1715839206494&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=-5upCIv7u6MYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&value=0&npa=0&ec_m=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)*LI%3Atrue%3A23%3Afalse*1&ec_sel=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)&ec_meta=LI%3Atrue%3A23%3Afalse&ec_lat=3&ec_s=1&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtRZpU7C91KbPXBfDJ4ExDbEN2nR752ADCQ&pscrd=IhMIhrKXk7-RhgMVJFCdCR0VSAn_MgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/609902077/?random=1085583075&cv=11&fst=1715839207284&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtRpF2uaKgxd-kVcsxG3eD89BHHLzftEYGg&pscrd=IhMI4IHMk7-RhgMVUmidCR1lHAfGMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/609902077/?random=730121051&cv=11&fst=1715839207382&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtXVemLdCuGrPXaRAMfZz75147rIeYBLcCQ&pscrd=IhMIruTNk7-RhgMVFkqdCR1Mdg37MgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1639657222/insta_before_v2nnfx.png", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;cto_bundle;s_sq;mbox;_ga_DJ3RGVFHJN;s_nr30;s_ips;s_tp;s_ppv;th_external_id;WZRK_S_679-6RK-976Z;_uetsid;_uetvid;_hjSessionUser_3400595", END_INLINE,
            //URL commented due to error code : ABORTED
            //"URL=https://media-ik.croma.com/prod/https://media.croma.com/video/upload/v1713436641/Croma%20Assets/Videos/2024%20Videos/304697_w9hxoc.mp4?tr=q-70", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Range:bytes=65536-", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:video", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;cto_bundle;s_sq;mbox;_ga_DJ3RGVFHJN;s_nr30;s_ips;s_tp;s_ppv;th_external_id;WZRK_S_679-6RK-976Z;_uetsid;_uetvid;_hjSessionUser_3400595", END_INLINE,
            "URL=https://13135721.fls.doubleclick.net/activityi;dc_pre=CMDD0pO_kYYDFcPvTAIdWeACBw;src=13135721;type=croma02;cat=croma0;ord=1682216487709;u=%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697;npa=0;auiddc=1421799651.1715839140;uaa=x86;uab=64;uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199;uamb=0;uam=;uap=Linux;uapv=5.15.0;uaw=0;pscdl=noapi;frm=0;gtm=45fe45f0z8889407033za201;gcd=13l3l3l3l1;dma=0;epver=2;~oref=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697?", END_INLINE
    );

    ns_end_transaction("json_2", NS_AUTO_STATUS);
    ns_page_think_time(0.613);

    ns_start_transaction("activityi_dc_pre_COqb5OK_kYY");
    ns_web_url("activityi_dc_pre_COqb5OK_kYY",
        "URL=https://adservice.google.com/ddm/fls/z/dc_pre=CMDD0pO_kYYDFcPvTAIdWeACBw;src=13135721;type=croma02;cat=croma0;ord=1682216487709;u=%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697;npa=0;auiddc=*;uaa=x86;uab=64;uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199;uamb=0;uam=;uap=Linux;uapv=5.15.0;uaw=0;pscdl=noapi;frm=0;gtm=45fe45f0z8889407033za201;gcd=13l3l3l3l1;dma=0;epver=2;~oref=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:image",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.google.com/pagead/1p-conversion/609902077/?random=1085583075&cv=11&fst=1715839207284&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMI4IHMk7-RhgMVUmidCR1lHAfGMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtqQhhkyOekwCwYLPTINDV0xuQjgfh7vg&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtf1uBXbpe-QEg--cYNZfYc4xSFv4yJESDA&random=2256832405", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-conversion/609902077/?random=730121051&cv=11&fst=1715839207382&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMIruTNk7-RhgMVFkqdCR1Mdg37MgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtqD6Fh93ul77Lo-ERDVCvakXzx2SMXjQ&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHteebxr5OnJ94p1IEeAGFRtghitn3gCy1EA&random=1565128704", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-conversion/1006316414/?random=1114128768&cv=11&fst=1715839206494&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=-5upCIv7u6MYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&value=0&npa=0&ec_m=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)*LI%3Atrue%3A23%3Afalse*1&ec_sel=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)&ec_meta=LI%3Atrue%3A23%3Afalse&ec_lat=3&ec_s=1&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMIhrKXk7-RhgMVJFCdCR0VSAn_MgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtqtyRYqgLF-w2dLZjTkEpHvLt4AKBnUQ&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtWXPkFBE2W61KjmBeaysSOFXmuCB4HUxwA&random=1252024436", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-conversion/1006316414/?random=1995214093&cv=11&fst=1715839206187&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&value=0&npa=0&ec_m=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)*LI%3Atrue%3A23%3Afalse*1&ec_sel=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)&ec_meta=LI%3Atrue%3A23%3Afalse&ec_lat=0&ec_s=1&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMI-ouUk7-RhgMVMEedCR2VZQZkMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtqBOAm0MvH8pSqhZE3VX1YpxnAUHtCCw&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtb1e2CVzLl75RseX5HA--5DsXuuaZr3Kuw&random=1158275764", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-conversion/1006316414/?random=36128514&cv=11&fst=1715839206129&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&value=0&npa=0&ec_m=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)*LI%3Atrue%3A23%3Afalse*1&ec_sel=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)&ec_meta=LI%3Atrue%3A23%3Afalse&ec_lat=19&ec_s=1&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMI_46Uk7-RhgMVCUWdCR0hSw-VMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtqMOqMWJPN7NRVUMt63rqPXAh65LawZQ&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtcp6muasP7BpzLCnYOFBoIAikzc93FftNA&random=3075534630", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("activityi_dc_pre_COqb5OK_kYY", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("page_7");
    ns_web_url("page_7",
        "URL=https://rs.fullstory.com/rec/page",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"OrgId":"11EGJ5","UserId":"","Url":"https://www.croma.com/oneplus-12r-5g-8gb-128gb-cool-blue-/p/304697","Base":"https://www.croma.com/oneplus-12r-5g-8gb-128gb-cool-blue-/p/304697","Width":1508,"Height":523,"ScreenWidth":1523,"ScreenHeight":650,"SnippetVersion":"1.3.0","Referrer":"https://www.croma.com/campaign/oneplus-r-5g/c/6407?q=%3Aprice-asc","Preroll":1723,"Doctype":"<!DOCTYPE html>","CompiledVersion":"2df08c2f7ddf811d40d34bea72f08ca3d8851de8","CompiledTimestamp":1715639152,"TabId":"11f174717b75170"}",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.google.co.in/pagead/1p-conversion/609902077/?random=1085583075&cv=11&fst=1715839207284&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMI4IHMk7-RhgMVUmidCR1lHAfGMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtqQhhkyOekwCwYLPTINDV0xuQjgfh7vg&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtf1uBXbpe-QEg--cYNZfYc4xSFv4yJESDA&random=2256832405&ipr=y", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-conversion/1006316414/?random=1114128768&cv=11&fst=1715839206494&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=-5upCIv7u6MYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&value=0&npa=0&ec_m=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)*LI%3Atrue%3A23%3Afalse*1&ec_sel=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)&ec_meta=LI%3Atrue%3A23%3Afalse&ec_lat=3&ec_s=1&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMIhrKXk7-RhgMVJFCdCR0VSAn_MgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtqtyRYqgLF-w2dLZjTkEpHvLt4AKBnUQ&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtWXPkFBE2W61KjmBeaysSOFXmuCB4HUxwA&random=1252024436&ipr=y", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-conversion/1006316414/?random=36128514&cv=11&fst=1715839206129&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&value=0&npa=0&ec_m=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)*LI%3Atrue%3A23%3Afalse*1&ec_sel=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)&ec_meta=LI%3Atrue%3A23%3Afalse&ec_lat=19&ec_s=1&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMI_46Uk7-RhgMVCUWdCR0hSw-VMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtqMOqMWJPN7NRVUMt63rqPXAh65LawZQ&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtcp6muasP7BpzLCnYOFBoIAikzc93FftNA&random=3075534630&ipr=y", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-conversion/1006316414/?random=1995214093&cv=11&fst=1715839206187&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&value=0&npa=0&ec_m=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)*LI%3Atrue%3A23%3Afalse*1&ec_sel=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)&ec_meta=LI%3Atrue%3A23%3Afalse&ec_lat=0&ec_s=1&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMI-ouUk7-RhgMVMEedCR2VZQZkMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtqBOAm0MvH8pSqhZE3VX1YpxnAUHtCCw&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtb1e2CVzLl75RseX5HA--5DsXuuaZr3Kuw&random=1158275764&ipr=y", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-conversion/609902077/?random=730121051&cv=11&fst=1715839207382&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMIruTNk7-RhgMVFkqdCR1Mdg37MgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtqD6Fh93ul77Lo-ERDVCvakXzx2SMXjQ&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHteebxr5OnJ94p1IEeAGFRtghitn3gCy1EA&random=1565128704&ipr=y", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("page_7", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_17");
    ns_web_url("v1_17",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=customer-hash:null",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=tdl-sso-version:4.1.15",
        "HEADER=session:false",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tL29uZXBsdXMtMTJyLTVnLThnYi0xMjhnYi1jb29sLWJsdWUtL3AvMzA0Njk3IiwiZXZlbnREYXRhIjpbeyJ0aW1lc3RhbXAiOjE3MTU4MzkyMDg3NzAsImRhdGEiOiJ7XCJldmVudFwiOlwicGFnZUxvYWRcIixcInBheWxvYWRcIjp7XCJ1cmxcIjpcImh0dHBzOi8vd3d3LmNyb21hLmNvbS9vbmVwbHVzLTEyci01Zy04Z2ItMTI4Z2ItY29vbC1ibHVlLS9wLzMwNDY5N1wiLFwiZGF0YVNvdXJjZVwiOlwibG9hZFwiLFwiaXNUcnVzdGVkXCI6dHJ1ZX19In1dLCJjdXN0b21lckhhc2giOm51bGwsImVjaWQiOiJVbmtub3duIiwiY2xpZW50SWQiOiJDUk9NQS1XRUItQVBQIiwic2Vzc2lvbklkIjoiYjY1ZjU0YjQtNWM5My00ODJlLThjZjgtYzIxMGVjZmZhNzhmLjkyM2JiNTdmLTk1MWItNDc4MC04Y2MzLTNlN2M4OWU5MDVmZSIsImJyYW5kIjoiQ1JPTUEiLCJ0aW1lc3RhbXAiOjE3MTU4MzkyMDg3NzAsImRldmljZSI6Ik1vemlsbGEvNS4wIChYMTE7IExpbnV4IHg4Nl82NCkgQXBwbGVXZWJLaXQvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lLzExNC4wLjAuMCBTYWZhcmkvNTM3LjM2IiwiamFydmlzSWQiOiI0ZDYwNDU3MC1iYWZiLTQ1NTEtYmFkMC04NTM4OTcxYzdlZTIiLCJhZHZlcnRpc2luZ0lkIjpudWxsLCJhcHBWZXJzaW9uIjoiMCJ9"}",
        BODY_END,
        INLINE_URLS,
            "URL=https://c.bing.com/c.gif?Red3=CTOMS_pd&cbid=k-0tyrVmhJLxG-nAKY3h_cmH_uE4L3d_G-DpKVpINPhVSXxwS3", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("v1_17", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_18");
    ns_web_url("v1_18",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=customer-hash:null",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=tdl-sso-version:4.1.15",
        "HEADER=session:false",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tL29uZXBsdXMtMTJyLTVnLThnYi0xMjhnYi1jb29sLWJsdWUtL3AvMzA0Njk3IiwiZXZlbnREYXRhIjpbeyJ0aW1lc3RhbXAiOjE3MTU4MzkyMDg3NzMsImRhdGEiOiJ7XCJldmVudFwiOlwibmV0d29yay1pbmZvXCIsXCJkYXRhXCI6e1wiZG93bmxpbmtcIjoxMCxcImVmZmVjdGl2ZVR5cGVcIjpcIjRnXCJ9fSJ9XSwiY3VzdG9tZXJIYXNoIjpudWxsLCJlY2lkIjoiVW5rbm93biIsImNsaWVudElkIjoiQ1JPTUEtV0VCLUFQUCIsInNlc3Npb25JZCI6ImI2NWY1NGI0LTVjOTMtNDgyZS04Y2Y4LWMyMTBlY2ZmYTc4Zi45MjNiYjU3Zi05NTFiLTQ3ODAtOGNjMy0zZTdjODllOTA1ZmUiLCJicmFuZCI6IkNST01BIiwidGltZXN0YW1wIjoxNzE1ODM5MjA4NzczLCJkZXZpY2UiOiJNb3ppbGxhLzUuMCAoWDExOyBMaW51eCB4ODZfNjQpIEFwcGxlV2ViS2l0LzUzNy4zNiAoS0hUTUwsIGxpa2UgR2Vja28pIENocm9tZS8xMTQuMC4wLjAgU2FmYXJpLzUzNy4zNiIsImphcnZpc0lkIjoiNGQ2MDQ1NzAtYmFmYi00NTUxLWJhZDAtODUzODk3MWM3ZWUyIiwiYWR2ZXJ0aXNpbmdJZCI6bnVsbCwiYXBwVmVyc2lvbiI6IjAifQ=="}",
        BODY_END,
        INLINE_URLS,
            "URL=https://cdn.appdynamics.com/adrum-ext.a57fe9a4dfa0e1d6b2dc001466e4e21d.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.croma.com/assets/favicon.ico", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;s_sq;mbox;RT;s_nr30;s_ips;s_test;s_stop;s_test14;s_stop14;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;WZRK_S_679-6RK-976Z;cto_bundle;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://www.croma.com/assets/js/shopWithVideo.min.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;s_sq;mbox;RT;s_nr30;s_ips;s_test;s_stop;s_test14;s_stop14;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;WZRK_S_679-6RK-976Z;cto_bundle;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE
    );

    ns_end_transaction("v1_18", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("adrum_6");
    ns_web_url("adrum_6",
        "URL=https://bom-col.eum-appdynamics.com/eumcollector/beacons/browser/v1/BO-AAB-FBK/adrum",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:text/plain",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/adrum_6_url_0_1_1715839393382.body",
        BODY_END
    );

    ns_end_transaction("adrum_6", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_14");
    ns_web_url("collect_14",
        "URL=https://analytics.google.com/g/collect?v=2&tid=G-DJ3RGVFHJN&gtm=45je45f0v893318240z8889407033za200&_p=1715839203848&gcd=13l3l3l3l1&npa=0&dma=0&cid=1944455297.1715839141&ul=en-us&sr=1523x650&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&frm=0&pscdl=noapi&_s=3&sid=1715839140&sct=1&seg=1&dl=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&dr=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&dt=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&en=scroll_depth&epn.scroll_percentage=10&_et=2448&tfd=11757",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("collect_14", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("iev_2");
    ns_web_url("iev_2",
        "URL=https://csm.sg1.as.criteo.net/iev?entry=c~Gum.ChromeSyncframe.FragmentData.onetag.Bundle.Origin.3~1&entry=c~Gum.ChromeSyncframe.SidReadSuccess~1&entry=h~Gum.ChromeSyncframe.SidReadSuccessDuration~306",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://in1.clevertap-prod.com/a?t=96&type=ping&d=N4IglgJiBcIGwHYCcBaOAlA0ipC4C0QAaEAcxngDMAGADgENaJKAWAYwFNqXLWAjBLTi0ArCIDMScXzgRxAJhHEQAZxgBGBOpG1J6lupIAHctHkl6lGKAA2YPhQDuHPihUQA1igBu6gHQIfuLKRgBOAPYALuFs4TYUABaRkUZqAL5pQA&rn=4&i=1715839261&sn=0&gc=6f08a8df4ce04ff4b786855393b6d325&arp=N4IgVg%2BgdiBcIC0DuBeFIA0ICW06IFsBOKAMwEEBVAYXPIDVtMQATCAFwGc4AGLFgBa8sANzgAmLJG7xgAX2bYW%2BAGwB2IgFoVAJQDSmompUJmAJw4yAjGqsBWABwBmIlYAsPOUA&tries=1&useIP=true&r=1715839261080", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://in1.clevertap-prod.com/a?t=96&type=push&d=N4IgLgngDgpiBcIYDcYDswgDROWAcgIYC2ciAxgM4D6ACgCK0AEAyjOWAJYD2aTAksSgAnGJUo802XGHqEwhBKFrDuAEwCuHJkVIIQAeTQxaAGw2UmARgBMAJSYBWAOJMAFAA5nAISzWbXr5MAMLc3KZM3uYwAJTSwfIwAObcwhBMADIADPq0ABa8YkwAZEwA6jCEwoQARqZi8YkpaZlW%2BgCy3DWc9Uz5hZSNYMmp6Rk2%2BgCCaGqqnGp9BcaDON7VM%2FpGJuYrIGbyAGapxPrkqsSEAHTk3Cc4bBySOiRkhqjCyJwwAO7SKupaMACNT6ADMWQALAA2ACcAHYQABfHDzfRQuEwgC0ULsAGlMfCoQAtaRJNEHLIeQgeNQHCHkGCQg50mpwjxQjyORygmGgmpQtSgmyOaSDeBWOFWRweHlWCFWHBQMnwGw4QgHJQgUycGr6b4wGqYyhqADWmOQVkucMuoOkIm4YG4N1M%2BjyYDAUEGyJAZSJeOowUm7Vomu%2BQgQWRw3044fgkfApnICAA2gBdKOcMCJlOpxGIoAAA&rn=4&i=1715839278&sn=2&gc=6f08a8df4ce04ff4b786855393b6d325&arp=N4IgVg%2BgdiBcIC0DuBeFIA0ICW06IFsBOKAMwEEBVAYXPIDVtMQATCAFwGc4AGLFgBa8sANzgAmLJG7xgAX2bYW%2BAGwB2IgFoVAJQDSmompUJmAJw4yAjGqsBWABwBmIlYAsPOUA&tries=1&useIP=true&r=1715839278929", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.croma.com/static/js/11.432a32f5.chunk.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;s_sq;mbox;RT;s_nr30;s_ips;s_test;s_stop;s_test14;s_stop14;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;_ga_DJ3RGVFHJN;s_tp;WZRK_S_679-6RK-976Z;s_ppv", END_INLINE,
            "URL=https://www.croma.com/static/js/63.8c772a17.chunk.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;s_sq;mbox;RT;s_nr30;s_ips;s_test;s_stop;s_test14;s_stop14;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;_ga_DJ3RGVFHJN;s_tp;WZRK_S_679-6RK-976Z;s_ppv", END_INLINE
    );

    ns_end_transaction("iev_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("X04697_2");
    ns_web_url("X04697_2",
        "URL=https://api.croma.com/productdetails/allchannels/v1/reviewcount/304697",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;s_sq;mbox;s_nr30;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;_ga_DJ3RGVFHJN;s_tp;WZRK_S_679-6RK-976Z;s_ppv"
    );

    ns_end_transaction("X04697_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("X04697_3");
    ns_web_url("X04697_3",
        "URL=https://api.croma.com/productdetails/allchannels/v1/bifurcationCount/304697",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;s_sq;mbox;s_nr30;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;_ga_DJ3RGVFHJN;s_tp;WZRK_S_679-6RK-976Z;s_ppv"
    );

    ns_end_transaction("X04697_3", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("page_8");
    ns_web_url("page_8",
        "URL=https://api.croma.com/cmstemplate/allchannels/v1/page?pageType=ContentPage&pageLabelOrId=304697&fields=FULL",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;s_sq;mbox;s_nr30;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;_ga_DJ3RGVFHJN;s_tp;WZRK_S_679-6RK-976Z;s_ppv",
        INLINE_URLS,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/video/upload/v1699267845/Croma%20Assets/A%2B%20Content%20Images/Images/304697_M_4_jq5vte.mp4", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Range:bytes=0-", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:video", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;s_sq;mbox;s_nr30;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;_ga_DJ3RGVFHJN;s_tp;WZRK_S_679-6RK-976Z;s_ppv", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/video/upload/v1699267845/Croma%20Assets/A%2B%20Content%20Images/Images/304697_M_14_zvtogu.mp4", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Range:bytes=0-", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:video", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;s_sq;mbox;s_nr30;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;_ga_DJ3RGVFHJN;s_tp;WZRK_S_679-6RK-976Z;s_ppv", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/video/upload/v1699267845/Croma%20Assets/A%2B%20Content%20Images/Images/304697_D_4_j7hbu1.mp4", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Range:bytes=0-", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:video", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;s_sq;mbox;s_nr30;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;_ga_DJ3RGVFHJN;s_tp;WZRK_S_679-6RK-976Z;s_ppv", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/video/upload/v1699267845/Croma%20Assets/A%2B%20Content%20Images/Images/304697_D_14_d950xd.mp4", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Range:bytes=0-", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:video", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;s_sq;mbox;s_nr30;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;_ga_DJ3RGVFHJN;s_tp;WZRK_S_679-6RK-976Z;s_ppv", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1699267845/Croma%20Assets/A%2B%20Content%20Images/Images/304697_D_1_sjtymb.png?tr=w-1024", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;s_sq;mbox;s_nr30;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;_ga_DJ3RGVFHJN;s_tp;WZRK_S_679-6RK-976Z;s_ppv", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1699267845/Croma%20Assets/A%2B%20Content%20Images/Images/304697_D_2_whtuqw.png?tr=w-1024", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;s_sq;mbox;s_nr30;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;_ga_DJ3RGVFHJN;s_tp;WZRK_S_679-6RK-976Z;s_ppv", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1699267845/Croma%20Assets/A%2B%20Content%20Images/Images/304697_D_3_twvohf.png?tr=w-1024", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;s_sq;mbox;s_nr30;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;_ga_DJ3RGVFHJN;s_tp;WZRK_S_679-6RK-976Z;s_ppv", END_INLINE
    );

    ns_end_transaction("page_8", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("adrum_7");
    ns_web_url("adrum_7",
        "URL=https://bom-col.eum-appdynamics.com/eumcollector/beacons/browser/v1/BO-AAB-FBK/adrum",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:text/plain",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"vr":"23.10.1.4359","dt":"R","rg":"0","es":[{"eg":"1","et":2,"eu":"0://1/2/3/4/5/6","ts":1715839279524,"mg":"0","au":"0://7/8/9/6","at":0,"pp":3,"mx":{"PLC":1,"FBT":123,"DDT":0,"DPT":1,"PLT":124,"ARE":0},"md":"GET","xs":200,"si":46},{"eg":"2","et":2,"eu":"0://1/2/3/4/10/6","ts":1715839279523,"mg":"0","au":"0://7/8/9/6","at":0,"pp":3,"mx":{"PLC":1,"FBT":147,"DDT":1,"DPT":0,"PLT":148,"ARE":0},"md":"GET","xs":200,"si":47},{"eg":"3","et":2,"eu":"0://1/11/3/4/12?13","ts":1715839279525,"mg":"0","au":"0://7/8/9/6","at":0,"pp":3,"mx":{"PLC":1,"FBT":353,"DDT":9,"DPT":1,"PLT":363,"ARE":0},"md":"GET","xs":200,"si":48}],"ai":"477d42aa_9f21_9144_1a76_688c9aa25e4b","gs":["06a6dd02_3c5f_2943_3169_f771971f39e1","97d2819d_b9a4_f548_267c_76359141c812","f3a1bce9_28ca_43f6_596b_6856325238bb","36b1910e_ec0d_f2dc_803e_1c21e37e6338"],"up":["https","api.croma.com","productdetails","allchannels","v1","bifurcationCount","304697","www.croma.com","oneplus-12r-5g-8gb-128gb-cool-blue-","p","reviewcount","cmstemplate","page","pageType=ContentPage&pageLabelOrId=304697&fields=FULL"]}",
        BODY_END
    );

    ns_end_transaction("adrum_7", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_15");
    ns_web_url("collect_15",
        "URL=https://analytics.google.com/g/collect?v=2&tid=G-DJ3RGVFHJN&gtm=45je45f0v893318240z8889407033za200&_p=1715839203848&_gaz=1&gcd=13l3l3l3l1&npa=0&dma=0&cid=1944455297.1715839141&ul=en-us&sr=1523x650&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&frm=0&pscdl=noapi&_s=4&sid=1715839140&sct=1&seg=1&dl=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&dr=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&dt=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&en=scroll_depth&epn.scroll_percentage=25&_et=87820&tfd=123791",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.google.co.in/ads/ga-audiences?v=1&t=sr&slf_rd=1&_r=4&tid=G-DJ3RGVFHJN&cid=1944455297.1715839141&gtm=45je45f0v893318240z8889407033za200&aip=1&dma=0&gcd=13l3l3l3l1&npa=0&frm=0&z=511872694", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("collect_15", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_19");
    ns_web_url("v1_19",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tL29uZXBsdXMtMTJyLTVnLThnYi0xMjhnYi1jb29sLWJsdWUtL3AvMzA0Njk3IiwiZXZlbnREYXRhIjpbeyJ0aW1lc3RhbXAiOjE3MTU4MzkzMjU5ODQsImRhdGEiOiJ7XCJldmVudFwiOlwid2ViLXZpdGFsc1wiLFwiZGF0YVwiOntcIm5hbWVcIjpcIkZJRFwiLFwidmFsdWVcIjowLjg5OTk5OTYxODUzMDI3MzQsXCJyYXRpbmdcIjpcImdvb2RcIixcImRlbHRhXCI6MC44OTk5OTk2MTg1MzAyNzM0LFwiZW50cmllc1wiOlt7XCJuYW1lXCI6XCJwb2ludGVyZG93blwiLFwiZW50cnlUeXBlXCI6XCJmaXJzdC1pbnB1dFwiLFwic3RhcnRUaW1lXCI6MzIyNDQuMzk5OTk5NjE4NTMsXCJkdXJhdGlvblwiOjgsXCJwcm9jZXNzaW5nU3RhcnRcIjozMjI0NS4yOTk5OTkyMzcwNixcInByb2Nlc3NpbmdFbmRcIjozMjI0NS41LFwiY2FuY2VsYWJsZVwiOnRydWV9XSxcImlkXCI6XCJ2My0xNzE1ODM5MjAzNTIzLTI5NTUyMjU3OTAwNDVcIixcIm5hdmlnYXRpb25UeXBlXCI6XCJuYXZpZ2F0ZVwifX0ifV0sImN1c3RvbWVySGFzaCI6bnVsbCwiZWNpZCI6IlVua25vd24iLCJjbGllbnRJZCI6IkNST01BLVdFQi1BUFAiLCJzZXNzaW9uSWQiOiJiNjVmNTRiNC01YzkzLTQ4MmUtOGNmOC1jMjEwZWNmZmE3OGYuOTIzYmI1N2YtOTUxYi00NzgwLThjYzMtM2U3Yzg5ZTkwNWZlIiwiYnJhbmQiOiJDUk9NQSIsInRpbWVzdGFtcCI6MTcxNTgzOTMyNTk4NCwiZGV2aWNlIjoiTW96aWxsYS81LjAgKFgxMTsgTGludXggeDg2XzY0KSBBcHBsZVdlYktpdC81MzcuMzYgKEtIVE1MLCBsaWtlIEdlY2tvKSBDaHJvbWUvMTE0LjAuMC4wIFNhZmFyaS81MzcuMzYiLCJqYXJ2aXNJZCI6IjRkNjA0NTcwLWJhZmItNDU1MS1iYWQwLTg1Mzg5NzFjN2VlMiIsImFkdmVydGlzaW5nSWQiOm51bGwsImFwcFZlcnNpb24iOiIwIn0="}",
        BODY_END,
        INLINE_URLS,
            "URL=https://in1.clevertap-prod.com/a?t=96&type=ping&d=N4IglgJiBcIGwHYCcBaOAlA0ipC4C0QAaEAcxngDMAGADgENaJKAWAYwFNqXLWAjBLTi0ArCIDMScXzgRxAJhHEQAZxgBGBOpG1J6lupIAHctHkl6lGKAA2YPhQDuHPihUQA1igBu6gHQIfuLKRgBOAPYALuFs4TYUABaRkUZqAL5pQA&rn=5&i=1715839327&sn=0&gc=6f08a8df4ce04ff4b786855393b6d325&arp=N4IgVg%2BgdiBcIC0DuBeFIA0ICW06IFsBOKAMwEEBVAYXPIDVtMQATCAFwGc4AGLFgBa8sANzgAmLJG7xgAX2bYW%2BAGwB2IgFoVAJQDSmompUJmAJw4yAjGqsBWABwBmIlYAsPOUA&tries=1&useIP=true&r=1715839327687", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.facebook.com/tr/?id=1369867960112522&ev=SubscribedButtonClick&dl=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&rl=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&if=false&ts=1715839372534&cd[buttonFeatures]=%7B%22classList%22%3A%22btn%20btn-secondary%20pdp-add-to-cart%22%2C%22destination%22%3A%22%22%2C%22id%22%3A%22%22%2C%22imageUrl%22%3A%22%22%2C%22innerText%22%3A%22Add%20to%20Cart%22%2C%22numChildButtons%22%3A0%2C%22tag%22%3A%22button%22%2C%22type%22%3A%22button%22%2C%22name%22%3A%22%22%2C%22value%22%3A%22%22%7D&cd[buttonText]=Add%20to%20Cart&cd[formFeatures]=%5B%5D&cd[pageFeatures]=%7B%22title%22%3A%22Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!%22%7D&sw=1523&sh=650&ud[external_id]=36ce52f2c012a86c9264c805b8cff82e07250268c3b1663e92a631306623bfc6&v=2.9.156&r=stable&ec=3&o=4126&fbp=fb.1.1715839140634.281453602&ler=empty&cdl=API_unavailable&it=1715839205604&coo=false&es=automatic&tm=3&rqm=GET", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("v1_19", NS_AUTO_STATUS);
    ns_page_think_time(0.299);

    //Page Auto split for Button '' Clicked by User
    ns_start_transaction("X_eb48c00b_chunk_js");
    ns_web_url("X_eb48c00b_chunk_js",
        "URL=https://www.croma.com/static/js/1.eb48c00b.chunk.js",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:script",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;RT;s_ips;s_stop;s_stop14;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;s_tp;s_ppv;_ga_DJ3RGVFHJN;WZRK_S_679-6RK-976Z;s_nr30;s_test;s_test14;s_sq",
        INLINE_URLS,
            "URL=https://www.croma.com/static/js/5.3ecd796d.chunk.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;RT;s_ips;s_stop;s_stop14;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;s_tp;s_ppv;_ga_DJ3RGVFHJN;WZRK_S_679-6RK-976Z;s_nr30;s_test;s_test14;s_sq", END_INLINE
    );

    ns_end_transaction("X_eb48c00b_chunk_js", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("multi");
    ns_web_url("multi",
        "URL=https://api.tatadigital.com/api/v1.1/msd/recommendations/multi",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Program-Id:01eae2ec-0576-1000-bbea-86e16dcb4b79",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"correlation_id":"sssaasss","client":"CROMA","catalog":"croma_products","config_name":"recommendations_in_croma_pdp","rec_params":{"catalog_item_id":"304697","pincode":"400049"},"num_results":15,"user_id":"08vHUbbT7lzWpSwZ","org_user_id":"","mad_uuid":"08vHUbbT7lzWpSwZ","fields":["product_image_url","product_image_text","product_title","product_price","discount_price","rating","product_detail_page_url"],"filters":[]}",
        BODY_END,
        INLINE_URLS,
            //URL commented due to error code : ABORTED
            //"URL=https://www.croma.com/assets/images/btn-loader.svg", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;RT;s_ips;s_stop;s_stop14;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;s_tp;s_ppv;_ga_DJ3RGVFHJN;WZRK_S_679-6RK-976Z;s_nr30;s_test;s_test14;s_sq", END_INLINE
    );

    ns_end_transaction("multi", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_20");
    ns_web_url("v1_20",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tL29uZXBsdXMtMTJyLTVnLThnYi0xMjhnYi1jb29sLWJsdWUtL3AvMzA0Njk3IiwiZXZlbnREYXRhIjpbeyJ0aW1lc3RhbXAiOjE3MTU4MzkzNzI1MjAsImRhdGEiOiJ7XG4gIFwidXJsXCI6IHtcbiAgICBcImFuY2VzdG9yT3JpZ2luc1wiOiB7fSxcbiAgICBcImhyZWZcIjogXCJodHRwczovL3d3dy5jcm9tYS5jb20vb25lcGx1cy0xMnItNWctOGdiLTEyOGdiLWNvb2wtYmx1ZS0vcC8zMDQ2OTdcIixcbiAgICBcIm9yaWdpblwiOiBcImh0dHBzOi8vd3d3LmNyb21hLmNvbVwiLFxuICAgIFwicHJvdG9jb2xcIjogXCJodHRwczpcIixcbiAgICBcImhvc3RcIjogXCJ3d3cuY3JvbWEuY29tXCIsXG4gICAgXCJob3N0bmFtZVwiOiBcInd3dy5jcm9tYS5jb21cIixcbiAgICBcInBvcnRcIjogXCJcIixcbiAgICBcInBhdGhuYW1lXCI6IFwiL29uZXBsdXMtMTJyLTVnLThnYi0xMjhnYi1jb29sLWJsdWUtL3AvMzA0Njk3XCIsXG4gICAgXCJzZWFyY2hcIjogXCJcIixcbiAgICBcImhhc2hcIjogXCJcIlxuICB9LFxuICBcInBhdGhcIjogW1xuICAgIHtcbiAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJidXR0b25cIixcbiAgICAgIFwiaWRcIjogXCJcIixcbiAgICAgIFwiY2xhc3NcIjogXCJidG4gYnRuLXNlY29uZGFyeSBwZHAtYWRkLXRvLWNhcnRcIlxuICAgIH0sXG4gICAge1xuICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImRpdlwiLFxuICAgICAgXCJpZFwiOiBcIlwiLFxuICAgICAgXCJjbGFzc1wiOiBcInBkLWFjdGlvbiBmbGV4LXNlY3Rpb25cIlxuICAgIH0sXG4gICAge1xuICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImRpdlwiLFxuICAgICAgXCJpZFwiOiBcIlwiLFxuICAgICAgXCJjbGFzc1wiOiBcInAtZnVsbCBmbG9hdGluZ19idG5fcGFuZWxcIlxuICAgIH0sXG4gICAge1xuICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImRpdlwiLFxuICAgICAgXCJpZFwiOiBcIlwiLFxuICAgICAgXCJjbGFzc1wiOiBcInJvdyBmbGV4LXNlY3Rpb25cIlxuICAgIH0sXG4gICAge1xuICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImRpdlwiLFxuICAgICAgXCJpZFwiOiBcIlwiLFxuICAgICAgXCJjbGFzc1wiOiBcImNvbnRhaW5lclwiXG4gICAgfSxcbiAgICB7XG4gICAgICBcImVsZW1lbnRUeXBlXCI6IFwiZGl2XCIsXG4gICAgICBcImlkXCI6IFwiYWRkX3RvX2NhcnRfZm9vdGVyX2NvbnRhaW5lclwiLFxuICAgICAgXCJjbGFzc1wiOiBcImNwLXByb2R1Y3QtZGV0YWlscyBzaG93X2FkZF90b19jYXJ0XCJcbiAgICB9LFxuICAgIHtcbiAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJkaXZcIixcbiAgICAgIFwiaWRcIjogXCJcIixcbiAgICAgIFwiY2xhc3NcIjogXCJjcC1zZWN0aW9uIGJhbm5lci1zcGFjaW5nIHNob3ctcGRwLWljb25cIlxuICAgIH0sXG4gICAge1xuICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImRpdlwiLFxuICAgICAgXCJpZFwiOiBcInBkcGRhdGFlbFwiLFxuICAgICAgXCJjbGFzc1wiOiBcImJzLWNvbnRlbnRcIlxuICAgIH0sXG4gICAge1xuICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImRpdlwiLFxuICAgICAgXCJpZFwiOiBcIlwiLFxuICAgICAgXCJjbGFzc1wiOiBcIm1haW5Db250YWluZXJcIlxuICAgIH0sXG4gICAge1xuICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImRpdlwiLFxuICAgICAgXCJpZFwiOiBcIlwiLFxuICAgICAgXCJjbGFzc1wiOiBcIlwiXG4gICAgfSxcbiAgICB7XG4gICAgICBcImVsZW1lbnRUeXBlXCI6IFwibWFpblwiLFxuICAgICAgXCJpZFwiOiBcImNvbnRhaW5lclwiLFxuICAgICAgXCJjbGFzc1wiOiBcImRhcmstdGhlbWVcIlxuICAgIH1cbiAgXSxcbiAgXCJ0YXJnZXRcIjogXCI8YnV0dG9uIHR5cGU9XFxcImJ1dHRvblxcXCIgY2xhc3M9XFxcImJ0biBidG4tc2Vjb25kYXJ5IHBkcC1hZGQtdG8tY2FydFxcXCI+QWRkIHRvIENhcnQ8L2J1dHRvbj5cIixcbiAgXCJ0aW1lU3RhbXBcIjogMTcxNTgzOTM3MjUyMCxcbiAgXCJ0eXBlXCI6IFwiY2xpY2tcIlxufSJ9XSwiY3VzdG9tZXJIYXNoIjpudWxsLCJlY2lkIjoiVW5rbm93biIsImNsaWVudElkIjoiQ1JPTUEtV0VCLUFQUCIsInNlc3Npb25JZCI6ImI2NWY1NGI0LTVjOTMtNDgyZS04Y2Y4LWMyMTBlY2ZmYTc4Zi45MjNiYjU3Zi05NTFiLTQ3ODAtOGNjMy0zZTdjODllOTA1ZmUiLCJicmFuZCI6IkNST01BIiwidGltZXN0YW1wIjoxNzE1ODM5MzcyNTIwLCJkZXZpY2UiOiJNb3ppbGxhLzUuMCAoWDExOyBMaW51eCB4ODZfNjQpIEFwcGxlV2ViS2l0LzUzNy4zNiAoS0hUTUwsIGxpa2UgR2Vja28pIENocm9tZS8xMTQuMC4wLjAgU2FmYXJpLzUzNy4zNiIsImphcnZpc0lkIjoiNGQ2MDQ1NzAtYmFmYi00NTUxLWJhZDAtODUzODk3MWM3ZWUyIiwiYWR2ZXJ0aXNpbmdJZCI6bnVsbCwiYXBwVmVyc2lvbiI6IjAifQ=="}",
        BODY_END,
        INLINE_URLS,
            "URL=https://api.croma.com/security/allchannels/v1/pkey/issuance", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=accessToken:null", "HEADER=client_id:CROMA-WEB-APP", "HEADER=customerHash:null", "HEADER=source:null", "HEADER=sec-ch-ua-mobile:?0", "HEADER=csc_code:null", "HEADER=Cache-Control:no-cache", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;s_tp;s_ppv;_ga_DJ3RGVFHJN;WZRK_S_679-6RK-976Z;s_nr30;s_sq", END_INLINE
    );

    ns_end_transaction("v1_20", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_21");
    ns_web_url("v1_21",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tL29uZXBsdXMtMTJyLTVnLThnYi0xMjhnYi1jb29sLWJsdWUtL3AvMzA0Njk3IiwiZXZlbnREYXRhIjpbeyJ0aW1lc3RhbXAiOjE3MTU4MzkzNzI1OTIsImRhdGEiOiJ7XCJldmVudFwiOlwid2ViLXZpdGFsc1wiLFwiZGF0YVwiOntcIm5hbWVcIjpcIkxDUFwiLFwidmFsdWVcIjoxNDQ3LjEwMDAwMDM4MTQ2OTcsXCJyYXRpbmdcIjpcImdvb2RcIixcImRlbHRhXCI6MTQ0Ny4xMDAwMDAzODE0Njk3LFwiZW50cmllc1wiOlt7XCJuYW1lXCI6XCJcIixcImVudHJ5VHlwZVwiOlwibGFyZ2VzdC1jb250ZW50ZnVsLXBhaW50XCIsXCJzdGFydFRpbWVcIjoxNDQ3LjEwMDAwMDM4MTQ2OTcsXCJkdXJhdGlvblwiOjAsXCJzaXplXCI6MTI0OTYyLFwicmVuZGVyVGltZVwiOjAsXCJsb2FkVGltZVwiOjE0NDcuMSxcImZpcnN0QW5pbWF0ZWRGcmFtZVRpbWVcIjowLFwiaWRcIjpcIjBwcm9kX2ltZ1wiLFwidXJsXCI6XCJodHRwczovL21lZGlhLWlrLmNyb21hLmNvbS9wcm9kL2h0dHBzOi8vbWVkaWEuY3JvbWEuY29tL2ltYWdlL3VwbG9hZC92MTcwNjc5MTEyOC9Dcm9tYSUyMEFzc2V0cy9Db21tdW5pY2F0aW9uL01vYmlsZXMvSW1hZ2VzLzMwNDY5N196cDhyd3AucG5nP3RyPXctNjQwXCJ9XSxcImlkXCI6XCJ2My0xNzE1ODM5MjAzNTI0LTM5NjE5NzAyMDg1NTJcIixcIm5hdmlnYXRpb25UeXBlXCI6XCJuYXZpZ2F0ZVwifX0ifV0sImN1c3RvbWVySGFzaCI6bnVsbCwiZWNpZCI6IlVua25vd24iLCJjbGllbnRJZCI6IkNST01BLVdFQi1BUFAiLCJzZXNzaW9uSWQiOiJiNjVmNTRiNC01YzkzLTQ4MmUtOGNmOC1jMjEwZWNmZmE3OGYuOTIzYmI1N2YtOTUxYi00NzgwLThjYzMtM2U3Yzg5ZTkwNWZlIiwiYnJhbmQiOiJDUk9NQSIsInRpbWVzdGFtcCI6MTcxNTgzOTM3MjU5MiwiZGV2aWNlIjoiTW96aWxsYS81LjAgKFgxMTsgTGludXggeDg2XzY0KSBBcHBsZVdlYktpdC81MzcuMzYgKEtIVE1MLCBsaWtlIEdlY2tvKSBDaHJvbWUvMTE0LjAuMC4wIFNhZmFyaS81MzcuMzYiLCJqYXJ2aXNJZCI6IjRkNjA0NTcwLWJhZmItNDU1MS1iYWQwLTg1Mzg5NzFjN2VlMiIsImFkdmVydGlzaW5nSWQiOm51bGwsImFwcFZlcnNpb24iOiIwIn0="}",
        BODY_END
    );

    ns_end_transaction("v1_21", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("carts");
    ns_web_url("carts",
        "URL=https://api.croma.com/useraccount/allchannels/v2/users/anonymous/carts",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=Content-Type:application/json",
        "HEADER=InCallReqHeader:AXaL9mb726in1mP1M9qeBXg6/qvwThm1mdhUkfN+KxiRbPGMdDut9ouQH3LNAfMJP7ruGNPBAzq/ZXJXp5vvAB84IhZ93ERYVcr2rucm3ZMi0kaotXpK3uv10rDB2fv9DUjzG4JqbiMq+XPcpQ3TQmhgRpeGwYKKyxSXOYVfTzhrDp7UZegAkFp8NueXBtqrlo5xohPiOTs44vy9DHOJE6sqXU0QB5DAPWlOkq/gsEsUGTK32sYfZ6KaPdmtRkRzC47WmbPBTWokjjOeZsbgGUBMpzH2+3dbvavErIhvreFh9pV8GxCF3z+6YNyzcp+Mbbkns5pxHZzSH3z3NQsJ+w==",
        "HEADER=googleRedirect:false",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;s_tp;s_ppv;_ga_DJ3RGVFHJN;WZRK_S_679-6RK-976Z;s_nr30;s_sq",
        BODY_BEGIN,
            "{}",
        BODY_END
    );

    ns_end_transaction("carts", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("entries");
    ns_web_url("entries",
        "URL=https://api.croma.com/useraccount/allchannels/v2/users/anonymous/carts/AN_CR-20958114759961ed485c6f419fa4d0cdfed6b9e2698801101019106380172848/entries?code=304697&qty=1&refreshCart=false&isPDP=true&isFreebiePresent=false&pincode=400049",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=Content-Type:application/json",
        "HEADER=InCallReqHeader:l4nV/7OJi31WsAIVZyVu9/rrbiJnpr8ygLJAwoMA0XrOd9xKiEn0ovZS+xtvuAHkYlpkaiODFZmkwVGzXJaXzMaMqlQgydkmsy1joHpica7keCFCksVEqWIEi1cVm+dXR8yOsWrL1v2PSYs/7km0rXORINBykC0UgqkwLTlt1WMbLz1wRzfIYBqCrc3+T+QOjzew+bRQbZEZ4dFbe+ZhMcvklodc11Sv+YLn4j4sIhBDeetd0DhTSuzRkl3z8E8KVNV2JY0YQEqIdsCoew1dPN7kls920ochMwU0l7CppY72iNWGNavEWkylmz+tzIdMtHwUNMszsCAGJxehYlOzxw==",
        "HEADER=googleRedirect:false",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;s_tp;s_ppv;_ga_DJ3RGVFHJN;WZRK_S_679-6RK-976Z;s_nr30;s_sq",
        BODY_BEGIN,
            "{}",
        BODY_END
    );

    ns_end_transaction("entries", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("count");
    ns_web_url("count",
        "URL=https://api.croma.com/useraccount/allchannels/v2/users/anonymous/carts/AN_CR-20958114759961ed485c6f419fa4d0cdfed6b9e2698801101019106380172848/count",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=googleRedirect:undefined",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;s_tp;s_ppv;_ga_DJ3RGVFHJN;WZRK_S_679-6RK-976Z;s_nr30;s_sq"
    );

    ns_end_transaction("count", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("events_6");
    ns_web_url("events_6",
        "URL=https://api.tatadigital.com/api/v1.1/msd/events",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Program-Id:73eb6345-9cc9-4c37-a8e8-8620d6d32cf5",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"page_type":"cdp","content_type":"product","action_name":"click","event_name":"add_to_cart","correlation_id":"sssaasss","medium":"pixel","metadata":{},"source":"CROMA","user_id_type":["CROMA"],"user_id":["08vHUbbT7lzWpSwZ"],"mad_uuid":"08vHUbbT7lzWpSwZ","utm":{"utm_source":"","utm_medium":"","utm_campaign":""},"epoch":"1715839373119","content_info":{"source_id":"304697","value":"0","quantity":1,"catalog_value":"0"},"pincode":"400049"}",
        BODY_END,
        INLINE_URLS,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s48424942986767?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%201%3A2%3A53%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=pdp&g=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&cc=INR&server=www.croma.com&events=event4%2CscAdd%2Cevent20%2Cevent21%2Cevent49%2Cevent53%3D592&products=%3B304697%3B1%3B%3Bevent20%3D39999%7Cevent21%3D39999%7Cevent49%3D1%3BeVar10%3D304697%7CeVar11%3D1%7CeVar14%3D39999%7CeVar13%3D39999%7CeVar23%3D%7CeVar24%3D%7CeVar25%3D%7CeVar26%3D%7CeVar27%3D%7CeVar28%3D%7CeVar29%3D%7CeVar41%3DOnePlus%2012R%205G%20%288GB%20128GB%20Cool%20Blue%29%7CeVar133%3Dundefined%7CeVar134%3Dundefined%7CeVar117%3DNo%7CeVar43%3DPhones%20%26%20Wearables%3AMobile%20Phones%3AAndroid%20Phones%7CeVar18%3D&c1=pdp&v1=D%3Dmid&c2=www.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&v2=new&c3=2024-5-16%201%3A2%3A53&c4=campaign&v4=guest%20user&v5=400049&v15=add%20to%20cart&v16=clicked&v17=bottom&v39=RL7f33593d247c4bebbc28ffe56be4c165&v53=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&v71=add%20to%20cart%7Cbottom%7Cclicked&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5BaddToCart%5D%20%3A%20Add%20To%20Cart&pe=lnk_o&pev2=add%20to%20cart&c.&a.&activitymap.&page=pdp&link=Add%20to%20Cart&region=add_to_cart_footer_container&pageIDType=1&.activitymap&.a&.c&pid=pdp&pidt=1&oid=function%28c%29%7Bvarq%3Db.Qa%28f%2Cc%2Ce%2Cl%29%3Ba.conf.ja%26%26%28q.oa%3Dx%29%3Breturnb.sh%28n%2Cq%2Cthis%2Carguments%29%7D&oidt=2&ot=BUTTON&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=1523&bh=523&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&lrt=351&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;s_tp;s_ppv;_ga_DJ3RGVFHJN;WZRK_S_679-6RK-976Z;s_nr30;s_sq", END_INLINE,
            "URL=https://sslwidget.criteo.com/event?a=56256&v=5.23.0&otl=1&p0=e%3Dexd%26site_type%3Dd&p1=e%3Dac%26tms%3Dgtm-template%26p%3D%255Bi%25253D304697%252526pr%25253D39999%252526q%25253D1%255D&p2=e%3Ddis&adce=1&bundle=WwpUr19ZQjN2TTdzaTNPd0Q0dlByN3VaMFZMOEIwMTVJWjE0ZXI3bEFGMGJFZUxUZ2pPQ2F0Vk5FdlY1cXpBTXJ6bWF1WlppM040TnV2MU1IMnJNbjFlMTQ1UllVN2hSbHpzJTJGT1J0eFNnZ1FnekxaakRmcGh3THpVMnd2VnJVMzMlMkJFU2U&tld=croma.com&dy=1&fu=https%253A%252F%252Fwww.croma.com%252Foneplus-12r-5g-8gb-128gb-cool-blue-%252Fp%252F304697&pu=https%253A%252F%252Fwww.croma.com%252Fcampaign%252Foneplus-r-5g%252Fc%252F6407%253Fq%253D%25253Aprice-asc&ceid=2294d954-feef-439c-bb34-17b290388555&dtycbr=61103", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("events_6", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("tr_2");
    ns_web_url("tr_2",
        "URL=https://capi.croma.com/tr?id=1369867960112522&ev=AddToCart&et=1715839373&es=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&referrer=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&eid=11146789180925&ua=Mozilla%2F5.0%20(X11%3B%20Linux%20x86_64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F114.0.0.0%20Safari%2F537.36&fbp=fb.1.1715839140634.281453602&external_id=dd225ea0e0a549dff2458f2b188bd0b54a923171c4b30d006a0ce3ff08d8fa8c&ud%5Bexternal_id%5D=36ce52f2c012a86c9264c805b8cff82e07250268c3b1663e92a631306623bfc6&cd%5Bvalue%5D=39999.00&cd%5Bcurrency%5D=INR&cd%5Bcontent_ids%5D=%5B%22304697%22%5D&cd%5Bcontent_type%5D=product&cd%5Bcontent_name%5D=OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;s_tp;s_ppv;_ga_DJ3RGVFHJN;WZRK_S_679-6RK-976Z;s_nr30;s_sq",
        INLINE_URLS,
            "URL=https://in1.clevertap-prod.com/a?t=96&type=push&d=N4IgLgngDgpiBcIYDcYDswgDROWAcgIYC2ciAxgM4D6AggCb0AEAKgPZMDChATpjijAARQmEIJQABUIBzGK2hkQUelGwhJAG1EAzNj2IIQ5Hm2KEAdOTPrJp%2BgFdyYJgEl6RgMwAGACwA2AE4Adlt7JxciUiMAeTQYLQdKJgBGACYAJSYAVgBxJgAKAA5cgCEsVLSS8q42Nk0mUs0HGABKdQBZDMkvQL7AzpiexE9%2BgZxXMBhiJiEAS0prBwwjbwBSdQBFB0IMOcgjFJAAXxw5j0R%2FYMCAWn8MgGkbkP8ALXUZI38dbyLCIvoOl85Bgfh0QIARsEiv4itlsqNPBD%2FPRPGlsupKAgUsEUtkiqMUr4UjgoJ94GkcIQdBIQJo5hCjAB3GAQm6UegAaxuyBSFmCFk86igpjAbGsmiMAAswGAoFjTiAAOqvR7UTi0DrDUBM4hqeDeHBMuZ6hCG8CacgIADaAF0jftLTbbcdjkAAA%3D&rn=6&i=1715839373&sn=0&gc=6f08a8df4ce04ff4b786855393b6d325&arp=N4IgVg%2BgdiBcIC0DuBeFIA0ICW06IFsBOKAMwEEBVAYXPIDVtMQATCAFwGc4AGLFgBa8sANzgAmLJG7xgAX2bYW%2BAGwB2IgFoVAJQDSmompUJmAJw4yAjGqsBWABwBmIlYAsPOUA&tries=1&useIP=true&r=1715839373201", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("tr_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_16");
    ns_web_url("collect_16",
        "URL=https://analytics.google.com/g/collect?v=2&tid=G-DJ3RGVFHJN&gtm=45je45f0v893318240z8889407033za200&_p=1715839203848&gcd=13l3l3l3l1&npa=0&dma=0&cid=1944455297.1715839141&ul=en-us&sr=1523x650&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&frm=0&pscdl=noapi&_eu=IA&_s=5&sid=1715839140&sct=1&seg=1&dl=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&dr=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&dt=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&uid=false&en=add_to_cart&_c=1&pr1=id304697~nmOnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)~afcroma.com~cpN%2FA~k0currency~v0N%2FA~ds0%25~lpN%2FA~brOnePlus~caPhones%20%26%20Wearables~c2Mobile%20Phones~c3Android%20Phones~c4N%2FA~c5N%2FA~liN%2FA~lnN%2FA~vaN%2FA~loN%2FA~pr39999~qt1~k1price_mrp~v139999&ep.pagetype=pdp&ep.source_page_url=%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ep.previous_page_url=%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407&ep.login_trigger=N%2FA&ep.login_status=false&ep.click_text=Add%20to%20Cart&ep.is_cross_sell=N%2FA&_et=47229&tfd=171028",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.googleadservices.com/pagead/conversion/1006316414/?random=1715839373216&cv=11&fst=1715839373216&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=aZMGCJi0kJYYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&value=0&bttype=purchase&npa=0&ec_m=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)*LI%3Atrue%3A23%3Afalse*1&ec_sel=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)&ec_meta=LI%3Atrue%3A23%3Afalse&ec_lat=5&ec_s=1&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&rfmt=3&fmt=4", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://13135721.fls.doubleclick.net/activityi;src=13135721;type=croma04;cat=croma0;ord=1832841054768;u=%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697;npa=0;auiddc=1421799651.1715839140;uaa=x86;uab=64;uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199;uamb=0;uam=;uap=Linux;uapv=5.15.0;uaw=0;pscdl=noapi;frm=0;gtm=45fe45f0z8889407033za201;gcd=13l3l3l3l1;dma=0;epver=2;~oref=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697?", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.croma.com/image/upload/f_auto,q_auto,d_Croma%20Assets:No_image.png/Croma%20Assets/UI%20Assets/lazyLoading.gif", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;s_tp;s_ppv;WZRK_S_679-6RK-976Z;s_nr30;s_sq;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://www.facebook.com/tr/?id=1369867960112522&ev=AddToCart&dl=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&rl=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&if=false&ts=1715839373184&cd[value]=39999.00&cd[currency]=INR&cd[content_ids]=%5B%22304697%22%5D&cd[content_type]=product&cd[content_name]=OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)&sw=1523&sh=650&ud[external_id]=36ce52f2c012a86c9264c805b8cff82e07250268c3b1663e92a631306623bfc6&v=2.9.156&r=stable&ec=4&o=4126&fbp=fb.1.1715839140634.281453602&ler=empty&cdl=API_unavailable&it=1715839205604&coo=false&eid=11146789180925&tm=1&rqm=GET", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("collect_16", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("floatingCart");
    ns_web_url("floatingCart",
        "URL=https://api.croma.com/useraccount/allchannels/v2/users/anonymous/carts/AN_CR-20958114759961ed485c6f419fa4d0cdfed6b9e2698801101019106380172848/floatingCart",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;s_tp;s_ppv;WZRK_S_679-6RK-976Z;s_nr30;s_sq;_ga_DJ3RGVFHJN"
    );

    ns_end_transaction("floatingCart", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("detail");
    ns_web_url("detail",
        "URL=https://api.croma.com/ew/allchannels/v7/detail?PRODUCT_CODE=304697&PRICE=39999&CATEGORYL2=95&CATEGORYL1=10&CATEGORYL0=1&BRAND_CODE=0948&STANDARD_WARRANTY_PERIOD=12",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=page-type:PDP",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;s_tp;s_ppv;WZRK_S_679-6RK-976Z;s_nr30;s_sq;_ga_DJ3RGVFHJN"
    );

    ns_end_transaction("detail", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_22");
    ns_web_url("v1_22",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=customer-hash:null",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=tdl-sso-version:4.1.15",
        "HEADER=session:false",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tL29uZXBsdXMtMTJyLTVnLThnYi0xMjhnYi1jb29sLWJsdWUtL3AvMzA0Njk3IiwiZXZlbnREYXRhIjpbeyJ0aW1lc3RhbXAiOjE3MTU4MzkzNzMyNTcsImRhdGEiOiJ7XCJldmVudFwiOlwiZ3RtLWV2ZW50XCIsXCJkYXRhXCI6XCJ7XFxcImV2ZW50XFxcIjpcXFwiYWRkX3RvX2NhcnRcXFwiLFxcXCJwYWdldHlwZVxcXCI6XFxcInBkcFxcXCIsXFxcInNvdXJjZV9wYWdlX3VybFxcXCI6XFxcIi9vbmVwbHVzLTEyci01Zy04Z2ItMTI4Z2ItY29vbC1ibHVlLS9wLzMwNDY5N1xcXCIsXFxcInByZXZpb3VzX3BhZ2VfdXJsXFxcIjpcXFwiL2NhbXBhaWduL29uZXBsdXMtci01Zy9jLzY0MDdcXFwiLFxcXCJsb2dpbl90cmlnZ2VyXFxcIjpcXFwiTi9BXFxcIixcXFwibG9naW5fc3RhdHVzXFxcIjpmYWxzZSxcXFwidXNlcl9pZFxcXCI6ZmFsc2UsXFxcImNsaWNrX3RleHRcXFwiOlxcXCJBZGQgdG8gQ2FydFxcXCIsXFxcImlzX2Nyb3NzX3NlbGxcXFwiOlxcXCJOL0FcXFwiLFxcXCJlY29tbWVyY2VcXFwiOntcXFwiaXRlbXNcXFwiOlt7XFxcIml0ZW1faWRcXFwiOlxcXCIzMDQ2OTdcXFwiLFxcXCJpdGVtX25hbWVcXFwiOlxcXCJPbmVQbHVzIDEyUiA1RyAoOEdCLCAxMjhHQiwgQ29vbCBCbHVlKVxcXCIsXFxcImFmZmlsaWF0aW9uXFxcIjpcXFwiY3JvbWEuY29tXFxcIixcXFwiY291cG9uXFxcIjpcXFwiTi9BXFxcIixcXFwiY3VycmVuY3lcXFwiOlxcXCJOL0FcXFwiLFxcXCJkaXNjb3VudFxcXCI6XFxcIjAlXFxcIixcXFwiaW5kZXhcXFwiOlxcXCJOL0FcXFwiLFxcXCJpdGVtX2JyYW5kXFxcIjpcXFwiT25lUGx1c1xcXCIsXFxcIml0ZW1fY2F0ZWdvcnlcXFwiOlxcXCJQaG9uZXMgJiBXZWFyYWJsZXNcXFwiLFxcXCJpdGVtX2NhdGVnb3J5MlxcXCI6XFxcIk1vYmlsZSBQaG9uZXNcXFwiLFxcXCJpdGVtX2NhdGVnb3J5M1xcXCI6XFxcIkFuZHJvaWQgUGhvbmVzXFxcIixcXFwiaXRlbV9jYXRlZ29yeTRcXFwiOlxcXCJOL0FcXFwiLFxcXCJpdGVtX2NhdGVnb3J5NVxcXCI6XFxcIk4vQVxcXCIsXFxcIml0ZW1fbGlzdF9pZFxcXCI6XFxcIk4vQVxcXCIsXFxcIml0ZW1fbGlzdF9uYW1lXFxcIjpcXFwiTi9BXFxcIixcXFwiaXRlbV92YXJpYW50XFxcIjpcXFwiTi9BXFxcIixcXFwibG9jYXRpb25faWRcXFwiOlxcXCJOL0FcXFwiLFxcXCJwcmljZVxcXCI6XFxcIjM5OTk5XFxcIixcXFwicXVhbnRpdHlcXFwiOlxcXCIxXFxcIixcXFwicHJpY2VfbXJwXFxcIjpcXFwiMzk5OTlcXFwifV19LFxcXCJndG0udW5pcXVlRXZlbnRJZFxcXCI6NDR9XCJ9In1dLCJjdXN0b21lckhhc2giOm51bGwsImVjaWQiOiJVbmtub3duIiwiY2xpZW50SWQiOiJDUk9NQS1XRUItQVBQIiwic2Vzc2lvbklkIjoiYjY1ZjU0YjQtNWM5My00ODJlLThjZjgtYzIxMGVjZmZhNzhmLjkyM2JiNTdmLTk1MWItNDc4MC04Y2MzLTNlN2M4OWU5MDVmZSIsImJyYW5kIjoiQ1JPTUEiLCJ0aW1lc3RhbXAiOjE3MTU4MzkzNzMyNTcsImRldmljZSI6Ik1vemlsbGEvNS4wIChYMTE7IExpbnV4IHg4Nl82NCkgQXBwbGVXZWJLaXQvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lLzExNC4wLjAuMCBTYWZhcmkvNTM3LjM2IiwiamFydmlzSWQiOiI0ZDYwNDU3MC1iYWZiLTQ1NTEtYmFkMC04NTM4OTcxYzdlZTIiLCJhZHZlcnRpc2luZ0lkIjpudWxsLCJhcHBWZXJzaW9uIjoiMCJ9"}",
        BODY_END,
        INLINE_URLS,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1706791128/Croma%20Assets/Communication/Mobiles/Images/304697_zp8rwp.png?tr=w-400", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;s_tp;s_ppv;WZRK_S_679-6RK-976Z;s_nr30;s_sq;_ga_DJ3RGVFHJN", END_INLINE,
            "URL=https://www.facebook.com/tr/?cd[content_ids]=%5B%22304697%22%5D&cd[content_name]=OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)&cd[content_type]=product&cd[currency]=INR&cd[value]=39999.00&cdl=API_unavailable&coo=false&dl=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ec=4&eid=11146789180925&ev=AddToCart&fbp=fb.1.1715839140634.281453602&id=1369867960112522&if=false&it=1715839205604&ler=empty&o=4126&r=stable&redirect=0&rl=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&rqm=GET&sh=650&sw=1523&tm=1&ts=1715839373184&ud[external_id]=36ce52f2c012a86c9264c805b8cff82e07250268c3b1663e92a631306623bfc6&v=2.9.156", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("v1_22", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("image_details");
    ns_web_url("image_details",
        "URL=https://api.croma.com/ew/allchannels/v2/product/image-details",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;s_tp;s_ppv;WZRK_S_679-6RK-976Z;s_nr30;s_sq;_ga_DJ3RGVFHJN",
        BODY_BEGIN,
            "{"itemDetails":[{"code":"277090"}]}",
        BODY_END
    );

    ns_end_transaction("image_details", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_23");
    ns_web_url("v1_23",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=customer-hash:null",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=tdl-sso-version:4.1.15",
        "HEADER=session:false",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tL29uZXBsdXMtMTJyLTVnLThnYi0xMjhnYi1jb29sLWJsdWUtL3AvMzA0Njk3IiwiZXZlbnREYXRhIjpbeyJ0aW1lc3RhbXAiOjE3MTU4MzkzNzMzOTcsImRhdGEiOiJ7XCJldmVudFwiOlwicmVxdWVzdFwiLFwicGF5bG9hZFwiOntcInJlcXVlc3RIZWFkZXJzXCI6e1wiQWNjZXB0XCI6XCJhcHBsaWNhdGlvbi9qc29uLCB0ZXh0L3BsYWluLCAqLypcIixcInBhZ2UtdHlwZVwiOlwiUERQXCIsXCJjbGllbnRfaWRcIjpcIkNST01BLVdFQi1BUFBcIn0sXCJyZXF1ZXN0VHlwZVwiOlwiR0VUXCIsXCJyZXF1ZXN0VXJsXCI6XCJodHRwczovL2FwaS5jcm9tYS5jb20vZXcvYWxsY2hhbm5lbHMvdjcvZGV0YWlsP1BST0RVQ1RfQ09ERT0zMDQ2OTcmUFJJQ0U9Mzk5OTkmQ0FURUdPUllMMj05NSZDQVRFR09SWUwxPTEwJkNBVEVHT1JZTDA9MSZCUkFORF9DT0RFPTA5NDgmU1RBTkRBUkRfV0FSUkFOVFlfUEVSSU9EPTEyXCIsXCJyZXNwb25zZVwiOlwie1xcXCJTRVJWSUNFU1xcXCI6e1xcXCJFWFRFTkRFRF9XQVJSQU5USUVTXFxcIjpbe1xcXCJDT0RFXFxcIjpcXFwiMjc3MDkwXFxcIixcXFwiTVJQXFxcIjoxNTA5LFxcXCJOQU1FXFxcIjpcXFwiMSBZZWFyIFppcENhcmUgUHJvdGVjdCAtIFN0YW5kYXJkIGZvciBTbWFydCBQaG9uZXNcXFwiLFxcXCJSRUZFUkVOQ0VfVFlQRVxcXCI6XFxcIlpJUENBUkVfRVdfTElURV9WMVxcXCIsXFxcIlNFTExJTkdfUFJJQ0VcXFwiOjE1MDksXFxcIkNPVkVSQUdFX1BFUklPRFxcXCI6MTIsXFxcIlBSSUNFX1BFUl9NT05USFxcXCI6MTI2LFxcXCJNSU5fQ09WRVJBR0VfUFJJQ0VcXFwiOjMwMDAwLFxcXCJNQVhfQ09WRVJBR0VfUFJJQ0VcXFwiOjQwMDAwfV0sXFxcIlNQSUxMU19BTkRfRFJPUFNcXFwiOlt7XFxcIkNPREVcXFwiOlxcXCIyNzYyMTlcXFwiLFxcXCJNUlBcXFwiOjE4NTksXFxcIk5BTUVcXFwiOlxcXCIxIFllYXIgWmlwQ2FyZSBQcm90ZWN0IC0gRGFtYWdlIFN0YW5kYXJkIGZvciBTbWFydCBQaG9uZXMgKE9zIEJhc2VkKVxcXCIsXFxcIlJFRkVSRU5DRV9UWVBFXFxcIjpcXFwiWklQQ0FSRV9QRFNfVjFcXFwiLFxcXCJTRUxMSU5HX1BSSUNFXFxcIjoxODU5LFxcXCJDT1ZFUkFHRV9QRVJJT0RcXFwiOjEyLFxcXCJQUklDRV9QRVJfTU9OVEhcXFwiOjE1NSxcXFwiTUlOX0NPVkVSQUdFX1BSSUNFXFxcIjozMDAwMCxcXFwiTUFYX0NPVkVSQUdFX1BSSUNFXFxcIjo0MDAwMH1dLFxcXCJNSU5fUFJJQ0VTXFxcIjp7XFxcIlpJUENBUkVfRVdfTElURV9WMVxcXCI6MTI2LFxcXCJaSVBDQVJFX1BEU19WMVxcXCI6MTU1fSxcXFwiU09VUkNFX1BST0RVQ1RfQ09ERVxcXCI6XFxcIjMwNDY5N1xcXCJ9fVwiLFwic3RhdHVzXCI6MjAwfX0ifV0sImN1c3RvbWVySGFzaCI6bnVsbCwiZWNpZCI6IlVua25vd24iLCJjbGllbnRJZCI6IkNST01BLVdFQi1BUFAiLCJzZXNzaW9uSWQiOiJiNjVmNTRiNC01YzkzLTQ4MmUtOGNmOC1jMjEwZWNmZmE3OGYuOTIzYmI1N2YtOTUxYi00NzgwLThjYzMtM2U3Yzg5ZTkwNWZlIiwiYnJhbmQiOiJDUk9NQSIsInRpbWVzdGFtcCI6MTcxNTgzOTM3MzM5NywiZGV2aWNlIjoiTW96aWxsYS81LjAgKFgxMTsgTGludXggeDg2XzY0KSBBcHBsZVdlYktpdC81MzcuMzYgKEtIVE1MLCBsaWtlIEdlY2tvKSBDaHJvbWUvMTE0LjAuMC4wIFNhZmFyaS81MzcuMzYiLCJqYXJ2aXNJZCI6IjRkNjA0NTcwLWJhZmItNDU1MS1iYWQwLTg1Mzg5NzFjN2VlMiIsImFkdmVydGlzaW5nSWQiOm51bGwsImFwcFZlcnNpb24iOiIwIn0="}",
        BODY_END
    );

    ns_end_transaction("v1_23", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_24");
    ns_web_url("v1_24",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=customer-hash:null",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=tdl-sso-version:4.1.15",
        "HEADER=session:false",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tL29uZXBsdXMtMTJyLTVnLThnYi0xMjhnYi1jb29sLWJsdWUtL3AvMzA0Njk3IiwiZXZlbnREYXRhIjpbeyJ0aW1lc3RhbXAiOjE3MTU4MzkzNzM0MTIsImRhdGEiOiJ7XCJldmVudFwiOlwicmVxdWVzdFwiLFwicGF5bG9hZFwiOntcInJlcXVlc3RIZWFkZXJzXCI6e1wiQWNjZXB0XCI6XCJhcHBsaWNhdGlvbi9qc29uLCB0ZXh0L3BsYWluLCAqLypcIn0sXCJyZXF1ZXN0VHlwZVwiOlwiR0VUXCIsXCJyZXF1ZXN0VXJsXCI6XCJodHRwczovL2FwaS5jcm9tYS5jb20vdXNlcmFjY291bnQvYWxsY2hhbm5lbHMvdjIvdXNlcnMvYW5vbnltb3VzL2NhcnRzL0FOX0NSLTIwOTU4MTE0NzU5OTYxZWQ0ODVjNmY0MTlmYTRkMGNkZmVkNmI5ZTI2OTg4MDExMDEwMTkxMDYzODAxNzI4NDgvZmxvYXRpbmdDYXJ0XCIsXCJyZXNwb25zZVwiOlwie1xcXCJwcm9kdWN0TGlzdFxcXCI6W3tcXFwicHJvZHVjdElkXFxcIjpcXFwiMzA0Njk3XFxcIixcXFwicHJvZHVjdERpc3BsYXlQcmljZUZvcm1hdHRlZFZhbHVlXFxcIjpcXFwi4oK5MzksOTk5LjAwXFxcIixcXFwicXVhbnRpdHlcXFwiOlxcXCIxXFxcIn1dLFxcXCJ0b3RhbEl0ZW1zXFxcIjpcXFwiMVxcXCIsXFxcInRvdGFsVW5pdENvdW50XFxcIjpcXFwiMVxcXCIsXFxcInRvdGFsUHJpY2VGb3JtYXR0ZWRWYWx1ZVxcXCI6XFxcIuKCuTM5LDk5OS4wMFxcXCJ9XCIsXCJzdGF0dXNcIjoyMDB9fSJ9XSwiY3VzdG9tZXJIYXNoIjpudWxsLCJlY2lkIjoiVW5rbm93biIsImNsaWVudElkIjoiQ1JPTUEtV0VCLUFQUCIsInNlc3Npb25JZCI6ImI2NWY1NGI0LTVjOTMtNDgyZS04Y2Y4LWMyMTBlY2ZmYTc4Zi45MjNiYjU3Zi05NTFiLTQ3ODAtOGNjMy0zZTdjODllOTA1ZmUiLCJicmFuZCI6IkNST01BIiwidGltZXN0YW1wIjoxNzE1ODM5MzczNDEyLCJkZXZpY2UiOiJNb3ppbGxhLzUuMCAoWDExOyBMaW51eCB4ODZfNjQpIEFwcGxlV2ViS2l0LzUzNy4zNiAoS0hUTUwsIGxpa2UgR2Vja28pIENocm9tZS8xMTQuMC4wLjAgU2FmYXJpLzUzNy4zNiIsImphcnZpc0lkIjoiNGQ2MDQ1NzAtYmFmYi00NTUxLWJhZDAtODUzODk3MWM3ZWUyIiwiYWR2ZXJ0aXNpbmdJZCI6bnVsbCwiYXBwVmVyc2lvbiI6IjAifQ=="}",
        BODY_END,
        INLINE_URLS,
            "URL=https://13135721.fls.doubleclick.net/activityi;dc_pre=COqb5OK_kYYDFcXyTAIdHEUMUA;src=13135721;type=croma04;cat=croma0;ord=1832841054768;u=%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697;npa=0;auiddc=1421799651.1715839140;uaa=x86;uab=64;uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199;uamb=0;uam=;uap=Linux;uapv=5.15.0;uaw=0;pscdl=noapi;frm=0;gtm=45fe45f0z8889407033za201;gcd=13l3l3l3l1;dma=0;epver=2;~oref=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697?", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1006316414/?random=320707954&cv=11&fst=1715839373216&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=aZMGCJi0kJYYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&value=0&npa=0&ec_m=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)*LI%3Atrue%3A23%3Afalse*1&ec_sel=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)&ec_meta=LI%3Atrue%3A23%3Afalse&ec_lat=5&ec_s=1&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtWxAi8CHlrzhfD_9rZv4eogFFMpe8Xx9KA&pscrd=IhMIoOzh4r-RhgMVbGCdCR2siAhKMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1695738016/Croma%20Assets/Extended%20Warranty/Images/277090_cnfhcv.png?tr=w-400", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;s_tp;s_ppv;s_nr30;s_sq;_ga_DJ3RGVFHJN;WZRK_S_679-6RK-976Z", END_INLINE,
            "URL=https://c.bing.com/c.gif?Red3=CTOMS_pd&cbid=k-0tyrVmhJLxG-nAKY3h_cmH_uE4L3d_G-DpKVpINPhVSXxwS3", END_INLINE,
            "URL=https://www.google.com/pagead/1p-conversion/1006316414/?random=320707954&cv=11&fst=1715839373216&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=aZMGCJi0kJYYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&value=0&npa=0&ec_m=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)*LI%3Atrue%3A23%3Afalse*1&ec_sel=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)&ec_meta=LI%3Atrue%3A23%3Afalse&ec_lat=5&ec_s=1&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMIoOzh4r-RhgMVbGCdCR2siAhKMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtqG46_qI7Njk-b9znYwGPUCC-r_TrQPw&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtb7nuSezkxjv9QeKLAxB7WvpUaCue6ynZQ&random=1511386552", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("v1_24", NS_AUTO_STATUS);
    ns_page_think_time(0);

    ns_start_transaction("cart");
    ns_web_url("cart",
        "URL=https://adservice.google.com/ddm/fls/z/dc_pre=COqb5OK_kYYDFcXyTAIdHEUMUA;src=13135721;type=croma04;cat=croma0;ord=1832841054768;u=%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697;npa=0;auiddc=*;uaa=x86;uab=64;uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199;uamb=0;uam=;uap=Linux;uapv=5.15.0;uaw=0;pscdl=noapi;frm=0;gtm=45fe45f0z8889407033za201;gcd=13l3l3l3l1;dma=0;epver=2;~oref=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:image",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.google.co.in/pagead/1p-conversion/1006316414/?random=320707954&cv=11&fst=1715839373216&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=aZMGCJi0kJYYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&value=0&npa=0&ec_m=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)*LI%3Atrue%3A23%3Afalse*1&ec_sel=%23specification_container%3E%3Anth-child(23)%3E%3Anth-child(2)%3E%3Anth-child(2)%3E%3Anth-child(1)%3E%3Anth-child(2)&ec_meta=LI%3Atrue%3A23%3Afalse&ec_lat=5&ec_s=1&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMIoOzh4r-RhgMVbGCdCR2siAhKMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtqG46_qI7Njk-b9znYwGPUCC-r_TrQPw&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtb7nuSezkxjv9QeKLAxB7WvpUaCue6ynZQ&random=1511386552&ipr=y", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("cart", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_25");
    ns_web_url("v1_25",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tL29uZXBsdXMtMTJyLTVnLThnYi0xMjhnYi1jb29sLWJsdWUtL3AvMzA0Njk3IiwiZXZlbnREYXRhIjpbeyJ0aW1lc3RhbXAiOjE3MTU4MzkzNzU1MzIsImRhdGEiOiJ7XG4gIFwidXJsXCI6IHtcbiAgICBcImFuY2VzdG9yT3JpZ2luc1wiOiB7fSxcbiAgICBcImhyZWZcIjogXCJodHRwczovL3d3dy5jcm9tYS5jb20vb25lcGx1cy0xMnItNWctOGdiLTEyOGdiLWNvb2wtYmx1ZS0vcC8zMDQ2OTdcIixcbiAgICBcIm9yaWdpblwiOiBcImh0dHBzOi8vd3d3LmNyb21hLmNvbVwiLFxuICAgIFwicHJvdG9jb2xcIjogXCJodHRwczpcIixcbiAgICBcImhvc3RcIjogXCJ3d3cuY3JvbWEuY29tXCIsXG4gICAgXCJob3N0bmFtZVwiOiBcInd3dy5jcm9tYS5jb21cIixcbiAgICBcInBvcnRcIjogXCJcIixcbiAgICBcInBhdGhuYW1lXCI6IFwiL29uZXBsdXMtMTJyLTVnLThnYi0xMjhnYi1jb29sLWJsdWUtL3AvMzA0Njk3XCIsXG4gICAgXCJzZWFyY2hcIjogXCJcIixcbiAgICBcImhhc2hcIjogXCJcIlxuICB9LFxuICBcInBhdGhcIjogW1xuICAgIHtcbiAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJidXR0b25cIixcbiAgICAgIFwiaWRcIjogXCJcIixcbiAgICAgIFwiY2xhc3NcIjogXCJidG4gYnRuLWRlZmF1bHQgcHJvY2VlZC10b2NhcnRcIlxuICAgIH0sXG4gICAge1xuICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImRpdlwiLFxuICAgICAgXCJpZFwiOiBcIlwiLFxuICAgICAgXCJjbGFzc1wiOiBcInBkLWFjdGlvblwiXG4gICAgfSxcbiAgICB7XG4gICAgICBcImVsZW1lbnRUeXBlXCI6IFwiZGl2XCIsXG4gICAgICBcImlkXCI6IFwiXCIsXG4gICAgICBcImNsYXNzXCI6IFwiYWN0aW9uLXdyYXBcIlxuICAgIH0sXG4gICAge1xuICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImRpdlwiLFxuICAgICAgXCJpZFwiOiBcIlwiLFxuICAgICAgXCJjbGFzc1wiOiBcIm1pbmktY2FydC1idXR0b24tY29udGFpbmVyLWZpbmFsXCJcbiAgICB9LFxuICAgIHtcbiAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJkaXZcIixcbiAgICAgIFwiaWRcIjogXCJcIixcbiAgICAgIFwiY2xhc3NcIjogXCJtaW5pLWNhcnQtc3R5bGVcIlxuICAgIH0sXG4gICAge1xuICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImRpdlwiLFxuICAgICAgXCJpZFwiOiBcIlwiLFxuICAgICAgXCJjbGFzc1wiOiBcIm1pbmktY2FydC1wcm9kdWN0LWNvbnRhaW5lclwiXG4gICAgfSxcbiAgICB7XG4gICAgICBcImVsZW1lbnRUeXBlXCI6IFwiZGl2XCIsXG4gICAgICBcImlkXCI6IFwiXCIsXG4gICAgICBcImNsYXNzXCI6IFwiTXVpRGlhbG9nQ29udGVudC1yb290XCJcbiAgICB9LFxuICAgIHtcbiAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJkaXZcIixcbiAgICAgIFwiaWRcIjogXCJcIixcbiAgICAgIFwiY2xhc3NcIjogXCJNdWlQYXBlci1yb290IE11aURpYWxvZy1wYXBlciBkYXJrLXRoZW1lIG1pbmktY2FydC1kYWlsb2cgTXVpRGlhbG9nLXBhcGVyU2Nyb2xsUGFwZXIgTXVpRGlhbG9nLXBhcGVyV2lkdGhTbSBNdWlQYXBlci1lbGV2YXRpb24yNCBNdWlQYXBlci1yb3VuZGVkXCJcbiAgICB9LFxuICAgIHtcbiAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJkaXZcIixcbiAgICAgIFwiaWRcIjogXCJcIixcbiAgICAgIFwiY2xhc3NcIjogXCJNdWlEaWFsb2ctY29udGFpbmVyIE11aURpYWxvZy1zY3JvbGxQYXBlclwiXG4gICAgfSxcbiAgICB7XG4gICAgICBcImVsZW1lbnRUeXBlXCI6IFwiZGl2XCIsXG4gICAgICBcImlkXCI6IFwiXCIsXG4gICAgICBcImNsYXNzXCI6IFwiTXVpRGlhbG9nLXJvb3RcIlxuICAgIH1cbiAgXSxcbiAgXCJ0YXJnZXRcIjogXCI8YnV0dG9uIHR5cGU9XFxcImJ1dHRvblxcXCIgY2xhc3M9XFxcImJ0biBidG4tZGVmYXVsdCBwcm9jZWVkLXRvY2FydFxcXCI+UHJvY2VlZCB0byBDYXJ0PC9idXR0b24+XCIsXG4gIFwidGltZVN0YW1wXCI6IDE3MTU4MzkzNzU1MzIsXG4gIFwidHlwZVwiOiBcImNsaWNrXCJcbn0ifV0sImN1c3RvbWVySGFzaCI6bnVsbCwiZWNpZCI6IlVua25vd24iLCJjbGllbnRJZCI6IkNST01BLVdFQi1BUFAiLCJzZXNzaW9uSWQiOiJiNjVmNTRiNC01YzkzLTQ4MmUtOGNmOC1jMjEwZWNmZmE3OGYuOTIzYmI1N2YtOTUxYi00NzgwLThjYzMtM2U3Yzg5ZTkwNWZlIiwiYnJhbmQiOiJDUk9NQSIsInRpbWVzdGFtcCI6MTcxNTgzOTM3NTUzMywiZGV2aWNlIjoiTW96aWxsYS81LjAgKFgxMTsgTGludXggeDg2XzY0KSBBcHBsZVdlYktpdC81MzcuMzYgKEtIVE1MLCBsaWtlIEdlY2tvKSBDaHJvbWUvMTE0LjAuMC4wIFNhZmFyaS81MzcuMzYiLCJqYXJ2aXNJZCI6IjRkNjA0NTcwLWJhZmItNDU1MS1iYWQwLTg1Mzg5NzFjN2VlMiIsImFkdmVydGlzaW5nSWQiOm51bGwsImFwcFZlcnNpb24iOiIwIn0="}",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.facebook.com/tr/?id=1369867960112522&ev=SubscribedButtonClick&dl=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&rl=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&if=false&ts=1715839375539&cd[buttonFeatures]=%7B%22classList%22%3A%22btn%20btn-default%20proceed-tocart%22%2C%22destination%22%3A%22%22%2C%22id%22%3A%22%22%2C%22imageUrl%22%3A%22%22%2C%22innerText%22%3A%22Proceed%20to%20Cart%22%2C%22numChildButtons%22%3A0%2C%22tag%22%3A%22button%22%2C%22type%22%3A%22button%22%2C%22name%22%3A%22%22%2C%22value%22%3A%22%22%7D&cd[buttonText]=Proceed%20to%20Cart&cd[formFeatures]=%5B%5D&cd[pageFeatures]=%7B%22title%22%3A%22Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!%22%7D&sw=1523&sh=650&ud[external_id]=36ce52f2c012a86c9264c805b8cff82e07250268c3b1663e92a631306623bfc6&v=2.9.156&r=stable&ec=5&o=4126&fbp=fb.1.1715839140634.281453602&ler=empty&cdl=API_unavailable&it=1715839205604&coo=false&es=automatic&tm=3&rqm=GET", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.croma.com/cart", END_INLINE
    );

    ns_end_transaction("v1_25", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_26");
    ns_web_url("v1_26",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=customer-hash:null",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=tdl-sso-version:4.1.15",
        "HEADER=session:false",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tL2NhcnQiLCJldmVudERhdGEiOlt7InRpbWVzdGFtcCI6MTcxNTgzOTM3NTU2NiwiZGF0YSI6IntcImV2ZW50XCI6XCJwYWdlTG9hZFwiLFwicGF5bG9hZFwiOntcInVybFwiOlwiaHR0cHM6Ly93d3cuY3JvbWEuY29tL2NhcnRcIixcImRhdGFTb3VyY2VcIjpcInB1c2hTdGF0ZVwifX0ifV0sImN1c3RvbWVySGFzaCI6bnVsbCwiZWNpZCI6IlVua25vd24iLCJjbGllbnRJZCI6IkNST01BLVdFQi1BUFAiLCJzZXNzaW9uSWQiOiJiNjVmNTRiNC01YzkzLTQ4MmUtOGNmOC1jMjEwZWNmZmE3OGYuOTIzYmI1N2YtOTUxYi00NzgwLThjYzMtM2U3Yzg5ZTkwNWZlIiwiYnJhbmQiOiJDUk9NQSIsInRpbWVzdGFtcCI6MTcxNTgzOTM3NTU2NiwiZGV2aWNlIjoiTW96aWxsYS81LjAgKFgxMTsgTGludXggeDg2XzY0KSBBcHBsZVdlYktpdC81MzcuMzYgKEtIVE1MLCBsaWtlIEdlY2tvKSBDaHJvbWUvMTE0LjAuMC4wIFNhZmFyaS81MzcuMzYiLCJqYXJ2aXNJZCI6IjRkNjA0NTcwLWJhZmItNDU1MS1iYWQwLTg1Mzg5NzFjN2VlMiIsImFkdmVydGlzaW5nSWQiOm51bGwsImFwcFZlcnNpb24iOiIwIn0="}",
        BODY_END
    );

    ns_end_transaction("v1_26", NS_AUTO_STATUS);
    ns_page_think_time(0.025);

    //Page Auto split for Button '' Clicked by User
    ns_start_transaction("X_0de94fe2_chunk_js");
    ns_web_url("X_0de94fe2_chunk_js",
        "URL=https://www.croma.com/static/js/3.0de94fe2.chunk.js",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:script",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;RT;s_ips;s_stop14;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;s_tp;s_ppv;s_test;s_test14;s_stop;_ga_DJ3RGVFHJN;WZRK_S_679-6RK-976Z;s_nr30;s_sq",
        INLINE_URLS,
            "URL=https://www.croma.com/static/css/7.283647d5.chunk.css", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;RT;s_ips;s_stop14;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;s_tp;s_ppv;s_test;s_test14;s_stop;_ga_DJ3RGVFHJN;WZRK_S_679-6RK-976Z;s_nr30;s_sq", END_INLINE,
            "URL=https://www.croma.com/static/js/7.e1dc87f9.chunk.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;RT;s_ips;s_stop14;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;s_tp;s_ppv;s_test;s_test14;s_stop;_ga_DJ3RGVFHJN;WZRK_S_679-6RK-976Z;s_nr30;s_sq", END_INLINE,
            "URL=https://www.croma.com/static/css/13.683aabc8.chunk.css", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;RT;s_ips;s_stop14;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;s_tp;s_ppv;s_test;s_test14;s_stop;_ga_DJ3RGVFHJN;WZRK_S_679-6RK-976Z;s_nr30;s_sq", END_INLINE,
            "URL=https://www.croma.com/static/js/13.374408f8.chunk.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;RT;s_ips;s_stop14;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;s_tp;s_ppv;s_test;s_test14;s_stop;_ga_DJ3RGVFHJN;WZRK_S_679-6RK-976Z;s_nr30;s_sq", END_INLINE,
            "URL=https://www.croma.com/static/css/21.9d17125e.chunk.css", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;RT;s_ips;s_stop14;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;s_tp;s_ppv;s_test;s_test14;s_stop;_ga_DJ3RGVFHJN;WZRK_S_679-6RK-976Z;s_nr30;s_sq", END_INLINE,
            "URL=https://www.croma.com/static/js/21.eefe35b7.chunk.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;RT;s_ips;s_stop14;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;s_tp;s_ppv;s_test;s_test14;s_stop;_ga_DJ3RGVFHJN;WZRK_S_679-6RK-976Z;s_nr30;s_sq", END_INLINE
    );

    ns_end_transaction("X_0de94fe2_chunk_js", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_27");
    ns_web_url("v1_27",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=customer-hash:null",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=tdl-sso-version:4.1.15",
        "HEADER=session:false",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tL2NhcnQiLCJldmVudERhdGEiOlt7InRpbWVzdGFtcCI6MTcxNTgzOTM3NjU1MiwiZGF0YSI6IntcImV2ZW50XCI6XCJyZXF1ZXN0XCIsXCJwYXlsb2FkXCI6e1wicmVxdWVzdEhlYWRlcnNcIjp7XCJBY2NlcHRcIjpcImFwcGxpY2F0aW9uL2pzb24sIHRleHQvcGxhaW4sICovKlwiLFwiQ29udGVudC1UeXBlXCI6XCJhcHBsaWNhdGlvbi9qc29uXCIsXCJvbXMtYXBpbS1zdWJzY3JpcHRpb24ta2V5XCI6XCIxMTMxODU4MTQxNjM0ZTJhYmUyZWZiMmIzYTJhMmE1ZFwiLFwiZXZlbnRzU2VxdWVuY2VJZFwiOlwiXCJ9LFwicmVxdWVzdFR5cGVcIjpcIlBPU1RcIixcInJlcXVlc3RVcmxcIjpcImh0dHBzOi8vYXBpLmNyb21hLmNvbS9pbnZlbnRvcnkvb21zL3YyL3Rtcy9kZXRhaWxzLXB3YS9cIixcInJlcXVlc3RCb2R5XCI6XCJ7XFxcInByb21pc2VcXFwiOntcXFwiYWxsb2NhdGlvblJ1bGVJRFxcXCI6XFxcIlNZU1RFTVxcXCIsXFxcImNoZWNrSW52ZW50b3J5XFxcIjpcXFwiWVxcXCIsXFxcIm9yZ2FuaXphdGlvbkNvZGVcXFwiOlxcXCJDUk9NQVxcXCIsXFxcInNvdXJjaW5nQ2xhc3NpZmljYXRpb25cXFwiOlxcXCJFQ1xcXCIsXFxcInByb21pc2VMaW5lc1xcXCI6e1xcXCJwcm9taXNlTGluZVxcXCI6W3tcXFwiZnVsZmlsbG1lbnRUeXBlXFxcIjpcXFwiSERFTFxcXCIsXFxcIm1jaFxcXCI6XFxcIlxcXCIsXFxcIml0ZW1JRFxcXCI6XFxcIjMwNDY5N1xcXCIsXFxcImxpbmVJZFxcXCI6XFxcIjFcXFwiLFxcXCJyZUVuZERhdGVcXFwiOlxcXCIyNTAwLTAxLTAxXFxcIixcXFwicmVxU3RhcnREYXRlXFxcIjpcXFwiXFxcIixcXFwicmVxdWlyZWRRdHlcXFwiOlxcXCIxXFxcIixcXFwic2hpcFRvQWRkcmVzc1xcXCI6e1xcXCJjb21wYW55XFxcIjpcXFwiXFxcIixcXFwiY291bnRyeVxcXCI6XFxcIlxcXCIsXFxcImNpdHlcXFwiOlxcXCJcXFwiLFxcXCJtb2JpbGVQaG9uZVxcXCI6XFxcIlxcXCIsXFxcInN0YXRlXFxcIjpcXFwiXFxcIixcXFwiemlwQ29kZVxcXCI6XFxcIjQwMDA0OVxcXCIsXFxcImV4dG5cXFwiOntcXFwiaXJsQWRkcmVzc0xpbmUxXFxcIjpcXFwiXFxcIixcXFwiaXJsQWRkcmVzc0xpbmUyXFxcIjpcXFwiXFxcIn19LFxcXCJleHRuXFxcIjp7XFxcIndpZGVyU3RvcmVGbGFnXFxcIjpcXFwiTlxcXCJ9fSx7XFxcImZ1bGZpbGxtZW50VHlwZVxcXCI6XFxcIlNUT1JcXFwiLFxcXCJtY2hcXFwiOlxcXCJcXFwiLFxcXCJpdGVtSURcXFwiOlxcXCIzMDQ2OTdcXFwiLFxcXCJsaW5lSWRcXFwiOlxcXCIyXFxcIixcXFwicmVFbmREYXRlXFxcIjpcXFwiMjUwMC0wMS0wMVxcXCIsXFxcInJlcVN0YXJ0RGF0ZVxcXCI6XFxcIlxcXCIsXFxcInJlcXVpcmVkUXR5XFxcIjpcXFwiMVxcXCIsXFxcInNoaXBUb0FkZHJlc3NcXFwiOntcXFwiY29tcGFueVxcXCI6XFxcIlxcXCIsXFxcImNvdW50cnlcXFwiOlxcXCJcXFwiLFxcXCJjaXR5XFxcIjpcXFwiXFxcIixcXFwibW9iaWxlUGhvbmVcXFwiOlxcXCJcXFwiLFxcXCJzdGF0ZVxcXCI6XFxcIlxcXCIsXFxcInppcENvZGVcXFwiOlxcXCI0MDAwNDlcXFwiLFxcXCJleHRuXFxcIjp7XFxcImlybEFkZHJlc3NMaW5lMVxcXCI6XFxcIlxcXCIsXFxcImlybEFkZHJlc3NMaW5lMlxcXCI6XFxcIlxcXCJ9fSxcXFwiZXh0blxcXCI6e1xcXCJ3aWRlclN0b3JlRmxhZ1xcXCI6XFxcIk5cXFwifX0se1xcXCJmdWxmaWxsbWVudFR5cGVcXFwiOlxcXCJTREVMXFxcIixcXFwibWNoXFxcIjpcXFwiXFxcIixcXFwiaXRlbUlEXFxcIjpcXFwiMzA0Njk3XFxcIixcXFwibGluZUlkXFxcIjpcXFwiM1xcXCIsXFxcInJlRW5kRGF0ZVxcXCI6XFxcIjI1MDAtMDEtMDFcXFwiLFxcXCJyZXFTdGFydERhdGVcXFwiOlxcXCJcXFwiLFxcXCJyZXF1aXJlZFF0eVxcXCI6XFxcIjFcXFwiLFxcXCJzaGlwVG9BZGRyZXNzXFxcIjp7XFxcImNvbXBhbnlcXFwiOlxcXCJcXFwiLFxcXCJjb3VudHJ5XFxcIjpcXFwiXFxcIixcXFwiY2l0eVxcXCI6XFxcIlxcXCIsXFxcIm1vYmlsZVBob25lXFxcIjpcXFwiXFxcIixcXFwic3RhdGVcXFwiOlxcXCJcXFwiLFxcXCJ6aXBDb2RlXFxcIjpcXFwiNDAwMDQ5XFxcIixcXFwiZXh0blxcXCI6e1xcXCJpcmxBZGRyZXNzTGluZTFcXFwiOlxcXCJcXFwiLFxcXCJpcmxBZGRyZXNzTGluZTJcXFwiOlxcXCJcXFwifX0sXFxcImV4dG5cXFwiOntcXFwid2lkZXJTdG9yZUZsYWdcXFwiOlxcXCJOXFxcIn19XX19fVwiLFwicmVzcG9uc2VcIjpcIntcXHJcXG4gIFxcXCJwcm9taXNlXFxcIjoge1xcclxcbiAgICBcXFwic3VnZ2VzdGVkT3B0aW9uXFxcIjoge1xcclxcbiAgICAgIFxcXCJvcHRpb25cXFwiOiB7XFxyXFxuICAgICAgICBcXFwicHJvbWlzZUxpbmVzXFxcIjoge1xcclxcbiAgICAgICAgICBcXFwicHJvbWlzZUxpbmVcXFwiOiBbXFxyXFxuICAgICAgICAgICAge1xcclxcbiAgICAgICAgICAgICAgXFxcInNvdXJjaW5nQ2xhc3NpZmljYXRpb25cXFwiOiBcXFwiRUNcXFwiLFxcclxcbiAgICAgICAgICAgICAgXFxcImxpbmVJZFxcXCI6IFxcXCIxXFxcIixcXHJcXG4gICAgICAgICAgICAgIFxcXCJpdGVtSURcXFwiOiBcXFwiMzA0Njk3XFxcIixcXHJcXG4gICAgICAgICAgICAgIFxcXCJyZXF1aXJlZFF0eVxcXCI6IFxcXCIxXFxcIixcXHJcXG4gICAgICAgICAgICAgIFxcXCJyZXF1aXJlZENhcGFjaXR5XFxcIjogXFxcIjFcXFwiLFxcclxcbiAgICAgICAgICAgICAgXFxcImV4dGVybmFsUmVmZXJlbmNlSWRcXFwiOiBcXFwiMTI3MzlcXFwiLFxcclxcbiAgICAgICAgICAgICAgXFxcIm9yZGVyVHlwZVxcXCI6IFxcXCJub3JtYWxcXFwiLFxcclxcbiAgICAgICAgICAgICAgXFxcInVuaXRPZk1lYXN1cmVcXFwiOiBcXFwiRUFcXFwiLFxcclxcbiAgICAgICAgICAgICAgXFxcImNhcnJpZXJTZXJ2aWNlQ29kZVxcXCI6IFxcXCJCbHVlRGFydCAtIDcgRGF5IEZyaWVnaHRcXFwiLFxcclxcbiAgICAgICAgICAgICAgXFxcInNjYWNcXFwiOiBcXFwiQmx1ZURhcnRcXFwiLFxcclxcbiAgICAgICAgICAgICAgXFxcInppcEVuYWJsZWRJblRNU1xcXCI6IHRydWUsXFxyXFxuICAgICAgICAgICAgICBcXFwiaXNUbXNFbmFibGVcXFwiOiB0cnVlLFxcclxcbiAgICAgICAgICAgICAgXFxcImZ1bGZpbGxtZW50VHlwZVxcXCI6IFxcXCJIREVMXFxcIixcXHJcXG4gICAgICAgICAgICAgIFxcXCJhc3NpZ25tZW50c1xcXCI6IHtcXHJcXG4gICAgICAgICAgICAgICAgXFxcInRvdGFsTnVtYmVyT2ZSZWNvcmRzXFxcIjogXFxcIjFcXFwiLFxcclxcbiAgICAgICAgICAgICAgICBcXFwiYXNzaWdubWVudFxcXCI6IFtcXHJcXG4gICAgICAgICAgICAgICAgICB7XFxyXFxuICAgICAgICAgICAgICAgICAgICBcXFwicXVhbnRpdHlcXFwiOiBcXFwiMVxcXCIsXFxyXFxuICAgICAgICAgICAgICAgICAgICBcXFwic2hpcE5vZGVcXFwiOiBcXFwiRDA2MVxcXCIsXFxyXFxuICAgICAgICAgICAgICAgICAgICBcXFwibG1kRmNJZFxcXCI6IFxcXCJYMDAzLUdPUkVHQU9OXFxcIixcXHJcXG4gICAgICAgICAgICAgICAgICAgIFxcXCJvZmZzZXRcXFwiOiBcXFwiNzIwXFxcIixcXHJcXG4gICAgICAgICAgICAgICAgICAgIFxcXCJkZWxpdmVyeURhdGVcXFwiOiBcXFwiMjAyNC0wNS0xOFQwODowMjo1Ni40OTErMDA6MDBcXFwiLFxcclxcbiAgICAgICAgICAgICAgICAgICAgXFxcInppcENvZGVcXFwiOiBcXFwiNDAwMDQ5XFxcIixcXHJcXG4gICAgICAgICAgICAgICAgICAgIFxcXCJwcm9kdWN0QXZhaWxEYXRlXFxcIjogXFxcIjIwMjQtMDUtMTZUMTE6MzI6NTYuNDkxXFxcIixcXHJcXG4gICAgICAgICAgICAgICAgICAgIFxcXCJwb2RJbmRpY2F0b3JcXFwiOiBcXFwiTlxcXCIsXFxyXFxuICAgICAgICAgICAgICAgICAgICBcXFwiaXNUcmFkZWluSW5kaWNhdG9yXFxcIjogXFxcIk5cXFwiLFxcclxcbiAgICAgICAgICAgICAgICAgICAgXFxcImRlbGl2ZXJ5RGF0ZXNcXFwiOiBbXVxcclxcbiAgICAgICAgICAgICAgICAgIH1cXHJcXG4gICAgICAgICAgICAgICAgXVxcclxcbiAgICAgICAgICAgICAgfSxcXHJcXG4gICAgICAgICAgICAgIFxcXCJhdmFpbGFibGVEZWxpdmVyeVxcXCI6IFtdLFxcclxcbiAgICAgICAgICAgICAgXFxcImV4dG5cXFwiOiB7XFxyXFxuICAgICAgICAgICAgICAgIFxcXCJwcmVPcmRlckl0ZW1cXFwiOiBcXFwiXFxcIixcXHJcXG4gICAgICAgICAgICAgICAgXFxcIndpZGVyU3RvcmVGbGFnXFxcIjogXFxcIk5cXFwiXFxyXFxuICAgICAgICAgICAgICB9XFxyXFxuICAgICAgICAgICAgfSxcXHJcXG4gICAgICAgICAgICB7XFxyXFxuICAgICAgICAgICAgICBcXFwic291cmNpbmdDbGFzc2lmaWNhdGlvblxcXCI6IFxcXCJFQ1xcXCIsXFxyXFxuICAgICAgICAgICAgICBcXFwibGluZUlkXFxcIjogXFxcIjNcXFwiLFxcclxcbiAgICAgICAgICAgICAgXFxcIml0ZW1JRFxcXCI6IFxcXCIzMDQ2OTdcXFwiLFxcclxcbiAgICAgICAgICAgICAgXFxcInJlcXVpcmVkUXR5XFxcIjogXFxcIjFcXFwiLFxcclxcbiAgICAgICAgICAgICAgXFxcInJlcXVpcmVkQ2FwYWNpdHlcXFwiOiBcXFwiMVxcXCIsXFxyXFxuICAgICAgICAgICAgICBcXFwiZXh0ZXJuYWxSZWZlcmVuY2VJZFxcXCI6IFxcXCIxMjc0MFxcXCIsXFxyXFxuICAgICAgICAgICAgICBcXFwib3JkZXJUeXBlXFxcIjogXFxcIm5vcm1hbFxcXCIsXFxyXFxuICAgICAgICAgICAgICBcXFwidW5pdE9mTWVhc3VyZVxcXCI6IFxcXCJFQVxcXCIsXFxyXFxuICAgICAgICAgICAgICBcXFwiY2FycmllclNlcnZpY2VDb2RlXFxcIjogXFxcIkNyb21hVHJhbnNwb3J0U2VydmljZSAtIEV4cHJlc3MgRGVsaXZlcnlcXFwiLFxcclxcbiAgICAgICAgICAgICAgXFxcInNjYWNcXFwiOiBcXFwiQ3JvbWFUcmFuc3BvcnRTZXJ2aWNlXFxcIixcXHJcXG4gICAgICAgICAgICAgIFxcXCJ6aXBFbmFibGVkSW5UTVNcXFwiOiBmYWxzZSxcXHJcXG4gICAgICAgICAgICAgIFxcXCJpc1Rtc0VuYWJsZVxcXCI6IHRydWUsXFxyXFxuICAgICAgICAgICAgICBcXFwiZnVsZmlsbG1lbnRUeXBlXFxcIjogXFxcIlNERUxcXFwiLFxcclxcbiAgICAgICAgICAgICAgXFxcImFzc2lnbm1lbnRzXFxcIjoge1xcclxcbiAgICAgICAgICAgICAgICBcXFwidG90YWxOdW1iZXJPZlJlY29yZHNcXFwiOiBcXFwiMVxcXCIsXFxyXFxuICAgICAgICAgICAgICAgIFxcXCJhc3NpZ25tZW50XFxcIjogW1xcclxcbiAgICAgICAgICAgICAgICAgIHtcXHJcXG4gICAgICAgICAgICAgICAgICAgIFxcXCJxdWFudGl0eVxcXCI6IFxcXCIxXFxcIixcXHJcXG4gICAgICAgICAgICAgICAgICAgIFxcXCJzaGlwTm9kZVxcXCI6IFxcXCJBMDIxXFxcIixcXHJcXG4gICAgICAgICAgICAgICAgICAgIFxcXCJsbWRGY0lkXFxcIjogXFxcIlxcXCIsXFxyXFxuICAgICAgICAgICAgICAgICAgICBcXFwib2Zmc2V0XFxcIjogXFxcIlxcXCIsXFxyXFxuICAgICAgICAgICAgICAgICAgICBcXFwiZGVsaXZlcnlEYXRlXFxcIjogXFxcIjIwMjQtMDUtMTZUMjE6MDArMDU6MzBcXFwiLFxcclxcbiAgICAgICAgICAgICAgICAgICAgXFxcInppcENvZGVcXFwiOiBcXFwiNDAwMDQ5XFxcIixcXHJcXG4gICAgICAgICAgICAgICAgICAgIFxcXCJwcm9kdWN0QXZhaWxEYXRlXFxcIjogXFxcIjIwMjQtMDUtMTZUMTE6MzI6NTYuNDkxXFxcIixcXHJcXG4gICAgICAgICAgICAgICAgICAgIFxcXCJwb2RJbmRpY2F0b3JcXFwiOiBcXFwiTlxcXCIsXFxyXFxuICAgICAgICAgICAgICAgICAgICBcXFwiaXNUcmFkZWluSW5kaWNhdG9yXFxcIjogXFxcIk5cXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgfVxcclxcbiAgICAgICAgICAgICAgICBdXFxyXFxuICAgICAgICAgICAgICB9LFxcclxcbiAgICAgICAgICAgICAgXFxcImF2YWlsYWJsZURlbGl2ZXJ5XFxcIjogW10sXFxyXFxuICAgICAgICAgICAgICBcXFwiZXh0blxcXCI6IHtcXHJcXG4gICAgICAgICAgICAgICAgXFxcInByZU9yZGVySXRlbVxcXCI6IFxcXCJcXFwiLFxcclxcbiAgICAgICAgICAgICAgICBcXFwid2lkZXJTdG9yZUZsYWdcXFwiOiBcXFwiTlxcXCJcXHJcXG4gICAgICAgICAgICAgIH1cXHJcXG4gICAgICAgICAgICB9XFxyXFxuICAgICAgICAgIF1cXHJcXG4gICAgICAgIH1cXHJcXG4gICAgICB9LFxcclxcbiAgICAgIFxcXCJ1bmF2YWlsYWJsZUxpbmVzXFxcIjoge1xcclxcbiAgICAgICAgXFxcInVuYXZhaWxhYmxlTGluZVxcXCI6IFtcXHJcXG4gICAgICAgICAge1xcclxcbiAgICAgICAgICAgIFxcXCJpdGVtSURcXFwiOiBcXFwiMzA0Njk3XFxcIixcXHJcXG4gICAgICAgICAgICBcXFwibGluZUlkXFxcIjogXFxcIjJcXFwiLFxcclxcbiAgICAgICAgICAgIFxcXCJ1bmF2YWlsYWJsZVJlYXNvblxcXCI6IFxcXCJTT1VSQ0lOR19SVUxFX05PVF9ERUZJTkVEXFxcIlxcclxcbiAgICAgICAgICB9XFxyXFxuICAgICAgICBdXFxyXFxuICAgICAgfVxcclxcbiAgICB9LFxcclxcbiAgICBcXFwib3B0aW9uc1xcXCI6IHt9XFxyXFxuICB9XFxyXFxufVwiLFwic3RhdHVzXCI6MjAwfX0ifV0sImN1c3RvbWVySGFzaCI6bnVsbCwiZWNpZCI6IlVua25vd24iLCJjbGllbnRJZCI6IkNST01BLVdFQi1BUFAiLCJzZXNzaW9uSWQiOiJiNjVmNTRiNC01YzkzLTQ4MmUtOGNmOC1jMjEwZWNmZmE3OGYuOTIzYmI1N2YtOTUxYi00NzgwLThjYzMtM2U3Yzg5ZTkwNWZlIiwiYnJhbmQiOiJDUk9NQSIsInRpbWVzdGFtcCI6MTcxNTgzOTM3NjU1MiwiZGV2aWNlIjoiTW96aWxsYS81LjAgKFgxMTsgTGludXggeDg2XzY0KSBBcHBsZVdlYktpdC81MzcuMzYgKEtIVE1MLCBsaWtlIEdlY2tvKSBDaHJvbWUvMTE0LjAuMC4wIFNhZmFyaS81MzcuMzYiLCJqYXJ2aXNJZCI6IjRkNjA0NTcwLWJhZmItNDU1MS1iYWQwLTg1Mzg5NzFjN2VlMiIsImFkdmVydGlzaW5nSWQiOm51bGwsImFwcFZlcnNpb24iOiIwIn0="}",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.facebook.com/tr/?id=1369867960112522&ev=PageView&dl=https%3A%2F%2Fwww.croma.com%2Fcart&rl=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&if=false&ts=1715839375587&sw=1523&sh=650&ud[external_id]=36ce52f2c012a86c9264c805b8cff82e07250268c3b1663e92a631306623bfc6&v=2.9.156&r=stable&ec=6&o=4126&fbp=fb.1.1715839140634.281453602&ler=empty&cdl=API_unavailable&it=1715839205604&coo=false&rqm=GET", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s46735454534919?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%201%3A2%3A55%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=pdp&g=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&cc=INR&server=www.croma.com&events=event4%2Cevent20%2Cevent21%2Cevent49&products=%3B304697%3B%3B%3Bevent20%3D39999%7Cevent21%3D39999%3BeVar10%3D304697%7CeVar13%3D39999%7CeVar14%3D39999%7CeVar43%3DPhones%20%26%20Wearables%3AMobile%20Phones%3AAndroid%20Phones%3AOnePlus%2012R%205G%20%288GB%20128GB%20Cool%20Blue%29%7CeVar136%3DP%7CeVar137%3DNA%7CeVar41%3DOnePlus%2012R%205G%20%288GB%20128GB%20Cool%20Blue%29&c1=pdp&v1=D%3Dmid&c2=www.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&v2=new&c3=2024-5-16%201%3A2%3A55&c4=plp&v4=guest%20user&v5=400049&v15=proceed%20to%20cart&v16=minicart_clicked&v17=body&v39=RC04f36c75b2ee4eb8bb80ea5bb1cbd471&v53=https%3A%2F%2Fwww.croma.com%2Fcart&v71=proceed%20to%20cart%7Cbody%7Cminicart_clicked&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5BotherClick%5D%20%3A%20Mini%20Cart%20Click&pe=lnk_o&pev2=proceed%20to%20cart&c.&a.&activitymap.&page=pdp&link=Proceed%20to%20Cart&region=BODY&pageIDType=1&.activitymap&.a&.c&pid=pdp&pidt=1&oid=function%28c%29%7Bvarq%3Db.Qa%28f%2Cc%2Ce%2Cl%29%3Ba.conf.ja%26%26%28q.oa%3Dx%29%3Breturnb.sh%28n%2Cq%2Cthis%2Carguments%29%7D&oidt=2&ot=BUTTON&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=1523&bh=523&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&lrt=132&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;s_tp;s_ppv;_ga_DJ3RGVFHJN;WZRK_S_679-6RK-976Z;s_nr30;s_sq", END_INLINE
    );

    ns_end_transaction("v1_27", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("X00049_5");
    ns_web_url("X00049_5",
        "URL=https://api.croma.com/pwagoogle/v1/getgeocode/400049?components=%27country:IN|postal_code:400049%27",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;s_tp;s_ppv;_ga_DJ3RGVFHJN;WZRK_S_679-6RK-976Z;s_nr30;s_sq"
    );

    ns_end_transaction("X00049_5", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_17");
    ns_web_url("collect_17",
        "URL=https://analytics.google.com/g/collect?v=2&tid=G-DJ3RGVFHJN&gtm=45je45f0v893318240z8889407033za200&_p=1715839203848&gcd=13l3l3l3l1&npa=0&dma=0&cid=1944455297.1715839141&ul=en-us&sr=1523x650&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&frm=0&pscdl=noapi&sid=1715839140&sct=1&seg=1&dl=https%3A%2F%2Fwww.croma.com%2Fcart&dr=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&dt=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&_s=6&tfd=173571",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "en=scroll_depth&epn.scroll_percentage=50&_et=2487
en=scroll_depth&epn.scroll_percentage=75&_et=14",
        BODY_END
    );

    ns_end_transaction("collect_17", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_18");
    ns_web_url("collect_18",
        "URL=https://analytics.google.com/g/collect?v=2&tid=G-DJ3RGVFHJN&gtm=45je45f0v893318240za200&_p=1715839203848&gcd=13l3l3l3l1&npa=0&dma=0&cid=1944455297.1715839141&ul=en-us&sr=1523x650&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&frm=0&pscdl=noapi&_eu=AEA&_s=7&sid=1715839140&sct=1&seg=1&dl=https%3A%2F%2Fwww.croma.com%2Fcart&dr=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&dt=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&en=scroll&epn.percent_scrolled=90&_et=16&tfd=173612",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.croma.com/assets/favicon.ico", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;RT;s_ips;s_stop14;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;s_test;s_test14;s_stop;WZRK_S_679-6RK-976Z;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE
    );

    ns_end_transaction("collect_18", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("storelocation_3");
    ns_web_url("storelocation_3",
        "URL=https://api.croma.com/lookup/mobile-app/v1/storelocation?latitude=19.1047153&longitude=72.826827&",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;WZRK_S_679-6RK-976Z;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN"
    );

    ns_end_transaction("storelocation_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("events_7");
    ns_web_url("events_7",
        "URL=https://api.tatadigital.com/api/v1.1/msd/events",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=client_id:CROMA-WEB-APP",
        "HEADER=Program-Id:01eae2ec-0576-1000-bbea-86e16dcb4b79",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"page_type":"cart","content_type":"product","action_name":"view","event_name":"page_view","correlation_id":"sssaasss","medium":"pixel","metadata":{},"source":"CROMA","user_id_type":["CROMA"],"user_id":["08vHUbbT7lzWpSwZ"],"mad_uuid":"08vHUbbT7lzWpSwZ","utm":{"utm_source":"","utm_medium":"","utm_campaign":""},"epoch":"1715839375908","pincode":"400049"}",
        BODY_END
    );

    ns_end_transaction("events_7", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("getcart");
    ns_web_url("getcart",
        "URL=https://api.croma.com/useraccount/allchannels/v2/users/anonymous/carts/AN_CR-20958114759961ed485c6f419fa4d0cdfed6b9e2698801101019106380172848/getcart?isGetCart=true&fields=FULL&isSubscribed=inactive",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;WZRK_S_679-6RK-976Z;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN",
        INLINE_URLS,
            "URL=https://www.croma.com/static/js/54.153186e5.chunk.js", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;jarvis-id;s_ecid;s_plt;s_pltp;s_cc;s_stop5;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;RT;s_ips;s_stop14;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;s_test;s_test14;s_stop;WZRK_S_679-6RK-976Z;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE,
            //URL commented due to error code : ABORTED
            //"URL=https://media-ik.croma.com/prod/?tr=w-400", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;WZRK_S_679-6RK-976Z;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN", END_INLINE
    );

    ns_end_transaction("getcart", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("getApplicationPromotionsForI_2");
    ns_web_url("getApplicationPromotionsForI_2",
        "URL=https://api.tatadigital.com/getApplicablePromotion/getApplicationPromotionsForItemOffer",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=client_id:CROMA",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Authorization:8Tksadcs85ad4vsasfasgf4sJHvfs4NiKNKLHKLH582546f646",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "%7B%22getApplicablePromotionsForItemRequest%22%3A%7B%22itemId%22%3A%22304697%22%2C%22programId%22%3A%2201eae2ec-0576-1000-bbea-86e16dcb4b79%22%2C%22channelIds%22%3A%5B%22TCPCHS0003%22%5D%2C%22status%22%3A%22ACTIVE%22%7D%7D=",
        BODY_END
    );

    ns_end_transaction("getApplicationPromotionsForI_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("emidata_2");
    ns_web_url("emidata_2",
        "URL=https://api.croma.com/product/allchannels/v1/emidata?fields=FULL&amount=39999",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;WZRK_S_679-6RK-976Z;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN"
    );

    ns_end_transaction("emidata_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("details_pwa_3");
    ns_web_url("details_pwa_3",
        "URL=https://api.croma.com/inventory/oms/v2/tms/details-pwa/",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=oms-apim-subscription-key:1131858141634e2abe2efb2b3a2a2a5d",
        "HEADER=Content-Type:application/json",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;WZRK_S_679-6RK-976Z;s_nr30;s_sq;s_tp;s_ppv;_ga_DJ3RGVFHJN",
        BODY_BEGIN,
            "{"promise":{"allocationRuleID":"SYSTEM","checkInventory":"Y","organizationCode":"CROMA","sourcingClassification":"EC","promiseLines":{"promiseLine":[{"fulfillmentType":"HDEL","mch":"","itemID":"304697","lineId":"1","reEndDate":"2500-01-01","reqStartDate":"","requiredQty":"1","shipToAddress":{"company":"","country":"","city":"","mobilePhone":"","state":"","zipCode":"400049","extn":{"irlAddressLine1":"","irlAddressLine2":""}},"extn":{"widerStoreFlag":"N"}},{"fulfillmentType":"STOR","mch":"","itemID":"304697","lineId":"2","reEndDate":"2500-01-01","reqStartDate":"","requiredQty":"1","shipToAddress":{"company":"","country":"","city":"","mobilePhone":"","state":"","zipCode":"400049","extn":{"irlAddressLine1":"","irlAddressLine2":""}},"extn":{"widerStoreFlag":"N"}},{"fulfillmentType":"SDEL","mch":"","itemID":"304697","lineId":"3","reEndDate":"2500-01-01","reqStartDate":"","requiredQty":"1","shipToAddress":{"company":"","country":"","city":"","mobilePhone":"","state":"","zipCode":"400049","extn":{"irlAddressLine1":"","irlAddressLine2":""}},"extn":{"widerStoreFlag":"N"}}]}}}",
        BODY_END
    );

    ns_end_transaction("details_pwa_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("adrum_8");
    ns_web_url("adrum_8",
        "URL=https://bom-col.eum-appdynamics.com/eumcollector/beacons/browser/v1/BO-AAB-FBK/adrum",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:text/plain",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"vr":"23.10.1.4359","dt":"R","rg":"0","es":[{"eg":"1","et":2,"eu":"0://1/2/3/4/5/6","ts":1715839372341,"mg":"0","au":"0://7/8/9/10","at":0,"pp":3,"mx":{"PLC":1,"FBT":138,"DDT":19,"DPT":0,"PLT":157,"ARE":0},"md":"GET","xs":200,"si":49},{"eg":"2","et":2,"eu":"0://1/11/3/12/13/14/15","ts":1715839372489,"mg":"0","au":"0://7/8/9/10","at":0,"pp":3,"mx":{"PLC":1,"FBT":93,"DDT":0,"DPT":0,"PLT":93,"ARE":0},"md":"POST","xs":201,"si":50},{"eg":"3","et":2,"eu":"0://16/17/18/19/20/21","ts":1715839372278,"mg":"0","au":"0://7/8/9/10","at":0,"pp":3,"mx":{"PLC":1,"FBT":368,"DDT":4,"DPT":1,"PLT":373,"ARE":0},"md":"POST","xs":200,"si":51},{"eg":"4","et":2,"eu":"0://1/11/3/12/13/14/15/22/23?24","ts":1715839372588,"mg":"0","au":"0://7/8/9/10","at":0,"pp":3,"mx":{"PLC":1,"FBT":233,"DDT":1,"DPT":0,"PLT":234,"ARE":0},"md":"POST","xs":201,"si":52},{"eg":"5","et":2,"eu":"0://1/11/3/12/13/14/15/22/25","ts":1715839372822,"mg":"0","au":"0://7/8/9/10","at":0,"pp":3,"mx":{"PLC":1,"FBT":152,"DDT":1,"DPT":0,"PLT":153,"ARE":0},"md":"GET","xs":200,"si":53},{"eg":"6","et":2,"eu":"0://1/26/3/27/28?29","ts":1715839372959,"mg":"0","au":"0://7/8/9/10","at":0,"pp":3,"mx":{"PLC":1,"FBT":126,"DDT":0,"DPT":0,"PLT":126,"ARE":0},"md":"GET","xs":200,"si":54},{"eg":"7","et":2,"eu":"0://1/11/3/12/13/14/15/22/30","ts":1715839372943,"mg":"0","au":"0://7/8/9/10","at":0,"pp":3,"mx":{"PLC":1,"FBT":163,"DDT":3,"DPT":0,"PLT":166,"ARE":0},"md":"GET","xs":200,"si":55},{"eg":"8","et":2,"eu":"0://16/17/18/19/31","ts":1715839372823,"mg":"0","au":"0://7/8/9/10","at":0,"pp":3,"mx":{"PLC":1,"FBT":259,"DDT":39,"DPT":0,"PLT":298,"ARE":0},"md":"POST","xs":200,"si":56},{"eg":"9","et":2,"eu":"0://32/33?34","ts":1715839372901,"mg":"0","au":"0://7/8/9/10","at":0,"pp":3,"mx":{"PLC":1,"FBT":235,"DDT":0,"DPT":0,"PLT":235,"ARE":0},"md":"GET","xs":200,"si":57},{"eg":"10","et":2,"eu":"0://16/35/31/4","ts":1715839372961,"mg":"0","au":"0://7/8/9/10","at":0,"pp":3,"mx":{"PLC":1,"FBT":198,"DDT":10,"DPT":0,"PLT":208,"ARE":0},"md":"POST","xs":200,"si":58},{"eg":"11","et":2,"eu":"0://16/35/31/4","ts":1715839373103,"mg":"0","au":"0://7/8/9/10","at":0,"pp":3,"mx":{"PLC":1,"FBT":173,"DDT":7,"DPT":0,"PLT":180,"ARE":0},"md":"POST","xs":200,"si":59},{"eg":"12","et":2,"eu":"0://16/35/31/4","ts":1715839373117,"mg":"0","au":"0://7/8/9/10","at":0,"pp":3,"mx":{"PLC":1,"FBT":167,"DDT":1,"DPT":0,"PLT":168,"ARE":0},"md":"POST","xs":200,"si":60},{"eg":"13","et":2,"eu":"0://1/26/3/12/36/37","ts":1715839373114,"mg":"0","au":"0://7/8/9/10","at":0,"pp":3,"mx":{"PLC":1,"FBT":225,"DDT":0,"DPT":1,"PLT":226,"ARE":0},"md":"POST","xs":200,"si":61},{"eg":"14","et":2,"eu":"0://1/38/4/39/40?41","ts":1715839375374,"mg":"0","au":"0://7/42","at":0,"pp":3,"mx":{"PLC":1,"FBT":203,"DDT":0,"DPT":0,"PLT":203,"ARE":0},"md":"GET","xs":200,"si":62},{"eg":"15","et":2,"eu":"0://16/35/31/4","ts":1715839375271,"mg":"0","au":"0://7/42","at":0,"pp":3,"mx":{"PLC":1,"FBT":314,"DDT":4,"DPT":0,"PLT":318,"ARE":0},"md":"POST","xs":200,"si":63},{"eg":"16","et":2,"eu":"0://1/43/44/4/45?46","ts":1715839375584,"mg":"0","au":"0://7/42","at":0,"pp":3,"mx":{"PLC":1,"FBT":50,"DDT":1,"DPT":0,"PLT":51,"ARE":0},"md":"GET","xs":200,"si":64},{"eg":"17","et":2,"eu":"0://16/17/18/19/31","ts":1715839375613,"mg":"0","au":"0://7/42","at":0,"pp":3,"mx":{"PLC":1,"FBT":157,"DDT":0,"DPT":0,"PLT":157,"ARE":0},"md":"POST","xs":200,"si":65},{"eg":"18","et":2,"eu":"0://1/11/3/12/13/14/15/22/47?48","ts":1715839375634,"mg":"0","au":"0://7/42","at":0,"pp":3,"mx":{"PLC":1,"FBT":428,"DDT":1,"DPT":0,"PLT":429,"ARE":0},"md":"GET","xs":200,"si":66},{"eg":"19","et":2,"eu":"0://1/36/3/4/49?50","ts":1715839376126,"mg":"0","au":"0://7/42","at":0,"pp":3,"mx":{"PLC":1,"FBT":37,"DDT":2,"DPT":1,"PLT":40,"ARE":0},"md":"GET","xs":200,"si":67},{"eg":"20","et":2,"eu":"0://1/51/52/12/53/54/","ts":1715839376127,"mg":"0","au":"0://7/42","at":0,"pp":3,"mx":{"PLC":1,"FBT":98,"DDT":1,"DPT":1,"PLT":100,"ARE":0},"md":"POST","xs":200,"si":68}],"ai":"477d42aa_9f21_9144_1a76_688c9aa25e4b","gs":["06a6dd02_3c5f_2943_3169_f771971f39e1","d7e1ff88_b8c7_7fba_7b74_903ced176b60","39b072ca_08a0_1887_f7c3_e7fc9735d205","8f87d1d2_cddd_7b33_d3b6_3d01544df1fb","adfbf69f_02b4_b468_6a40_4bb9a7640657","ae6498d4_764e_7056_decf_5f79f7fbfca7","5254ad97_2161_b7b8_fe16_dac1fa99777d","c79ed79d_93e1_7dca_750d_a5f04ce555fd","4f6b4352_e506_e168_af0a_f6fc8fb13c40","9daf9c1e_a9c2_b0d6_1ce5_fd1623d2b207","3041db85_bb67_99ac_439e_b8fc3259676d","481a571d_5e8b_d5b5_3d87_7fa7c02227ec","adda42cd_e0a9_4829_19a9_57cc3875d99c","d853d1cf_0bb8_bc9e_ae6e_d93e31bbe595","12b7d327_51d5_ab93_88d0_6d9eb1b2baf9","03e657f0_64e6_2200_e827_dee6df1f9f67","728cd05a_2ebb_2d91_ab6c_46b281cf51de","1ec83ad3_4bd5_29ec_d2ca_7e735bf7addf","0575f4e6_8208_c942_7cd1_8f0a63810b2d","82b27d3e_7276_1585_be3a_056ff512a7f9","b95c2e66_e0dc_a168_dbeb_9dd46ecc009e"],"up":["https","api.croma.com","security","allchannels","v1","pkey","issuance","www.croma.com","oneplus-12r-5g-8gb-128gb-cool-blue-","p","304697","useraccount","v2","users","anonymous","carts","api.tatadigital.com","api","v1.1","msd","recommendations","multi","AN_CR-20958114759961ed485c6f419fa4d0cdfed6b9e2698801101019106380172848","entries","code=304697&qty=1&refreshCart=false&isPDP=true&isFreebiePresent=false&pincode=400049","count","ew","v7","detail","PRODUCT_CODE=304697&PRICE=39999&CATEGORYL2=95&CATEGORYL1=10&CATEGORYL0=1&BRAND_CODE=0948&STANDARD_WARRANTY_PERIOD=12","floatingCart","events","capi.croma.com","tr","id=1369867960112522&ev=AddToCart&et=1715839373&es=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&referrer=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&eid=11146789180925&ua=Mozilla%2F5.0%20(X11%3B%20Linux%20x86_64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F114.0.0.0%20Safari%2F537.36&fbp=fb.1.1715839140634.281453602&external_id=dd225ea0e0a549dff2458f2b188bd0b54a923171c4b30d006a0ce3ff08d8...","analytics-engine","product","image-details","pwagoogle","getgeocode","400049","components='country:IN|postal_code:400049'","cart","lookup","mobile-app","storelocation","latitude=19.1047153&longitude=72.826827&","getcart","isGetCart=true&fields=FULL&isSubscribed=inactive","emidata","fields=FULL&amount=39999","inventory","oms","tms","details-pwa"]}",
        BODY_END
    );

    ns_end_transaction("adrum_8", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_19");
    ns_web_url("collect_19",
        "URL=https://analytics.google.com/g/collect?v=2&tid=G-DJ3RGVFHJN&gtm=45je45f0v893318240z8889407033za200&_p=1715839203848&gcd=13l3l3l3l1&npa=0&dma=0&cid=1944455297.1715839141&ul=en-us&sr=1523x650&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&frm=0&pscdl=noapi&_s=8&sid=1715839140&sct=1&seg=1&dl=https%3A%2F%2Fwww.croma.com%2Fcart&dr=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&dt=Buy%20OnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)%20online%20at%20best%20prices%20from%20Croma.%20Check%20product%20details%2C%20reviews%20%26%20more.%20Shop%20now!&en=scroll_depth&epn.scroll_percentage=100&_et=36&tfd=174418",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.facebook.com/tr/?id=1369867960112522&ev=PageView&dl=https%3A%2F%2Fwww.croma.com%2Fcart&rl=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&if=false&ts=1715839376618&sw=1523&sh=650&ud[external_id]=36ce52f2c012a86c9264c805b8cff82e07250268c3b1663e92a631306623bfc6&v=2.9.156&r=stable&ec=7&o=4126&fbp=fb.1.1715839140634.281453602&ler=empty&cdl=API_unavailable&it=1715839205604&coo=false&eid=35807886546432&tm=1&rqm=GET", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googleadservices.com/pagead/conversion/1006316414/?random=1715839376624&cv=11&fst=1715839376624&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Fcart&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Croma%20Cart%20Page&value=0&bttype=purchase&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&rfmt=3&fmt=4", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googleadservices.com/pagead/conversion/609902077/?random=1715839376630&cv=11&fst=1715839376630&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Fcart&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=Croma%20Cart%20Page&value=0&bttype=purchase&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&rfmt=3&fmt=4", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("collect_19", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("v1_28");
    ns_web_url("v1_28",
        "URL=https://api.tatadigital.com/analytics-engine/events/v1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"secure": "true", "ePayload": "eyJpc1JlZGlyZWN0ZWRGcm9tVENQIjpmYWxzZSwidXJsIjoiaHR0cHM6Ly93d3cuY3JvbWEuY29tL2NhcnQiLCJldmVudERhdGEiOlt7InRpbWVzdGFtcCI6MTcxNTgzOTM5MDY4MSwiZGF0YSI6IntcImV2ZW50XCI6XCJ3ZWItdml0YWxzXCIsXCJkYXRhXCI6e1wibmFtZVwiOlwiQ0xTXCIsXCJ2YWx1ZVwiOjAuNDA1MTE2MTU2OTgzODgwOCxcInJhdGluZ1wiOlwicG9vclwiLFwiZGVsdGFcIjowLjQwNTExNjE1Njk4Mzg4MDgsXCJlbnRyaWVzXCI6W3tcIm5hbWVcIjpcIlwiLFwiZW50cnlUeXBlXCI6XCJsYXlvdXQtc2hpZnRcIixcInN0YXJ0VGltZVwiOjMwMzQuMTAwMDAwMzgxNDY5NyxcImR1cmF0aW9uXCI6MCxcInZhbHVlXCI6MC4zNjUwODAxMDU4NDI1MDAzLFwiaGFkUmVjZW50SW5wdXRcIjpmYWxzZSxcImxhc3RJbnB1dFRpbWVcIjowLFwic291cmNlc1wiOlt7XCJwcmV2aW91c1JlY3RcIjp7XCJ4XCI6NzY5LFwieVwiOjExNixcIndpZHRoXCI6NTU1LFwiaGVpZ2h0XCI6NjAsXCJ0b3BcIjoxMTYsXCJyaWdodFwiOjEzMjQsXCJib3R0b21cIjoxNzYsXCJsZWZ0XCI6NzY5fSxcImN1cnJlbnRSZWN0XCI6e1wieFwiOjc2OSxcInlcIjoxMjEsXCJ3aWR0aFwiOjU1NSxcImhlaWdodFwiOjYwLFwidG9wXCI6MTIxLFwicmlnaHRcIjoxMzI0LFwiYm90dG9tXCI6MTgxLFwibGVmdFwiOjc2OX19LHtcInByZXZpb3VzUmVjdFwiOntcInhcIjo0MDAsXCJ5XCI6MTgsXCJ3aWR0aFwiOjkyNCxcImhlaWdodFwiOjQyLFwidG9wXCI6MTgsXCJyaWdodFwiOjEzMjQsXCJib3R0b21cIjo2MCxcImxlZnRcIjo0MDB9LFwiY3VycmVudFJlY3RcIjp7XCJ4XCI6NDI0LFwieVwiOjE4LFwid2lkdGhcIjo5MDAsXCJoZWlnaHRcIjo0MixcInRvcFwiOjE4LFwicmlnaHRcIjoxMzI0LFwiYm90dG9tXCI6NjAsXCJsZWZ0XCI6NDI0fX0se1wicHJldmlvdXNSZWN0XCI6e1wieFwiOjE4NCxcInlcIjoxNTgsXCJ3aWR0aFwiOjkwLFwiaGVpZ2h0XCI6MzY1LFwidG9wXCI6MTU4LFwicmlnaHRcIjoyNzQsXCJib3R0b21cIjo1MjMsXCJsZWZ0XCI6MTg0fSxcImN1cnJlbnRSZWN0XCI6e1wieFwiOjAsXCJ5XCI6MCxcIndpZHRoXCI6MCxcImhlaWdodFwiOjAsXCJ0b3BcIjowLFwicmlnaHRcIjowLFwiYm90dG9tXCI6MCxcImxlZnRcIjowfX0se1wicHJldmlvdXNSZWN0XCI6e1wieFwiOjc2OSxcInlcIjozMzgsXCJ3aWR0aFwiOjU1NSxcImhlaWdodFwiOjE0OSxcInRvcFwiOjMzOCxcInJpZ2h0XCI6MTMyNCxcImJvdHRvbVwiOjQ4NyxcImxlZnRcIjo3Njl9LFwiY3VycmVudFJlY3RcIjp7XCJ4XCI6MCxcInlcIjowLFwid2lkdGhcIjowLFwiaGVpZ2h0XCI6MCxcInRvcFwiOjAsXCJyaWdodFwiOjAsXCJib3R0b21cIjowLFwibGVmdFwiOjB9fSx7XCJwcmV2aW91c1JlY3RcIjp7XCJ4XCI6NzY5LFwieVwiOjE5NCxcIndpZHRoXCI6NTU1LFwiaGVpZ2h0XCI6MTI5LFwidG9wXCI6MTk0LFwicmlnaHRcIjoxMzI0LFwiYm90dG9tXCI6MzIzLFwibGVmdFwiOjc2OX0sXCJjdXJyZW50UmVjdFwiOntcInhcIjo3NjksXCJ5XCI6MTk5LFwid2lkdGhcIjo1NTUsXCJoZWlnaHRcIjoxMjgsXCJ0b3BcIjoxOTksXCJyaWdodFwiOjEzMjQsXCJib3R0b21cIjozMjcsXCJsZWZ0XCI6NzY5fX1dfSx7XCJuYW1lXCI6XCJcIixcImVudHJ5VHlwZVwiOlwibGF5b3V0LXNoaWZ0XCIsXCJzdGFydFRpbWVcIjozMjI0LjEwMDAwMDM4MTQ2OTcsXCJkdXJhdGlvblwiOjAsXCJ2YWx1ZVwiOjAuMDMyMzY5ODkxNDc1MjAzNTU1LFwiaGFkUmVjZW50SW5wdXRcIjpmYWxzZSxcImxhc3RJbnB1dFRpbWVcIjowLFwic291cmNlc1wiOlt7XCJwcmV2aW91c1JlY3RcIjp7XCJ4XCI6MjA4LFwieVwiOjE1OCxcIndpZHRoXCI6OTAsXCJoZWlnaHRcIjozNjAsXCJ0b3BcIjoxNTgsXCJyaWdodFwiOjI5OCxcImJvdHRvbVwiOjUxOCxcImxlZnRcIjoyMDh9LFwiY3VycmVudFJlY3RcIjp7XCJ4XCI6MCxcInlcIjowLFwid2lkdGhcIjowLFwiaGVpZ2h0XCI6MCxcInRvcFwiOjAsXCJyaWdodFwiOjAsXCJib3R0b21cIjowLFwibGVmdFwiOjB9fSx7XCJwcmV2aW91c1JlY3RcIjp7XCJ4XCI6MjA4LFwieVwiOjUxOCxcIndpZHRoXCI6OTAsXCJoZWlnaHRcIjo1LFwidG9wXCI6NTE4LFwicmlnaHRcIjoyOTgsXCJib3R0b21cIjo1MjMsXCJsZWZ0XCI6MjA4fSxcImN1cnJlbnRSZWN0XCI6e1wieFwiOjIwOCxcInlcIjoxNTgsXCJ3aWR0aFwiOjkwLFwiaGVpZ2h0XCI6MTgsXCJ0b3BcIjoxNTgsXCJyaWdodFwiOjI5OCxcImJvdHRvbVwiOjE3NixcImxlZnRcIjoyMDh9fSx7XCJwcmV2aW91c1JlY3RcIjp7XCJ4XCI6MCxcInlcIjowLFwid2lkdGhcIjowLFwiaGVpZ2h0XCI6MCxcInRvcFwiOjAsXCJyaWdodFwiOjAsXCJib3R0b21cIjowLFwibGVmdFwiOjB9LFwiY3VycmVudFJlY3RcIjp7XCJ4XCI6MjA4LFwieVwiOjE4NCxcIndpZHRoXCI6OTAsXCJoZWlnaHRcIjoxMjgsXCJ0b3BcIjoxODQsXCJyaWdodFwiOjI5OCxcImJvdHRvbVwiOjMxMixcImxlZnRcIjoyMDh9fSx7XCJwcmV2aW91c1JlY3RcIjp7XCJ4XCI6MCxcInlcIjowLFwid2lkdGhcIjowLFwiaGVpZ2h0XCI6MCxcInRvcFwiOjAsXCJyaWdodFwiOjAsXCJib3R0b21cIjowLFwibGVmdFwiOjB9LFwiY3VycmVudFJlY3RcIjp7XCJ4XCI6MjA4LFwieVwiOjMyMCxcIndpZHRoXCI6OTAsXCJoZWlnaHRcIjoxMjcsXCJ0b3BcIjozMjAsXCJyaWdodFwiOjI5OCxcImJvdHRvbVwiOjQ0NyxcImxlZnRcIjoyMDh9fSx7XCJwcmV2aW91c1JlY3RcIjp7XCJ4XCI6MCxcInlcIjowLFwid2lkdGhcIjowLFwiaGVpZ2h0XCI6MCxcInRvcFwiOjAsXCJyaWdodFwiOjAsXCJib3R0b21cIjowLFwibGVmdFwiOjB9LFwiY3VycmVudFJlY3RcIjp7XCJ4XCI6MjA4LFwieVwiOjQ1NixcIndpZHRoXCI6OTAsXCJoZWlnaHRcIjo2NyxcInRvcFwiOjQ1NixcInJpZ2h0XCI6Mjk4LFwiYm90dG9tXCI6NTIzLFwibGVmdFwiOjIwOH19XX0se1wibmFtZVwiOlwiXCIsXCJlbnRyeVR5cGVcIjpcImxheW91dC1zaGlmdFwiLFwic3RhcnRUaW1lXCI6MzM0NSxcImR1cmF0aW9uXCI6MCxcInZhbHVlXCI6MC4wMDcyOTI1ODM0MDQwODc2OTA0LFwiaGFkUmVjZW50SW5wdXRcIjpmYWxzZSxcImxhc3RJbnB1dFRpbWVcIjowLFwic291cmNlc1wiOlt7XCJwcmV2aW91c1JlY3RcIjp7XCJ4XCI6MCxcInlcIjowLFwid2lkdGhcIjowLFwiaGVpZ2h0XCI6MCxcInRvcFwiOjAsXCJyaWdodFwiOjAsXCJib3R0b21cIjowLFwibGVmdFwiOjB9LFwiY3VycmVudFJlY3RcIjp7XCJ4XCI6MjA4LFwieVwiOjQ1NixcIndpZHRoXCI6OTAsXCJoZWlnaHRcIjo2NyxcInRvcFwiOjQ1NixcInJpZ2h0XCI6Mjk4LFwiYm90dG9tXCI6NTIzLFwibGVmdFwiOjIwOH19LHtcInByZXZpb3VzUmVjdFwiOntcInhcIjoyMDgsXCJ5XCI6MTg0LFwid2lkdGhcIjo5MCxcImhlaWdodFwiOjEyOCxcInRvcFwiOjE4NCxcInJpZ2h0XCI6Mjk4LFwiYm90dG9tXCI6MzEyLFwibGVmdFwiOjIwOH0sXCJjdXJyZW50UmVjdFwiOntcInhcIjowLFwieVwiOjAsXCJ3aWR0aFwiOjAsXCJoZWlnaHRcIjowLFwidG9wXCI6MCxcInJpZ2h0XCI6MCxcImJvdHRvbVwiOjAsXCJsZWZ0XCI6MH19LHtcInByZXZpb3VzUmVjdFwiOntcInhcIjoyMDgsXCJ5XCI6MzIwLFwid2lkdGhcIjo5MCxcImhlaWdodFwiOjEyNyxcInRvcFwiOjMyMCxcInJpZ2h0XCI6Mjk4LFwiYm90dG9tXCI6NDQ3LFwibGVmdFwiOjIwOH0sXCJjdXJyZW50UmVjdFwiOntcInhcIjoyMDgsXCJ5XCI6MTY1LFwid2lkdGhcIjo5MCxcImhlaWdodFwiOjg3LFwidG9wXCI6MTY1LFwicmlnaHRcIjoyOTgsXCJib3R0b21cIjoyNTIsXCJsZWZ0XCI6MjA4fX0se1wicHJldmlvdXNSZWN0XCI6e1wieFwiOjIwOCxcInlcIjo0NTYsXCJ3aWR0aFwiOjkwLFwiaGVpZ2h0XCI6NjcsXCJ0b3BcIjo0NTYsXCJyaWdodFwiOjI5OCxcImJvdHRvbVwiOjUyMyxcImxlZnRcIjoyMDh9LFwiY3VycmVudFJlY3RcIjp7XCJ4XCI6MjA4LFwieVwiOjI2MixcIndpZHRoXCI6OTAsXCJoZWlnaHRcIjo4NyxcInRvcFwiOjI2MixcInJpZ2h0XCI6Mjk4LFwiYm90dG9tXCI6MzQ5LFwibGVmdFwiOjIwOH19LHtcInByZXZpb3VzUmVjdFwiOntcInhcIjowLFwieVwiOjAsXCJ3aWR0aFwiOjAsXCJoZWlnaHRcIjowLFwidG9wXCI6MCxcInJpZ2h0XCI6MCxcImJvdHRvbVwiOjAsXCJsZWZ0XCI6MH0sXCJjdXJyZW50UmVjdFwiOntcInhcIjoyMDgsXCJ5XCI6MzU5LFwid2lkdGhcIjo5MCxcImhlaWdodFwiOjg3LFwidG9wXCI6MzU5LFwicmlnaHRcIjoyOTgsXCJib3R0b21cIjo0NDYsXCJsZWZ0XCI6MjA4fX1dfSx7XCJuYW1lXCI6XCJcIixcImVudHJ5VHlwZVwiOlwibGF5b3V0LXNoaWZ0XCIsXCJzdGFydFRpbWVcIjozNDMyLjEwMDAwMDM4MTQ2OTcsXCJkdXJhdGlvblwiOjAsXCJ2YWx1ZVwiOjAuMDAwMzczNTc2MjYyMDg5MzIyNDQsXCJoYWRSZWNlbnRJbnB1dFwiOmZhbHNlLFwibGFzdElucHV0VGltZVwiOjAsXCJzb3VyY2VzXCI6W3tcInByZXZpb3VzUmVjdFwiOntcInhcIjoyMDgsXCJ5XCI6MTY1LFwid2lkdGhcIjo5MCxcImhlaWdodFwiOjg3LFwidG9wXCI6MTY1LFwicmlnaHRcIjoyOTgsXCJib3R0b21cIjoyNTIsXCJsZWZ0XCI6MjA4fSxcImN1cnJlbnRSZWN0XCI6e1wieFwiOjIwOCxcInlcIjoxNTgsXCJ3aWR0aFwiOjkwLFwiaGVpZ2h0XCI6ODUsXCJ0b3BcIjoxNTgsXCJyaWdodFwiOjI5OCxcImJvdHRvbVwiOjI0MyxcImxlZnRcIjoyMDh9fSx7XCJwcmV2aW91c1JlY3RcIjp7XCJ4XCI6MjA4LFwieVwiOjI2MixcIndpZHRoXCI6OTAsXCJoZWlnaHRcIjo4NyxcInRvcFwiOjI2MixcInJpZ2h0XCI6Mjk4LFwiYm90dG9tXCI6MzQ5LFwibGVmdFwiOjIwOH0sXCJjdXJyZW50UmVjdFwiOntcInhcIjoyMDgsXCJ5XCI6MjUzLFwid2lkdGhcIjo5MCxcImhlaWdodFwiOjg1LFwidG9wXCI6MjUzLFwicmlnaHRcIjoyOTgsXCJib3R0b21cIjozMzgsXCJsZWZ0XCI6MjA4fX0se1wicHJldmlvdXNSZWN0XCI6e1wieFwiOjIwOCxcInlcIjozNTksXCJ3aWR0aFwiOjkwLFwiaGVpZ2h0XCI6ODcsXCJ0b3BcIjozNTksXCJyaWdodFwiOjI5OCxcImJvdHRvbVwiOjQ0NixcImxlZnRcIjoyMDh9LFwiY3VycmVudFJlY3RcIjp7XCJ4XCI6MjA4LFwieVwiOjM0OCxcIndpZHRoXCI6OTAsXCJoZWlnaHRcIjo4NSxcInRvcFwiOjM0OCxcInJpZ2h0XCI6Mjk4LFwiYm90dG9tXCI6NDMzLFwibGVmdFwiOjIwOH19LHtcInByZXZpb3VzUmVjdFwiOntcInhcIjoyMDgsXCJ5XCI6NDU2LFwid2lkdGhcIjo5MCxcImhlaWdodFwiOjY3LFwidG9wXCI6NDU2LFwicmlnaHRcIjoyOTgsXCJib3R0b21cIjo1MjMsXCJsZWZ0XCI6MjA4fSxcImN1cnJlbnRSZWN0XCI6e1wieFwiOjIwOCxcInlcIjo0NDMsXCJ3aWR0aFwiOjkwLFwiaGVpZ2h0XCI6ODAsXCJ0b3BcIjo0NDMsXCJyaWdodFwiOjI5OCxcImJvdHRvbVwiOjUyMyxcImxlZnRcIjoyMDh9fV19XSxcImlkXCI6XCJ2My0xNzE1ODM5MjA0MDcwLTc0MTgwNTcwNjE4NzNcIixcIm5hdmlnYXRpb25UeXBlXCI6XCJuYXZpZ2F0ZVwifX0ifV0sImN1c3RvbWVySGFzaCI6bnVsbCwiZWNpZCI6IlVua25vd24iLCJjbGllbnRJZCI6IkNST01BLVdFQi1BUFAiLCJzZXNzaW9uSWQiOiJiNjVmNTRiNC01YzkzLTQ4MmUtOGNmOC1jMjEwZWNmZmE3OGYuOTIzYmI1N2YtOTUxYi00NzgwLThjYzMtM2U3Yzg5ZTkwNWZlIiwiYnJhbmQiOiJDUk9NQSIsInRpbWVzdGFtcCI6MTcxNTgzOTM5MDY4MiwiZGV2aWNlIjoiTW96aWxsYS81LjAgKFgxMTsgTGludXggeDg2XzY0KSBBcHBsZVdlYktpdC81MzcuMzYgKEtIVE1MLCBsaWtlIEdlY2tvKSBDaHJvbWUvMTE0LjAuMC4wIFNhZmFyaS81MzcuMzYiLCJqYXJ2aXNJZCI6IjRkNjA0NTcwLWJhZmItNDU1MS1iYWQwLTg1Mzg5NzFjN2VlMiIsImFkdmVydGlzaW5nSWQiOm51bGwsImFwcFZlcnNpb24iOiIwIn0="}",
        BODY_END,
        INLINE_URLS,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1006316414/?random=1170075992&cv=11&fst=1715839376624&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Fcart&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Croma%20Cart%20Page&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtXdMUkBAvxPUZEiVSGJTbUkbnRkRKvT4zw&pscrd=IhMIo8-n5L-RhgMVhGOdCR3fugQAMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/609902077/?random=1977269669&cv=11&fst=1715839376630&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Fcart&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=Croma%20Cart%20Page&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtfsYYihDsouLIYH1JmhdEQiYrBPb6gafaw&pscrd=IhMItsqn5L-RhgMVs0ydCR1fMg7zMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-conversion/1006316414/?random=1170075992&cv=11&fst=1715839376624&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Fcart&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Croma%20Cart%20Page&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMIo8-n5L-RhgMVhGOdCR3fugQAMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtqQhIwFXD9B9aREwDmL4iJjOp1CnyAzQ&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtS1pFGFOxLbVhSle2qqeCWCdB59riecDLg&random=1706481906", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-conversion/609902077/?random=1977269669&cv=11&fst=1715839376630&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Fcart&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=Croma%20Cart%20Page&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMItsqn5L-RhgMVs0ydCR1fMg7zMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtqN09pjLajL2Sls_31fVLoViuNJRrWqg&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtQScR29UAd7Yqrt_MQhJLy71Hd-bI4Xpfw&random=1877529763", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-conversion/1006316414/?random=1170075992&cv=11&fst=1715839376624&bg=ffffff&guid=ON&async=1&gtm=45be45f0v888728434z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Fcart&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=l32ZCPPk06AYEP7W7N8D&hn=www.googleadservices.com&frm=0&tiba=Croma%20Cart%20Page&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMIo8-n5L-RhgMVhGOdCR3fugQAMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtqQhIwFXD9B9aREwDmL4iJjOp1CnyAzQ&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtS1pFGFOxLbVhSle2qqeCWCdB59riecDLg&random=1706481906&ipr=y", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-conversion/609902077/?random=1977269669&cv=11&fst=1715839376630&bg=ffffff&guid=ON&async=1&gtm=45be45f0z8889407033za201&gcd=13l3l3l3l1&dma=0&u_w=1523&u_h=650&url=https%3A%2F%2Fwww.croma.com%2Fcart&ref=https%3A%2F%2Fwww.croma.com%2Fcampaign%2Foneplus-r-5g%2Fc%2F6407%3Fq%3D%253Aprice-asc&label=6hOZCKey4boDEP276aIC&hn=www.googleadservices.com&frm=0&tiba=Croma%20Cart%20Page&value=0&npa=0&pscdl=noapi&auid=1421799651.1715839140&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&fdr=QA&fmt=3&ct_cookie_present=false&sscte=1&crd=CJW3sQIIscGxAgiwwbECCLnBsQI&pscrd=IhMItsqn5L-RhgMVs0ydCR1fMg7zMgIIAzICCAQyAggHMgIICDICCAkyAggKMgIIAjICCAs6Fmh0dHBzOi8vd3d3LmNyb21hLmNvbS8&is_vtc=1&cid=CAQSGwB7FLtqN09pjLajL2Sls_31fVLoViuNJRrWqg&eitems=ChAI8LqRsgYQoaS_qvXXjZ84Eh0Ar4sHtQScR29UAd7Yqrt_MQhJLy71Hd-bI4Xpfw&random=1877529763&ipr=y", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://sslwidget.criteo.com/event?a=56256&v=5.23.0&otl=1&p0=e%3Dexd%26site_type%3Dd&p1=e%3Dvb%26tms%3Dgtm-template%26p%3D%255Bi%25253D304697%252526pr%25253D39999%252526q%25253D1%255D&p2=e%3Ddis&adce=1&bundle=WwpUr19ZQjN2TTdzaTNPd0Q0dlByN3VaMFZMOEIwMTVJWjE0ZXI3bEFGMGJFZUxUZ2pPQ2F0Vk5FdlY1cXpBTXJ6bWF1WlppM040TnV2MU1IMnJNbjFlMTQ1UllVN2hSbHpzJTJGT1J0eFNnZ1FnekxaakRmcGh3THpVMnd2VnJVMzMlMkJFU2U&tld=croma.com&dy=1&fu=https%253A%252F%252Fwww.croma.com%252Fcart&pu=https%253A%252F%252Fwww.croma.com%252Fcampaign%252Foneplus-r-5g%252Fc%252F6407%253Fq%253D%25253Aprice-asc&ceid=9d438582-2844-449d-bd4c-e9267fd42d23&dtycbr=53569", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://in1.clevertap-prod.com/a?t=96&type=push&d=N4IgLgngDgpiBcIYDcYDswgDROWAcgIYC2ciAxgM4D6AagJYwDuABAMKEBOmOKYAIoTCEEoAAoAbIQDMA9p2IIQ5TrOKEAdOTUgAvjnoATJQDYA7AE4AtCYBKAaSsWzJgFrYQAc1PSADAA5Cf0NpABZyGF9Q6TCAIzN%2FE38AVmSAZgs02JNDNIAmZI9KBABGMxLk%2FwyS0JKcKG94PJxCaVEQCXpYpSYYWKtKQwBrK2QSjTMNNI8oVTBZbQklAAswMChi%2FRAAdVcHajYAQQBZMXamYigEXxwmekvrnDAJcgQAbQBdW%2Fpn1%2FhP3S6IAA%3D%3D&rn=7&i=1715839381&sn=0&gc=6f08a8df4ce04ff4b786855393b6d325&arp=N4IgVg%2BgdiBcIC0DuBeFIA0ICW06IFsBOKAMwEEBVAYXPIDVtMQATCAFwGc4AGLFgBa8sANzgAmLJG7xgAX2bYW%2BAGwB2IgFoVAJQDSmompUJmAJw4yAjGqsBWABwBmIlYAsPOUA&tries=1&useIP=true&r=1715839381424", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("v1_28", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_20");
    ns_web_url("collect_20",
        "URL=https://analytics.google.com/g/collect?v=2&tid=G-DJ3RGVFHJN&gtm=45je45f0v893318240za200&_p=1715839203848&gcd=13l3l3l3l1&npa=0&dma=0&cid=1944455297.1715839141&ul=en-us&sr=1523x650&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&frm=0&pscdl=noapi&_eu=AEA&_s=9&dl=https%3A%2F%2Fwww.croma.com%2Fcart&dr=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&sid=1715839140&sct=1&seg=1&dt=Croma%20Cart%20Page&en=page_view&_et=796&tfd=179251",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("collect_20", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_21");
    ns_web_url("collect_21",
        "URL=https://analytics.google.com/g/collect?v=2&tid=G-DJ3RGVFHJN&gtm=45je45f0v893318240z8889407033za200&_p=1715839203848&gcd=13l3l3l3l1&npa=0&dma=0&cid=1944455297.1715839141&ul=en-us&sr=1523x650&uaa=x86&uab=64&uafvl=Not.A%252FBrand%3B8.0.0.0%7CChromium%3B114.0.5735.199&uamb=0&uam=&uap=Linux&uapv=5.15.0&uaw=0&frm=0&pscdl=noapi&_eu=IA&_s=10&cu=INR&sid=1715839140&sct=1&seg=1&dl=https%3A%2F%2Fwww.croma.com%2Fcart&dr=https%3A%2F%2Fwww.croma.com%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&dt=Croma%20Cart%20Page&uid=false&en=view_cart&_c=1&pr1=id304697~nmOnePlus%2012R%205G%20(8GB%2C%20128GB%2C%20Cool%20Blue)~afcroma.com~qt1~lp0~pr39999~k0currency~v0INR&epn.value=39999&ep.pagetype=cart%20summary&ep.source_page_url=%2Fcart&ep.previous_page_url=%2Foneplus-12r-5g-8gb-128gb-cool-blue-%2Fp%2F304697&ep.login_trigger=N%2FA&ep.login_status=false&ep.click_text=N%2FA&_et=4824&tfd=179251",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://smetrics.croma.com/b/ss/infinitipwa/1/JS-2.25.0-LDQM/s43977144746541?AQB=1&ndh=1&pf=1&t=16%2F4%2F2024%201%3A3%3A1%204%20300&mid=23208888194201725061814923007976880958&aamlh=12&ce=UTF-8&pageName=cart%20summary&g=https%3A%2F%2Fwww.croma.com%2Fcart&c.&getNewRepeat=3.0.1&getPageLoadTime=2.0.2&performanceWriteFull=n%2Fa&performanceWritePart=n%2Fa&performanceCheck=n%2Fa&p_fo=3.0&inList=3.0&apl=4.0&getPreviousValue=3.0.1&.c&cc=INR&ch=croma%3Acart&server=www.croma.com&events=scView%2Cevent20%2Cevent21%2Cevent49%2Cevent100%2Cevent54%2Cevent1%3D5.5%2Cevent50%3Dundefined&products=%3B304697%3B1%3B%3Bevent20%3D39999%7Cevent21%3D39999%7Cevent54%3D%7Cevent49%3D1%3BeVar10%3D304697%7CeVar11%3D1%7CeVar13%3D39999%7CeVar14%3D39999%7CeVar111%3DNA%7CeVar117%3DNo%7CeVar58%3Dstandard%20delivery%20%2B%20express%20delivery%7CeVar115%3D%7CeVar135%3D%7CeVar114%3Dno%7CeVar118%3D0%7CeVar119%3D%7CeVar120%3D%7CeVar116%3D%7CeVar41%3DOnePlus%2012R%205G%20%288GB%20128GB%20Cool%20Blue%29&aamb=6G1ynYcLPuiQxYZrsz_pkqfLG9yMXBpb2zX5dvJdYQJzPXImdj0y&c1=cart%20summary&v1=D%3Dmid&c2=www.croma.com%2Fcart&v2=new&c3=2024-5-16%201%3A3%3A1&c4=pdp&v4=guest%20user&v5=400049&v38=pdp%20%7C%20highestPercentViewed%3D292%20%7C%20initialPercentViewed%3D53&v39=RL425eb72221864237b3ecc22b172469cc&v51=5.5&v54=1&v55=1&v56=39999&v57=0&v59=0&v76=%2Fcart&v121=no&v131=Direct%20Call%20%2850%29%20%3A%20%5Bpageload%5D%20%3A%20Send%20Beacon&v198=www.croma.com%2Fcart&s=1523x650&c=24&j=1.6&v=N&k=Y&bw=1523&bh=523&mcorgid=E78F53F05EFEF21E0A495E58%40AdobeOrg&lrt=195&AQE=1", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=AKA_A2;at_check;AMCVS_E78F53F05EFEF21E0A495E58%40AdobeOrg;s_ecid;s_plt;s_pltp;s_cc;_gcl_au;AMCV_E78F53F05EFEF21E0A495E58%40AdobeOrg;_hjSession_3400595;_fbp;_ga;WZRK_G;mbox;s_ips;th_external_id;_uetsid;_uetvid;_hjSessionUser_3400595;cto_bundle;WZRK_S_679-6RK-976Z;s_sq;_ga_DJ3RGVFHJN;s_nr30;s_tp;s_ppv", END_INLINE,
            "URL=https://c.bing.com/c.gif?Red3=CTOMS_pd&cbid=k-0tyrVmhJLxG-nAKY3h_cmH_uE4L3d_G-DpKVpINPhVSXxwS3", "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("collect_21", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("adrum_9");
    ns_web_url("adrum_9",
        "URL=https://bom-col.eum-appdynamics.com/eumcollector/beacons/browser/v1/BO-AAB-FBK/adrum",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:text/plain",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"vr":"23.10.1.4359","dt":"R","rg":"0","es":[{"eg":"1","et":2,"eu":"0://1/2/3","ts":1715839376125,"mg":"0","au":"0://4/5","at":0,"pp":3,"mx":{"PLC":1,"FBT":140,"DDT":61,"DPT":0,"PLT":201,"ARE":0},"md":"POST","xs":200,"si":69},{"eg":"2","et":2,"eu":"0://1/6/7/8","ts":1715839376256,"mg":"0","au":"0://4/5","at":0,"pp":3,"mx":{"PLC":1,"FBT":132,"DDT":1,"DPT":0,"PLT":133,"ARE":0},"md":"POST","xs":200,"si":70}],"ai":"477d42aa_9f21_9144_1a76_688c9aa25e4b","gs":["06a6dd02_3c5f_2943_3169_f771971f39e1","15ca73b9_34eb_f4ef_c7e1_1083accd8eb4","1fed589c_54dd_a132_b7ce_d5b873df0507"],"up":["https","api.tatadigital.com","getApplicablePromotion","getApplicationPromotionsForItemOffer","www.croma.com","cart","analytics-engine","events","v1"]}",
        BODY_END
    );

    ns_end_transaction("adrum_9", NS_AUTO_STATUS);
    ns_page_think_time(9.092);

}

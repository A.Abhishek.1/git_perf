/*-----------------------------------------------------------------------------
    Name: exit_script
    Created By: Abhishek
    Date of creation: 4.13.0 (build# 111)
    Flow details:
    Build details: 05/18/2024 05:16:19
    Modification History:
-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include "ns_string.h"

int exit_script ()
{
    return 0;
}

#!/bin/bash

# Define batch size
BATCH_SIZE=5

# Function to process directory recursively
process_directory() {
    local current_directory="$1"

    # Change to the current directory
    cd "$current_directory" || return

    # Get list of modified files
    MODIFIED_FILES=$(git status --porcelain | grep "^ M" | cut -c 4-)

    # Check if there are modified files
    if [ -z "$MODIFIED_FILES" ]; then
        echo "No modified files found in $current_directory"
        cd ..
        return
    fi

    # Split modified files into batches
    IFS=$'\n' read -d '' -r -a FILES_ARRAY <<<"$MODIFIED_FILES"

    # Loop through batches and perform add, commit, push
    for ((i=0; i<${#FILES_ARRAY[@]}; i+=BATCH_SIZE)); do
        batch=("${FILES_ARRAY[@]:i:BATCH_SIZE}")
        git add "${batch[@]}"
        git commit -m "Batch commit: adding ${#batch[@]} files"
        git push
    done

    echo "All batches pushed successfully in $current_directory"

    # Process subdirectories recursively
    for subdir in */; do
        process_directory "$current_directory/$subdir"
    done

    # Move back to the parent directory
    cd ..
}

# Start processing from the root directory
process_directory "/home/cavisson/Controller_NV/workspace/admin/system/cavisson/"

